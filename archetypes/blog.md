---
date: {{ .Date }}
draft: true
title: {{ replace .Name "-" " " | title }}
#subtitle: A witty line as a subtitle
slug: {{ .Name }}
description: |-
  Describe the post for social networks
#cover:
#  banner: true
#  src: image.jpg
#  alt: Image alternative text
#  by: image author
#  link: optional-original-beautiful-image.jpg
#  socialSrc: optional-alternative-image.jpg
author: chop
#categories: [ software-creation ]
#tags: [ java ]
#keywords: [ key idea ]

#references:
#- id: the-ref-id
#  name: The reference title
#  title: true
#  url: https://url-to-the.ref
#  lang: en
#  author:
#    name: Author, organization
#    url: author page
---
