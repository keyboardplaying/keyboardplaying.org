import purgeCSSPlugin from '@fullhuman/postcss-purgecss';
import autoprefixer from 'autoprefixer';
import cssnanoPlugin from 'cssnano';

// These classes are those hugo may not detect (dynamic, some exceptions...)
const additionalClasses = [
    // Dynamic classes
    // - Comment form
    'replying',
    'loading',
    'success',
    'spam',
];

const purgecss = purgeCSSPlugin({
    content: ['./hugo_stats.json'],
    defaultExtractor: content => {
        const els = JSON.parse(content).htmlElements;
        return [...additionalClasses, ...els.tags, ...els.classes, ...els.ids];
    },
});

export default {
    plugins: [
        autoprefixer,
        ...(process.env.HUGO_ENVIRONMENT === 'production' ? [purgecss] : []),
        cssnanoPlugin({ preset: 'default' }),
    ],
};
