---
title: Linux
description: |-
  Linux is a free and open source operating system.
  Using it implies some new habits and, to the newcomer, some of those may look like tricks.
weight: 10
slug: linux
---
