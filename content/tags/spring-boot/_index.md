---
title: Spring & Spring Boot
description: 
  Spring and all the projects in the Spring family have changed the way we develop in Java.
  This has been even accelerated with the arrival of Spring Boot.
  Yet, the family is so wide that there is still much to learn and share about Spring.
weight: 16
slug: spring-boot

termCover:
  banner: false
  src: spring.jpg
  alt: Spring and Spring Boot logos
  by: chop
  license: ASF 2.0
  basedOn:
    - link: https://spring.io/trademarks
      title: Spring Framework logo
      by: Pivotal Software
      license: ASF 2.0
    - link: https://unsplash.com/photos/CgyrwbE6Hm4
      title: Sunset on a green grass field
      by: Aniket Bhattacharya
      license: Unsplash
---
