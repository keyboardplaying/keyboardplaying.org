---
title: Spring & Spring Boot
description: 
  Spring et tous les projets de la famille ont changé notre façon de développer en Java.
  C'est encore plus vrai de Spring Boot.
  Pourtant, cette famille est si large qu'il reste toujours quelque chose à apprendre et partager sur Spring.
weight: 16
slug: spring-boot

termCover:
  banner: false
  src: spring.jpg
  alt: Logos de Spring et Spring Boot
  by: chop
  license: ASF 2.0
  basedOn:
    - link: https://spring.io/trademarks
      title: Spring Framework logo
      by: Pivotal Software
      license: ASF 2.0
    - link: https://unsplash.com/photos/CgyrwbE6Hm4
      title: Coucher de soleil sur un champ d'herbe
      by: Aniket Bhattacharya
      license: Unsplash
---
