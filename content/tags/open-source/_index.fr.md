---
title: Open Source
description: |-
  Ouvrir votre logiciel --- ou votre matériel --- signifie que les utilisateurs pourront découvrir son fonctionnement, le modifier et continuer à l'utiliser lorsque vous ne pourrez plus offrir de support.
weight: 17
slug: open-source
---
