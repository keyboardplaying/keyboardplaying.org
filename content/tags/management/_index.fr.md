---
title: Gestion de développeurs & de projets informatiques
description: |-
  Les développeurs et chefs de projets sont des espèces différentes.
  Pourtant, il leur faut travailler ensemble pour produire de grandes choses et réaliser de grands projets.
  Nous adorerions partager quelques éclairages tirés de nos expériences en tant que développeurs et meneurs d'équipe, afin de vous aider à mutuellement mieux vous comprendre.
weight: 6
slug: gestion-de-projets-informatiques
aliases: [ /mots-cles/management ]

termCover:
  banner: false
  src: austin-distel-wD1LRb9OeEo-unsplash.jpg
  alt: Trois hommes assis écoutent un quatrième qui utilise un tableau blanc avec des post-it
  by: Austin Distel
  authorLink: https://unsplash.com/@austindistel
  link: https://unsplash.com/photos/wD1LRb9OeEo
  license: Unsplash
---
