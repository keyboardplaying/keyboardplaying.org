---
title: Java
description: |-
  Java is a great programming language for building portable software, still quite widespread.
  There may be something left to learn about it yet.
weight: 10
slug: java

termCover:
  banner: false
  src: mike-kenneally-tNALoIZhqVM-unsplash.jpg
  alt: A coffee mug amidst coffee beans
  by: Mike Kenneally
  authorLink: https://unsplash.com/@asthetik
  link: https://unsplash.com/photos/tNALoIZhqVM
  license: Unsplash
---
