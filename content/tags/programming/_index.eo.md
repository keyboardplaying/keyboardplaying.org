---
title: Programado
description: |-
  Skirbante kodon, ni bezonas iloj kaj bonajn praktikojn.
  Ĉi tie ni povas dividi tiujn.
weight: 5
slug: programado

termCover:
  banner: false
  src: fabian-grohs-XMFZqrGyV-Q-unsplash.jpg
  alt: MacBook kun kodredaktilo montrata, apud malfermitaj kajeroj kun kontrollisto kaj malnetoj
  by: Fabian Grohs
  authorLink: https://unsplash.com/@grohsfabian
  link: https://unsplash.com/photos/XMFZqrGyV-Q
  license: Unsplash
---
