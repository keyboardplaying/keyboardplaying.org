---
title: Securité
description: |-
  La sécurité devrait être un souci majeur lors de la création de nouvelles solutions logicielle, mais aussi un critère de sélection lorsque nous choisissons un service auquel nous confions nos données.
weight: 20
slug: securite

termCover:
  banner: false
  src: chris-panas-0Yiy0XajJHQ-unsplash.jpg
  alt: A yellow padlock on a dark door
  by: chris panas
  link: https://unsplash.com/photos/0Yiy0XajJHQ
  license: Unsplash
---
