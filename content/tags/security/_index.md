---
title: Security
description: |-
  Security should be a paramount preoccupation when building new software, but also a criteria for any tool or service we choose to entrust our data to.
weight: 20

termCover:
  banner: false
  src: chris-panas-0Yiy0XajJHQ-unsplash.jpg
  alt: A yellow padlock on a dark door
  by: chris panas
  link: https://unsplash.com/photos/0Yiy0XajJHQ
  license: Unsplash
---
