---
title: Go
description: |-
  Go is an open source programming language, designed by Google to improve productivity and known for being fast.
weight: 10
slug: golang

termCover:
  banner: false
  src: traefik-montage.png
  alt: A montage from several Traefik illustrations
  by: Traefik
  link: https://traefik.io
---
