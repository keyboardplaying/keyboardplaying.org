---
title: Go
description: |-
  Go est un langage de programmation open source développé par Google, conçu pour accélérer la programmation à grande échelle et réputé pour sa rapidité.
weight: 10
slug: golang

termCover:
  banner: false
  src: traefik-montage.png
  alt: Un montage de plusieurs illustrations de Traefik
  by: Traefik
  link: https://traefik.io
---
