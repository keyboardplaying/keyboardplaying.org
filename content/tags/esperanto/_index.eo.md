---
title: Esperanto
description: |-
  Esperanto estas la konstruita lingvo la plej parolata tra la mondo.
  Ĝi estis kreita por esti universala dua lingvo.
weight: 10
slug: esperanto

termCover:
  banner: false
  src: sidharth-bhatia-YbRd8Qqem_Y-unsplash.jpg
  alt: Kvin homoj kuŝantaj en herba kampo, farante stelsignon.
  by: Sidharth Bhatia
  authorLink: https://unsplash.com/@sidharthbhatia
  link: https://unsplash.com/photos/YbRd8Qqem_Y
  license: Unsplash
---
