---
title: Esperanto
description: |-
  Esperanto is the world's most spoken constructed language.
  It was created to be a universal second language.
weight: 10
slug: esperanto

termCover:
  banner: false
  src: sidharth-bhatia-YbRd8Qqem_Y-unsplash.jpg
  alt: Five people laying on a grass field, making a star sign.
  by: Sidharth Bhatia
  authorLink: https://unsplash.com/@sidharthbhatia
  link: https://unsplash.com/photos/YbRd8Qqem_Y
  license: Unsplash
---
