---
title: Espéranto
description: |-
  L'espéranto est la langue construite la plus parlée au monde.
  Elle a été conçue pour être une deuxième langue universelle.
weight: 10
slug: esperanto

termCover:
  banner: false
  src: sidharth-bhatia-YbRd8Qqem_Y-unsplash.jpg
  alt: Cinq personnes allongées dans un champ d'herbe, dessinant une étoile.
  by: Sidharth Bhatia
  authorLink: https://unsplash.com/@sidharthbhatia
  link: https://unsplash.com/photos/YbRd8Qqem_Y
  license: Unsplash
---
