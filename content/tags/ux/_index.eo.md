---
title: Spertoj de uzantoj
description: |-
   Programaro-kreado havas neniun absolutan intereson.
   Estas per la servoj, kiujn ĝi provizas al uzantoj, ke ĝi trovas sian valoron.
   Por esti adoptita, ankaŭ necesas, ke ĝia uzo estu agrabla.
   Ĉi tie, ni povas diskuti sperton de uzanto, uzeblecon, ergonomion…
weight: 15
slug: spertoj-uzantoj

termCover:
  banner: false
  src: hal-gatewood-tZc3vjPCk-Q-unsplash.jpg
  alt: Tri ekranprototipoj, desegnitaj kaj komparitaj mane
  by: Hal Gatewood
  authorLink: https://unsplash.com/@halgatewood
  link: https://unsplash.com/photos/tZc3vjPCk-Q
  license: Unsplash
---
