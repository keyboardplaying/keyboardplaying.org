---
title: User eXperience
description: |-
  Software creation is nothing _per se_.
  Its real value lies in the service it brings to users.
  Yet, to be adopted, it must be agreeable to use.
  Here, we'll talk about user experience, usability, ergonomy...
weight: 15
slug: user-experience
aliases: [ /tags/ux ]

termCover:
  banner: false
  src: hal-gatewood-tZc3vjPCk-Q-unsplash.jpg
  alt: Three screens mock-ups, hand-drawn and -colored
  by: Hal Gatewood
  authorLink: https://unsplash.com/@halgatewood
  link: https://unsplash.com/photos/tZc3vjPCk-Q
  license: Unsplash
---
