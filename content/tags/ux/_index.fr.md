---
title: User eXperience
description: |-
  La création logicielle n'a pas d'intérêt dans l'absolu.
  C'est à travers les services qu'elle rend aux utilisateurs qu'elle trouve sa valeur.
  Encore faut-il, pour être adoptée, que son utilisation soit agréable.
  Ici, nous pourrons discuter d'expérience utilisateur, d'utilisabilité, d'ergonomie...
weight: 15
slug: user-experience
aliases: [ /mots-cles/ux ]

termCover:
  banner: false
  src: hal-gatewood-tZc3vjPCk-Q-unsplash.jpg
  alt: Trois prototypes d'écrans, dessinés et colloriés à la main
  by: Hal Gatewood
  authorLink: https://unsplash.com/@halgatewood
  link: https://unsplash.com/photos/tZc3vjPCk-Q
  license: Unsplash
---
