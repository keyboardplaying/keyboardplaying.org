---
date: 2019-10-21T18:52:58+02:00
title: Saluton, mondo!
#subtitle: And what a brave new world it is!
slug: saluton-mondo
cover:
  src: /img/duke-hello.jpg
  alt: La maskoto de Ĝavo, Duko, svingas al la ekrano.
  by: alcibiade
  license: CC BY 4.0
author: chop
categories: [ kp.org ]
tags: [ news, ux, esperanto ]
keywords: [ blogo, gitlab, rakontoj, esperanto, verkado, spertoj de uzantoj ]
---

[blog-board]: https://gitlab.com/keyboardplaying/website/keyboardplaying.org/-/boards/1340192

Ĉi tiu retejo estis kelkajn jarojn dormanta --- 3,5 jarojn, fakte.
Hodiaŭ, mi volas revivigi ĝin, kaj espereble gardi pli viva ol en la pasinteco.

Ĉi tiu unua blogaĵo estas la okazo por prezenti _Keyboard Playing_ kaj miajn esperojn por ĝin en la venontaj semajnoj.
Ĝi eble ankaŭ klarigos kial vi ne trovis la blogaĵon, kiun via serĉilo celis al vi.

<!--more-->

## Pri _Keyboard Playing_ kaj ĝia pasinteco

Mi kreis _Keyboard Playing_ en 2011.
Ĝia komencis kiel loko por mi gastigi iujn ilojn de mia kreo enreti.

Trovi taŭgan domajnan nomon estis defio!
Mi ne volis io ke estu etikedita al mia nomo, ĉar mi esperis fari ĝin kunlabora spaco poste.
Mi ankaŭ ne volis esti tro restrikta ĉar, kankvam komputeko estas mia ĉefa okupo, ĝi ne estas mia sola pasio.

La retejo konis plurajn transformojn tra la jaroj, ofte por lerni novajn ilojn por mia laboro[^twbs] aŭ pro scivolemo[^mdl].
En 2012 aperis WordPress blogo.
Ĝi restis aktiva ĉirkaŭ du jaroj kaj ekdormis.
Mi provis ree en 2015, sed rezignis unu jaron poste, esence ĉar mi ne trovis la tempon plu.

[^twbs]: Jen kiel mi pasigis unu semajnfinon lernante [Bootstrap](https://getbootstrap.com/).
[^mdl]: Kaj jen kiel mi refaris la retejo kun [Material Design Lite](https://getmdl.io/) por vidi ĝiajn eblecojn.

Hodiaŭ, la volo de konigi brulas varmege.
Mi pasigis iom da tempo elektante miajn ilojn kaj preparante la bazojn de ĉi tiu v2.0 de _Keyboard Playing_.
Estas ankoraŭ laboro, sed, laŭ la Agile filozofio, mi liveras ĉi tiun unuan version hodiaŭ kaj daŭre laboros por plibonigi ĝin iom post iom.


## Pri ĝia (proksima) estonteco

### Nun en du (tri?) lingvoj!

Antaŭe, _Keyboard Playing_ estis nur en la angla, ĉar ĝi estas la lingvo de internacia interŝanĝo kaj estas pli facile komprenebla por la plimulto de la homoj sur la planedo.

Tamen ĉi tiu versio estos dulingva kaj inkluzivos francan version.
Ĉar la franca estas mia denaska lingvo kaj mi amas ĉi tiun lingvon.
Krome mi konstatis, kiom pli komforte legi en via denaska lingvo, eĉ se vi parolas tre bone la anglan.
Ĉi tio estos io, kion mi ĝojas proponi al ĉiuj miaj hipotezaj francaj legantoj.

Mi ankaŭ komencis lerni Esperanton kaj inkluzivi ĝin kiel trian alternativon al alian lingvoj.
Mi ankoraŭ estas komencanto, kaj bonvenas korektadojn kaj konsiloj.
Vi povas komenti, forki la paĝojn aŭ mesaĝi mi [en Twitter](https://twitter.com/cyChop).

Se vi serĉas aliajn versiojn de la paĝojn, trovu la flagon-ikonon ({{< icon icon="flag" title="Flago" >}}) kaj vi vidis ligojn al tradukajn versiojn de la paĝo apud se ili disponeblas.


### La blogo kiel la finŝtono

La plej aktiva parto de _Keyboard Playing_ ĉiam estis ĝia blogo, do ĝi estis la ŝtono, sur kiu mi rekonstruos ĝin, sed mi faros iujn ŝanĝojn sur ĝi.


#### Rekomenco de nulo

Mi elektis forgesi ĉion, kion mi faris en la pasinteco, enarkivigante ĝin por komenci la blogon denove.
Leganto, se vi serĉis unu el miaj antaŭaj blogaĵoj, ĝi haveblas [ie en la Gitlab-deponejo de ĉi tiu retejo](https://gitlab.com/keyboardplaying/website/keyboardplaying.org/tree/archive/site/content/archive).
Se la temo interesis vin, bonvolu mencii ĝin sur [la temepanelo ĉe Gitlab][blog-board].


#### Diversaj temoj

Pri kiuj temoj mi blogaĵis?

* Nu, softvara kreo (mallonge: "**kodado**") estas la temo, kiu okupas la plej grandan parton de mia tempo kaj vera pasio, do mi verkis pri ĝi.
  Mi ĉefe diskutos aferojn, kiuj mi lernis laŭ sperto, de ordinaraj konsiloj ĝis bonaj praktikoj.
  Espereble mi trovos tempon por plonĝi en novaj lingvoj, iloj aŭ framoj, kiuj konigi interesajn resumojn pri ilin.

* Donita la nomon de la retejo, estus domaĝe ne verki pri **klavoroj**.
  Mi skribos iom pri la historio de ĉi tioj aparatoj kaj konigos iujn pensijn pri la estonteco, alternativoj...

* Eble ŝajnas malsama kiuj mia aliaj temoj, sed mi ankaŭ volas verki pri daŭrigebla evoluo en mia specialaĵo.
  Alivorte, mi ŝatus konigi iujn pensojn pri **daŭrebla** aŭ **respondeca cifereca**.
  Jes, ni pensas ke ni laboras en kampo, ke kreas nur virtualajn varojnn sed tiuj varoj _ja_ bezonas vere materiala kaj contaminanta maŝinoj.
  Aldone al tio, la _planeda_ aspekto ne sufiĉas: _homoj_ ankaŭ estas kolono de daŭripova evoluo.
  Sed pli pri tiu poste.

Mi eble verkos pri temoj pri kiuj mi ne estas sperta.
En tiaj kazoj, mi esploros ĝis mi sentos, ke mi havas sufiĉe da materialo por plene kompreni kaj enmetos fontojn en miaj blogaĵoj, provante sintezi kio estas plej grava.
Tio ne igos ĝin ĝusta aŭ kompleta.
Mi fidos je viaj reagoj, ĉar mi kredas, ke konstrua kritiko povas helpi min pliboniĝi.

[Estas panelo sur Gitlab][blog-board] kie mi listigis kelkaj temoj mi ŝatus verki pri.
Ĝi estu publike havebla, do se vi havas ideon pri temo, pri kiu vi pensas, ke mi estus kompententa/trafa verkado, bonvolu sendi ĝin!


#### Novaj, pli larĝaj horizontoj

Kiel mi diris pli frue, mi kris ĉi tiun retejon esperante, ke iuj amikoj (instruistoj, pianistioj, bioinformatikistoj...) volus blogi pri siaj propraj spertoj kun klavaroj, muzikludado aŭ komponado, algoritmoj, verkado, instruado...
Ĝi ne funkciis tiutempe, kaj mi havas nenion kialon pensi, ke ĝi estos malsama ĉi-foje.

Tamen, se vi pretas foje blogi, kiujn vi faras per klavaro (muziko, verkado, videoredaktado... Kion mi scias?), ne hezitu kontakti min.
Ni vidos, kion ni povus fari.
Ĉi tio povas esti unufoja afero: la nova strukturo de la blogo faciligas al mi gastigi blogaĵoj de aliaj aŭtoroj.


### Dozon de fikcio en la skribo

Laboro ne sufiĉas por vivi.
Mi ĉias deziris esti artisto, sed mi neniam rekonis talenton al mi.
Tantem, multaj amikoj kaj kolegoj diris al mi, ke ili ŝatas mian manieron skribi.

De kelkaj semajnoj, mi aliĝis al grupo de homoj, kiuj provas disvolvi ĉi tiun kapablon per reciproka instigo.
En la venontaj versioj de la retejo, mi konigos iujn el miajn kreaĵoj en nova sekcio nomata "_rakontoj_."


### Pli okulplaca retejo

Mi ĵus diris, ke mi ne estas artisto.
Ĉi tiu retejo plenumas uzeblajn postulojn[^ux], sed ĝi eble aspektas pli bona.
Se iu kun pli certaj gustoj ol la mia venas por legi ĉi tiun blogaĵo, [viaj konsiloj estas bonvenaj](https://gitlab.com/keyboardplaying/website/keyboardplaying.org/issues/6).

[^ux]: Tio estas, mi esperas, ke ĝi estas uzebla kaj agrabla por uzi.
Se vi pensas ke aferojn povas esti plibonigitaj, vi povas [fari sugeston ĉi tie](https://gitlab.com/keyboardplaying/website/keyboardplaying.org/issues/new).

Ekzemple, mi tre ŝatus igi la defaŭltan temon malhela, sed mi ŝajnas esti nekapabla krei tian, kiu ne sentas subprema.
Misupozas, ke multaj aferoj pri la ĝenerala aranĝo ankaŭ povus esti pli ergonomiaj.
Ĉiu ideo por plibonigi la retejo bonvenas!


### Vivtenante la flamon

Ĝuste nun, mi esperas, ke mi povos konservi certan oftecon en la produktado de enhavo.
Unu blogaĵo semajne estus bela, unu monate ŝajnas pli realisma.

Neniam hezitu komenti.
Scii, ke me interesas legantojn, estas potenca motiviĝo!
