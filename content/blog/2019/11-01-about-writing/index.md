---
date: 2019-11-01T21:15:47+01:00
title: About Writing
subtitle: Coding extends Writing
slug: about-writing
description: |-
  I've always wished I had some artistic talent.
  Sometimes (often? always?), skill comes from hard work.
  I've started training, and I'll be sharing my first results.
author: chop
categories: [ writing ]
tags: [ news ]
keywords: [ writing, art, training, practice ]
---

I always wished I had an artistic talent of some sort.
Some friends once diagnosed me to be a "latent artist."
The truth however is that I've never found myself any sort of skill, but maybe that's because skill comes from hard work.

<!--more-->

## Admiring Others and Learning How They Do It

Know-how and the mastery of a skill have always left me appreciative when not in awe.
That's true in all areas, but I also sometimes feel a tinge of envy when it comes to creative arts.

We all have that friend who can draw anything and usually does it during a boring meeting or hour of class, leaving you admirative in the end.
Or the one who can play almost any tune without having studied it before.
Those skills seem innate and I used to feel it's not always fair to see that someone can do that and I can't.

<aside><p>Talent is not innate, it is the result of work.</p></aside>

Fortunately, I've grown up a bit since then.
I've also got the opportunity to hang out with some drawers and musicians, for instance.
This gave me the opportunity to discover: this is not innate.
Just as any skill, people may have a affinity for it but it still has to be honed through hard work and training before reaching its full potential.

When I realized that, I suppose I was more desperate than ever about developing an artistic talent.


## Finding My Talent

Unlike all those artists I admire, I don't have any of these facility.
My drawings are most often limited to stick men and schemas.
I'm lucky friends who had to endure my singing are still friends.
I tried my luck with some instruments---I bought a guitar and never learned to play it, and wanted to learn playing my father's old [combo organ](https://en.wikipedia.org/wiki/Combo_organ)[^smartass-playing-on-keyboard], only to discover it was out of order.
I edit simple videos as a dilettante on occasions, and the result is generally appreciated, but this is time-consuming and I know I am no filmmaker.

[^smartass-playing-on-keyboard]: Playing on a keyboard should be easier for me, right?

No, I think none of these arts is for me.
I'm not a performer.
So, what is the skill I have an affinity with that I can work on?
And then it dawned on me: writing.


## Writing in My Past and Present

Writing is something I've always been attracted to.
I was an avid reader when I was younger---still am, in some measure---and words have their way with me.
I began with some teenager attemps at poetry.
The results look childish and unsatisfactory to me now, but I was quite proud at the time.

In more recent years, my closest friend has asked me many times to proofread some of her written works, and she often tells me my writing is "excellent"---her word, not mine.
Other friends and colleagues have made comments in this direction too.

I've never written any long fiction.
There have been some drafts and ideas listed over paper, but nothing was ever finished.
Writing takes three things: time, motivation/energy and inspiration.
The hardest for me to find has always been the latest.
I have some clues at universes I'd like to develop, but I want a story to tell in those.

Maybe practice can help.
Skills have to be honed.
So I need to exercise.


## An Unexpected Practice

A few years back, I realized writing is what I do for a living.
Think about it!

Really, writing is taking words and punctuation, assemble everything in a correct way, according to a grammar others can understand.
Sometimes, you start from nothing.
On other occasions, you rephrase or translate someone else's work.

There are ways to mistreat the language and still produce something that will convey your ideas, though in an ugly way.
There are also several correct ways to write the same things.
And of course, there usually is one or more beautiful way, the ones any author craves.

<aside><p>Coding <em>is</em> writing.</p></aside>

When you think of it in these terms, coding _is_ writing.
In another language, with another grammar, but still: I am _already_ writing everyday.
The difference in my job is that I don't need to look for the ideas: they're given to me.
I just have to write them in the best possible way while taking the imposed constraints into account.


## Training and Sharing

Now, to go from an affinity to talent, there's some working to do.
I joined a group of like-minded training writers---and drawers---humbly dubbed _The Fellowship of the Pen_.
We motivate each other and share some advice and constructive criticism.

We have established two kind of recurring challenges:

- a weekly challenge, where the purpose is to do something really short but that may take us out of our comfort zone;
- a bimonthly challenge[^bimonthly-challenge], where we have to really write a short story with some constraints (often the theme).

[^bimonthly-challenge]: Theoretically bimonthly, but the deadline for the second challenge was pushed for five months...

<aside><p>My creations are available in the <a href="/stories/">Stories</a> section.</p></aside>

I may not participate of all---we all have only so much time---but I'm willing to share here some of what I write in this context---or for fun.
This is why the [Stories]({{< relref path="/stories" >}}) section is now available on the website.
Please enjoy.


## Helping Me Getting Better

Practice is one thing, but for it to be efficient, one needs to know when they practice poorly.
Constructive feedback is always welcome.

I don't need to know when something is great---though it's always pleasant---but I _do_ need to know when I make mistakes, or when things could be better.
Pray tell me in such occurrences.
There are two ways you can do this:

- Write a comment on the page you read.
- Or fork the repo---there's now a link at the bottom of any page---and submit a merge request.

Keep in mind that I write primarily in French, so I need even more advice and guidance to correctly write in English.


## Closing with Some Thanks

On a final note, I'd like to thank the other members of the _Fellowship_ for their support and advice:
{{< author "tony" >}},
{{< author "lorine" >}},
{{< author "sancho" >}},
{{< author "mart1n" >}},
{{< author "ngorzo" >}},
{{< author "sweepincomics" >}},
{{< author "kaisuke" >}},
{{< author "gio" >}},
{{< author "vinzouille" >}} and
{{< author "captain-jahmaica" >}}.

I'd also like to thank {{< author "a-so" >}} for always supporting me and giving me the feedback, way before I joined the fellowship.
