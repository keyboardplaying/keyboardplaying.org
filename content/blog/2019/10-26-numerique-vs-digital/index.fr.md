---
date: 2019-10-25T23:42:52+02:00
title: 'Comment dit-on « digital » en français ?'
subtitle: Creusons pour démêler les racines...
slug: numerique-v-digital
author: chop
description: |-
  On entend souvent les défenseurs de la langue française opposer « numérique » à « digital ».
  Faisons un peu d'étymologie pour comprendre cette opposition...
categories: [ software-creation ]
tags: [ langue-francaise ]
keywords: [ numérique, digital, étymologie ]

references:
- id: wiktionnaire
  name: Wiktionnaire
  url: https://fr.wiktionary.org
  lang: fr
- id: daf
  name: Dictionnaire de l'Académie Française
  url: https://www.dictionnaire-academie.fr
  lang: fr
- id: tlfi
  name: Trésor de la Langue Française informatisé (TLFi)
  url: https://www.cnrtl.fr/definition/
  lang: fr
- id: wiktionary
  name: Wiktionary
  url: https://en.wiktionary.org
  lang: en
- id: wiki-montre-numerique
  name: Montre numérique
  title: true
  url: https://fr.wikipedia.org/wiki/Montre_num%C3%A9rique
  lang: fr
  author:
    name: Wikipédia
---

Ceux qui me connaissent le savent : je peux être assez tatillon sur la langue française.
On me reproche parfois l'usage du néologisme « [téléverser] » plutôt qu'« [_uploader_][uploader] ».

Car oui, même si les anglicismes sont omniprésents dans les technologies de l'information, je les évite dans la mesure du raisonnable.
Il y en a un qui est pourtant bien ancré dans ce domaine : « digital » (avec son compère « digitalisation »).

<!--more-->

Si les emprunts à l'anglais sont si courants dans les nouvelles technologies, c'est que celles-ci sont habituellement nommées dans la langue de Shakespeare.
Généralement, l'Académie française s'en empare quelques mois plus tard et [propose des traductions françaises](https://www.nextinpact.com/news/105172-850-termes-officiellement-francises-dans-secteur-nouvelles-technologies.htm).
Il est souvent difficile des les faire appliquer, dans le monde professionnel tout du moins, l'usage ayant ordinairement été consacré dans l'intervalle de temps entre l'apparition du mot anglais et de son équivalent français, sans compter les [occasionnels](http://leplus.nouvelobs.com/contribution/766873-mot-diese-le-hashtag-a-la-francaise-est-une-horreur-esthetique.html) [tollés](https://www.lefigaro.fr/culture/2013/01/23/03004-20130123ARTFIG00550-twitter-mot-diese-contre-hashtag.php).

« Digital » ne rentre pas réellement dans ce cadre, cependant.
En effet, la traduction française de ce mot remonte au <span class="century">xvii</span><sup>ème</sup> siècle.
Le compte Twitter [ @NumeriqueBordel][numerique-bordel] a été créé pour rappeler ce mot.
Il s'est révélé le 25 juin 2014 avec [un tweet inaugural][numerique-bordel-1st] :

> On dit numérique, pas digital. Bordel.
> <cite><a href="https://twitter.com/NumeriqueBordel" title="Numérique, bordel !">@NumeriqueBordel</a></cite>

La neuvième édition du _Dictionnaire de l'Académie française_ fait elle aussi explicitement ce rappel dans une [indication d'usage][dire-ne-pas-dire-digital], bien qu'elle le formule en d'autres termes.

Certains lèvent certainement les yeux au ciel de voir un post entier consacré à un sujet si dérisoire.
Je souhaitais pourtant revenir un peu sur l'histoire de ces deux mots et, si j'ai de la chance, vous convaincre de rejoindre le mouvement de ceux refusant un anglicisme simple à contourner.


## Un peu d'étymologie

### « Numérique »

Le [_Dictionnaire de l'Académie française_][daf-numerique] (DAF[^daf]) et le [TLFi](https://www.cnrtl.fr/definition/num%C3%A9rique "« Numérique »")[^tlfi] s'accordent tant sur la définition que sur l'origine : relatif aux nombres ou, dans le cas particulier de l'informatique :

> Se dit, par opposition à « analogique », de la représentation discrète de données ou de grandeurs physiques au moyen de caractères (des chiffres généralement) ; se dit aussi des systèmes, dispositifs ou procédés employant ce mode de représentation.
> <cite><a href="http://www.culture.fr/franceterme/terme/INFO174">FranceTerme</a></cite>

L'adjectif est tiré presque directement du latin _numerus_, qui signifie « nombre ».
Ce nom est d'ailleurs [issu de la même racine](https://www.dictionnaire-academie.fr/article/A9N0548 "Nombre").


### « Digital »

_Dictionnaire de l'Académie française_ et TLFi concordent ici encore.
Tout d'abord, ils distinguent deux versions de l'adjectif.

<aside><p>
« Digital », dans son origine française, porte sur tout ce qui est relatif au doigt.
Le sens relatif au numérique est un anglicisme.
</p></aside>

* La première version de l'épithète ([DAF](https://www.dictionnaire-academie.fr/article/A9D2485 "« Digital [1] »"), [TLFi][tlfi-digital]) porte sur tout ce qui est relatif aux doigts.
L'étymologie est ici aussi latine, le mot descendant de _digitalis_, qui se traduit par « qui a la grosseur d'un doigt ».

* Dans sa seconde définition ([DAF](https://www.dictionnaire-academie.fr/article/A9D2486 "« Digital [2] »") ; [TLFi][tlfi-digital] (cliquer sur l'onglet « Digital<sup>2</sup> »)), le mot se rapporte effectivement aux nombres ; pour citer le TLFi : « Qui est exprimé par un nombre, qui utilise un système d'informations, de mesures à caractère numérique. ».
Les deux dictionnaires indiquent dans ce sens un emprunt à l'anglais au <span class="century">xx</span><sup>ème</sup> siècle, et non plus une étymologie latine.


### « _Digital_ » (en anglais dans le texte)

Je connais moins les dictionnaires anglais de référence disponibles gratuitement et vais donc m'appuyer sur le simple / simplifié mais néanmoins efficace [Wiktionary][wiktionary].
En regardant [ses définitions](https://en.wiktionary.org/wiki/digital#Adjective), on constate que l'adjectif anglais est une traduction valable pour aussi bien pour le « digital » français (relatif aux doigts) que pour « numérique » (relatif aux nombres).
[L'étymologie](https://en.wiktionary.org/wiki/digital#Etymology) est la même qu'en français : du latin _digitalis_.

Le nom associé à « _digital_ » en anglais est « [_digit_](https://en.wiktionary.org/wiki/digit#English) ».
La racine commune est claire, mais ce sont davantage [ses définitions](https://en.wiktionary.org/wiki/digit#Noun) qui vont nous intéresser.

<aside><p>
« <em>Digit</em> » désigne un chiffre.
« <em>Bit</em> » est l'abréviation de « <em>binary digit</em> ».
</p></aside>

S'il existe bien une définition liée aux doigts et orteils, elle n'est qu'en sixième position.
Quelques autres font référence à ces organes comme unité de mesure, mais la première définition interpelle : un _digit_ est un chiffre, au sens mathématique du terme ; il s'agit d'un caractère servant à écrire un nombre (compris entre 0 et 9).
La définition est réellement identique à la nôtre et s'applique également aux chiffres en base hexadécimale (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F) ou binaire (0 et 1).
Dans ce dernier cas, l'expression « _binary digits_ » a été [contractée en 1946](https://en.wiktionary.org/wiki/bit#Etymology_3) en « _bits_ ».
Voilà ! Si vous ignoriez l'origine de ce mot, vous aurez au moins appris cela.

Mais par quel miracle passe-t-on de _digitus_, une partie de notre anatomie, à des glyphes constituant notre communication écrite ?
C'est très simple : nous nous servons de nos doigts pour compter, [ce qui explique][dire-ne-pas-dire-digital] que le nom de ces symboles numériques ait été tiré du mot latin désignant ces utiles extrémités.


## Le passage de l'anglais au français

### Les commerciaux et la ~~digitalisation~~ transformation numérique

La digitalisation, de l'anglais « _digitalization_ », est un nom que l'on utilise couramment, pour ne pas dire exclusivement, lorsque l'on souhaite parler de la transformation numérique (improprement appelée également « transformation digitale »).
Il s'agit d'une évolution par laquelle des process existants et s'appuyant sur des formulaires papier et des intervenants humains sont remplacés par des solutions informatisées et automatisées, renforçant l'autonomie du consommateur final et réduisant les coûts pour les fournisseurs.

J'ai lu récemment[^ref-origine-digitalisation] que le passage du mot « digitalisation » au français était en partie dû aux commerciaux.
L'auteur expliquait effectivement que les consultants et commerciaux qui vendaient ce type de prestation jugeaient l'usage de cet anglicisme légitime.
En effet, la transformation numérique couplée au développement des téléphones dits « intelligents » et autres appareils tactiles allait permettre aux individus d'effectuer leurs démarches grâce à l'extrémité de leur _doigt_.
Oui, on rend les choses _digitales_, dans le sens premier du terme.

[^ref-origine-digitalisation]: Je ne parviens pas à retrouver la référence ; j'en suis désolé et frustré.

<aside><p>
Le passage de l'anglais « <em>digital</em> » au français prédate la « digitalisation ».
</p></aside>

La lecture de ce passage ne m'a pas convaincu.
Ma réaction première a été l'incompréhension : je me souviens de parler de « digital » dans mon enfance --- j'en parlerai davantage au prochain paragraphe --- tandis que la digitalisation n'est apparue qu'après mon entrée dans la vie professionnelle.
Il s'est certes répandu à une très grande vitesse, avec une définition parfois floue et souvent un relent de _buzzword_, mais il n'est pas celui qui a fait entrer cette racine anglaise chez nous.


### Les années 80

Enfant des années 80, je me souviens depuis toujours d'avoir entendu parler de « montres digitales », par opposition aux « montres à aiguilles ».
Oui, l'anglicisme était déjà présent.
Fait intéressant, l'article Wikipédia pour « Montre digitale » redirige aujourd'hui vers « [Montre numérique][wiki-montre-numerique] » --- j'avoue ne jamais avoir entendu cette dénomination --- avec une remarque en bas de page concernant l'appellation « digitale » :

> Dans ce cas le qualificatif digital est un anglicisme de l'anglais _digit_ (nombre) et prête à confusion car en français digitale signifie _appartenant au doigt_ (ex : des empreintes digitales), d'où le risque de confusion avec les montres « tactiles ».
> {{< ref-cite "wiki-montre-numerique">}}

Wikipédia irait donc dans le sens de mon auteur oublié en nous disant que _digital_ peut aujourd'hui être confondu avec _tactile_.


### L'origine

Ce que je comprends de [l'étymologie du TLFi][tlfi-digital] indique que l'anglicisme a été introduit ou en tout cas documenté en 1961, lorsque « _digital computer_ » a été traduit par « ordinateur digital ».
Cela fait presque donc soixante ans qu'un immigrant clandestin fait concurrence à une épithète bien de chez nous, installée depuis 1616.


### Les usages

Le TLFi ne le précise pas (le DAF, si), mais en dépit de leurs définitions similaires, cet intrus n'est utilisé qu'en lien avec les nouvelles technologies.
On n'utilisera jamais « valeur digitale » pour qualifier un nombre.

Le _Dictionnaire de l'Académie française_ consacre [une indication d'usage concernant « digital »][dire-ne-pas-dire-digital].
En résumé, elle insiste sur le fait que les deux versions de l'adjectif « digital » n'ont ni la même racine ni le même sens, et qu'il est préférable d'utiliser « numérique » pour tout ce qui ne se rapporte pas aux doigts.


## Anecdote

Il y a un an, la responsabilité d'une communauté technique « Digital » m'a été confiée.
L'objet devait porter sur le développement des interfaces homme-machine, notamment sur le web ou mobiles.
Nous devions également aborder un volet IA, chatbot et pourquoi pas réalités virtuelle et augmentée.
Je me suis permis de le synthétiser en parlant des modes d'interaction utilisateur d'aujourd'hui et de demain, ce qui m'a permis de faire entrer l'UX dans le périmètre.

Encore a-t-il fallu présenter cette communauté à mes collègues pour que les intéressés soient au courant qu'elle existe.
Au grand dam de certains amis présents parmi la centaine de personnes qui m'ont entendu à cette occasion, j'ai effectivement commencé par rappeler que « digital » voulait dire « en relation avec le doigt ».


## Pour conclure

Nous avons vu que « digital » dans le sens de « numérique » est un intrus présent dans la langue française depuis une soixantaine d'années.
Nombreux sont ceux qui pensent qu'il s'agit là d'un détail sans important --- et je n'ai d'autre argument que les convictions qu'a chacun pour les contredire.
Certains préfèreraient voir le mot français reprendre sa place, avec souvent un certain désespoir.

[@NumeriqueBordel][numerique-bordel] résume ce sentiment dans un tweet récent :

> J'ai cinq ans aujourd'hui 🎂
> 
> Au bilan, au delà de quelques sourires, quelques (plus rares) rires et une citation dans un magazine (la gloire), je ne constate pas d'évolution dans l'usage de termes inappropriés pour désigner le numérique.
> <cite><a href="https://twitter.com/NumeriqueBordel/status/1143450657837522944">@NumeriqueBordel</a></cite>

Je pense qu'on peut néanmoins modérer ce pessimisme.
Si les professionnels sont exécrables sur ce point et s'obstinent à vendre du digital et de la digitalisation, j'ai l'impression que les administrations publiques et la presse spécialisée font des efforts.
Nous avons un Conseil National du *Numérique*, que diable !

Sur une thématique que j'aborderai prochainement, plusieurs entités développent la notion de « Numérique Responsable ».
Tout n'est pas perdu.
La route est longue, il y aura toujours des réfractaires, mais nous avançons.


[^daf]: Il ne s'agit pas d'une abréviation officielle, mais mes articles sont suffisamment verbeux pour que l'on puisse se le permettre.
[^tlfi]: _Trésor de la Langue Française informatisé_. Contrairement à la précédente, cette abréviation est couramment acceptée.

{{% references %}}
[téléverser]: https://fr.wiktionary.org/wiki/t%C3%A9l%C3%A9verser "Téléverser"
[uploader]: https://fr.wiktionary.org/wiki/t%C3%A9l%C3%A9verser "Uploader"
[numerique-bordel]: https://twitter.com/NumeriqueBordel "Numérique, bordel !"
[numerique-bordel-1st]: https://twitter.com/NumeriqueBordel/status/481819387511701504 "On dit numérique, pas digital. Bordel. "
[daf-numerique]: https://www.dictionnaire-academie.fr/article/A9N0816 "Numérique"
[tlfi-digital]: https://www.cnrtl.fr/definition/digital "Digital "
[dire-ne-pas-dire-digital]: https://www.dictionnaire-academie.fr/article/DNP0430 "[Dire, ne pas dire] Digital "

