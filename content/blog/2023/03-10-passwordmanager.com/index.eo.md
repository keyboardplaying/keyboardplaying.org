---
date: 2023-03-10T07:00:00+01:00
title: Trovu la administrilo por pasvortojn por viaj bezonoj
slug: find-plej
description: |-
  Looking for a password manager, not sure what is best for your own use case?
  Maybe have a look at passwordmanager.com and use their comparisons to decide what's best for you.
author: chop
categories: [ software ]
tags: [ security ]
keywords: [ sekureco, pasvorto, administrilo por pasvortojn, dufaza aŭtentigo ]
---

Antaŭ iom da tempo, [mi skribis pri ebla solvo por memgastigita administrilo por pasvortojn][pwd-manager].
Kompreneble, memgastigado povas esti timiga.
Feliĉe, aliaj solvoj haveblas, pretaj por uzi.
[PasswordManager.com] ekzistas por helpi vin trovi la plej taŭga por via kazo.

<!--more-->

> Stoki sentemajn informojn en aparatoj kaj interrete implicas krei kaj memori dekojn da pasvortojn por konservi tiujn informojn sekurajn.
> Malbonaj pavortkutimoj, kiel reuzado de pasvortoj, povas konduki al datumrompoj, identecŝtelo, fraŭdo kaj tiel plu.
>
> Administriloj por pasvortoj estas oportunaj iloj, kiuj helpas generi kaj memori fortajn pasvortojn.
> Uzado de tiaj administriloj signifas, ke vi devas memori nur unu ĉefan pasvorton.
> Cetere, tiuj programaroj kutime ankaŭ provizas kroman protekton per dufaza aŭtentido kaj sekurecaj atentigoj.
>
> Ĉar haveblas multaj administriloj por pasvortoj, PasswordManager.com kreis ĉi tiun liston por helpi vin elekti kiu estas plej bona por vi, via familio aŭ via komerco.
> Vi lernos kelkajn el la plej rekomenditaj administriloj bazitaj sur sekureco, aparato-kongruo, stokado kaj funkcioj.
>
> Legu pli ĉi tie: https://www.passwordmanager.com/best-password-managers/
>
> <cite>Jessie Williams, Esploristino ĉe PasswordManager.com</cite>

La supra ligilo kondukas al plena listo, sed PasswordManager.com ankaŭ provizas aliajn, pli celitajn.
Ekzemple, [unu el ili][passwordmanager.com-free] fokusiĝas al administriloj por pasvortoj, kion vi povas uzi senpage, kaj [alia][passwordmanager.com-2fa] koncentriĝas pro dufaza aŭtentigo.

Se lista estas tro longa por vi, la retejo ankaŭ havas kelkajn detalajn recenzojn kaj unu-al-unu komparojn.
Kiam vi serĉas novan hejmon por viaj pasvortoj, ĝi estas bonega rimedo.
Nur unu rimarkinda limigo: la retejo disponeblas nur en la angla.

**Avizo de travidebleco**: Tiu afiŝo _NE ESTAS_ sponsorita enhavo.
PasswordManager.com proponis ke mi helpis ilin esti konataj, kaj iliaj rimedoj ŝajnis sufiĉe rilataj al ĉies sekureco, kion me akceptis.

[pwd-manager]: {{< relref path="/blog/2021/03-08-open-source-pwd-sync" >}}
[PasswordManager.com]: https://passwordmanager.com
[passwordmanager.com-2fa]: https://www.passwordmanager.com/best-password-managers-with-two-factor-authentication/
[passwordmanager.com-free]: https://www.passwordmanager.com/best-free-password-managers/
