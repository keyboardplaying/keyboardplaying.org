---
date: 2020-10-06T07:21:32+02:00
title: Installing Scrivener 3 on Linux
subtitle: Would you care for some wine to get started?
slug: install-scrivener-3-linux
description: |-
  Scrivener doesn't provide support for Linux.
  Yet, all hope is not lost.
cover:
  banner: false
  src: scrivener-kubuntu.jpg
  alt: Scrivener 3 splash screen
  by: chop
  license: CC BY-NC 4.0
author: chop
categories: [ writing, software ]
tags: [ linux ]
keywords: [ wine, scrivener, winetricks ]

references:
- id: pletcher-scrivener3-linux
  name: "Update: Scrivener 3 on Linux"
  title: true
  url: https://writeside.com/update-scrivener-3-on-linux/
  date: 08/2020, updated 01/2021
  lang: en
  author:
    name: Thomas Pletcher
---

As I'm preparing for my first attempt at [NaNoWriMo], I know I must have my writing software ready.
I used [Scrivener] in the past and know it's a solution I love to use.
My only problem was: my mobility OS is Linux, which Scrivener doesn't provide support for.

But that's no longer a problem as [Thomas Pletcher proved][pletcher-scrivener3-linux] that the latest betas of [Scrivener 3] work with [Wine].
Here's how.


<!--more-->

**2024-12-18 update**: *[Ryan Dewalt's guide](https://rdewalt.substack.com/p/installing-scrivener-on-linux) is now more up to date than my own.
Don't hesitate to use it. Thank Brazz for the information.

**2022-11-25 update**: Brazz shared some updated insights from running this tutorial lately, updating accordingly.
The tutorial was tried again from scratch with Scrivener 3.1.2 on Kubuntu 22.04.

**2021-08-12 update**: I added some forewarning about the known issues, so that you can decide if they're a blocker before you go with the installation.

**2021-07-17 update**: This page was updated to work with Scrivener 3.0.1 for Windows.
It seems OK on my setup, please keep me posted if you see ways to make this better.

**2022-01-15 update**: Alwerto found a solution about the dead keys problem, the _Troubleshooting_ and _Known Issues_ sections have been updated accordingly.



## Forewarning: Known Issues

Be warned that this tutorial is provided as is: it works on a Kubuntu 22.04 laptop, but I won't be able to provide individual support.
You might need adaptations for your particular case, but I will _not_ be able to assist you or provide individual consultations.

Though Scrivener works on my Linux setup, everything's not perfect and some glitches remain.
Those are things you can probably live with, but it may good to know of those beforehand, to avoid any surprise.


### Sticky Drag in Drag and Drop

First, there's a slight problem with drag and drop: whenever you drop something, the element remains in a transparent overlay, just as if you were still dragging it.
That's mainly a cosmetic issue, though, as the element is correctly moved and you can press <kbd>Esc</kbd> to bust that ghost.


## Getting Scrivener 3 to work on Linux

### Installing wine

To make Scrivener run on Linux, we'll use [Wine], a compatibilty layer for running Windows applications on POSIX systems (understand: "Linux OSs").

**Note**: This tutorial was written for people who don't know what Wine is and are probably installing it only to run Scrivener.
If you have a more advanced knowledge, you may wish to use prefixes.
Please do. I won't here.

Now, to the flesh of it:

1. We'll need Wine, [Winetricks] and winbind.
If those are available in your distribution's repos (they are for Ubuntu), installing them can be achieved with this command: `sudo apt install wine winetricks winbind`

2. The Wine installer in the Ubuntu repository has a slight defect: it doesn't create an important symbolic link, meaning you'll have to create yourself: `sudo ln -s /usr/share/doc/wine/examples/wine.desktop /usr/share/applications/`


### Provisioning Wine

Now that Wine is installed, you need to add the Windows components you will need to install and run Scrivener.

1. Optional: `winetricks corefonts` will install Windows default fonts (e.g. Times New Roman).
You may have already installed those from another source (e.g. `sudo apt install ttf-mscorefonts-installer`), in which case this is not required for you.

2. `winetricks win10` will install a Windows 10 architecture.

3. `winetricks dotnet48` will install .NET 4.8 (Scrivener needs at least .NET 4.6).
Check the "restart now" when required. Don't worry, you won't have to actually reboot.

4. `winetricks speechsdk`: the SpeechSDK is not listed as a requirement for Scrivener, but I couldn't run it without it.\
When asking for a _User Name_ and _Organization_, you can leave those fields blank.


### Installing Scrivener

1. You need to go and download Scrivener 3 (you can get the latest version [here][scrivener-download]).

2. To run the installer, open a terminal, navigate to the directory where you downloaded the installer (e.g., `cd ~/Downloads`), then run the command `wine Scrivener-installer.exe` (or adapt the program name to your own case).\
At the end of the installation, if you check "Create a desktop shortcut," you will get an error about Wine being unable to extract the icon.
As far as I can tell, it doesn't change the way Scrivener will work.

That's it!
At the end of the installation, Wine should have created an entry for Scrivener in your start menu, and you should be able to run it.
Don't hesitate to give additional tips as comments.

3. Optionnally, you can change the icon in your application menu.
Your applications are stored in your personal folder, as `.desktop` files.
Typically, you can find `Scrivener.desktop` and `Uninstall Scrivener.desktop` under `~/.local/share/applications/wine/Programs/Scrivener 3`.\
You can also find `icon.ico` and `scriv.ico` under `~/.wine/dosdevices/c:/Program Files/Scrivener3/resources`.\
You can therefore change the `Icon` line to use one of those two icons. For instance:
`Icon=/home/myuser/.wine/dosdevices/c:/Program Files/Scrivener3/resources/icon.ico`.[^fn-desktop-files]

[^fn-desktop-files]: Recommendations are not to edit those files directly, but to modify them elsewhere, then use the `desktop-file-install` command.
However, I feel that explaining how to do it properly would require a tutorial in this tutorial.\
Modifying the files manually will work.
You will need to log out and back in, and then wait a few minutes, to see the changes.

{{<figure src="icon.png" title="icon.ico">}}
{{<figure src="scriv.png" title="scriv.ico">}}

4. When you launch Scrivener, if you get a warning about it not being recommended to run on Windows 7, do the following:
  1. Close Scrivener.
  2. Open _Wine configuration_ (or type the command `winecfg`).
  3. At the bottom of the _Applications_ tab, select _Windows 10_.
  4. Close Wine configuration and launch Scrivener again.


### Command summary

If you follow this tutorial, here are all the commands you should have run.

```bash
# Install Wine
sudo apt install wine winetricks winbind
sudo ln -s /usr/share/doc/wine/examples/wine.desktop /usr/share/applications/

# Provision Wine for Scrivener
winetricks corefonts
winetricks win10
winetricks dotnet48
winetricks speechsdk

# Install Scrivener
cd ~/Downloads
wine Scrivener-installer.exe
```


## Troubleshooting

Here are some issues that readers met and solutions that were found.


### An error pops up when running the installer

Thanks to [OOzyPal] for making me have a look at this.

Symptoms
: When running Scrivener-installer.exe, a pop-up appears with the following message:
: > called Tcl_Close on channel with refCount > 0.

Solution
: This error only seem to happen when you run the installer via the graphical interface.
Running from the command line instead solves the issue: `wine Scrivener-installer.exe`.


### License activation fails

Thanks to [qbit] for sharing this problem and their solution.

Symptoms
: qbit could run Scrivener but not activate their license. Somehow, it resulted in the following message:
: > There was a problem activating Scrivener. Please try again in a moment, or restart your computer and ensure it is connected to the Internet, then try again. […] Object reference not set to an instance of an object.
: Running `wine ping google.com` ascertained this was not an Internet access issue.

Cause
: It seems the issue lied in the .NET 4.8 installation.

Solution
: qbit solved their problem by reinstalling .NET through Winetricks, via the command: `winetricks --force dotnet48`.


### Dead Keys Don't Work Properly

Thanks to [Alwerto] for finally finding a solution to that one.

Symptoms
: Some [dead keys][wiki-dead-keys] wouldn't work properly.
: For instance, the first <kbd>^</kbd> then <kbd>e</kbd> would produce the expected `ê`, but all following combos would get an `ê`, too, just as if the software would keep printing the first combo.
: From time to time, it would refresh to a newer combo, but I never found a pattern or action to force this refresh.

Cause
: Not fully understood, but this appears to not being caused specifically by Wine or Kubuntu, as [a user reproduced it in Linux Mint with Cinnamon][winehq-forum-kbd-mapping].

Solution
: Alwerto followed [the proposed solution][winehq-forum-kbd-mapping] and it solved the problem for him.
: The idea is to set the input method to Ibus. On my KDE, the command `im-config -n ibus` did the trick. I'll let you find how your distribution handles it.


### When Nothing Works…

[Marie] tried the installation procedure, but it did not work for her and she looked for a way to undo everything.
Here's the solution I suggested:

1. `sudo apt purge wine winetricks winbind` removes all Wine-related software.
2. `rm -rf ~/.wine` deletes the directory where it should have created its Windows image.



{{% references %}}
[NaNoWriMo]: https://nanowrimo.org/
[Scrivener]: https://www.literatureandlatte.com/scrivener/overview
[Scrivener 3]: https://www.literatureandlatte.com/scrivener-3-for-windows-update
[scrivener-download]: https://www.literatureandlatte.com/scrivener/download
[Wine]: https://www.winehq.org/
[Winetricks]: https://wiki.winehq.org/Winetricks
[wiki-dead-keys]: https://en.wikipedia.org/wiki/Dead_key
[winehq-forum-kbd-mapping]: https://forum.winehq.org/viewtopic.php?t=29209#p119201

[OOzyPal]: {{< relref path="/blog/2020/10-07-scrivener-linux" >}}#1b81f9e0-e394-11eb-ba33-f7737f1b8e3b
[qbit]: {{< relref path="/blog/2020/10-07-scrivener-linux" >}}#2484e0a0-e922-11eb-be49-af0855a433cf
[Marie]: {{< ref path="/blog/2020/10-07-scrivener-linux" lang="fr" >}}#14cb3d20-4905-11eb-af45-ef5ddb6294fc
[Alwerto]: {{< relref path="/blog/2020/10-07-scrivener-linux" >}}#4686ba60-74b5-11ec-a447-21c0d47b28cc
