---
date: 2020-12-21T22:37:42+01:00
title: Gérer un projet perso
subtitle: Ce que j'en retiens
slug: gerer-projet-perso
description: |-
  Les développeurs ont souvent des projets personnels à côté du travail, mais avez-vous déjà essayé d'en gérer un ?
  Moi, oui.
  Voici ce que j'en retiens.
cover:
  banner: false
  src: gantt-2020-08-30.jpg
  alt: Un diagramme de Gantt représentant l'état du projet à la fin août
  by: chop
  license: CC BY-NC 4.0
author: chop
categories: [ projects ]
tags: [ management ]
keywords: [ carrefour des imaginaires, gestion de projet, écriture, gantt, temps, burn out, collaboration ]
---


En tant que développeur, j'ai eu plusieurs projets perso, mais j'ai toujours travaillé seul.
Cette année, la [communauté créative dont je fais partie][commudustylo] a décidé de réaliser quelque chose qui impliquerait du travail.
Je pensais avoir une vision claire et j'ai décidé de me lancer dans la gestion de ce projet.

Voici ce que j'en retiens.

<!--more-->

## Le projet

Avant d'aller plus loin, mettons-nous d'accord sur le vocabulaire : un « projet perso » désigne ici un projet sur lequel vous travaillez pendant votre temps libre, avec peu d'espoir de le rentabiliser financièrement.
Ce ne sont pas que des définitions, ce sont des motivations qu'il faut garder à l'esprit quand on gère ce genre de projet, mais j'y reviendrai plus loin.


### Le périmètre

<aside><p>Nous voulions autoéditer un livre pour Noël.</p></aside>

Alors, quel était-il, ce projet ?
Il se résume plutôt simplement : nous voulions autoéditer un livre que nous pourrions offrir à nos proches pour Noël.
Nous souhaitions le construire autour de nos [défis bimestriels] et inclure des illustrations par nos « dessineux » afin de représenter ce qui fait la particularité de notre communauté.

Les tâches principales se détachaient aisément :
- Créer de nouveaux contenus, particulièrement pour permettre à ceux qui n'avaient pas participé à tous les défis de fournir d'autres contributions.
- Relire. Tout. Plusieurs fois. Par des personnes différentes.
- Créer une illustration pour chaque défi et créer une couverture.
- Éditer le livre et le promouvoir si nous décidions de le rendre publiquement disponible.

Les délais auraient pu être confortables, mais l'expérience a montré qu'ils n'étaient pas excessifs non plus : nous avons commencé en juin et notre objectif était de tout boucler pour début novembre, afin de se laisser un peu de mou pour traiter les imprévus.


### Divulgâchons un peu

Premièrement, _oui_, nous sommes parvenus à créer notre livre.
Nous l'avons presque tous reçu une semaine pile avant Noël, bien que l'un de nous ait eu un souci technique à la commande et ne le recevra pas à temps.
Bien que la prestation de [Bookelis] soit satisfaisante sur presque tous les aspects, le SAV n'est pas leur point fort.

L'autre question que vous pourriez vous poser maintenant est la suivante : le livre sera-t-il publiquement disponible ?
Oui, il le sera.
Pour un prix que nous jugeons raisonnable.
Nous partagerons le lien cette semaine.
Les éventuels bénéfices seront mis de côté pour des projets futurs de la communauté.


## Ce que j'en retiens

Je ne parlerai pas de leçons.
Certains de ces points sont davantage des tentatives pour éviter de reproduire ce que je pense être des erreurs observées dans ma vie professionnelle.

Je ne suis pas un chef de projet et cette expérience confirme que ce n'est pas le rôle où je suis le plus pertinent.
Cela ne m'empêche pas de travailler main dans la main avec eux tous les jours, et d'avoir un rôle d'encadrant pour mes coéquipiers.
J'oserai donc me targuer d'avoir quelque expérience sur laquelle me baser.


### Donnez une vision à votre équipe {#vision}

J'ai travaillé sur un projet qui m'a [pratiquement valu un _burn-out_][exhaustion] il y a quelques années.
J'en retiens une chose : nous ne savions pas où nous allions ni où nous voulions aller.
Tout semblait radicalement changer bien trop souvent.

Bien sûr, on ne peut pas prédire dès le départ tout ce qui se produira sur un projet, mais on a besoin d'un fil directeur.
Donnez à vos équipiers un cap et tenez-vous-y du mieux que vous le pourrez.
Les détails évolueront, il y aura des ajustements de trajectoire, mais ceux qui naviguent avec vous ont besoin de connaître la destination afin de collaborer au mieux.

Dans notre cas, quand le projet a démarré, j'ai pris le temps de décrire nos objectifs principaux (avoir un livre de nous à offrir à Noël), quelques idées bonus (le commercialiser) et je les ai soumis au vote de la communauté.

L'étape suivante a été de lister toutes les tâches auxquelles je pensais et mettre au point un diagramme de Gantt qui les organisait de façon à tenir notre date cible.
Je connais beaucoup de développeurs rebutés par ce type de représentations, mais ici, plusieurs membres ont exprimé un ressenti positif.
Ils étaient satisfaits d'avoir une idée de ce qu'il y avait à faire, et pour quand.

<aside><p>Donnez à votre équipe un but auquel elle peut aspirer.</p></aside>

Plus qu'une simple direction, donnez une vision à votre équipe, un but auquel elle peut aspirer.
Nous savions, instinctivement, que pouvoir tenir un livre papier dans nos mains, une manifestation physique de notre travail, et l'offrir à celles et ceux que nous aimons serait quelque chose dont nous serions fiers.
Ça nous a permis d'avancer.


### Ne sous-estimez pas le temps nécessaire

Dans mon travail, on me demande souvent d'estimer le temps dont j'ai besoin pour achever une tâche.
Après dix ans à le faire, je suis encore trop souvent optimiste.

<aside><p>Même avec une marge confortable, nous avons dépassé.</p></aside>

Cette fois, j'ai fait preuve de plus de mesure.
J'ai alloué deux mois entiers à la relecture de contenu existant.
Ces mêmes deux mois devaient permettre l'écriture de nouvelles histoires qui seraient relues également.
Au global, nous devions avoir bouclé les corrections en trois mois, début septembre.

Pourtant, j'ai encore trouvé des erreurs et fait des relectures assistées par logiciel quand j'ai commencé à préparer le corps du livre, _fin_ septembre.
En dépit d'une marge que je pensais confortable, nous avons dépassé.

Notre diagramme de Gantt était régulièrement mis à jour pour prendre en compte les tâches imprévues et celles qui prenaient plus longtemps que prévu.
Sur la première version, je pensais que la couverture serait illustrée fin août.
C'est l'un des derniers chantiers que nous avons bouclés.

![Diagramme de Gantt, 9 juin 2020](gantt-2020-06-09.svg)

![Diagramme de Gantt, 25 juillet 2020](gantt-2020-07-25.svg)

![Diagramme de Gantt, 13 septembre 2020](gantt-2020-09-13.svg)

Il y a des explications à cela.
Le temps dans un projet perso n'est pas une quantité aussi facile à manipuler que dans un projet professionnel.


#### Le temps alloué à un projet perso une notion complexe

Un projet professionnel bénéficie de temps alloué, où les personnes concernées ne s'occuperont de rien d'autre (oui, on parle de vos heures de bureau).
Sur un projet perso, je n'ai pas (trop) de mal à réserver des créneaux pour ça, par exemple dans le train matin et soir.
Cependant, quand c'est un projet de groupe, les choses sont différentes.

<aside><p>Vous ne pouvez avoir qu’une portion du temps libre de chaque membre.</p></aside>

Chaque personne travaillant sur le projet offre une part de son temps libre, mais « temps libre » est une notion relative.
Un jeune célibataire pourra peut-être en mobiliser beaucoup par exemple, mais la plupart du temps, chaque participant a ses contraintes, engagements, mérite de profiter de sa famille, doit se reposer...
Pour moi, cela a impliqué de mettre en pause mes autres projets, ce qui explique le calme sur ce site depuis juin.

La production n'est pas linéaire quand on travaille sur un projet perso.
Quand on a de la chance, tout le monde a du temps et est à fond.
La plupart du temps, seuls quelques membres sont à l'optimum, pendant que les autres gèrent des soucis personnels dont vous n'avez pas idée.

Ne vous croyez pas au-dessus de ça !
Même si vous êtes pressé de voir le projet aboutir, vous aurez aussi vos mauvaises passes.
La mienne a été octobre-novembre, quand un gros sujet avec une échéance courte est arrivé au travail.
Mon temps libre a été réduit au strict minimum et j'étais tellement fatigué en novembre que je n'avais plus d'énergie pour un projet perso.
Je ne cherchais qu'à me reposer.

Oui, vous devez gérer votre temps et [votre énergie][jauges].
Ce n'est pas facile, mais c'est nécessaire pour ne pas se vider complètement.


#### Le temps libre peut être repris

On reste sur le thème du temps libre.
Certes, on n'a jamais tout le temps libre d'un membre, mais ils peuvent aussi se lasser de travailler sans fin sur le même sujet ennuyeux.

<aside><p>Ils auront de nouvelles idées qu’ils voudront essayer, mais pas pour votre projet.</p></aside>

Imaginez un instant que vous avez relu les mêmes nouvelles depuis quatre semaines à la recherche d'erreurs, alors qu'une nouvelle idée vous démange, comme apprendre à vous servir des derniers outils de dessin que vous vous êtes offert.
C'est naturel, on a besoin de nouveauté, mais il faut en tenir compte pour le projet.

Cela implique de trouver comment garder ses troupes motivées, mais on n'est pas dans la vie professionnelle : vous n'êtes pas un chef en colère qui peut exercer sa toute-puissance sur ses employés, et ils ne touchent pas de salaire pour le travail qu'ils fournissent.
Il vous faut autre chose.
Pour moi, c'est là que la [vision](#vision) est un élément clé.

Cela ne nous a pas empêchés de tremper dans d'autres projets (j'ai essayé [le Lemontober et le Writober][creatober], par exemple).
Mais dans l'ensemble, le projet a continué à avancer.


### On ne gère pas en régentant tout

Oui, je parle pour moi.
J'essaie de ne pas chercher à tout contrôler, mais j'aime que les choses soient faites comme je les ai imaginées.
Ou mieux, mais je pense qu'il est commun de vouloir croire que ce qu'on a pensé est le mieux possible.

Au travail, on me demande depuis des années de déléguer, mais je préfère souvent faire les choses moi-même.
Je commence seulement à progresser dans ce domaine.

Un manager qui veut que tout soit fait à sa façon peut devenir nuisible de deux façons : soit en se lançant dans le micromanagement, soit en faisant tout lui-même.

Dans l'univers professionnel, quand un manager devient trop intrusif, on peut le supporter ou tenter d'améliorer les choses.
On n'a pas le choix, on _doit_ travailler avec cette personne.

Sur un projet perso, si vous ne voulez pas travailler avec quelqu'un qui ne le comprend pas, eh bien il est souvent plus simple de fuir le projet et récupérer ce temps pour quelque chose qui vous fait davantage envie.

N'essayez pas de régenter les actions de votre équipe, faites confiance à vos équipiers et à la [vision](#vision).

Quant au cas du manager qui s'affecte tout le travail qui l'intéresse, c'était moi.
Cela peut mener à toutes sortes de déboires, mais nous avons eu de la chance et ce sera mon dernier point.


### Demandez pardon, pas la permission

Je n'invente rien, probablement parce qu'il y a du vrai dans cette phrase.

J'ai déjà parlé de la disponibilité des équipiers et de leur motivation.
J'ai eu des hauts au moment où il y avait des bas en face.
J'ai donc pris des initiatives, fait des choses moi-même.
Nous savions qu'il fallait les faire, mais nous n'avions pas encore échangé sur le _comment_.

Qu'à cela ne tienne.
J'avais le choix entre attendre que tout le monde soit disponible pour en parler ou faire une première version pour offrir une base aux échanges.
Certaines propositions ont été immédiatement adoptées.
D'autres ont été rejetées pour recréer de zéro.
Je me souviens d'une proposition de couverture qui m'a occupé un après-midi complet et que nous avons abandonnée (je ne regrette pas au vu de la couverture finale).
Un autre brouillon que j'ai soumis était inexploitable, mais il a permis des échanges et donné des idées pour la suite.

Gardez juste à l'esprit que, lorsque vous prenez des initiatives, il se peut qu'elles soient rejetées.
Que cela ne vous empêche pas de le faire, mais ne vous bloquez pas sur le travail réalisé « en vain ».


### Faites confiance à vos équipiers

Je pourrais continuer, mais ce billet devient long, donc un dernier conseil : faites confiance à vos équipiers.
Il y a une raison à ce qu'on travaille en équipe : on va plus vite tout seul, on va plus loin à plusieurs.
Ils auront des talents et des idées qui vous feraient défaut.
Les échanges ne rendront les choses que meilleures.

Vous avez choisi de faire partie d'une équipe, ne jouez pas solo.

Merci de votre lecture.


[commudustylo]: https://twitter.com/commudustylo
[défis bimestriels]: /histoires/inspirations/bimestriels/
[Bookelis]: https://bookelis.com/
[exhaustion]: /histoires/epuisement/
[jauges]: https://skwi.fr/2020/09/28/les-jauges-d-energie/
[creatober]: https://twitter.com/chopAuteur/status/1322834283115982848
