---
date: 2020-01-27T23:56:40+01:00
title: Data Is the New Oil
subtitle: All your data belong to us.
slug: intro-sustainable-it/data-new-blood
aliases: [ intro-sustainable-digital/data-new-blood ]
description: |-
  Harvesting data seems to be the purpose of every digital solution nowadays.
  Some thoughts about it.
cover:
  src: franki-chamaki-1K6IQsQbizI-unsplash.jpg
  alt: Neon letters reading "Data has a better idea"
  by: Franki Chamaki
  link: https://unsplash.com/photos/1K6IQsQbizI
  authorLink: https://unsplash.com/@franki
  license: Unsplash
author: chop
categories: [ software ]
tags: [ sustainable-it ]
keywords: [ data, big techs, privacy, artifial intelligence, AI, GDPR, advertisement, DNA ]
series: [ intro-sustainable-it ]

references:
- id: internet-human-face
  name: The Internet with a Human Face
  title: true
  url: https://idlewords.com/talks/internet_with_a_human_face.htm
  date: 05/2014
  lang: en
  author:
    name: Maciej Cegłowski
- id: cnet-facial-recognition
  name: Facial recognition could take over, one 'convenience' at a time
  title: true
  url: https://www.cnet.com/news/at-ces-facial-recognition-creeps-into-everything/
  date: 01/2020
  lang: en
  author:
    name: Alfred Ng, CNET News
- id: forbes-stats-data
  name: How Much Data Do We Create Every Day? The Mind-Blowing Stats Everyone Should Read
  title: true
  url: https://www.forbes.com/sites/bernardmarr/2018/05/21/how-much-data-do-we-create-every-day-the-mind-blowing-stats-everyone-should-read/
  date: 05/2018
  lang: en
  author:
    name: Bernard Marr, Forbes
- id: data-never-sleeps
  name: Data Never Sleeps 7.0
  title: true
  url: https://www.domo.com/learn/data-never-sleeps-7
  date: 2019
  lang: en
  author:
    name: Domo
- id: nbcnews-fb-leveraged-data
  name: Leaked documents show Facebook leveraged user data to fight rivals and help friends
  title: true
  url: https://www.nbcnews.com/news/all/leaked-documents-show-facebook-leveraged-user-data-fight-rivals-help-n1076986
  date: 11/2019
  lang: en
  author:
    name: Olivia Solon & Cyrus Farivar, NBC News
- id: krebs-fb-passwords-plain
  name: Facebook Stored Hundreds of Millions of User Passwords in Plain Text for Years
  title: true
  url: https://krebsonsecurity.com/2019/03/facebook-stored-hundreds-of-millions-of-user-passwords-in-plain-text-for-years/
  date: 03/2019
  lang: en
  author:
    name: Krebs on Security
- id: forbes-fb-db-leaked
  name: Unsecured Facebook Databases Leak Data Of 419 Million Users
  title: true
  url: https://www.forbes.com/sites/daveywinder/2019/09/05/facebook-security-snafu-exposes-419-million-user-phone-numbers/
  date: 09/2019
  lang: en
  author:
    name: Davey Winder, Forbes
- id: amz-alexa-transcripts
  name: Answers from Amazon to Senator Coons about Alexa policies
  url: https://www.coons.senate.gov/imo/media/doc/Amazon%20Senator%20Coons__Response%20Letter__6.28.19%5B3%5D.pdf
  date: 06/2019
  lang: en
  author:
    name: Brian Huseman, Amazon
- id: google-speech-data
  name: More information about our processes to safeguard speech data
  title: true
  url: https://www.blog.google/products/assistant/more-information-about-our-processes-safeguard-speech-data/
  date: 07/2019
  lang: en
  author:
    name: David Monsees, Google
- id: wiki-gdpr
  name: General Data Protection Regulation
  title: true
  url: https://en.wikipedia.org/wiki/General_Data_Protection_Regulation
  lang: en
  author:
    name: Wikipedia
- id: wiki-ccpa
  name: California Consumer Privacy Act
  title: true
  url: https://en.wikipedia.org/wiki/California_Consumer_Privacy_Act
  lang: en
  author:
    name: Wikipedia
- id: shift-rapport
  name: Déployer la sobirété numérique
  title: true
  url: https://theshiftproject.org/article/rapport-intermediaire-deployer-sobriete-numerique/
  lang: fr
  author:
    name: The Shift Project
- id: amnesty-data-threat
  name: "Surveillance giants: How the business model of Google and Facebook threatens human rights"
  title: true
  url: https://www.amnesty.org/en/documents/pol30/1404/2019/en/
  date: 11/2019
  author:
    name: Amnesty International
- id: nytimes-clearview
  name: The Secretive Company That Might End Privacy as We Know It
  title: true
  url: https://www.nytimes.com/2020/01/18/technology/clearview-privacy-facial-recognition.html
  date: 01/2020
  lang: en
  author:
    name: Kashmir Hill, The New York Times
- id: nature-genomic-surveillance
  name: Crack down on genomic surveillance
  title: true
  url: https://www.nature.com/articles/d41586-019-03687-x
  date: 12/2019
  lang: en
  author:
    name: Yves Moreau, Nature
- id: cbs-biobank
  name: California Biobank Stores Every Baby's DNA; Parents Unaware Of Practice
  title: true
  url: https://sanfrancisco.cbslocal.com/2018/05/08/california-biobank-stores-baby-dna-access/
  date: 05/2018
  lang: en
  author:
    name: Julie Watts, CBS SF BayArea
---

Every project we hear about these days seems to be about data---or artificial intelligence, which is mainly the same.
Data is not something new, but the enthusiasm about it is growing rapidly.
People are not always aware of how much a website or an application is collecting about them, pushing legislators to write laws about what a company can and can't do with a user's data.

Why is that the problem of software creators?
Well, the mere amount of data we now collect could never be processed in a lifetime without our digital skills.
That makes us at least partly responsible for what is done with it.

<!--more-->

## We Give Our Data Away

### The Data We Produce

A few years ago, [some people began worrying about the data we put on the Internet and how it may be used][internet-human-face].
The social networks encouraged us to share ever more.
It seemed almost innocent at first.
Then came IoT.
Everything became connected and producing even more data to sell to companies and advertisers.
Builders now seem to do the same with [facial recognition][cnet-facial-recognition].

<aside><p>Ninety percent of the data in the world has been generated in the last two years.</p></aside>

In 2018, [Forbes published some stats about data][forbes-stats-data].
For instance, they said that 90% of the data in the world had been generated in the preceding two years.
Domo's [Data Never Sleeps infographic][data-never-sleeps] is enlightening.
Every tweet, every post, every click produces data that can be sold to an advertiser, to help them understanding your habits and deduce patterns.

A more recent study by IDC estimated that **the volume of stored data will reach 175 ZB in 2025**, which is **5.3 more than in 2018**.

{{< figure src="size-global-datasphere.png" alt="A bar chart demonstrating the growth of the global datasphere per year" attr="Source: _Data Age 2025---The Digitization of the World from Edge to Core_" attrlink="https://www.seagate.com/fr/fr/our-story/data-age-2025/" >}}

<aside><p>“If it’s free, you are the product.”</p></aside>

This data has become a business and a currency.
For instance, [Facebook leveraged user data depending on their relation with their partners and rivals][nbcnews-fb-leveraged-data].



### Why We Give This Data Away

<aside><p>At first, we didn’t have choice or knowledge.</p></aside>

At first, we didn't have much of choice, nor were we even really conscious of what was beginning.
We browsed the Internet and cookies were stored on our computers, but we didn't really know what they were.
Ads caught our eyes and clicks and we produced data without even knowing it.
We began posting, tweeting, producing our own content on social networks, thinking we were sharing with our friends, not realizing that we were also sharing with big companies.

Then, the harvesting of data became even more pervasive, through IoT, assistants, and so on.
I can't find the source today, but one of Google's founders declared a few years back that people want to be assisted.
They want to be reminded they have to buy milk when they're passing near the grocery store.
This implies that, for this comfort, those people would accept to let Google know where they are and what's in their fridge.
That's how we started knowingly sharing our data with big companies.
This data includes basic things but goes to health or intimate data: your physical activity and shape, your blood pressure, your genome...

**Is it worth it? And should we trust those companies?**



### What Happens to Your Data

Have you ever stopped to wonder what your data becomes?
Do you feel those companies you give it to have earned your trust?
In 2019 only, Facebook has been known to [rediscover hundreds of millions of user passwords stored in plain text for years][krebs-fb-passwords-plain] and [user data was accessible on a forgotten server][forbes-fb-db-leaked].

Ok, everybody makes mistakes---though such mistakes are huge, given the means Facebook has---but even the normal processing of your data may surprise you.
Do you know [transcripts of your Alexa requests are preserved][amz-alexa-transcripts]?
Or that [human "experts" may listen to your Google Home requests][google-speech-data]?

Are you comfortable with that?
Legislators aren't.


## The Laws to Protect Us and Our Data

In 2016, the European Union adopted a law to protect users, the [General Data Protection Regulation (or GDPR)][wiki-gdpr].
It applies to any enterprise processing personal information of subjects inside the European Economic Area.
Without getting into the details, the spirit of the law states that users must know:
- which of their data is collected;
- to which purpose;
- how long they will be retained;
- with whom they will be shared.

This law also imposes that users _explicitly opt in_ to their data collection.
The banner saying, "By navigating this website, you accept that..." is no longer a viable solution.

One last interesting point about this law is that a service provider can _not_ prevent you from accessing their service because you refuse sharing your data if that data is not required for the service to work.
And that means that if it needs your first name only and you refuse to provide your last name also, it _must_ allow you to access the service.

Of course, the GDPR would be useless without an enforcement incentive.
As always, money is the most efficient incentive there is for big companies, so those who do not respect those directives might be imposed a fine up to €20 million or up to 4% of the worldwide annual turnover, whichever is greater.

And you may think this is European only, but it's not!
California, the home of the Silicon Valley and the big digital companies, voted the [California Consumer Privacy Act][wiki-ccpa], the intentions of which are quite similar to the GDPR's.

Basically, it's about being informed and in control of our data.


## The Vicious Cycle of Data Collection

[The Shift Project's 2019 study "_Déployer la sobriété numérique_"][shift-rapport] put it in words concisely and eloquently: as we handle more data, the infrastructure that transports, processes and stores it grows, allowing for new uses.
Those novelties themselves won't quench the thirst for data but make it even more acute.

And so it goes: having more data means being able to handle more data means wanting more data.
This, of course, has an adverse effect on [the environmental footprint of digital technology][footprint].
This also means that [we don't think really much][creating-the-future] of how relevant or privacy-compliant these new uses are.


## A Brave New World

I want to keep it short, so I'll be brief, but feel free to follow the links when they are available.

We've seen Facebook and Google---among others---have access to a large amount of data.
[Amnesty International deems their business model a threat to human rights][amnesty-data-threat].

You've probably never heard about Clearview AI, but if a picture of you ever made it to the Internet, [it's almost sure to be able to recognize you][nytimes-clearview], even with your face covered.
Its database contains 3 billion faces, for "only" 411 million in the FBI's.

Even more intrusive surveillance, the DNA you may have sent to know about your ancestry [may be used to aid human-rights abuses][nature-genomic-surveillance].
To make it worse, [the DNA of babies born in California in 2018 may have been stored and sold to private research][cbs-biobank].
Some French geneticists also sometimes choose to ignore the restrictions of the GDPR, sharing the genomic data of patients with other research teams without asking for their consent.
Our genome is the most intimate data we could share, it's an important part of who we are, but it doesn't seem to receive any more respect than any other piece of data.

These are only a few examples, and that's not even talking about the many scandals that are popping around the world about facial recognition.
Everyone is interested in your data: the companies, the police and now the state.

And thinking about the future, what'll happen when Google proposes a mortgage insurance?
Will you be comfortable subscribing it knowing it has all your Google Fit data?



## Words of Parting

I understand that most of us do as we're told and think that's the only thing they can do, but hiding behind orders resembles the coward's way.
The role of software creators is not only to type on a keyboard.
You know things your superiors don't---not because you're a developer, just because every human has knowledge and experience of their own---and you should always share your mind to your boss, project manager or client if you think it is necessary.

And yes, where there is data, there tends to be AI nowadays, but, please! do you really need AI for what you're creating?
Buzzwords and fads don't make a solution better, and AI often feels that way to me.
But I'll come back to it in [the last post of this series][creating-the-future].


{{% references %}}

[footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}
[creating-the-future]: {{< relref path="/blog/2020/01-intro-sustainable-it/04-creating-the-future" >}}
