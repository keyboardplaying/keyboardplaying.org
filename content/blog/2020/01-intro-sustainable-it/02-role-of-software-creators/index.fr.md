---
date: 2020-01-20T23:54:30+01:00
title: La création logicielle pour un numérique responsable
#subtitle: No funny subtitle found
slug: intro-numerique-responsable/creation-logicielle
description: |-
  On entend beaucoup parler de « développement durable », d'actions pour la planètes ou les peuples...
  Et nous, créateur et créatrices logicielles, comment pouvons-nous contribuer ?
cover:
  src: safar-safarov-MSN8TFhJ0is-unsplash.jpg
  alt: Des plantes vertes en pot à côté d'un ordinateur portable sur lequel est affiché un éditeur de code.
  by: Safar Safarov
  link: https://unsplash.com/photos/MSN8TFhJ0is
  authorLink: https://unsplash.com/@codestorm
  license: Unsplash
author: chop
categories: [ software, software-creation ]
tags: [ sustainable-it ]
keywords: [ numérique responsable, création logicielle, matériel, hardware, obsolescence, logiciel, software, obésiciel, bloatware, support, écoconception, bonne pratique, outil, gras numérique ]
series: [ intro-sustainable-it ]

references:
- id: ifixit
  name: iFixit
  url: https://www.ifixit.com/
  lang: en
- id: fairphone
  name: Fairphone
  url: https://www.fairphone.com/
  lang: en
- id: phonebloks
  name: Phonebloks
  url: https://phonebloks.com/
  lang: en
- id: computerhistory-apollo
  name: Apollo Guidance Computer read-only rope memory
  url: https://www.computerhistory.org/timeline/1969/
  lang: en
  author:
    name: Computer History Museum
- id: lifewire-emails
  name: Why Are Email Files so Large?
  title: true
  url: https://www.lifewire.com/what-is-the-average-size-of-an-email-message-1171208
  date: 2020
  lang: en
  author:
    name: Heinz Tschabitscher, Lifewire
- id: kendall-power-smartphone
  name: Would your mobile phone be powerful enough to get you to the moon?
  title: true
  url: https://theconversation.com/would-your-mobile-phone-be-powerful-enough-to-get-you-to-the-moon-115933
  date: 02/2019
  lang: en
  author:
    name: Graham Kendall, University of Nottingham
- id: cnn-slowing-iphones
  name: "Apple: Yes, we're slowing down older iPhones"
  title: true
  url: https://money.cnn.com/2017/12/21/technology/apple-slows-down-old-iphones/
  date: 12/2017
  lang: en
  author:
    name: Ivana Kottasová, CNN Business
- id: androidverge-s7-oreo
  name: "Android 8.1 Oreo for Galaxy S7 and S7 Edge : Release and Features"
  title: true
  url: https://androidverge.com/android-8-1-oreo-galaxy-s7-s7-edge-release-features/
  date: 09/2018
  lang: en
  author:
    name: AJ, Android Verge
- id: androidauthority-android8-s7
  name: "Android 8.0 Oreo update tracker"
  title: true
  url: https://www.androidauthority.com/android-8-0-update-784308/
  lang: en
  author:
    name: Mitja Rutnik, Android Authority
- id: visser-un-sdgs
  name: UN Sustainable Development Goals – Finalised Text & Diagrams
  title: true
  url: http://www.waynevisser.com/report/sdgs-finalised-text
  date: 08/2015
  lang: en
  author:
    name: Wayne Visser
- id: wiki-green-computing
  name: Informatique durable
  title: true
  url: https://fr.wikipedia.org/wiki/Informatique_durable
  lang: fr
  author:
    name: Wikipédia
- id: inr
  name: Institut du Numérique Responsable
  url: https://institutnr.org/
  lang: fr
- id: wiki-ecoconception
  name: Écoconception
  title: true
  url: https://fr.wikipedia.org/wiki/%C3%89coconception
  lang: fr
  author:
    name: Wikipédia
- id: greenspector
  name: Greenspector
  url: https://greenspector.com/
- id: cnumr-ecoconception-web
  name: Référentiel d'écoconception web
  title: true
  url: https://collectif.greenit.fr/ecoconception-web/
  lang: fr
  author:
    name: Collectif Conception Numérique Responsable
- id: ecometer
  name: Ecometer
  url: https://ecometer.org
  lang: en
- id: gtmetrix
  name: GTmetrix
  url: https://gtmetrix.com
  lang: en
- id: ecoindex
  name: Ecoindex
  url: https://ecoindex.fr
  lang: fr
- id: greenit-logomotion
  name: "éco-conception numérique : la parole aux experts > Logomotion"
  title: true
  url: https://www.greenit.fr/2016/06/05/eco-conception-numerique-la-parole-aux-experts-logomotion/
  date: 06/2016
  lang: fr
  author:
    name: Bertrand Laboureau, PDG de Logomotion
- id: fb-hiphop-php
  name: "HipHop for PHP: Move Fast"
  title: true
  url: https://www.facebook.com/notes/facebook-engineering/hiphop-for-php-move-fast/280583813919
  lang: en
  author:
    name: Haiping Zhao, Facebook Engineering
- id: wiki-hiphop-php
  name: HipHop for PHP
  title: true
  url: https://fr.wikipedia.org/wiki/HipHop_for_PHP
  lang: fr
  author:
    name: Wikipédia
- id: art-energy-efficiency
  name: Energy Efficiency across Programming Languages — How Do Energy, Time, and Memory Relate?
  title: true
  url: https://greenlab.di.uminho.pt/wp-content/uploads/2017/10/sleFinal.pdf
  date: 10/2017
  lang: en
  author:
    name: Rui Pereira, Marco Couto, Francisco Ribeiro, Rui Rua, Jácome Cunha, João Paulo Fernandes, João Saraiva
- id: art-docker
  name: How does Docker affect energy consumption? Evaluating workloads in and out of Docker containers
  title: true
  url: https://arxiv.org/abs/1705.01176
  date: 05/2017
  lang: en
  author:
    name: Eddie Antonio Santos, Carson McLean, Christopher Solinas, Abram Hindle (Cornell University)
- id: medium-dark-oled
  name: Designing a Dark Theme for OLED iPhones
  title: true
  url: https://medium.com/lookup-design/designing-a-dark-theme-for-oled-iphones-e13cdfea7ffe
  date: 05/2019
  lang: en
  author:
    name: Vidit Bhargava
- id: greenit-appli-ecoconcue
  name: Une appli éco-conçue pour favoriser les circuits-courts
  title: true
  url: https://www.greenit.fr/2020/01/14/une-appli-eco-concue-pour-favoriser-les-circuits-courts/
  date: 01/2020
  lang: fr
  author:
    name: Joël Mossand
- id: adobe-personas
  name: "Putting Personas to Work in UX Design: What They Are and Why They’re Important"
  title: true
  url: https://blog.adobe.com/en/2017/09/29/putting-personas-to-work-in-ux-design-what-they-are-and-why-theyre-important.html#gs.zfdpuh
  date: 09/2017
  lang: en
  author:
    name: Nick Babich, Adobe
- id: fb-2g-tuesdays
  name: "Building for emerging markets: The story behind 2G Tuesdays"
  title: true
  url: https://engineering.fb.com/2015/10/27/networking-traffic/building-for-emerging-markets-the-story-behind-2g-tuesdays/
  date: 10/2015
  lang: en
  author:
    name: Chris Marra, Facebook Engineering
- id: hal-methodology-greenit
  name: Software development methodology in a Green IT environmen
  title: true
  url: https://tel.archives-ouvertes.fr/tel-01724069/document
  date: 2017
  lang: en
  author:
    name: Hayri Acar, Université de Lyon
- id: ecoconception-note-synthese
  name: "Note de synthèse numérique : version abrégée"
  title: true
  url: https://www.eco-conception.fr/library/h/note-de-synthese-numerique-version-abregee.html
  date: 04/2019
  lang: fr
  author:
    name: Pôle Éco-conception
---

La semaine dernière, [j'ai écrit au sujet de l'empreinte environnementale du numérique][footprint-of-digital].
On peut penser que cela concerne essentiellement le matériel --- et plusieurs sociétés ou associations combattent pour une électronique plus modulaire, réparable et, plus généralement, durable.

Vous avez envie de contribuer à ce mouvement, mais vous ne voyez pas comment, car vous travaillez exclusivement sur le côté _logiciel_ et n'avez pas votre mot à dire sur le matériel ?
Ce billet vous donnera quelques pistes sur de possibles façons de tenir compte de ces considérations lorsque vous concevrez votre application.

<!--more-->

## Initiatives pour le matériel

<aside><p>Les buts sont de produire moins, plus intelligemment, et de renouveler moins.</p></aside>

Je n'irai pas en profondeur sur ce sujet, puisque ce billet doit se concentrer sur le logiciel.
Si vous n'en avez jamais entendu parler, je vous invite à regarder les initiatives d'[iFixit][ifixit], [Fairphone][fairphone] et [Phonebloks][phonebloks], pour ne citer que ces trois-là.

J'en détache trois grandes idées :
- Réparer est bien meilleur pour la planète que remplacer.
- Remplacer un composant/module d'un appareil est meilleur que remplacer l'appareil dans son ensemble.
- Sélectionner des [sources éthiques/responsables pour le coltan][coltan-blood-mineral] est une tâche ardue, mais Fairphone y est parvenu.

Pour résumer cette philosophie, **l'idée qui sous-tend cette approche est de produire moins d'équipement, depuis des ressources éthiques, et de conserver ces équipements plus longtemps**.



## Quelques causes logicielles

Oui, je parle de matériel à un lectorat dont je suppose que la majorité travaille dans la création logicielle.
Comme [je l'ai expliqué la semaine dernière][impact-code], le logiciel et le matériel sont liés.
Votre code aura besoin d'appareils sur lesquels être exécuté.

Et selon la façon dont votre code a été conçu, il pourrait participer au renouvellement accéléré des parcs numériques.
Ci-dessous, je vous présente les deux causes logicielles les plus courantes.


### Les obésiciels {#obesiciel}

<aside><p>Le poids moyen d'un courriel est supérieur à la ROM de l'ordinateur de guidage d'Apollo 11.</p></aside>

En 1969, la mission Apollo 11 a aluni.
À bord, l'ordinateur de guidage (l'Apollo Guidance Computer, ou AGC) [embarquait l'équivalent de 72 ko de ROM (read-only memory)][computerhistory-apollo].
De nos jours, [un courriel pèse en moyenne 75 ko][lifewire-emails].
[Graham Kendall estime][kendall-power-smartphone] qu'une TI-84 est près de 350 fois plus rapide que l'AGC, et que le dernier iPhone a plus de {{< numfmt 100000 >}} fois la puissance de calcul de cet ordinateur.
Et on se plaint de ne pas avoir d'espace pour nos applications et nos photos sur nos smartphones de 64 Go.

Retournons quelques années dans le passé, dans une banque.
La plupart des programmes sont écrits en Cobol et s'exécutent sur un serveur z/OS.
IBM vous facture pour le fait de faire tourner ces machines.
Plus elles tournent, plus vous payez, donc vous avez tout intérêt à écrire des jobs efficaces.
Par ailleurs, puisque vous travaillez dans une banque, des batches sont exécutés chaque nuit pour gérer toutes les tâches comptables hors des heures de travail.
Vous savez qu'un job Cobol se termine toujours, mais vous devez être efficace : tout doit être terminé lorsque la journée commence, et votre programme n'est qu'un petit engrenage dans une machine bien, bien plus grande.

Avançons de quelques années.
De nouveaux langages sont apparus.
De nouveaux systèmes d'exploitation également : Unix, et bientôt Linux.
« Les ressources ne sont pas chères », et leur prix continue de baisser.
La population qui tape sur les claviers est à l'aise, moins menacée par les contraintes d'infrastructure et les spécifications non fonctionnelles.
Nous n'avons qu'à faire le job.
Si la machine n'est pas assez puissante, on demandera à ce qu'elle soit améliorée.
Un de mes mentors a déjà déclaré à l'exploitation un magnifique : « Non mais attends, je vais t'acheter les barrettes à l'hypermarché et tu les installes ! »

_Il arrive_ que cette approche soit justifiée, et que nous _ayons_ besoin de ces ressources.
Malheureusement, trop souvent, nous ne nous posons même pas la question de savoir si nous pourrions faire mieux.
C'est à ce moment que nous arrivons à l'**obésiciel**.

{{< figure src="commitstrip-scumbag-chrome.fr.jpg" link="https://www.commitstrip.com/fr/2015/09/28/scumbag-chrome/" alt="Chrome réclame de la mémoire pour tous les logiciels, le développeur lui en donne, mais il mange tout et va dire aux éditeurs que le développeur les a envoyés promener." attr="CommitStrip" attrlink="https://www.commitstrip.com" >}}

Une anecdote estudiantine : une amie de l'époque avait un ordinateur portable en parfait état, mais il ne pouvait pas exécuter Firefox et Windows Live Messenger en même temps, parce que les deux logiciels étaient particulièrement friands de mémoire.
En tant qu'étudiants en école d'ingénieurs, rechercher des informations et échanger avec nos camarades étaient les tâches les plus basiques que nous pouvions demander à nos ordinateurs...

Un autre exemple : il y a quelques années, j'ai été contraint d'acheter à ma mère un nouvel ordinateur parce que les 20 Go du disque étaient occupés par les mises à jour cumulatives de Windows XP que je n'ai pas pu nettoyer en dépit de toute ma bonne volonté.

Voilà le piège de l'obésiciel : **les utilisateurs sont contraints de remplacer leurs appareils, bien que ceux-ci fonctionnent parfaitement, seulement parce que le logiciel n'a pas été conçu pour être économe en ressources**.
Et je ne parle bien entendu pas des cas où [l'éditeur ralentit volontairement ses anciens appareils par une mise à jour du système][cnn-slowing-iphones].


### Le support disparu {#end-of-support}

En 2015, [Amazon a présenté son Dash Button][wiki-amazon-dash].
Il permettait d'exécuter d'une simple touche une commande régulière --- et il a été reprogrammé pour bien d'autres usages.
En 2019 toutefois, Amazon a abandonné la série car les abonnements permettaient un réapprovisionnement automatique et les commandes vocales d'Alexa lui succèderaient.

<aside><p>Vous devez acheter les derniers équipements pour bénéficier des dernières fonctionnalités.</p></aside>

Prenons un autre exemple, un qui parlera davantage au commun des mortels : les smartphones.
J'ai acheté le mien, un [Nexus 5X][wiki-5x], en 2016, juste après la fin de la série.
La dernière mise à jour officielle que j'ai reçue était Android 8.1.0 December 2018.
**Un an s'est déjà écoulé sans mise à jour de sécurité, pour un téléphone qui n'a que trois ans**.
Il fonctionne parfaitement bien ! Il pourrait juste être plus sécurisé.
Et on parle d'un téléphone « officiel » Google, un Nexus, avec un Android « nature » !

Que se passe-t-il pour un téléphone avec une surcouche du fabricant, ce qui implique que ce dernier doit compiler et distribuer lui-même une version modifiée d'Android ?
Regardons le [Samsung Galaxy S7][wiki-s7] : il a été commercialisé un an après le Nexus 5X --- soit six mois avant que celui-ci soit retiré du marché.
Pourtant, [le S7 n'a jamais reçu Android 8.1.0 par les canaux officiels][androidverge-s7-oreo] ; il est resté coincé à Android 8.0.
Quand on y regarde de plus près, **[la dernière mise à jour que Samsung a poussée à la famille S7][androidauthority-android8-s7] date de juillet 2018, six mois avant la dernière mise à jour à la famille de mon téléphone, d'un an son aînée**.
Si les utilisateurs souhaitent bénéficier des fonctionnalités introduites dans les dernières versions du système d'exploitation, ils n'ont d'autre choix que d'acheter un nouvel appareil.

Soyons clairs : **la fin de support est un fléau pour le monde numérique !**
Bien entendu, il est économiquement inenvisageable de maintenir _toutes_ les versions d'un système d'exploitation, mais changer de smartphone tous les deux ans n'est pas quelque chose que tout le monde peut se permettre.
En fait, la plupart des gens _ne peuvent pas_ se le permettre.

Et **les êtres humains sont un pilier que nous oublions souvent de prendre en compte**, bien qu'il soit fondamental.



## Introduction au numérique responsable


### De préoccupations au développement durable {#3p-developpement-durable}

Être responsable requiert d'être conscient de ses impacts négatifs et d'en assurer la _responsabilité_.
Si vous suivez ce précepte, vous voudrez également réduire ces impacts.

<aside><p>Pour minimiser nos impacts négatifs, nous devons développer « durable ».</p></aside>

Jusqu'ici, nous avons vu que le numérique a des conséquences négatives [sur l'environnement][environmental-footprint], [sur la société][social-footprint], et on commence à voir [percoler][wikt-percoler] des intérêts et moteurs économiques.

Quand j'étais en école, on nous a répété à l'envi qu'il y avait quelque chose à l'intersection de ces trois piliers.
J'ai vu ce diagramme de Venn si souvent que j'y pense dès lors que l'on mentionne au moins deux de ses secteurs : **_Planet_, _People_ et _Prosperity_ sont les 3 P du développement durable**.[^fn-5ps]

{{< inline-svg src="venn-sustainable-diagram.fr.svg"
    title="Le diagramme de Venn qui définissait le développement durable quand j'étais en école d'ingénieur" >}}

[^fn-5ps]: En faisant des recherches pour ce billet, j'ai découvert que, [en 2015, les Nations Unies ont proposé deux autres **p**iliers][visser-un-sdgs] : **Paix** et **Partenariat**.
Bien que nous n'ayons que peu d'emprise sur le premier --- bien que cela soit discutable, notamment vis-à-vis du [coltan][coltan-blood-mineral] ---, travailler main dans la main avec nos pairs peut nous aider dans le cadre de l'[écoconception](#ecoconception).

Donc, fondamentalement, pour minimiser nos impacts sur l'environnement, **nous devons faire du développement durable** --- oui, le jeu de mots est voulu.

Ce n'est _pas_ un concept nouveau.
Le programme [Energy Star][wiki-energy-star] fut lancé en 1992 et marque, d'après [Wikipédia][wiki-green-computing], le début du _green IT_ ou éco-TIC.

> L'**informatique durable**, l'**informatique verte**, le **numérique responsable**, ou encore le **_green IT_** (appellation officielle en français : **éco-TIC**) est un ensemble de techniques visant à réduire l’empreinte sociale, économique et environnementale du numérique.
> {{< ref-cite "wiki-green-computing" >}}

On retrouve bien dans cette définition l'ensemble des enjeux auxquels nous nous sommes intéressés jusqu'alors.
J'ai tendance à préférer la dénomination « numérique responsable », les autres laissant trop souvent penser que l'on se concentre uniquement sur l'aspect environnemental.
L'[Institut du Numérique Responsable][inr] est apparu en France il y a quelques mois pour promouvoir ces thématiques et fédérer des entreprises s'y intéressant.

Tout cela est bel et bon.
Nous avons donc un nom, mais comment, pourriez-vous demander, faisons-nous de ce joli mot digne du marketing une réalité qui ne soit pas juste des balivernes pour le _green washing_ ?
Là encore, je n'inventerai rien et me contenterai de partager avec vous un concept qui existe depuis des années.


### L'écoconception de services numériques {#ecoconception}

L'écoconception est une notion apparue en 1996.
Voyons sa définition selon notre grand ami Wikipédia :

> L'**écoconception** est un terme désignant la volonté de concevoir des produits respectant les principes du développement durable et de l'environnement, en --- selon l'Ademe --- recourant « aussi peu que possible aux ressources non renouvelables en leur préférant l'utilisation de ressources renouvelables, exploitées en respectant leur taux de renouvellement et associées à une valorisation des déchets qui favorise le réemploi, la réparation et le recyclage », dans un contexte qui évoluerait alors vers une économie circulaire.
> {{< ref-cite "wiki-ecoconception" >}}

Elle a en premier lieu essentiellement été appliquée à des produits physiques, mais cette approche se décline aujourd'hui également pour les services numériques.

Il y aurait assez à dire sur l'écoconception (numérique) pour écrire un billet entier qui lui soit dédié, mais je vais rester simple aujourd'hui et laisser ça mûrir chez vous.
Quand on revient aux essentiels, il reste deux idées :
- Sachez ce dont utilisateurs ont besoin (ou pas) et préservez la sobriété de votre solution.
- Ayez conscience des conséquences que vos choix auront à l'avenir, et pesez le pour et le contre.

Je vous propose quelques exemples concrets de solutions s'inscrivant dans ce cadre, afin de vous donner une meilleure idée de ce dont on parle.



## Exemples de solutions techniques


### Nettoyez votre code


#### Visez l'efficience

<aside><p>Rendre votre code efficient en usage de ressources le rendra performant <em>et</em> efficient en énergie.</p></aside>

[Comme narré plus haut](#obesiciel), nous nous souciions jadis davantage d'écrire un code efficient.
C'est pourtant le premier levier que nous avons --- pas le seul ni le plus puissant --- pour nous assurer que notre code est économe en énergie : **plus on utilise de calculs, de mémoire ou de bande passante, plus on consomme d'énergie**.
C'est d'ailleurs sur ce principe que fonctionnent la plupart des logiciels qui mesu-devinent la consommation de votre application : mesurer combien de RAM/processeur/réseau un programme utilise et en déduire une valeur énergétique.

Un exemple Java _old school_ mais concret :

```java
for (int i = 0; i < collection.size(); i++) {
    // Faire quelque chose avec collection et i
}
```

Pour l'exemple, oublions la syntaxe `foreach`.
Deux choses pourraient être améliorées sur cette ligne :
- `++i` est plus efficace que `i++` car il ne requiert pas l'utilisation d'une variable temporaire.
- Si vous savez que la taille de la collection ne changera pas, pourquoi demander à la JVM de la calculer à chaque itération ?
Il serait plus efficace de la mémoriser une fois pour toutes.

Juste en appliquant ces petites optimisations, on obtient :

```java
final int size = collection.size();
for (int i = 0; i < size; ++i) {
    // Faire quelque chose avec collection et i
}
```

Ça semble être du pinaillage ?
Possible, mais [Greenspector][greenspector] estime qu'on a un gain de 5 % en consommation électrique pour cette simple modification.

Pour aborder un langage au sujet duquel je n'écrirai pas souvent, Greenspector a également indiqué que **l'utilisation du `foreach` de PHP plutôt que son `for` permet un gain de 30 % d'énergie avec des performances identiques**[^fn-greenspector-lg-versions].

[^fn-greenspector-lg-versions]: Greenspector m'a partagé ces informations en 2016.
Mon plus gros problème avec est que les versions des langages ne sont jamais précisées.
Nous savons que les langages évoluent et améliorent leurs points faibles, et PHP et Java ont grandement travaillé dessus ces dernières années.


#### Outils et bonnes pratiques

Quelles sont les bonnes pratiques pour optimiser votre code ?
Pour commencer, vous pouvez vous référer à toutes les bonnes pratiques que vous devriez déjà connaitre.
Elles visent souvent à minimiser une utilisation suboptimale de vos ressources.

Pour le développement web, il existe une jolie checklist de 115 recommandations [ici][cnumr-ecoconception-web] --- bien que je sois encore déçu de n'y voir figurer aucun conseil concernant les _single-page applications_.
Des outils comme [ecometer.org][ecometer] et [GTmetrix][gtmetrix] peuvent vous aider à voir quelles bonnes pratiques vous avez mises en œuvre et lesquelles peuvent être intéressantes.
[Ecoindex][ecoindex] donne également à votre page une note environnementale, à titre indicatif.
Ces outils sont également disponibles comme plug-in [dans Firefox][greenit-firefox] et [Chrome][greenit-chrome].

{{< figure src="gtmetrix-kp.org.png" caption="L'évaluation du [billet de la semaine dernière](/blog/2020/01/intro-numerique-responsable/empreinte-numerique/) par GTmetrix" >}}

Et oui, les gains sont concrets, même pour l'utilisateur final.
En 2015, [Logomotion][greenit-logomotion] a développé deux sites identiques.
Même interface, même contenu, approches différentes :
- Le premier a été réalisé en utilisant WordPress 4.4.2 sans optimisation particulière.
- Le second a été fait dans l'état de l'art, avec un générateur statique --- comme Jekyll ou Hugo, par exemple.

Soixante-dix bonnes pratiques ont été respectées dans le second cas contre 39 dans le premier.
Le DOM contenait 25 % moins d'éléments et nécessitait 39 % moins de requêtes.
**Le temps de chargement moyen a diminué de moitié et le poids moyen d'une page a été divisé par plus de deux**.


### Mélangez les technologies à votre avantage

Ce point est souvent difficile à accepter pour un développeur.
Nous avons tendance à devenir des prosélytistes de ce que nous utilisons et n'aimons pas penser à utiliser autre chose.
Encore moins le mélanger avec autre chose.
Quel blasphème !

Et pourtant, c'est parfois une excellente voie pour des gains conséquents.
Pour exemple, connaissez-vous la méthode [`System.arrayCopy`][doc-arraycopy] en Java ?
Elle porte bien son nom et fait précisément ce qu'il indique.
En Java, ce genre d'action nécessite de copier les éléments un par un, en _n_ opérations individuelles.
C'est pourquoi cette méthode existe et **c'est pourquoi elle est [native][baeldung-native], car il suffit d'un unique `memcpy` / [`memmove`][cpp-memmove] en C++**.

Un exemple plus imposant d'une petite société.
Il était une fois un petit réseau social que Mark codait pour son université.
Quelques années plus tard, ce réseau était devenu un phénomène mondial, nommé Facebook.

Quand Mark avait commencé, PHP était une solution idéale, mais son application grossissait, ainsi que le nombre d'utilisateurs et la masse de données.
Facebook commençait à être limité par le langage, mais il était inenvisageable de tout réécrire.

Une nuit, lors d'un hackathon, [un employé a commencé à écrire un programme pour transformer le PHP en C++][fb-hiphop-php].
Ce fut l'aube de [HipHop for PHP][wiki-hiphop-php], un transpileur de PHP vers C++ : les développeurs écrivaient du PHP mais c'était finalement du C++ qui était compilé pour faire tourner Facebook.
Par comparaison avec Zend, qui était le cadriciel utilisé jusqu'alors, **la génération des pages pouvait aller jusqu'à six fois plus vite, en utilisant moins de ressources** !


### Savoir, c'est pouvoir

Pour faire un choix éclairé, **vous devez en _connaitre_ les tenants et aboutissants**.
Ce billet est déjà long et j'ai encore des choses à vous dire, donc je vais faire vite.

Pour écoconcevoir votre solution, vous devez être conscients que [les langages n'ont pas tous les mêmes performances en termes de rapidité, mémoire et énergie][art-energy-efficiency].
Vous devez comprendre que [Docker impacte la consommation énergétique et les performances][art-docker], et c'est un sacrifice que vous acceptez pour une maintenance plus aisée.
Vous devez savoir que [le thème sombre n'est pas qu'une lubie de développeur, c'est aussi une économie d'énergie sur des smartphones OLED][medium-dark-oled][^fn-only-oled].

[^fn-only-oled]: Ceci n'est cependant vrai que pour les écrans **O**LED, en raison de la différence de technologie. [Blackle][blackle] est un mythe. Cela ferait peut-être d'ailleurs un bon sujet d'écriture.

Et il est important de _savoir_, pas deviner.
Cela signifie que vous aurez besoin de _mesurer_.
Utilisez JMeter, Gatling, benchmark.js, YSlow...
Tout indicateur de performance est généralement utile, même si incomplet.
Intégrez ces mesures dans votre intégration continue si vous le pouvez, afin de détecter des goulots d'étranglement ou des régressions.


### Adaptez-vous au contexte

J'ai un défaut --- enfin, plus d'un, mais concentrons-nous --- je suis perfectionniste.
J'adore faire les choses dans l'état de l'art, de rendre la solution générique et réutilisable...

Vous savez quoi ?
Si votre but est d'allumer une ampoule, vous n'avez pas besoin de construire une centrale nucléaire ; contentez-vous d'une pile.
Adaptez-vous à votre contexte et **restez sobre**.
Tout ce que vous ajoutez est une surcharge.
S'il n'est pas nécessaire, c'est juste une surcharge inutile.



## Au-delà de la technique

### Il n'est question que de l'utilisateur

<aside><p>Une approche UX vous aide à vous concentrer sur ce qui est réellement utile.</p></aside>

J'ai dit [plus tôt](#3p-developpement-durable) que le développement durable s'appuie sur 3 P : la planète, les **personnes** et la prospérité.
Il est temps de s'en souvenir et de remettre les personnes au centre : ce que nous faisons est pour elles.
**Une approche fondamentale de l'écoconception est de [concevoir l'expérience utilisateur][greenit-appli-ecoconcue]**.

Si vous concevez votre application du point de vue de vos utilisateurs --- en utilisant des [personas][adobe-personas] par exemple --- plutôt que de simplement lire / écrire des spécifications fonctionnelles, vous penserez différemment et gagnerez une nouvelle perspective.

En premier lieu, vous pourrez séparer l'important du _nice to have_.
Trop souvent, les personnes responsables des specs pensent : « C'est important (pour moi). »
Seulement, le « pour moi » est muet. 
C'est un biais, et les biais sont des choix qui ne sont pas forcément les attentes des utilisateurs.
Peut-être que c'était important mais qu'aucun utilisateur ne s'en servira jamais.
Dans ce cas, ce n'est que du gras.

Si on pense à Microsoft Word, je pense être plus avancé que la moyenne des utilisateurs.
Je pense aussi que je ne connais pas la moitié de ses fonctionnalités.
Qui utilise les autres ? Sont-ce des cas ultra-spécifiques ? Ont-elles leur place dans un traitement de texte ?

Cette nouvelle perspective vous aidera également à mieux vous projeter.
Quand les specs disent « cette application devra respecter toutes les recommandations d'accessibilité », on ne voit qu'une liste de règles et on applique le [WCAG][wcag] sans se poser de questions.
Revenons à nos personas.
Maria est aveugle, mais elle a besoin de gérer son argent comme tout le monde.
Comment le ferais-je à sa place ?
Puis-je utiliser un lecteur d'écran ?
L'application est-elle compatible ?
**Changer la façon de présenter le problème change la façon d'aborder la solution**, et l'engouement autour de la recherche de cette solution.
Dans la première approche, j'applique des règles ; dans la seconde, c'est un jeu ou un défi.
Et les créateurices de logiciel adorent les défis.



### Rester maigre

Au-delà de l'UX, d'autres pratiques peuvent éviter d'ajouter du gras à votre application.

**Concevant la version mobile d'abord**.
La société de chemins de fer allemande, Deutsche Bahn, propose deux versions de son outil de recherche d'horaire.
Le premier, sur smartphone, pèse 3 ko, contre plus de 2000 pour le second sur ordinateur, le tout pour un résultat identique.

Souvenez-vous aussi que **tous vos utilisateurs n'auront pas la dernière machine de gamer**.
[Les machines puissantes sont utiles pour développer]({{< relref path="/blog/2019/11-11-cost-of-time" >}}), mais elles sont généralement chères pour le quidam.
Lorsque Google préparait Android KitKat pour son futur smartphone, le Nexus 5, les équipes de développement ont dû le tester sur le précédent téléphone, le Nexus 4.

Les équipements régionaux sont aussi à considérer.
En 2015, [Facebook a lancé ses _2G Tuesdays_][fb-2g-tuesdays].
L'idée était de simuler un réseau lent afin que les développeurs puissent expérimenter leur solution telle qu'elle était ressentie dans les pays émergents --- et ainsi l'adapter à ces marchés et utilisateurs.

Dernier conseil : **restez sobres**.
Il y a quelques années, Bing s'est rendu compte que l'immense majorité de ses utilisateurs ne passaient jamais la première page de résultats.
Pourtant, le moteur chargeait bien plus de résultats, au prix de ressources non négligeables.
Les équipes ont donc réduit le nombre de résultats à 20.
Dans les cas où les utilisateurs demandaient à voir plus, on allait en chercher davantage.
Dans de rares cas, cela peut aboutir à une infime dégradation de performances, mais la facture électrique des serveurs a baissé de 80 %.[^fn-bing]

[^fn-bing]: Je suis terriblement frustré de ne pas avoir trouvé de source pour cette info.
Je fais confiance aux personnes qui me l'ont communiquée, mais j'aurais aimé avoir davantage de détails.



## Avant de nous séparer

Ce billet a été écrit pour vous donner un aperçu de comment faire de l'écoconception de solutions numériques.
[Beaucoup de lecture](#references) existe sur le sujet, qui est vaste et en croissance, et dont nous avons à peine effleuré la surface.
Je pense que deux idées doivent être retenues : **sachez les conséquences de vos choix** et **évitez le gras**.
Quand on y pense, ce sont de bonnes recommandations générales pour la programmation.

Car c'est ainsi que je le perçois : **l'écoconception n'est qu'un autre niveau de qualité de code**, nous forçant à nous assurer que notre code n'est pas seulement maintenable et performant, mais aussi **durable**.

Le premier but de cet objectif, aussi indirect qu'il soit, est de **produire moins d'équipement et de les conserver plus longtemps**.
Le second but est de **réduire les besoins énergétiques des appareils numériques**.
Ce dernier point, à l'échelle des centres informatiques, ne pourra être achevé qu'en réduisant la quantité de données stockées --- mais ce sera [le billet de la semaine prochaine][data].

Un dernier conseil et son anecdote : votre application sera utilisée par des humains.
Les humains font des erreurs.
Une équipe avait livré un CMS Drupal optimisé.
Le module Boost générait des versions statiques de chaque page --- plus besoin de les générer à la volée, les pages étaient prêtes à servir.
Quelques semaines plus tard, le client se plaint de la lenteur du site.
L'équipe revient, recherche, et constate des images très haute résolution au sein des pages.
Le CMS n'avait pas été pensé pour être utilisé ainsi.
L'éducation fera toujours partie de notre métier.


{{% references %}}

[footprint-of-digital]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}
[environmental-footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}#empreinte-environnementale
[impact-code]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}#impact-code
[social-footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}#empreinte-societale
[coltan-blood-mineral]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}#coltan-mineral-conflits
[data]: {{< relref path="/blog/2020/01-intro-sustainable-it/03-thirst-for-data" >}}

[wiki-amazon-dash]: https://en.wikipedia.org/wiki/Amazon_Dash#Replenishment_service "Amazon Dash"
[wiki-5x]: https://fr.wikipedia.org/wiki/Nexus_5X "Nexus 5X"
[wiki-s7]: https://fr.wikipedia.org/wiki/Samsung_Galaxy_S7 "Galaxy S7"
[wikt-percoler]: https://fr.wiktionary.org/wiki/percoler#Verbe
[wiki-energy-star]: https://fr.wikipedia.org/wiki/Energy_Star
[greenit-firefox]: https://addons.mozilla.org/fr/firefox/addon/ecoindex/
[greenit-chrome]: https://chrome.google.com/webstore/detail/greenit-analysis/mofbfhffeklkbebfclfaiifefjflcpad
[doc-arraycopy]: https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/System.html#arraycopy(java.lang.Object,int,java.lang.Object,int,int)
[baeldung-native]: https://www.baeldung.com/java-native
[cpp-memmove]: https://en.cppreference.com/w/c/string/byte/memmove
[blackle]: https://blackle.com
[wcag]: https://www.w3.org/TR/WCAG20/
