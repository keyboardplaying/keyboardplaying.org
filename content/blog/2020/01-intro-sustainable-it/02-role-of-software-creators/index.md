---
date: 2020-01-20T23:54:30+01:00
title: The Role of Software Creators for a Sustainable IT
#subtitle: No funny subtitle found
slug: intro-sustainable-it/role-software-creators
aliases: [ intro-sustainable-digital/role-software-creators ]
description: |-
  We hear a lot about "sustainable development," actions for the planet and the people...
  But how can we contribute, as software creators?
cover:
  src: safar-safarov-MSN8TFhJ0is-unsplash.jpg
  alt: Potted green plants beside a laptop with a code editor on screen.
  by: Safar Safarov
  link: https://unsplash.com/photos/MSN8TFhJ0is
  authorLink: https://unsplash.com/@codestorm
  license: Unsplash
author: chop
categories: [ software, software-creation ]
tags: [ sustainable-it ]
keywords: [ sustainable, software creation, hardware, obsolescence, software, bloatware, support, ecodesign, best practice, tool, digital fat ]
series: [ intro-sustainable-it ]

references:
- id: ifixit
  name: iFixit
  url: https://www.ifixit.com/
  lang: en
- id: fairphone
  name: Fairphone
  url: https://www.fairphone.com/
  lang: en
- id: phonebloks
  name: Phonebloks
  url: https://phonebloks.com/
  lang: en
- id: computerhistory-apollo
  name: Apollo Guidance Computer read-only rope memory
  url: https://www.computerhistory.org/timeline/1969/
  lang: en
  author:
    name: Computer History Museum
- id: lifewire-emails
  name: Why Are Email Files so Large?
  title: true
  url: https://www.lifewire.com/what-is-the-average-size-of-an-email-message-1171208
  date: 2020
  lang: en
  author:
    name: Heinz Tschabitscher, Lifewire
- id: kendall-power-smartphone
  name: Would your mobile phone be powerful enough to get you to the moon?
  title: true
  url: https://theconversation.com/would-your-mobile-phone-be-powerful-enough-to-get-you-to-the-moon-115933
  date: 02/2019
  lang: en
  author:
    name: Graham Kendall, University of Nottingham
- id: cnn-slowing-iphones
  name: "Apple: Yes, we're slowing down older iPhones"
  title: true
  url: https://money.cnn.com/2017/12/21/technology/apple-slows-down-old-iphones/
  date: 12/2017
  lang: en
  author:
    name: Ivana Kottasová, CNN Business
- id: androidverge-s7-oreo
  name: "Android 8.1 Oreo for Galaxy S7 and S7 Edge : Release and Features"
  title: true
  url: https://androidverge.com/android-8-1-oreo-galaxy-s7-s7-edge-release-features/
  date: 09/2018
  lang: en
  author:
    name: AJ, Android Verge
- id: androidauthority-android8-s7
  name: "Android 8.0 Oreo update tracker"
  title: true
  url: https://www.androidauthority.com/android-8-0-update-784308/
  lang: en
  author:
    name: Mitja Rutnik, Android Authority
- id: visser-un-sdgs
  name: UN Sustainable Development Goals – Finalised Text & Diagrams
  title: true
  url: http://www.waynevisser.com/report/sdgs-finalised-text
  date: 08/2015
  lang: en
  author:
    name: Wayne Visser
- id: wiki-green-computing
  name: Green computing
  title: true
  url: https://en.wikipedia.org/wiki/Green_computing
  lang: en
  author:
    name: Wikipedia
- id: inr
  name: Institut du Numérique Responsable
  url: https://institutnr.org/
  lang: fr
- id: wiki-ecodesign
  name: Ecological design
  title: true
  url: https://en.wikipedia.org/wiki/Ecological_design
  lang: en
  author:
    name: Wikipedia
- id: greenspector
  name: Greenspector
  url: https://greenspector.com/
- id: cnumr-ecoconception-web
  name: Référentiel d'écoconception web
  title: true
  url: https://collectif.greenit.fr/ecoconception-web/
  lang: fr
  author:
    name: Collectif Conception Numérique Responsable
- id: ecometer
  name: Ecometer
  url: https://ecometer.org
  lang: en
- id: gtmetrix
  name: GTmetrix
  url: https://gtmetrix.com
  lang: en
- id: ecoindex
  name: Ecoindex
  url: https://ecoindex.fr
  lang: fr
- id: greenit-logomotion
  name: "éco-conception numérique : la parole aux experts > Logomotion"
  title: true
  url: https://www.greenit.fr/2016/06/05/eco-conception-numerique-la-parole-aux-experts-logomotion/
  date: 06/2016
  lang: fr
  author:
    name: Bertrand Laboureau, PDG de Logomotion
- id: fb-hiphop-php
  name: "HipHop for PHP: Move Fast"
  title: true
  url: https://www.facebook.com/notes/facebook-engineering/hiphop-for-php-move-fast/280583813919
  lang: en
  author:
    name: Haiping Zhao, Facebook Engineering
- id: wiki-hiphop-php
  name: HipHop for PHP
  title: true
  url: https://en.wikipedia.org/wiki/HipHop_for_PHP
  lang: en
  author:
    name: Wikipedia
- id: art-energy-efficiency
  name: Energy Efficiency across Programming Languages — How Do Energy, Time, and Memory Relate?
  title: true
  url: https://greenlab.di.uminho.pt/wp-content/uploads/2017/10/sleFinal.pdf
  date: 10/2017
  lang: en
  author:
    name: Rui Pereira, Marco Couto, Francisco Ribeiro, Rui Rua, Jácome Cunha, João Paulo Fernandes, João Saraiva
- id: art-docker
  name: How does Docker affect energy consumption? Evaluating workloads in and out of Docker containers
  title: true
  url: https://arxiv.org/abs/1705.01176
  date: 05/2017
  lang: en
  author:
    name: Eddie Antonio Santos, Carson McLean, Christopher Solinas, Abram Hindle (Cornell University)
- id: medium-dark-oled
  name: Designing a Dark Theme for OLED iPhones
  title: true
  url: https://medium.com/lookup-design/designing-a-dark-theme-for-oled-iphones-e13cdfea7ffe
  date: 05/2019
  lang: en
  author:
    name: Vidit Bhargava
- id: greenit-appli-ecoconcue
  name: Une appli éco-conçue pour favoriser les circuits-courts
  title: true
  url: https://www.greenit.fr/2020/01/14/une-appli-eco-concue-pour-favoriser-les-circuits-courts/
  date: 01/2020
  lang: fr
  author:
    name: Joël Mossand
- id: adobe-personas
  name: "Putting Personas to Work in UX Design: What They Are and Why They’re Important"
  title: true
  url: https://blog.adobe.com/en/2017/09/29/putting-personas-to-work-in-ux-design-what-they-are-and-why-theyre-important.html#gs.zfdpuh
  date: 09/2017
  lang: en
  author:
    name: Nick Babich, Adobe
- id: fb-2g-tuesdays
  name: "Building for emerging markets: The story behind 2G Tuesdays"
  title: true
  url: https://engineering.fb.com/2015/10/27/networking-traffic/building-for-emerging-markets-the-story-behind-2g-tuesdays/
  date: 10/2015
  lang: en
  author:
    name: Chris Marra, Facebook Engineering
- id: hal-methodology-greenit
  name: Software development methodology in a Green IT environmen
  title: true
  url: https://tel.archives-ouvertes.fr/tel-01724069/document
  date: 2017
  lang: en
  author:
    name: Hayri Acar, Université de Lyon
- id: ecoconception-note-synthese
  name: "Note de synthèse numérique : version abrégée"
  title: true
  url: https://www.eco-conception.fr/library/h/note-de-synthese-numerique-version-abregee.html
  date: 04/2019
  lang: fr
  author:
    name: Pôle Éco-conception
---

Last week, [I posted about the environmental footprint of digital technology][footprint-of-digital].
Most of it may have seemed pretty hardware considerations---and several companies or associations fight for more modular, repairable and globally speaking sustainable electronics.

Now, you're willing to help, but you can't see how you can make a difference because work on _software_ exclusively and have no say about the hardware that it'll run on?
This post will give you some hints about how to include these considerations into the design of your application.

<!--more-->

## Initiatives for Hardware

<aside><p>The purposes are to build less, in a smarter way and keep it longer.</p></aside>

I won't dig deep into this topic, since this post is supposed to be about software.
If you don't know of them already, I invite you to have a look at [iFixit][ifixit]'s, [Fairphone][fairphone]'s and [Phonebloks][phonebloks]'s initiatives---among others.

Three great ideas can be retained:
- Repairing is much better for the planet than replacing is.
- Replacing a part/component/module of your device is better than replacing the whole device.
- Selecting [sustainable sources of coltan][coltan-blood-mineral] is hard, but Fairphone did it.

To sum up the hardware philosophy, **the main idea is to produce less equipment from ethically selected resources, and keep this equipment longer**.



## Some Software Causes

Yes, I'm talking hardware to readers I expect to be software creators.
As [I explained last week][code-impact], software and hardware are linked.
Your code will need devices to run.

And depending of how your code was designed, it may play its part in accelerating the turnover of digital devices.
Below are the two most prominent software-related causes.


### The Bloatware Phenomenon {#bloatware}

<aside><p>Today's average email weighs more than Apollo 11's guidance computer's ROM.</p></aside>

In 1969, the Apollo 11 mission landed on the Moon.
Onboard, [the Apollo Guidance Computer (AGC) had the equivalent of 72 kB read-only memory][computerhistory-apollo].
Today, [an average email weighs about 75 kB][lifewire-emails].
[Graham Kendall estimated that][kendall-power-smartphone] a TI-84 was almost 350 times faster than the AGC, and that the latest iPhone as over {{< numfmt 100000 >}} times the processing power of that computer.
And we complain we don't have enough space to run our apps and store our pictures on our 64 GB smartphones!

Let's go back a few years and go in a bank.
Most programs are written in Cobol and run on a z/OS machine.
IBM bills you for running those machines.
The more you run, the more you pay, so you have to write efficient programs.
Plus, since you're a bank, you perform nightly batch jobs that do all accounting while off hours.
You know a Cobol job always end, but you have to be efficient: everything must be done before the next work day begins, and you have to remember your job was only a little gear in a much, much bigger machine.

Let's flash forward a few years later.
New languages have appeared.
New operating systems, too: Unix, and soon Linux.
"Resources are cheap," and they get cheaper all the time.
We as developers feel comfortable, less threatened with infrastructure and non-functional requirements.
We just need to get the job done, and if the machine is not powerful enough, we ask for more power.
One of my mentors once told ops something along the lines of, "Come on, I'll buy you that RAM and you can install it in the servers."

Sometimes, that approach _is_ justified and we _need_ those resources.
The thing is, too often, we don't even ask ourselves if we could do better.
That's when we get to **bloatware**.

{{< figure src="commitstrip-scumbag-chrome.en.jpg" alt="Google asks for more memory for him and other software, but when the developer gives it some, it eats all and blames it on him to the editors." link="https://www.commitstrip.com/en/2015/09/28/scumbag-chrome/" attr="CommitStrip" attrlink="https://www.commitstrip.com" >}}

Consider this: when I was a student, I had a friend whose laptop could not run Firefox and Windows Live Messenger at the same time, because both were memory hogs---I mean, that's a web browser and an instant messenger, and since we were student engineers, keeping in touch with the world and searching for information were among the most menial of tasks for our computers...

Another example: a few years back, I had to buy my mother a new computer because the 20 Gb hard disk drive was full with Windows XP updates that, despite my best will, I couldn't clean.

That's the thing about bloatware: **users are forced to replace their devices, though those still work, only because the software was not designed to be resources-friendly**.
And that's of course not taking into account cases where [the editor voluntarily slows down its older devices][cnn-slowing-iphones].


### The Forsaken Support {#end-of-support}

In 2015, [Amazon introduced the Dash Button][wiki-amazon-dash].
It allowed you to reorder something you needed on a regular basis---and was reprogrammed for many other uses.
In 2019, however, Amazon discontinued the series because subscriptions ensured automatic replenishment and Alexa's voice-activated shopping would be the successor.

<aside><p>You have to keep buying new devices to benefit from the latest features.</p></aside>

Let's choose another example, one that will speak to more of you: smartphones.
I bought mine, a [Nexus 5X][wiki-5x], in 2016, just after it was discontinued.
The latest official OS update I got was Android 8.1.0 December 2018.
**That's already one year without a security patch, for a phone that's only three years old**.
It's working fine! It's just not as secure as it could be.
And that's a Google-backed Nexus phone, with a "Vanilla" Android!

Now, what about a phone with a custom launcher, which implies the constructor has to rebuild and distribute the modified version of Android themselves?
Let's have a look at the [Samsung Galaxy S7][wiki-s7]: it was released one year after the Nexus 5X---that is, six months before the latter was discontinued.
Yet, [it never got Android 8.1.0 from official channels][androidverge-s7-oreo] and remained stuck behind, with Android 8.0.
In fact, **[Samsung's last update to the S7 family][androidauthority-android8-s7] was pushed in July 2018, six months before the last update my one-year-older phone got**.
If the users wished to benefit from the features introduced in the latest version of the OS, they have to buy a new device.

Let's be clear: **end of support is a plague in the digital world!**
Of course, it's not economically viable to care about all the versions of the OS, but changing smartphone every two years is not something everybody can afford.
Actually, most people _can't_ afford it.

And **people are the pillar we often fail to consider**, though it is fundamental.



## The Introduction of Sustainable IT


### From Preoccupations to Sustainable Development {#3p-sustainable-development}

Being responsible is being aware of your negative impacts and taking accountability for it.
If you do that, you'll also want to reduce those impacts.

<aside><p>To minimize our negative impacts, we must make our development sustainable.</p></aside>

So far, we saw that digital technology had negative consequences [on the environment][environmental-footprint], [on the society][social-footprint], and we're beginning to see economic interests and drivers [percolate][wiki-percolate] through it all.

I don't know about you, but my school was repetitive about a certain Venn diagram, si much that when I see together at least two of economy, society and environment, I always think of it: **Planet, People and Prosperity are the 3 P's of sustainable development**.[^fn-5ps]

{{< inline-svg src="venn-sustainable-diagram.svg"
    title="The Venn diagram that defined sustainable diagram when I was in engineering school" >}}

[^fn-5ps]: Actually, while researching for this post, I discovered that, [in 2015, the United Nations proposed two more **p**illars][visser-un-sdgs]: **Peace** and **Partnership**.
While we don't seem to have much impact on the first---even though this can be discussed, especially about [coltan][coltan-blood-mineral]---working with peers can help us in the context of [ecological design](#ecodesign).

So, basically, to minimize our impact on the environment, **we must make our development sustainable** (pun intended).

This is _not_ a new concept.
The [Energy Star][wiki-energy-star] program was launched in 1992 and is, according to [Wikipedia][wiki-green-computing], the beginning of green computing.

> **Green computing**, **green ICT** as per International Federation of Global & Green ICT "IFGICT", **green IT**, or **ICT sustainability**, is the study and practice of environmentally sustainable computing or IT.
> {{< ref-cite "wiki-green-computing" >}}

The one aspect of this definition that I don't like---and I know I'm not alone---is that it solely focuses on the ecological aspect.
In France, the [_Institut du Numérique Responsable_][inr]---which translates to "Institute of Sustainable IT"---was born.
I've not seen this concept in English yet, so I propose **Sustainable IT**.[^fn-sustainabledigitalcom]

[^fn-sustainabledigitalcom]: Not to be confused with [Sustainable Digital.com](https://www.sustainabledigital.com/), though the [founder's purpose](https://www.sustainabledigital.com/about) seems interesting.

This is all nice and great, but how, you may wonder, do we translate that pretty, marketing-ready name into something that isn't just green washing bullshit?
Well, then again, let's start with something that's been around for years.


### The Basics of Ecodesign {#ecodesign}

"Ecological design" is a term that was coined in 1996, and its definition is quite straightforward.

> **Ecological design** or **ecodesign** [...] was defined [...] as "any form of design that minimizes environmentally destructive impacts by integrating itself with living processes."
> {{< ref-cite "wiki-ecodesign" >}}

It was first essentially applied to physical elements, but this approach is now declined to digital services, too.

There's enough to say about (digital) ecodesign to write a whole other blog post about it, but I'll keep it simple today and let it simmer: basically, it boils down to knowing:
- what your users need---and what they don't---and keep your solution simple;
- that the choices you make when implementing your solution will have impacts later: weigh the pros and cons, and be conscious about the consequences.

I propose to follow with some concrete examples of solutions, to give you a better idea of what this covers.



## Examples of Technical Solutions


### Trim Your Code


#### Make It Efficient

<aside><p>Making your code resource-efficient will make it more performant <em>and</em> energy-efficient.</p></aside>

[As I said earlier](#bloatware), we were once more concerned with writing efficient code than most of us are today.
Yet, this is the first lever we have---not the most powerful and not the only one---to make sure our code is energy-efficient: **the more computations, memory or bandwidth it needs, the more energy it consumes**.
That's actually how most measu-guessing software works: measure how much memory/CPU/network a program uses and estimate how much energy this represents.

Concrete (old school) Java example:

```java
for (int i = 0; i < collection.size(); i++) {
    // Do something with collection and i
}
```

For the sake of example, we'll forget about the `foreach` syntax here.
Two things could be better on that line:
- `++i` is more efficient than `i++`, as it doesn't require to store the initial value in a temporary variable.
- If you know the size of the collection won't change during the loop, why do you make the JVM calculate it on each iteration? Better to store it once and for all.

With just a bit of optimization, the previous lines become:

```java
final int size = collection.size();
for (int i = 0; i < size; ++i) {
    // Do something with collection and i
}
```

Seems ridiculous?
Might be, but [Greenspector][greenspector] estimates this allows a gain of 5% in electric consumption for this line.

To speak of a language I won't write about often, Greenspector also indicated that **using PHP's `foreach` instead of `for` saves you 30% energy with the same performances**[^fn-greenspector-lg-versions].

[^fn-greenspector-lg-versions]: Greenspector shared this data with me in 2016.
The only problem I have with it is that they didn't indicate which version of the language was evaluated.
We know languages always work on improving their performances, and PHP and Java especially made great efforts in that area.


#### Tools and Best Practices

So, what are the best practices to optimize your code?
Well, you can begin with all the best practices you (should) already know, those which reduce the unnecessary use of your resources.

For web development, you can have a nice checklist of 115 recommendations [here][cnumr-ecoconception-web]---though I'm sad to see there's nothing about single-page applications.
Tools like [ecometer.org][ecometer] and [GTmetrix][gtmetrix] can help you see which best practices you implemented and which ones you should consider.
[Ecoindex][ecoindex] (in French) also gives your page an environmental note, for indicative purpose only.
This is even available as plugins [in Firefox][greenit-firefox] and [Chrome][greenit-chrome].

{{< figure src="gtmetrix-kp.org.png" caption="GTmetrix's evaluation of [last week's post](/blog/2020/01-intro-sustainable-it/footprint-digital/)" >}}

And yes, there are concrete gains, even for the final users.
In 2015, [Logomotion][greenit-logomotion] developed two identical websites, with the same content and interface, but following two different approaches:
- The first one used WordPress 4.4.2, without any special optimization.
- The second one was state-of-the-art and used a static generator---such as Jekyll or Hugo, for instance.

Seventy best practices were used in the second case versus 39 in the first.
The DOM contained 25% fewer elements and required 39% fewer queries.
**The mean loading time was almost halved and the average page weight was divided by more than two**.


### Mix Technologies to Your Advantage

This is often difficult to grasp for a developer.
We tend to be aficionados of the technologies we use and don't like to consider using anything else.
Much less mixing with something else.
Stop talking blasphemy!

And yet, this is sometimes the best way to achieve an unexpected gain.
For instance, do you know the [`System.arrayCopy`][doc-arraycopy] in Java?
It is well named and does just what it suggests.
If you wish to perform such an action in Java, you'll have to copy each element separately, resulting in _n_ operations.
That's why this method exists and **that's why it's [native][baeldung-native], because it can be done with a single `memcpy` / [`memmove`][cpp-memmove] in C++**.

A larger example comes from a small company.
Once upon a time, Marked created a social network for his university.
A few years later, it became a worldwide phenomenon, called Facebook.

When Mark began, PHP was a great solution.
But the application and source code grew, and so did the number of users.
Facebook was reaching the limits of PHP, but rewriting the whole codebase would have been impossible.

One night at a Hackathon, [an employee started writing a program to transform PHP into C++][fb-hiphop-php].
This was the beginning of [HipHop for PHP][wiki-hiphop-php], a PHP-to-C++ transpiler: developers wrote PHP but, in the end, it was C++ code that was compiled to run Facebook.
Compared to the Zend framework that was previously used, **page generation was up to six times faster to generate, using fewer resources**!


### It's All About Knowing

In the end, to make an enlightened choice, **you have to _know_ what it implies**.
This post is getting long already and there's still another part to cover, so I'll be quick.

To ecodesign your solution, you need to be aware that [languages do not perform identically in terms of time, memory and energy][art-energy-efficiency].
You need to understand that [Docker induces an energy and performance overhead][art-docker], and that's a sacrifice you're acknowledging for the sake of easier maintenance.
You need to be clear that [a dark theme is not only a developer's fantasy, it's also a battery saver for OLED smartphones][medium-dark-oled][^fn-only-oled].

[^fn-only-oled]: This is however specific to **O**LED screens, due to the difference of technology from LCD screens. [Blackle][blackle] is a myth. Maybe I should write about that.

And it's important to _know_, not guess.
That means you'll need to _measure_.
Use JMeter, Gatling, benchmark.js, YSlow...
Any performance measure is usually a good, if incomplete, indicator.
Integrate measures in your CI if you can, so you can detect bottlenecks and regressions early.


### Don't Be Overkill

I have a flaw---more than one, actually, but let's focus---I like things to be perfect.
I love to do state-of-the-art solutions, to make something entirely generic so that it can be reused...

Guess what?
If your purpose is to kill a fly, don't go getting a white elephant; just fetch a swat.
Do things in accordance with your context and **remain sober**: everything you add is an overhead somewhere.
If it's unnecessary, it's just a useless overhead.



## Beyond Pure Technique

### Remembering It's All About the User

<aside><p>UX building helps you focus on what's really important for the user—and therefore for your application.</p></aside>

I said [earlier](#3p-sustainable-development) that sustainable development relies on 3 P's: planet, **people** and prosperity.
It's time to remember to put the people at the center: what we do is for them.
**One fundamental approach to ecodesign is design thinking and user experience designing**.

If you design your application from the point of view of your users---say, using [personas][adobe-personas]---instead of just writing/reading functional specifications, you think differently and gain a different perspective.

First, you can guess what's really important and what's not.
Too often, people who write the specs think, "This is important (to me)."
Yet, the "to me" part, rarely vocalized, is a bias, and biases are choices that can differ from the user's expectations.
Maybe "this" will never be used by any user.
If that's the case, it's just fat.

Think of Microsoft Word, for instance.
I think I'm a fairly advanced user, and I suppose I know about half of its features.
Who uses the rest? Are those ultra-specific cases? If they are, do they belong in a word processing software?

Another win from this perspective is that it can help make things concrete.
What's about this line, "The application should respect all accessibility standards?"
Ok, I'll apply a set of rules and best practices from the [WCAG][wcag] and be done with it.
Now, I have my personas.
This one is blind, but she has to manage her money like anybody else.
How would I do it in her stead?
Can I use a screen reader?
Is the application compatible?
**Shifting the way to present the problem also shifts the paradigm we use to solve it**, and the willingness to do it.
In the first approach, it's a chose and a set of rules; in the second, it can become a game or a challenge.
Software creators love to be challenged.



### Staying Lean

Beyond UX, other practices can help you avoid adding fat to your application.

**Mobile-first strategy** is a good example.
Deutsche Bahn, a German railway company, developed two versions of its train time searching tool: it weighs 3 kB for the smartphone version versus over 2000 kB for the desktop, for the same result.

Remember that **not every user will be equipped with state-of-the-art machines** either.
[Powerful machines are great for software creators]({{< relref path="/blog/2019/11-11-cost-of-time" >}}), but they're usually expensive for the layman.
When Google was preparing Android KitKat for its flagship-to-come, the Nexus 5, they made the developers test it on the past flagship, the Nexus 4.

Regional equipment is also part of the equation.
In 2015, [Facebook launched its 2G Tuesdays][fb-2g-tuesdays].
The purpose was to simulate a slow Internet access so that developers could see how Facebook behaves for emerging countries---and therefore adapt it so that it works properly for those users, too.

Last advice about lean for today: **don't be overkill**.
A few years ago, Bing saw that users rarely look at the second page of results.
Yet, loading all those thousands of results was resource-demanding.
So they reduced it: only the first twenty results are returned for a query; if the user needs more, then we'll search for it.
It may be a performance degradation for, like 5% of users, but it's invisible for most.
And it saved 80% energy for the servers.[^fn-bing]

[^fn-bing]: I'm terribly frustrated not to have a source for this fact.
Though I trust the people who shared it with me (the founder of Greenspector, actually), I'm hungry for details on how that worked before and after the change.
A whole post detailing this wouldn't be too much, I guess.



## Words of Parting

This post was written to give you an insight about how ecodesign can be achieved for digital solutions.
[Much reading](#references) can be found on this topic.
It's vast and growing, and we barely scratched the surface.
I think two ideas ought to be remembered: **know the consequences of your choices** and **cut the fat**.
Which are general good recommendations for programming, actually.

And that's how I see it: **ecodesign is just another level of code quality**, making sure our code is not only maintenable, performant, but also **sustainable**.

The first purpose of this objective, indirect as it may be, is to **produce less equipment and be able to keep them longer**.
The second purpose is to **reduce the energy requirements of digital devices**.
This latter one can be achieved for data centers by reducing the amount of stored data---but that's [next week's post][data].

One last bit of advice and its anecdote, though: remember your application will be used by humans.
Humans do mistakes.
Once, a Drupal CMS was implemented for a client, optimized with the boost module to generate static versions of each page---no need to compute the page on consultation, it's already there, ready to be served.
After a few weeks, the client called, saying the website was too slow.
Upon examination, the provider discovered the user put ultra-high resolution images, which was not how the CMS had been intended.
Education will always play a role in our jobs.



{{% references %}}

[footprint-of-digital]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}
[environmental-footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}#empreinte-environnementale
[code-impact]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital#impact-code" >}}
[social-footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}#empreinte-societale
[coltan-blood-mineral]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}#coltan-mineral-conflits
[data]: {{< relref path="/blog/2020/01-intro-sustainable-it/03-thirst-for-data" >}}

[wiki-amazon-dash]: https://en.wikipedia.org/wiki/Amazon_Dash#Replenishment_service "Amazon Dash"
[wiki-5x]: https://en.wikipedia.org/wiki/Nexus_5X "Nexus 5X"
[wiki-s7]: https://en.wikipedia.org/wiki/Samsung_Galaxy_S7 "Galaxy S7"
[wiki-percolate]: https://en.wiktionary.org/wiki/percolate#Verb
[wiki-energy-star]: https://en.wikipedia.org/wiki/Energy_Star
[greenit-firefox]: https://addons.mozilla.org/fr/firefox/addon/ecoindex/
[greenit-chrome]: https://chrome.google.com/webstore/detail/greenit-analysis/mofbfhffeklkbebfclfaiifefjflcpad
[doc-arraycopy]: https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/System.html#arraycopy(java.lang.Object,int,java.lang.Object,int,int)
[baeldung-native]: https://www.baeldung.com/java-native
[cpp-memmove]: https://en.cppreference.com/w/c/string/byte/memmove
[blackle]: https://blackle.com
[wcag]: https://www.w3.org/TR/WCAG20/
