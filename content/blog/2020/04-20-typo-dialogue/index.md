---
date: 2020-04-20T08:00:51+02:00
title: Formatting Dialogue
subtitle: When you're inside quotation marks, someone's speaking
slug: formatting-dialogue
description: |-
  Formatting a dialogue is quite straightforward, but here is a reminder in case you need one.
author: chop
categories: [ writing ]
tags: [ typography ]
keywords: [ typography, writing, dialogue, monologue, typographic conventions, formatting, quotation marks ]

cover:
  banner: false
  src: mel-poole-lBsvzgYnzPU-unsplash.jpg
  alt: Close-up photography of a dialogue in a book
  by: Mel Poole
  authorLink: https://unsplash.com/@melipoole
  link: https://unsplash.com/photos/lBsvzgYnzPU

references:
- id: formatting-dialogue
  name: Formatting Dialogue
  title: true
  url: https://www.dailywritingtips.com/formatting-dialogue/
  lang: en
  author:
    name: Maeve Maddox, Daily Writing Tips
---

Following the usual rules when writing a dialogue in a piece of fiction makes it so much easier to read.
Fortunately, those rules are quite straightforward.
Much more so than [their French equivalent]({{< ref path="/blog/2020/04-20-typo-dialogue" lang="fr" >}}).

<!--more-->

Now, I'd like something to be clear: I focus on formatting here, which helps making a dialogue clear and frees some brain load for your readers.
There's much more to writing good dialogues, though, but you'll find better advice than mine on that topic.


## The rules

- When someone starts speaking, use an opening quotation mark (`“`).
- When someone stops speaking, use a closing quotation mark (`”`).
- When a new speaker begins, start a new paragraph.



## The quotation marks

That's the basic part: **the quotation marks delimit the portions that are spoken**.
You open them when someone starts talking, you close them when you're coming back to the narration.

Sometimes, you may have punctuation that doesn't belong in a quotation.
In any case, **the punctuation should go within the quotation marks**.



## Paragraphs

**Each speaker gets his or her own paragraph**.
It makes it much easier for the reader to keep track.

In [a post][formatting-dialogue] on the topic, Maeve Maddox chose this example:

> The phone rang, and they heard Clare answer in the kitchen.
> After a few minutes, Clare came back in.
> She was smiling.
> "Was that Paul?" Sarah asked.\
> "Yeah," Clare said.
> "He’s in the park, tracking an owl."
> "He called to tell you that"\
> Clare nodded, her smile growing.
> "Grit, I think you’ve brought us luck."
> "I doubt that," Grit said, before she could stop herself.
> <cite>Luanne Rice, <em>Luanne Rice</em></cite>

Now, let's see how it should be formatted.

> The phone rang, and they heard Clare answer in the kitchen.
> After a few minutes, Clare came back in.
> She was smiling.
>
> "Was that Paul?" Sarah asked.
>
> "Yeah," Clare said.
> "He’s in the park, tracking an owl."
>
> "He called to tell you that?"
>
> Clare nodded, her smile growing.
> "Grit, I think you’ve brought us luck."
>
> "I doubt that," Grit said, before she could stop herself.

Isn't that clearer?



## Speech Tags {#speech-tags}

As you've seen, when there are more than two speakers, adding speech tags helps keeping track too.

**If the speech should end with a period, replace it with a comma**.
Otherwise, punctuate as normal.

> "Was that Paul?" Sarah asked.
>
> "Yeah," Clare said.

**If the speech tag comes first, always separate with a comma**.

> Marci said, "Please, don't leave."

The speech tag doesn't begin with a capital letter, even when you might think it does.

> "What would you drink?" the pretty waitress asked.

In order not to add "he/she said" on each line, you can also mix it up a bit with narration.
All you need is to make clear who's speaking.

> Clare nodded, her smile growing.
> "Grit, I think you’ve brought us luck."


## Monologues

Some characters talk _a lot_.
That's the case of Sherlock Holmes almost every time he explains Watson how he cracked a case.

But you can't write it as a single, illegible block of text.
Just like narration, you need to be able to create paragraphs.

So, when you have a talkative character, just create new paragraphs.
Don't close the quotation marks---your character isn't done speaking---but **add an opening quotation mark in front of each new paragraph this character speaks**.

> "On entering the house this last inference was confirmed.
> My well-booted man lay before me.
> The tall one, then, had done the murder, if murder there was.
> There was no wound upon the dead man's person, but the agitated expression upon his face assured me that he had foreseen his fate before it came upon him.
> Men who die from heart disease, or any sudden natural cause, never by any chance exhibit agitation upon their features.
> Having sniffed the dead man's lips I detected a slightly sour smell, and I came to the conclusion that he had had poison forced upon him.
> Again, I argued that it had been forced upon him from the hatred and fear expressed upon his face.
> By the method of exclusion, I had arrived at this result, for no other hypothesis would meet the facts.
> Do not imagine that it was a very unheard of idea.
> The forcible administration of poison is by no means a new thing in criminal annals.
> The cases of Dolsky in Odessa, and of Leturier in Montpellier, will occur at once to any toxicologist. 
>
> "And now came the great question as to the reason why.
> Robbery had not been the object of the murder, for nothing was taken.
> Was it politics, then, or was it a woman?
> [...]"
>
> <cite>Sir Arthur Conan Doyle, <em>A Study in Scarlet</em></cite>


## Words of Parting

That's it!
Now get to your pen and write us a great story!


{{% references %}}
