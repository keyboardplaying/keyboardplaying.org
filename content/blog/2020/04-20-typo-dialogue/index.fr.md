---
date: 2020-04-20T08:00:51+02:00
title: Règles typographiques du dialogue
subtitle: Un dialogue commence par un guillemet ouvrant et se termine par un guillemet fermant
slug: regles-typographiques-dialogue
description: |-
  Il existe deux conventions typographiques d'écriture du dialogue en français.
  Voici un rapide rappel des règles à connaître.
author: chop
categories: [ writing ]
tags: [ typography ]
keywords: [ typographie, conventions typographiques, écriture, dialogue, monologue, guillemets, tiret, incise, raccourci clavier ]

cover:
  banner: false
  src: mel-poole-lBsvzgYnzPU-unsplash.jpg
  alt: Plan rapproché d'un dialogue dans un livre
  by: Mel Poole
  authorLink: https://unsplash.com/@melipoole
  link: https://unsplash.com/photos/lBsvzgYnzPU

references:
- id: relges-typo-dialogue
  name: Règles typographiques du dialogue
  title: true
  url: https://www.aproposdecriture.com/regles-typographiques-du-dialogue
  date: 02/2013
  lang: fr
  author:
    name: Marie-Adrienne Carrara, À propos d'écriture
- id: messien-ecrire-dialogues
  name: "Typographie : écrire des dialogues"
  title: true
  url: https://lesoufflenumerique.com/2012/10/23/typographie-ecrire-des-dialogues/
  date: 10/2012
  lang: fr
  author:
    name: Pierrick Messien
---

Je suis de la vieille école.
Dans l'enseignement primaire, on m'a appris certaines règles sur la façon d'écrire un dialogue.
Pourtant, quand je prends un livre, je constate que les maisons d'édition ont tendance à appliquer d'autres règles.
Il devient difficile de s'y retrouver, je me suis donc dit qu'un petit récapitulatif pourrait aider.

<!--more-->

Attention, on ne s'attardera bien ici que sur la forme.
Pour des conseils sur comment manier les incises efficacement, éviter d'alourdir votre dialogue ou autre, je vous invite à chercher des conseils ailleurs.
J'ai notamment apprécié ces [trois règles de base du dialogue][regles-base-dialogue].


## Résumé des règles

- On ouvre le dialogue avec des **guillemets ouvrants** (`«`).
- On le ferme par des **guillemets fermants** (`»`).
- Chaque changement d'interlocuteur est signalé par un **tiret** (`—` ; ne concerne pas la première réplique).
Le tiret est suivi d'une espace (idéalement, une espace insécable).

Le _Lexique des règles typographiques en usage à l'Imprimerie Nationale_ donne cet exemple :

> « Ma vue s'affaiblit, dit Irène.
>
> — Prenez des lunettes, dit Esculape
>
> — Je m'affaiblis moi-même, continue-t-elle, et je ne suis ni si forte ni si saine que j'ai été.
>
> — C'est, dit le dieu, que vous vieillissez.
>
> — Mais quel moyen de guérir de cette langueur ?
>
> — Le plus court, Irène, c'est de mourir comme ont fait votre mère et votre aïeule. »
> <cite>La Bruyère, <em>Caractères</em>


## Résumé : variante sans guillemets

C'est une tendance qui semble avoir été popularisée par les maisons d'édition.[^fn-origin-dashes]

[^fn-origin-dashes]: Dans mon inculture, je pensais que cette habitude avait été importée du monde anglo-saxon, mais non, [eux font très attention à toujours les utiliser]({{< ref path="/blog/2020/04-20-typo-dialogue" lang="en" >}}).

- Chaque prise de parole d'un interlocuteur (première réplique, changement) est précédée d'un **tiret** : `—`

C'est tout ?
Eh bien oui.
Enfin pas tout à fait : le tiret est suivi d'une espace insécable.

Un petit exemple ci-dessous.

> Vers sept heures du soir, tous les invités étaient arrivés, sous la conduite de Fred et de George qui les avaient attendus au bout de la route.
> Pour l'occasion Hagrid portait son plus beau – et horrible – costume marron et pelucheux.
> Malgré le grand sourire que Lupin lui adressa en lui serrant la main, Harry trouvait qu'il avait l'air malheureux.
> C'était curieux, car Tonks, qui se tenait à côté de lui, paraissait tout simplement radieuse.
> 
> — Joyeux anniversaire, Harry, dit-elle, et elle le serra dans ses bras.
> 
> — Alors, ça y est, tu as dix-sept ans ? lança Hagrid en prenant le verre de vin de la taille d'un seau que lui tendait Fred.
> Ça fait six ans qu'on s'est vu pour la première fois, Harry, tu te souviens?
> 
> — Vaguement, répondit celui-ci avec un sourire.
> C'était le jour où vous avez défoncé la porte, où vous avez fait pousser une queue de cochon à Dudley et où vous m'avez annoncé que j'étais sorcier ?
> 
> — J'ai oublié les détails, gloussa Hagrid.
> Ça va, Ron, Hermione ?
> 
> — Très bien, assura Hermione.
> Et vous ?
> 
> — Oh, pas mal.
> Beaucoup de travail, on a eu des bébés licornes, je vous les montrerai quand vous viendrez.
> 
> Harry évita le regard de Ron et d'Hermione pendant que Hagrid fouillait dans sa poche.
> 
> — Tiens, Harry, reprit Hagrid.
> Savais pas quoi t'offrir, mais je me suis rappelé que j'avais ça.
> 
> Il sortit une petite bourse à l'aspect légèrement duveteux, dotée d'un long cordon destiné de toute évidence à être passé autour du cou.
> 
> — De la peau de Moke.
> On peut cacher ce qu'on veut, là-dedans, et seul son propriétaire peut récupérer ce qu'il y a mis.
> Rares, ces trucs-là.
> 
> — Merci, Hagrid !
> <cite>J. K. Rowling, <em>Harry Potter et les Reliques de la Mort</em></cite>


## Usage des guillemets

On qualifie de « guillemets français » ces deux symboles : `«` et `»`.
Ceci s'oppose aux guillemets anglais `“` et `”`.
Si vous avez lu [mon billet sur les espaces en français]({{< relref path="/blog/2020/04-13-intro-espace" >}}), vous avez dû constater deux choses :
- le guillemet ouvrant est précédé d'une espace et suivi d'une espace insécable (c'est l'inverse pour le guillemet fermant) ;
- je ne traite pas des guillemets anglais qui ne suivent pas les mêmes règles et sont à éviter dans un écrit français.

Les guillemets encadrent le dialogue.
Pour chaque échange verbal, il ne doit y avoir qu'une seule paire de guillemets.

> « Docteur Watson, M. Sherlock Holmes, dit Stamford en nous présentant l'un à l'autre.
>
> --- Comment allez-vous ? » dit-il cordialement.
> <cite>Arthur Conan Doyle, <em>Une étude en rouge</em></cite>

Nous reviendrons [plus tard](#incises) sur les incises, mais notez que la première, qualifiant l'action de Stamford, n'interrompt pas le dialogue et ne nécessite pas de fermer les guillemets.


## Usage des tirets

Le tiret du dialogue est **toujours placé en début de ligne et suivi d'une espace**.
Dans l'idéal, on utilisera le **tiret cadratin**.

Oui, il existe des tirets de plusieurs longueurs : le tiret court (quart de cadratin, trait d'union ou « tiret du 6 »), le moyen (demi-cadratin) ou long (cadratin).
C'est ce dernier qui est à privilégier pour les dialogues.

Les exemples précédents devraient vous suffire.


## Les incises {#incises}

Les incises sont des précisions narratives qui s'insèrent dans le dialogue.
On observe généralement une inversion du sujet.
C'est le fameux « dit-il » et toutes ses variantes.

Notez qu'une incise ne commence jamais par une majuscule, quoiqu'en pense votre correcteur orthographique.
Ainsi, la question « Comment vas-tu ? » devient « Comment vas-tu ? demanda-t-elle. »

Si votre réplique se termine par un point, celui-ci sera remplacé par une virgule.
La réponse « Je vais bien. » de notre personnage se transcrirait donc : « Je vais bien, répondit-il sobrement. ».

L'incise ne nécessite pas de fermer les guillemets, mais si jamais elle conclut le dialogue, elle s'écrit après le guillemet fermant.

> « Comment vas-tu ? demanda-t-elle.
>
> --- Je vais bien, » répondit-il sobrement.


## Répliques longues et monologues

Certains personnages aiment parler.
C'est par exemple le cas de Sherlock Holmes qui devient toujours loquace lorsqu'il explique à Watson comment il a résolu une enquête.
Si l'on ne fait pas de coupure, on peut se retrouver avec un indigeste bloc de texte, d'où l'intérêt de faire des paragraphes.

Pour signaler que ces nouveaux paragraphes font toujours partie du dialogue, on les fait **précéder d'un guillemet ouvrant** (`«`)^[^fn-old-monologues].
Il est sous-entendu que l'on n'ajoute pas de guillemet fermant avant la fin du dialogue.

[^fn-old-monologues]: Le guillemet fermant (`»`) était traditionnellement utilisé comme préfixe dans ce cas, ce qui pourrait aujourd'hui nous laisser perplexe.

> « Cette dernière déduction se confirma quand j'entrai dans la maison.
> L'homme coquettement chaussé gisait devant moi.
> Par conséquent, c'était l'autre, je veux dire le grand, qui avait commis le meurtre, si meurtre il y avait.
> Le cadavre ne présentait aucun signe de blessure ; en revanche, son expression tourmentée laissait croire qu'il avait vu la mort s'approcher : celle d'un homme emporté par une crise cardiaque ou par toute autre cause naturelle ne traduit jamais une semblable agitation.
> Je flairai les lèvres.
> Il s'en exhalait une odeur aigrelette ; j'en inférai qu'il avait été empoisonné de force.
> Qu'il l'eût été de force se devinait d'après son visage à la fois haineux et terrifié.
> C'est par la méthode d'exclusion que j'étais arrivé à ce résultat ; en effet, aucune autre hypothèse ne s'ajustait aux faits.
> D'ailleurs, ne vous imaginez pas que l'idée de faire prendre du poison de force soit bien nouvelle : elle se retrouve dans les annales du crime.
> Tout toxicologue se rappellera les cas de Dolsky, à Odessa, et de Leturier, à Montpellier.
> 
> « Quel était le motif ? Voilà le hic ! Ce ne pouvait pas être le vol : on n'avait rien pris.
> [...] »
> <cite>Arthur Conan Doyle, <em>Une étude en rouge</em></cite>

Cette convention semble s'appliquer également dans le cas d'un dialogue sans guillemets.

> — Oh non, Gérard, lança Michel en grimaçant, je sens que tu vas encore nous faire une de ces longues tirades dont tu as le secret  !
>
> — Moi, faire de longues tirades ? s'offusqua Gérard, vexé.
> Tu plaisantes ?
> Jamais, ô grand jamais on ne m'a accusé avec une telle fourberie.
> Incision et rapidité de propos sont les qualités qu'on m'attribue régulièrement.
> Les longues tirades sont pour les pleutres, pour les politiciens, pour les menteurs !
> Comment toi, ami de longue date, peux-tu ne serait-ce qu'oser insinuer que mes répliques sont longues ?
>
> « À la vérité, mon cher ami Michel, je pense que tu es jaloux.
> Jaloux de ma prestance, jaloux de mon charisme.
> Ô, toi, qui aimes parler pour ne rien dire, tu as du mal à concevoir que je sois à ce point capable de rendre mes phrases concises et claires !
> Mais je ne t'en veux pas Michel, car je reste magnanime en toute circonstance.
> À ce propos : de quoi parlait-on, déjà ?
>
> — Gérard, tu es un incorrigible fripon ! s'amusa Michel, qui ne pouvait décidément pas en vouloir à son ami.
>
> {{< ref-cite "messien-ecrire-dialogue" >}}
> <cite><a href="https://lesoufflenumerique.com/2012/10/23/typographie-ecrire-des-dialogues/">Pierrick Messien</a></cite>


## Quelques raccourcis clavier utiles

Les claviers d'ordinateur ne sont malheureusement pas adaptés aux conventions typographiques, encore moins à celles de France qui utilisent quelques exceptions.
Certains logiciels savent faire les adaptations qu'il faut, mais ce n'est pas le cas de tous, beaucoup se contentant des conventions anglophones.

Heureusement, si vous avez la chance d'avoir un pavé numérique, vous pouvez entrer bien plus de caractères que ce qu'affichent vos touches.
Pour ce faire, maintenez la touche <kbd>Alt</kbd>, tapez la séquence de chiffres indiquée ci-dessous pour le caractère qui vous intéresse, et relâchez <kbd>Alt</kbd>.

| Caractère               | Séquence de saisie                                                |
| ----------------------- | ----------------------------------------------------------------- |
| Guillemet ouvrant (`«`) | <kbd>Alt</kbd> + <kbd>0</kbd><kbd>1</kbd><kbd>7</kbd><kbd>1</kbd> |
| Guillemet fermant (`»`) | <kbd>Alt</kbd> + <kbd>0</kbd><kbd>1</kbd><kbd>8</kbd><kbd>7</kbd> |
| Espace insécable (` `)  | <kbd>Alt</kbd> + <kbd>0</kbd><kbd>1</kbd><kbd>6</kbd><kbd>0</kbd> |
| Tiret cadratin (`—`)    | <kbd>Alt</kbd> + <kbd>0</kbd><kbd>1</kbd><kbd>5</kbd><kbd>1</kbd> |



[regles-base-dialogue]: https://www.aproposdecriture.com/3-regles-de-base-du-dialogue
