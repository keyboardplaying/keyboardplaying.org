---
date: 2020-03-02T21:40:50+01:00
title: Comment choisir votre pilote OJDBC
subtitle: C'est quoi l'abscisse, c'est quoi l'ordonnée ?
slug: choisir-pilote-ojdbc
description: |-
  Si vous avez déjà eu à choisir un pilote OJDBC, vous avez dû vous interroger sur le numéro et la version.
  Ou pas, si ça fonctionnait, ce qui est presque toujours le cas.
  Je vais quand même me permettre d'expliquer.
author: chop
categories: [ software-creation ]
tags: [ java, maven, database, oracle ]
keywords: [ java, maven, database, oracle, ojdbc, pilote jdbc ]

references:
- id: ojdbc-faq
  name: Oracle JDBC FAQ
  url: https://www.oracle.com/database/technologies/faq-jdbc.html
  lang: en
- id: medium-ojdbc-maven
  name: Oracle JDBC drivers on Maven Central
  title: true
  url: https://medium.com/oracledevs/oracle-jdbc-drivers-on-maven-central-64fcf724d8b
  date: 09/2019
  lang: en
  author:
    name: Kuassi Mensah, Medium
- id: mvn-central-ojdbc
  name: ojdbc artifacts on Maven Central
  url: https://search.maven.org/search?q=g:com.oracle.ojdbc
---

Quand vous concevez un programme Java qui va se connecter à une base de données, vous avez probablement besoin d'un pilote JDBC.
Quand cette base de données est un produit Oracle, vous gardez le O et ça devient OJDBC.

Pendant des années, je me suis contenté d'utiliser la version sélectionnée par un architecte.
Puis je suis devenu l'architecte et il m'a fallu comprendre quelle version choisir.
Ce n'est pas vraiment compliqué, seulement pas très bien expliqué.
Je vais donc tenter d'apporter ma contribution...

<!--more-->


## Un mot sur JDBC

« JDBC » est l'acronyme de « Java Database Connectivity ».
Il s'agit d'une API pour Java --- comme son nom l'indique --- et définit comment un client peut accéder à une base de données.

Je vais éviter d'être technique ici, ce n'est pas ce qui m'intéresse aujourd'hui.
Dans la plupart des cas, vous aurez besoin d'un pilote JDBC qui dépend de la base de données à laquelle vous voulez vous connecter, et qui vous apportera des optimisations qui dépendront du SGBD.
La version du pilote est généralement déterminée par celle de la base de données.

Dans le cas d'Oracle JDBC (OJDBC), il y a souvent une certaine confusion parce qu'il y a la version d'une part et un autre nombre dans le nom du Jar d'autre part.


## Derrière les versions d'ODJBC

### Explication des nombreux nombres

Si vous téléchargez votre pilote JDBC depuis le site d'Oracle, une fois résolue l'énigme de trouver [la page qui vous intéresse][ojdbc-download], vous devez d'abord sélectionner la version de votre base de données Oracle.
À ce stade, vous aurez plusieurs possibilités.
Par exemple, **[pour Oracle 19.3][ojdbc-19-download], vous devez choisir entre ojdbc10 et ojdbc8**.
Mais que sont-ils ?

<aside><p>
Le numéro après <code>ojdbc</code> fait référence à la version du JDK.
</p></aside>

Eh bien, le numéro entre `ojdbc` et `.jar` spécifie la version du JDK pour laquelle ce pilote a été conçu.
Par exemple, `ojdbc8.jar` a été écrit pour le JDK 8.
Bien entendu, il fonctionnera avec les JDK 9, 10, et postérieurs --- peut-être pas indéfiniment, mais je suppose que vous aurez mis votre base de données à jour d'ici là.


### Habitudes avec Maven {#habitudes-maven}

Au cours de ma carrière, tous nos projets Java étaient construits en utilisant Maven.
Cela signifie que l'artefact pour le pilote OJDBC devait être disponible sur notre dépôt.

À chaque fois, ce Jar était téléversé dans le dépôt des dépendances tierces avec les caractéristiques suivantes :

`groupId`
: un nom de package avec `oracle` ou `ojdbc` quelque part

`artifactId`
: le nom du Jar (p. ex. `ojdbc8`)

`version`
: la version de la base de données


### Et pour les projets existants avec des bases de données récentes ?

Attendez, j'ai dit que pour Oracle 19, il n'y avait qu'ojdbc8 et 10.
Mais vous travaillez sur un projet d'entreprise qui a dix ans, qui est toujours en Java 6.
Toutes les bases de données passent à 19 en revanche.
Qu'allez-vous faire ?

Théoriquement, vous devriez mettre à jour votre version de Java afin de pouvoir prendre le dernier pilote.
La FAQ d'Oracle JDBC est assez explicite sur ce point :

> **Quelles sont les versions d'Oracle JDBC pour les versions de JDK ?**
>
> Le pilote Oracle JDBC est toujours conforme à la dernière version du JDK à chaque nouvelle release.
> Dans certaines versions, les pilotes JDBC supportent plusieurs versions du JDK.
> Utilisez la table ci-dessous pour choisir le bon pilote JDBC sur base de votre version préférée du JDK.
>
> | Version d'Oracle Database | JDBC Jar files specific to the release |
> | ------------------------- | -------------------------------------- |
> | 19.3                      | **ojdbc10.jar** avec JDK10, JDK11<br>**ojdbc8.jar** avec JDK8, JDK9, JDK11 |
> | 18.3                      | **ojdbc8.jar** avec JDK8, JDK9, JDK10, JDK11 |
> | 12.2 ou 12cR2             | **ojdbc8.jar** avec JDK 8               |
> | 12.1 ou 12cR1             | **ojdbc7.jar** avec JDK 7 et JDK 8<br>**ojdbc6.jar** avec JDK 6 |
> | 11.2 ou 11gR2             | **ojdbc6.jar** avec JDK 6, JDK 7 et JDK 8<br>(Note : JDK7 et JDK8 sont supportés en 11.2.0.3 et 11.2.0.4 uniquement)<br>**ojdbc5.jar** avec JDK 5 |
>
> {{< ref-cite "ojdbc-faq" >}}

_Pourtant_, il n'y a pas besoin de paniquer.
Oui, être à jour est une bonne pratique.
De la même FAQ, on peut citer ce qui suit :

> La recommandation est que la version du pilote JDBC soit toujours égale ou supérieure à celle de la base de données Oracle afin de bénéficier de tout le potentiel du pilote.
>
> {{< ref-cite "ojdbc-faq" >}}

Et j'insiste malgré tout : il n'est pas nécessaire de céder à la panique.
Vous pouvez être pratiquement sûr que votre application fonctionnera avec une autre version.
C'est probablement pourquoi nous ne savons pas vraiment comment choisir notre pilote OJDBC et n'avons pas envie d'apprendre.




## Pourquoi personne ne le sait ou ne s'y intéresse

### C'est contre-intuitif

Il y a une bonne raison pour que personne ne comprenne cette double numérotation : elle va à l'encontre du versioning habituel.
C'est encore plus clair en adoptant le point de vue de Maven.

Dans un dépôt Maven, le GAV (groupe, artefact, version) est un ensemble de coordonnées pour une dépendance.
Le `groupId` et l'`artefactId` en définissent l'identité : `org.springframework.boot:spring-boot-starter-parent` est un kit de démarrage pour construire des projets Spring Boot.
Vous chercherez généralement ceux-ci en premier, avant de parcourir les versions disponibles, qui ne sont que des itérations d'objets ayant la même valeur sémantique.

<aside><p>
Pour OJDBC, la version est l'identifiant principal d'une bibliothèque qui se décline sous plusieurs formes.
</p></aside>

Avec OJDBC, on marche à l'inverse : il vous faut d'abord connaître votre version, parce que c'est elle qui définit la nature du Jar que vous cherchez : le pilote OJDBC en version 19.3.0.0 est un pilote pour Oracle 19.3.
Ce n'est qu'à ce moment que vous pouvez regarder les artefacts disponibles et choisir le plus adapté à votre situation.

Celles et ceux qui viennent du monde Maven pourraient parcourir les dépendances du groupe, voir qu'`ojdbc10` est au-dessus d'`ojdbc8`, supposer que plus grand signifie meilleur.
Ensuite, ne sachant pas quelle version prendre, ils prendraient certainement la plus grande, là encore.
Et vous savez quoi ?
Ça fonctionnerait certainement...[^fn-jdk-version]

[^fn-jdk-version]: La plus importante contrainte de cet exemple est que le pilote soit compatible avec le JDK du projet.


### Ça marche de toute façon

Pourquoi passer du temps à apprendre ce que signifient ces nombres et comment les choisir quand il suffit de prendre les plus élevés ?

<aside><p>
Pas besoin de passer du temps à choisir une version, celle-ci fonctionne.
</p></aside>

Les développeur·es adorent résoudre des problèmes, mais les bon·nes développeur·es sont partisan·es du moindre effort.
Elles ou ils adorent passer des jours à écrire un script qui va leur économiser quelques heures en leur évitant une tâche répétitive.

Mais le moindre effort signifie aussi, quand le sujet ne les intéresse pas vraiment, de s'arrêter à « ça marche ».
Pourquoi perdre du temps à perfectionner quelque chose qui fonctionne ?
Les chefs de projet ont tendance à ne pas aider sur ce front.
Combien de fois ai-je entendu : « Le mieux est l'ennemi du bien » avec, en sous-titre : « Arrête de bosser là-dessus. Ça marche et tu vas nous coûter des sous. »

Tout n'est pas la faute des développeur·es, cependant.
Là où il y a un apprenti, il doit y avoir un enseignant.
C'est là qu'intervient l'architecte / lead dev / tech lead.
Aucun d'eux ne m'a jamais expliqué ceci et je ne l'ai appris que le jour où je l'ai cherché.

Quoiqu'il en soit, oui, le moindre effort sera probablement suffisant, peu importent les versions choisies.
SQL n'évolue pas tant que ça, donc il ne vous manquera que les dernières nouveautés, peut-être quelques améliorations de perf, mais un pilote plus vieux fera tout aussi bien le job.
Si vous ne pouvez pas obtenir la version exacte du pilote pour votre base, Oracle recommande d'en prendre un plus récent.

> **Quelle est la matrice d'interopérabilité ou la matrice de certification JDBC / SGBD ?**
>
> Merci de vous référer à la table qui couvre l'interopérabilité du pilote JDBC pour les versions de base de données Oracle supportées.
> La recommandation est que la version du pilote JDBC soit toujours égale ou supérieure à celle de la base de données Oracle afin de bénéficier de tout le potentiel du pilote.
>
> | Matrice d'interopérabilité | BdD 19.3 | BdD 18.3 | BdD 12.2 et 12.1 | BdD 11.2.0.4 |
> | -------------------------- | -------- | -------- | ---------------- | ------------ |
> | **JDBC 19.3**              | Oui      | Oui      | Oui              | Oui          |
> | **JDBC 18.3**              | Oui      | Oui      | Oui              | Oui          |
> | **JDBC 12.2 and 12.1**     | Oui      | Oui      | Oui              | Oui          |
> | **JDBC 11.2.0.4**          | Oui      | Oui      | Oui              | Oui          |
>
> {{< ref-cite "ojdbc-faq" >}}


## Une anecdote

Pour la note amusante, les versions de Java ont longtemps été préfixées d'un `1.`.
Java 5 était ainsi en vrai Java 1.5.
La plupart des développeurs sait que ce préfixe est inutile et historique, mais il pourrait avoir quelques conséquences amusantes assez prochainement : un développeur de mon équipe a récemment ajouté `ojdbc14` pour un projet tout aussi historique.
Non, il n'y a pas d'erreur : c'est le pilote OJDBC pour Java 1.4 (et Oracle 10 dans cet exemple).

Ce mois-ci devrait normalement sortir Java 14, la nouvelle version LTS.
Il est tout à fait possible d'imaginer que le pilote pour Oracle 20 existera sous la forme d'un `ojdbc14`, la fausse résurrection d'un ancien artefact...


## La cerise sur le gâteau

<aside><p>Les pilotes Oracle JDBC sont maintenant disponibles sur Maven Central.</p></aside>

Depuis septembre dernier, et à partir de la version 19.3.0.0, [les pilotes Oracle JDBC sont disponibles sur Maven Central][medium-ojdbc-maven].
Ils ont respecté les habitudes dont j'ai parlé [plus haut](#habitudes-maven).

Vous pouvez les trouver en cherchant [le `groupId` com.oracle.ojdbc][mvn-central-ojdbc].


## Ce qu'il faut retenir

Si vous cherchez un pilote Oracle JDBC :
- sélectionnez la version qui correspond à celle de votre base de données Oracle ;
- choisissez `ojdbcN` où _N_ est le plus grand nombre inférieur ou égal à la version du JDK utilisé ;
- téléchargez-le directement depuis Maven Central pour une version 19.3 ou ultérieure ;
- expliquez à votre équipe quelle version vous utilisez et pourquoi.


{{% references %}}

[ojdbc-download]: https://www.oracle.com/database/technologies/appdev/jdbc-downloads.html
[ojdbc-19-download]: https://www.oracle.com/database/technologies/appdev/jdbc-ucp-19c-downloads.html
