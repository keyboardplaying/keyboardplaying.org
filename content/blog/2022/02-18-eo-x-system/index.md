---
date: 2022-02-18T07:00:00+01:00
title: The X-System for Esperanto
#subtitle: A witty line as a subtitle
slug: x-system-esperanto
description: |-
  Esperanto uses some uncommon diacritics, that not all OSs will recognize.
  Some approaches propose solutions, however.
  Here's today's most common one.
author: chop
categories: [ writing ]
tags: [ esperanto ]
keywords: [ esperanto, diacritics, H-system, X-system, digraph, transliteration, substitution, Open VSX, VSCodium, Theia ]

references:
- id: wiki-eo-substitutions
  name: Substitutions of the Esperanto alphabet
  title: true
  url: https://en.wikipedia.org/wiki/Substitutions_of_the_Esperanto_alphabet
  lang: en
  author:
    name: Wikipedia
---

When you start Esperanto, the first thing you learn about is the alphabet.
Some latin letters are not used.
Others are used with diacritics in combination that are not seen in other languages.
Typing those characters may be complicated on a usual computer system.
Here's a workaround.

<!--more-->

## Esperanto's Uncommon Letters

So, here are the letters from Esperanto that you won't find in other languages: _ĉ_, _ĝ_, _ĥ_, _ĵ_, _ŝ_ and _ŭ_ (and their uppercase equivalent).

Now, unless you already configured your system to be able to type Esperanto, you'd expect that typing <key>^</key>, then <key>g</key> would print `^g` , not `ĝ`, and that doesn't even start to explain how to type _ŭ_.
There are ways to do that on your system, but I won't address those in this post.

Today, I'd rather tell you of a way to write Esperanto when you can't do that configuration.


## Using Digraphs

### A Digression

In the few weeks I spent in Germany, I still had some administrative steps to take, with computerized forms later printed to paper.
Something caught my eye once.
The clerk was working with Cobol-like computer screen, the kind I saw numerous times in pharmacies and other small shops in France, but that's not what I noticed.
What I saw was that, each time she should have typed a character with an _Umlaut_ (a diaeresis), she typed the non-accented letter followed with an _e_ instead.

Of course, it's not rocket science: accented letters are not part of the ASCII characters[^fn-ascii].
If you need to have the _Umlaut_[^fn-umlaut] in a system that doesn't support it, you must have a workaround.
Using digraphs was the common solution in Germany.
It was also adopted to replace the _Esszett_ (_ß_), usually with _ss_ or occasionnally _sz_.

[^fn-ascii]: Well, they are part of the _extended_ ASCII code, but I'm digressing enough as it is.

[^fn-umlaut]: The _Umlaut_ is important in German, as it changes both the pronunciation and the meaning of a word.
For instance, _schon_ means "already" while _schön_ means "beautiful."
Some embarrassing situations might also arise from confusing _schwül_ (humid) with _schwul_ (homosexual).


### First Attempt in Esperanto

Zamenhof suggested something similar for Esperanto: 

* For _ŭ_, use _u_.
* For circumflex accent, use an _h_ after the letter (ex. _ŝ_ ↦ _sh_).

This was dubbed the H-system.

There were things to keep in mind about this suggestion, though.
The first one is that some attention had to be paid to the writing of composed words.
For instance, an airport is called a _flughaveno_, the contraction of _flugo_ (flight) and _haveno_ (harbour), but you need to know it's not _fluĝaveno_ written in H-system, so rules were made to add an hyphen or an apostrophe in case the _h_ was in a correct place (thus turning our example to _flug'haveno_).

The other issue was with sorting.
Typically, all words beginning with _ĉ_ should be after words beginning with _c_ (_ci_ would be before _ĉu_).
With H-system though, a basic sorting would be messed up, and _ci_ is _after_ _ĉu/chu_.



## The X-System

The system I discovered when I started learning on [Ikurso][ikurso] is quite similar to the H-system:

* For _ŭ_, use _ux_.
* For circumflex accent, use an _x_ after the letter (ex. _ŝ_ ↦ _sx_).

Quite unsurprisingly, it's called the X-system.
It (nearly) solves the problems from the H-system:

* _x_ is not a letter in the Esperanto alphabet, so you can kiss those ambiguities goodbye.
* Sorting is mainly correct (_ĉu/cxu_ is indeed after _ci_).
Some error remain in the case of a _z_ in a bad place, but such occurrences are quite rare.

This system is not perfect and doesn't solve all issues, but it's a convenient way to write in Esperanto when you can't use the Esperanto diacritics.
In most cases, you can even automate the replacement of those digraphs with the correct, accented letter.



## In VSCodium

Want to use the X-System in your favourite VS Code-compatible editor?
There's an extension for that!
[Klavaro][klavaro-gh] ("keyboard" in Esperanto) allows you to replace X-digraphs on the fly.
You can turn it off with a click when you aren't typing Esperanto.

And since the developer also deployed it [on Open VSX][klavaro-vsx], [you can easily add it to VSCodium or Theia.][openvsx]
Hope you enjoy it!


[ikurso]: https://ikurso.esperanto-france.org/
[klavaro-gh]: https://github.com/paulbarre/vscode-klavaro
[klavaro-vsx]: https://open-vsx.org/extension/paulbarre/klavaro
