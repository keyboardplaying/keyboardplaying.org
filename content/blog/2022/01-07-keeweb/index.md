---
date: 2022-01-07T07:00:00+01:00
title: Keeweb, a Nextcloud Extension to Access Your KeePass Passwords
#subtitle: A witty line as a subtitle
slug: keeweb-nextcloud-keepass-integration
description: |-
  Keeweb is an extension to manage KeePass files from Nextcloud's web interface.
  If that's a need you have, this could be the solution.
author: chop
categories: [ software ]
tags: [ security ]
keywords: [ security, password, password manager, 2fa, otp, nextcloud, keepass, keepassxc, keepass2android, open source ]
---

Last year, I [wrote a post][pwd-manager] to propose an open source alternative to ready-to-use password managers like LastPass, 1Password and so on.
Something that you would host yourself, so that you would remain in control of your sensitive data.

There was one main limitation, though: the solution I exposed implied you had to install an application to read your password on a computer.
Today, I share a quick note if you _can't_ install an application on the said computer.

<!--more-->

## Quick Reminders

The basics of my previous post remain valid.
If you're looking for an open-source way to save and manage your passwords, [give it a read][pwd-manager] (and don't hesitate to share feedback if need be).

If you just want a summary, here's the gist of it.

1. Create your KDBX file with KeePass or KeePassXC.
2. Save it into your Nextcloud.

You can then download the file on any computer to use it with KeePass(XC) or on your smartphone with the appropriate app, like Keepass2Android.

![A diagram explaining the general solution]({{< relref path="/blog/2021/03-08-open-source-pwd-sync" >}}/sync-pwd-keepassxc-nextcloud-otp-authy.svg)

But here comes the problem: you might need to access your passwords on a computer you can't install KeePass on.
If such is the case, continue reading.


## Accessing Your Passwords in a Web Interface

Here comes in [Keeweb][keeweb]: this is a Nextcloud extension to open a KBDX file in your browser.
You must install it as an application on your Nextcloud server (in the _Integration_ section).

Then, you'll be able to open your files in your web interface.
The easiest way I found is to navigate to your KDBX file and click it.
It'll automatically switch the screen to Keeweb and you'll be asked the master password of your file.

From there, you can search your passwords, copy them and see the current value of a TOTP, if you chose to manage those in KeePassXC.
The text will fade as the OTP grows older, then come back to full color when a new OTP is generated.

You can use the web interface to create new passwords and update your data, too.

It's not the best interface there is (a friend actually reported to me that it doesn't play nice with mobile devices), but it'll help you if you're in a pinch.


## Don't Add This Extension "Just Because"

You should really be careful when it comes to your KDBX file.
It is something sensitive and precious.
It contains all the keys to your online identity and personal data.
No one should be able to access it.[^fn-gif-gandalf]

Even though it's password-protected, every new way for you to access your file may become a new way for an attacker to steal it if a vulnerability is discovered.

I'm not saying you should completely avoid Keeweb, I'm just advising to avoid it if you don't really need it.

[^fn-gif-gandalf]: I'd use [Gandalf][gif-gandalf] to emphasize this point [if GIFs didn't have an environmental footprint][it-footprint].
The point is: "Keep is secret. Keep it safe."

[pwd-manager]: {{< relref path="/blog/2021/03-08-open-source-pwd-sync" >}}
[keeweb]: https://apps.nextcloud.com/apps/keeweb

[gif-gandalf]: https://media.giphy.com/media/GQSpO8dpDesuI/giphy.gif
[it-footprint]: {{< relref path="/series/intro-sustainable-it" >}}
