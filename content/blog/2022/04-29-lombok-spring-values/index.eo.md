---
date: 2022-04-29T07:00:00+02:00
title: Injekto de dependecoj per konstruilo kun Spring kaj Lombok
subtitle: La pecoj de la puzlo kinuĝas.
slug: spring-lombok-injekto-dependecoj-konstruilo
description: |-
  Lombok faciligas injekti dependecoj per konstruilo.
  Estas nur unu konsileto vi bezonas scii por uzi kun la `@Value` prinoto de Spring.
author: chop
categories: [ software-creation ]
tags: [ spring-boot, java, programming ]
keywords: [ injekto de dependeco, java, spring, spring boot, lombok, autowired, konstruilo, neŝanĝebla ]
---

En la pasinteco, ni konsilis [uzi la konstruilo por injekti dependecoj][kp-spring] kun Spring, kaj ni [prezentis Lombok][kp-lombok].
Divenu…
Tiuj iras tre bone kune, krom eble por iom da ruzo por uzi la `@Value` de Spring.

<!--more-->

## Generi la injektantan konstruilon kun Lombok

Ni faru neŝanĝeblan servon, kiu injektas ĝiajn dependecojn per sia konstruilo.

```java
import org.springframework.stereotype.Service;

@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    public Library(BooksDatabase booksDb, BorrowersDatabase borrowersDb) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
    }
}
```

Per petado de [kion ni vidis kun Lombok lasta fojo][kp-lombok], pli konciza maniero fari ĉi tion estus:

```java
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;
}
```

Finita!
Kaj neniu bontenado de la konstruilo estas necesa se ni aldonas aŭ forigas kampojn.
Ĉu tio ne estas vera plezuro fidi ĉi tiujn du?


## La ĝena kazo de `@Value`

### Ne blindu

Lombok havas `lombok.Value`, Spring havas `org.springframework.beans.factory.annotation.Value`.
Ili tute ne havas la saman celon.
En ĉi tiu afiŝo, ni enfokusigos la `@Value` de Spring.


### La problemo

Por uzi la injekton per konstruilo, ĉio devas trairi la konstruilon.
Se necesas agordon, oni devas specifi `@Value` pri la koncerna parametro.

```java
@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;
    private final int maxLendingDays;

    public Library(
      BooksDatabase booksDb,
      BorrowersDatabase borrowersDb,
      @Value("${library.lending.days.max}") maxLendingDays
    ) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
        this.maxLendingDays = maxLendingDays;
    }
}
```

Kiel fari tamen, se Lombok verkas la konstruilo anstataŭ vi?


### La solution naïve

Kiam unue alfrontis ĉi tiu, la instinkto de mia teamo estis fari tion:

```java
@Service
@RequiredArgsConstructor
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private int maxLendingDays;
}
```

Sed kio okazas en ĉi tiu kazo?

Unue, Lombok generos konstruilon, kiu prenas argumenton por ambaŭ `final` kampoj, sed ignoros la entjeron.
La generita kodo aspektus tiel:

```java
@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private int maxLendingDays;

    public Library(BooksDatabase booksDb, BorrowersDatabase borrowersDb) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
    }
}
```

Kiam Spring renkontas ĉi tiun kodon, ĝi instigas la klason en du stadioj:

1. Ĉar ekzistas konstruilo, ĝi injektas la du necesajn kampojn.
2. Ĝi tiam uzas la reflektadon por agordi la ne-`final`, `@Value`-prinotatan kampon.
La tuta profito de la konstruilo-bazitan injekto tiam estas perdita.


### La vera solvo

[Ni diris][kp-lombok] ke la konduto de Lombok povas esti adaptita per `lombok.config` dosiero.
Ĉi tiu estas unu el la plej gravaj kazoj.

Ekzemple, eblas fari Lombok [kopii iujn prinotojn de la kampoj al la respondaj parametroj de la konstruilo][lombok-constructors].
Por ĉi tio, simple aldonu la sekvan linion al la agorda dosiero:

```properties
lombok.copyableAnnotations += org.springframework.beans.factory.annotation.Value
```

Tiam skribu vian servon tiel:

```java
@Service
@RequiredArgsConstructor
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private final int maxLendingDays;
}
```

La generita kodo devus esti ekvivalenta al la sekva:

```java
@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private final int maxLendingDays;

    public Library(
      BooksDatabase booksDb,
      BorrowersDatabase borrowersDb,
      @Value("${library.lending.days.max}") maxLendingDays
    ) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
        this.maxLendingDays = maxLendingDays;
    }
}
```


## Miaj aliaj konsiloj

### `@RequiredArgsConstructor` aŭ `@AllArgsConstructor`

Mi ŝatas `@RequiredArgsConstructor`, ĉar ĝi ofertas iom da kontrolo pri kiuj kampoj devas esti transdonitaj al la konstruilo.
Tamen, por Spring servoj, ĉiuj kampoj devus tiel komencitaj, do `@AllArgsConstructor` estas tute korekta solvo.


### Aldoni `@Autowired` al generita konstruilo

Vi eble deziras, ke via konstruilo estas prinotata kun `@Autowired`.
Kvankam laŭvola[^fn-autowired-constructor], ĝi povas helpi programistojn, kiuj ne konas bone Spring, kompreni kiel la klaso estas ŝarĝita dum rultempo, ekzemple.
Ĉi tio povas esti farita per agordo de via Lombok prinoto:

```java
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Library {
    // ...
}
```

[^fn-autowired-constructor]: `@Autowired` estas necesa sur la konstruilo nur se la klaso havas aliajn konstruilojn.
En ĉi tiu kazo, akurate unu konstruilo devas esti prinotata.


## Konkludo

Mi esperas, ke ĉi tiu mallonga afiŝo helpos vin forigi iujn el la _boilerplate_ kodo necesita por la instanco de viaj Spring servojn.

Ne hezitu dividi viajn konsiletojn!


[kp-spring]: {{< relref path="/blog/2021/01-04-spring-constructor-injection" lang="en" >}}
[kp-lombok]: {{< relref path="/blog/2022/04-08-lombok" >}}
[lombok-constructors]: https://projectlombok.org/features/constructor
