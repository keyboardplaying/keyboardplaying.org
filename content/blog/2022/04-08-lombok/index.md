---
date: 2022-04-08T07:00:00+02:00
title: About Lombok
subtitle: There's an annotation for that!
slug: about-lombok
description: |-
  Java is said to be one of the most verbose programming language.
  In particular, it often requires some boilerplate (constructor, accessors…).
  Lombok's a solution to help you reduce that verbosity.
author: chop
categories: [ software-creation ]
tags: [ java, programming ]
keywords: [ Java, Lombok, annotations, boilerplate code, accessors, constructors, Maven, IDE, logging, immutable ]

references:
- id: lombok
  name: Project Lombok
  title: false
  url: https://projectlombok.org/
  lang: en
---

A few months back, a new guy joined our team and the Java world after a few years of being a .NET programmer.
He had many reactions like, "What, you need to write that yourself in Java? In .NET, the compiler does it for you!"

It's true that, in Java, the language and conventions drive you to write a non-negligible amount of low-value code.
But don't fear!
Lombok aims at simplifying that task for you.

<!--more-->


**Please note this post is not a tutorial**, but merely a quick intro to what Lombok can do for you.


## The Purpose

### What Is Boilerplate Code?

Imagine writing a Java class, assuming you respect the best practices.
Each field must be private and accessed only through… well, accessors.

It means that, each time you add a field, you must declare new getters and setters.
If your object is immutable, all fields must be initialized at instantiation.
That implies your constructor must set all these fields, and if you add new fields, you must update your constructor too.

_That_ is boilerplate code.
It's code that you write, no matter the project, no matter the class, that has little logic in it.
It's code that is frustrating to write, because a machine could do it in your stead (actually, you probably do it with a few strokes and shortcuts in your IDE).

Now, when you come from other languages where all this is implicit or declarative (see TypeScript or C#), writing this code explicitly seems like a artefact from an ancient world.
It seems obsolete or, at the best, just painful.

That's what Lombok was created for.


### How Does Lombok Solve That?

<aside><p>Lombok replaces annotations with actual (boilerplate) code, at compile-time.</p></aside>

The heart of Lombok is annotations: instead of writing your boilerplate code, you declare what you want to be there without having to write it yourself.
For instance, if you need your class to have an all-args constructor, you annotate it with `@AllArgsConstructor`.
See [below](#examples) for some other examples.

Now, what happens to these annotations?
We're used to them being used at runtime to customize some behaviour, do some AOP or so (Spring, Jackson, we're fondly thinking of you).
It's not how Lombok works, though.

In addition to providing a collection of annotations, Lombok is also an annotation processor: when on the path of your Java compiler, it'll analyze your annotated code at compilation time and generate its classes accordingly.
This means it brings you comfort for writing code, but has no impact on your application performance since it won't use reflection.[^fn-reflection]

[^fn-reflection]: At the worst, it'll have a negative impact on your compilation time, but unless you have a very, very large project with massive use of Lombok, I'm not sure you'll be able to tell.


## How To Get Started?

Please note **this post is not a tutorial**.
I'm only scratching the surface here, so that you may look more into it if you're interested.

To work with Lombok, here's what you'll need:

* some Maven configuration;
* a plugin for your IDE.

Let's dive a bit into this.


### Maven Configuration

It's not complicated, you'll see.
In your POM, add your dependency:

```xml
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.18.22</version>
  <scope>provided</scope>
</dependency>
```

That's it!
Usually, this is enough.


#### One Gotcha

There is one additional thing to do in two cases:
* if you're using JDK9+ with module-info.java;
* if you've already configured an annotation processor[^fn-annot-proc].

[^fn-annot-proc]: Normally, the compiler plugin picks up annotation processors automatically from the dependencies.
Manually setting the processors, however, deactivates that sensing of what's on the classpath.

If that's your situation, you must [configure the `annotationProcessorPaths` of the Maven Compiler Plugin](https://maven.apache.org/plugins/maven-compiler-plugin/compile-mojo.html#annotationProcessorPaths).
It looks like this:

```xml
<annotationProcessorPaths>
	<path>
		<groupId>org.projectlombok</groupId>
		<artifactId>lombok</artifactId>
		<version>1.18.22</version>
	</path>
</annotationProcessorPaths>
```

That's it!


#### A Note About the Dependency Scope

You may have noticed the Lombok dependency is in `provided` scope.
It basically means "I don't need this to be included at runtime."
You do need the annotations when compiling, but they're eliminated from the generated class.
That's why having them on the classpath when running would be a waste, since they'll never be loaded.

**If you're using Spring Boot's fat jar** (and probably other similar mechanism), **the `provided` scope is ignored**.
If that is the case, you should add the `<optional>true</optional>` to your dependency declaration.

```xml
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.18.22</version>
  <scope>provided</scope>
  <optional>true</optional>
</dependency>
```


### IDE Plugin

Your IDE usually compiles classes as soon as you modify them, but it does it its own way, not caring about annotation processors or such, unless you install plugins or spend time configuring.
When using Lombok, that usually results in it whining about not knowing the getter you're calling.

Lombok got it covered and provides plugins, whether you're using [an Eclipse-based IDE](https://www.projectlombok.org/setup/eclipse), [an IntelliJ product](https://www.projectlombok.org/setup/intellij), [Netbeans](https://www.projectlombok.org/setup/netbeans) or a [Visual Studio Code-compatible editor](https://www.projectlombok.org/setup/vscode).
Just open the most relevant page on [Lombok's webite][lombok] (under the "Install" menu) and discover the installation procedure for your tool.


## Lombok Annotation Examples {#examples}

Now you're set up and ready to go.
What should you do?

I suggest we begin with the basics: accessors.


### Accessors

OK, so let's admit you got a DTO with fields and you don't want to explicitly write the getters and setters.
Just annotate your class (or your fields) with [`@Getter` and `@Setter`][lombok-getter-setter], and you're done!

By default, the accessors will be `public` but you can override that to suit your needs.

```java
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book {
    private String title;
    private String author;

    @Getter(access = AccessLevel.NONE)
    private String somePrivateFieldThatMustNotHaveAGetter;
}
```

Much less verbose than your usual Java POJO, eh?


### Constructors

You need a constructor to instantiate your class.
[Three annotations are available in Lombok][lombok-constructor] especially for that purpose:

* `@NoArgsConstructor` is quite explicit.
* `@AllArgsConstructor` is too.
* `@RequiredArgsConstructor` creates a constructor with a parameter for each field that needs initializing at instantiation (eg. `final` fields).

As is the case with the accessors, you can set the scope with the `access` parameter.

```java
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Book {
    private String title;
    private String author;
}
```

One fabulous thing: you don't need to update your constructor if you add or remove fields.
That will be automatic![^fn-refactor-calls]

[^fn-refactor-calls]: Of course, the calls to the constructors will require refactoring, but you'll locate those quite easily through the compiler errors.


### Builders

As time passes, I tend to do more and more immutable objects.
However, an immutable POJO with a lot of fields is a pain to initialize and the all-args constructor is not clear.
That is why I love the _builder_ approach.

And [Lombok has an annotation for that][lombok-builder], too!

Say I update my `Book` class as follows:

```java
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Book {
    private String title;
    private String author;
}
```

I can then init a book using this:

```java
Book.builder()
    .title("Roverandom")
    .author("John Ronald Reuel Tolkien")
    .build();
```

If you ever tried to write a builder on your own, you must appreciate the time this saves you.


### And Much, Much More

I'll stop here for the examples as I suppose you are getting bored.
Just know that there are annotations to generate your [`equals()` and `hashcode()`][lombok-equals] methods, your [`toString()`][lombok-tostring], or even to [add a `log` field](https://projectlombok.org/features/log) from your favourite [logging framework][about-logging].

Also know that you can specify some additional or default behaviours in a `lombok.config` file to place at the root of your project, just beside the `pom.xml`.


## To Go Further

Of course, you can find a lot of tutorials about Lombok, but I warmly invite you to have a look at [the official doc][lombok-doc].
It's well done, each annotation comes with an example showing you how it's translated in "vanilla" Java, and it tells you the possible configurations.

And as with any new tool, a bit of experimentation can only help.



{{% references %}}

[lombok-getter-setter]: https://projectlombok.org/features/GetterSetter
[lombok-constructor]: https://projectlombok.org/features/constructor
[lombok-builder]: https://projectlombok.org/features/Builder
[lombok-equals]: https://projectlombok.org/features/EqualsAndHashCode
[lombok-tostring]: https://projectlombok.org/features/ToString
[lombok-doc]: https://projectlombok.org/features/all

[about-logging]: {{< relref path="/blog/2021/04-12-about-logging" >}}
