---
date: 2022-01-28T07:00:00+01:00
title: Introducing Regex Quizzes
#subtitle: A witty line as a subtitle
slug: introducing-regex-quizzes
description: |-
  A new little game will be happening on Keyboard Playing, as a way to discover things about regular expressions you may not know yet.
author: chop
categories: [ software-creation ]
tags: [ regex, news ]
keywords: [ regex, regular expressions, rational expressions, quiz ]

references:
- id: wiki-regex
  name: Regular expression
  title: true
  url: https://en.wikipedia.org/wiki/Regular_expression
  lang: en
  author:
    name: Wikipedia
---

I like regular expressions, very much.
A bit too much, probably, but I'm aware of that and I don't use them that much.

That doesn't prevent me from trying some funny patterns, and I'll be sharing some of them here for the sake of learning.

<!--more-->


## About Regular Expressions

If you work in software creation, there's a sentence you've probably heard quite often[^fn-sentence]: "Let's just use a regex to do that!"
Yes, that's how it is: regular expressions are a wonderful tool.
For developers that just discover them, there's often something quite magical about it.
Or quite repulsive.

[^fn-sentence]: If you just began your career, come back to this post in a few months and tell me you've never heard it. :)

I was among the fascinated ones, and still am.
They have some wonderful uses.
However, as you can guess, like everytime a developer enthuses over something new, they try to [use it everywhere][xkcd-regex], even when it's not the best thing to go for in that context.

But I'm getting ahead of myself.


### What Are Regular Expressions?

I won't go make a lesson or tutorial here.
Wikipedia defines them thus:

> A **regular expression** (shortened as **regex** or **regexp**; also referred to as **rational expression**) is a sequence of characters that specifies a _search pattern_ in text.
> Usually such patterns are used by string-searching algorithms for "find" or "find and replace" operations on strings, or for input validation.
> {{< ref-cite "wiki-regex" >}}


### Say, Whaaaat?

Ok, here's a brief example to help you wrap your mind around it.
If you really don't know about them, several websites have very well designed introductions or lessons.

Let's take the pattern `^(Hello|Bonjour|Saluton)`.
It would be used to detect if a string begins (`^`) with "Hello," "Bonjour" or "Saluton."
If it does match the rule, the regex will also capture that word in a _group_.
So, "Hello, World!" would match this pattern (the group would contain `Hello`), but "Hey World! Hello!" wouldn't.

You could of course tweak the pattern to test only if the string _contains_ those keywords: `(Hello|Bonjour|Saluton)` would match both strings.

As I said, I won't go too far into the details.
I might introduce some concepts in the future, to explain the regex I share.


### What I Reproach Regular Expressions

Well, actually nothing.
It's developers I reproach for using them when it's not the best way to go.

See, the problem with regex is that they're like another programming language.
One that's not obvious for everyone.
Of course, those who write complex patterns almost never take the time to document what they do or match.
So, using them comes at a cost for code readability and, therefore, maintenability.

There are also performance[^fn-perf-java-regex] and security problems, depending on the language you're coding in and the pattern you're using.

[^fn-perf-java-regex]: I remember a professor in engineering school, who wanted to try and enhance the performances of regex parsing in Java, and share his solution with Sun.
I never knew whether the project succeeded.

So, basically, I'd advise limiting your use of regular expressions to simple cases of input validation, search, or search and replace.
Those are the cases regular expressions excel in.

And if your pattern becomes too complicated/recursive/illegible, maybe that's a sign you should be looking for another solution.


## About Those Quizzes

So yeah, I've met some funny regex in the past years.
I've even written a few myself.

Like I said, I like them, even if I do my best to avoid using them in my code unless I see no better solution.
In many occasions, I found myself thinking "could I write a pattern to match this with a regex?"
The answer was often yes, but those expressions were for the most part unwieldy or hard to read.
Or they were the occasion to discover some nice concept about regex I didn't know yet.

The quizzes will be based upon such expressions.
I'll give you the regex one day ahead on [Twitter][tw-kp].

Then, on Friday, I'll share it in a post.
The regex will be at the top to give you a chance to understand it before getting the explanation, in case you couldn't see it on Twitter beforehand.

Those won't happen too often, as I wouldn't them to bore you out (and I don't have such an impressive stock either).
Once every two months seems a good rythm.
I hope you'll enjoy the format.

See you in two months for the first quiz.

[xkcd-regex]: https://xkcd.com/208/
[tw-kp]: https://twitter.com/KeyboardPlaying
