---
date: 2022-01-14T07:00:00+01:00
title: Visual Studio Code eble ne estas tiel malfermitkoda kiel vi pensas
#subtitle: A witty line as a subtitle
slug: visual-studio-code-ne-open-source
description: |-
  Kiam Vida Studio-Kodo estis anoncita, mia kompreno estis, ke ĝi estas malfermitkoda.
  Nu, estas nuanco al tio. 
author: chop
categories: [ software, software-creation ]
tags: [ open-source, programming, web-dev ]
keywords: [ Visual Studio Code, VS Code, vscode, Microsoft, malfermitkoda, FLOSS, VSCodium, Theia, IDE ]

references:
- id: vscodium-site
  name: VSCodium retejo
  title: false
  url: https://vscodium.com/
  lang: en
- id: theia-site
  name: Theia IDE retejo
  title: false
  url: https://theia-ide.org/
  lang: en
---

Microsoft publikiganta siajn ilojn sub malfermitkoda permesilo havis iom da efiko al la programista komunumo.
Estas tamen unu subtileco: la MIT-permesilo validas por la fontkodo, sed ne por la distribuita binaro.

Jen kelkaj klarigoj kaj du eblaj alternativoj.

<!--more-->

## Kelkaj detaloj pri la (ne) malfermitkoda afero

### La mallonga versio

En miaj memoroj, la mondo tremis kiam VS Code estis anoncita.
Microsoft, ĉu publikigi ilon sub liberkoda permesilo?
Kia noveco!
Sed ŝajnas, ke Microsoft ne precize anoncis ĝin.

Microsoft kreis Github-deponejon, `vscode`, kiu _estas_ licencita de MIT.
Tamen, la produkto, kiun ili disponigas por elŝuto, Visual Studio Code, ne estas.
Anstataŭe, ĝi estas publikigita sub [ĉi tiu ne-FLOSS-licenco][vscode-license].

Jen kelkaj eltiraĵoj el la permesilo (tradukita):

> Vi povas malaktivigi plurajn [datumkolektojn], sed ne ĉiujn.

> Ĉi tiu interkonsento nur donas al vi iujn rajtojn uzi la programaron. Microsoft rezervas ĉiujn aliajn rajtojn. Krom kie aplikebla leĝo donas al vi pli da rajtoj malgraŭ ĉi tiu limigo, vi nur rajtas uzi ĉi tiun programaron kiel eksplicite permesite en ĉi tiu interkonsento. Farante tion, vi devas observi ajnan teknikan limigon de la programaro, kiu nur permesas vin uzi ĝin en iuj manieroj.

En tuta justeco, ĉi tiu ne estas la sola produkto en la programada mondo por pruvi ĉi tiun duecon.

Ekzemple, [en ĉi tiu komento][vscode-maintainer-comment], prizorganto de `vscode` donas kelkajn ekzemplojn: Chrome estas konstruita el la fontkodo de Chromium, la Oracle JDK estas kompilita de OpenJDK kaj la produktoj de Jetbrains estas bazitaj sur la platformo IntelliJ (dum ni atendas Fleet).

Tamen estas unu afero: en tiuj ekzemploj, la nomado helpas diferencigi inter kio estas malferma fonto kaj kio ne estas.
Por Visual Studio Code, la mallongigo VS Code kaj la deponejo `vscode`, la distingo estas tiom evidenta.


### Klarigo

En [la sama komento][vscode-maintainer-comment], la prizorganto klarigas al ni kio okazas inter kiam la kodo estas malfermita kaj kiam la fina, fermita binaro estas publikigita.

> La mirinda afero [...] estas, ke vi havas la elekton uzi la markon de Visual Studio Code sub nia permesilo aŭ vi povas konstrui version de la ilo rekte el la deponejo `vscode`, sub la permesilo MIT.
>
> Jen kiel ĝi funkcias. Kiam vi konstruas el la deponejo `vscode`, vi povas agordi la rezultan ilon per agordo de la dosiero `product.json`. Ĉi tiu dosiero kontrolas aferojn kiel la Galeriajn finpunktojn, "Send-a-Smile" finpunktojn, telemetriajn finpunktojn, emblemojn, nomojn kaj pli.
>
> Kiam ni konstruas Visual Studio Code, ni faras ĝuste tion. Ni klonas la deponejon `vscode`, ni metas personecigitan `product.json` kiu havas Microsoft-specifan funkcion (telemetrio, galerio, emblemo, ktp.), kaj poste produktas konstruaĵon, kiun ni liberigas sub nia permesilo.
>
> Kiam vi klonas kaj konstruas el la deponejo `vscode`, neniu el ĉi tiuj finpunktoj estas agordita en la defaŭlta `product.json`. Tial, vi generas "puran" konstruon, sen la Mikrosoftaj personigoj, kiu estas defaŭlte licencita sub la MIT-licenco.

Do, esence, tio resumas, ke, kiam Microsoft konstruas Visual Sudio Code, ili aldonas sian markadon, permesilon kaj telemetrian modulon.
"Telemetrio?"
La afero, kiu faros la datumkolektojn, de kiuj vi ne rajtas ĉerpi vin, kaj nun vi vidas, ke vi eĉ ne povas vidi tion, kion ili kolektas travidebla.

Vi eble ne ĝenas tion.
Alie, la jenaj povus interesi vin.


## Malfermitkodaj alternativoj al Visual Studio Code

### VSCodium

La teamo [VSCodium][vscodium-site] sekvis la konsilojn de la prizorganto: ili prenis la fonton de `vscode`, kreis sian` product.json` kaj dividis sian binaron — konservante ĉi-foje la MIT-permesilo, farante ĝin FLOSS-programaro.
Kompreneble, ili tute malaktivigis la telemetriajn funkciojn.

Vi povas foliumi [ilian deponejon][vscodium-repo] aŭ simple instali ĝin, uzante vian plej ŝatatan pakaĵadministrilon: ĝi subtenas Windows Package Manager, Chocolatey, Homebrew, Scoop, Snap, kaj la plej oftajn pakadministrantojn de Linux (se vi aldonas la taŭga deponejo).
Vi devus trovi solvon por vi.


### Theia IDE

[Eklipso Theia][theia-site] estas alia solvo.
La kreintoj trovas VS Code bonega ilo, sed ne tute tion, kion ili atendis.
Prefere ol envolvi ĝin en Elektronon, ili programis platformon por IDE funkcii sur la komputilo aŭ en la nubo.

Atentu la _platformon por IDE_: Theia ne havas la pretendon esti uzebla IDE, sed la fundamentojn por konstrui redaktilon.
Pluraj iloj bazitaj sur ĝi jam ekzistas, kelkaj eĉ estas reliefigitaj en la projektpaĝo.

La grafika fasado multe similas al VS Code, sed la dizajnistoj diras, ke la arkitekturo estas malsama, pli modula.

Ili proponas [elŝuti version de labortabla Theia][theia-blueprint], kiun ili nomas Eclipse Theia Blueprint.
Ili insistas, ke ĉi tio ne estas taŭga solvo por anstataŭigi IDE kaj atentigas, ke Theia Blueprint estas nuntempe en beta.

Koncerne la permesilon, donita la nomon, vi eble divenis, ke ĝi estas la Eclipse Public License 2.0.



### Konsileto pri etendoj

Ambaŭ VSCodium kaj Theia subtenas la etendaĵojn de VS Code, sed kun malgranda kapto: ili ne povas aliri la merkaton de Microsoft.
Ili uzas Open VSX anstataŭe (pli pri tio [venontsemajne][open-vsx]).
Ĝi signifas, ke vi povas serĉi etendon kaj eble trovi ĝin se la programisto publikigis ambaŭ ĉe Microsoft kaj Open VSX.

Se vi ne trovas, ne paniku!
Iru al la oficiala vendopaĝo, alklaku _Download_ kaj permane instalu la dosieron .vsix en via redaktilo. 



{{% references %}}

[vscode-license]: https://code.visualstudio.com/license
[vscode-maintainer-comment]: https://github.com/Microsoft/vscode/issues/60#issuecomment-161792005

[vscodium-repo]: https://github.com/VSCodium/vscodium

[theia-blueprint]: https://theia-ide.org/docs/blueprint_download

[open-vsx]: {{< relref path="/blog/2022/01-21-open-vsx" >}}
