---
date: 2022-01-14T07:00:00+01:00
title: Visual Studio Code May Not Be As Open Source As You Think
subtitle: The devil's in the details
slug: visual-studio-code-not-open-source
description: |-
  When Visual Studio Code was announced, my understanding was that it was open source.
  Well, there's a nuance to that.
author: chop
categories: [ software, software-creation ]
tags: [ open-source, programming, web-dev ]
keywords: [ Visual Studio Code, VS Code, vscode, open source, Microsoft, FLOSS, VSCodium, Theia, IDE ]

references:
- id: vscodium-site
  name: VSCodium website
  title: false
  url: https://vscodium.com/
  lang: en
- id: theia-site
  name: Theia IDE website
  title: false
  url: https://theia-ide.org/
  lang: en
---

Microsoft publishing its tools under an open-source license did quite an effect.
Thing is, the MIT-license applies to the source code, but not to the binary they distribute.

Here are some explanation on this, and two possible alternatives.

<!--more-->

## Some Details About the (Not) Open-Source Thing

### The Short Version

As I remember it, VS Code shook the developer community when first announced.
Microsoft publishing a tool under an open-source license was something quite new.
But it appears it was not exactly what Microsoft announced.

Microsoft created a repository, `vscode`, which _is_ MIT-licensed.
The product they make available for download however, Visual Studio Code, is not.
Instead, it's published under [this non-FLOSS license][vscode-license].

Here are some excerpts from the license today:

> You may opt-out of many [data collections], but not all.

> This agreement only gives you some rights to use the software. Microsoft reserves all other rights. Unless applicable law gives you more rights despite this limitation, you may use the software only as expressly permitted in this agreement. In doing so, you must comply with any technical limitations in the software that only allow you to use it in certain ways.

To be fair, this is not the only product we use in development being based on an open-source codebase and being delivered under a custom license.
For instance, [in this comment][vscode-maintainer-comment], a Visual Studio Code maintainer gives some examples: Chrome is built from Chromium, the Oracle JDK is built from OpenJDK and JetBrains products are built on top of the IntelliJ platform (while we wait for Fleet).

There's one thing, though: in those examples, the naming helped understand the difference, in most cases.
For Visual Studio Code, VS Code and the `vscode` repository, getting it all together is not that obvious.


### Explanation

In the same comment, he gives some details about what happens between the moment the code is open and the one the final, closed binary is published.

> The cool thing [...] is that you have the choice to use the Visual Studio Code branded product under our license or you can build a version of the tool straight from the `vscode` repository, under the MIT license.
>
> Here's how it works. When you build from the `vscode` repository, you can configure the resulting tool by customizing the `product.json` file. This file controls things like the Gallery endpoints, “Send-a-Smile” endpoints, telemetry endpoints, logos, names, and more.
>
> When we build Visual Studio Code, we do exactly this. We clone the `vscode` repository, we lay down a customized `product.json` that has Microsoft specific functionality (telemetry, gallery, logo, etc.), and then produce a build that we release under our license.
>
> When you clone and build from the `vscode` repo, none of these endpoints are configured in the default `product.json`. Therefore, you generate a "clean" build, without the Microsoft customizations, which is by default licensed under the MIT license.

So, basically, what it boils down to is that, when Microsoft builds Visual Sudio Code, they add their branding, license and telemetry module.
"Telemetry?"
The thing that'll do the data collections you can't completely opt out of, and now you see that you can't either see what they do collect in a transparent manner.

It may be OK with you.
If it isn't, you might be interested in what comes next.


## Open Source Alternatives to Visual Studio Code

### VSCodium

[VSCodium][vscodium-site] did just what the maintainer suggested: take the `vscode` sources, cook up their `product.json` and make the binary available — keeping the MIT license, this time, which makes it FLOSS.
Of course, they totally deactivated the telemetry features.

You can have a look [at their repo][vscodium-repo] or just install it, using your favourite package manager: it supports Windows Package Manager, Chocolatey, Homebrew, Scoop, Snap, and Linux's most common package managers (if you add the adequate repository).
You should find a solution for you.



### Theia IDE

[Eclipse Theia][theia-site] is an alternative.
Basically, the creators say VS Code is great, but not yet what they were looking for.
Instead of wrapping it in an Electron, they developed an IDE platform to run in both cloud or desktop.

Mind the _IDE platform_: Theia doesn't have the pretention to be a ready-to-use IDE, but the foundations to build an editor on.
There are already tools built on it, some of which are featured on the project's homepage.

The UI has a distinct VS Code feeling, but the designers say the architecture is different, more modular.

They propose to [download a version of desktop Theia][theia-blueprint], which they call Eclipse Theia Blueprint, which they insist is not meant to be a replacement for an IDE.
They also note Theia Blueprint is currently a beta version.

About the license, given the full name of the project, I suppose you've guessed it's the Eclipse Public License 2.0.


### A Tip About Extensions

Both VSCodium and Theia support VS Code's extensions, but there's a trick: they can't access Microsoft's marketplace, so they're using Open VSX instead (more on that [next week][open-vsx]).
It means you can look for an extension and may find it if the developer published both at Microsoft and Open VSX.
If you don't, don't panic!
Just go to the official marketplace, click _Download_ and manually install the .vsix file into your editor.


{{% references %}}

[vscode-license]: https://code.visualstudio.com/license
[vscode-maintainer-comment]: https://github.com/Microsoft/vscode/issues/60#issuecomment-161792005

[vscodium-repo]: https://github.com/VSCodium/vscodium

[theia-blueprint]: https://theia-ide.org/docs/blueprint_download

[open-vsx]: {{< relref path="/blog/2022/01-21-open-vsx" >}}
