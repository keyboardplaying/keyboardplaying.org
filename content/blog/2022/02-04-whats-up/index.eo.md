---
date: 2022-02-04T07:00:00+01:00
title: Kio okazas? (februaro 2022)
#subtitle: A witty line as a subtitle
slug: kio-okazas
description: |-
  La blogo evolvas, kun nova afiŝado kalendaro kaj novaj formatoj.
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ blogo, rakontoj, stilfolioj, minimalismo ]
---

Nova jaro komencis.
Ĝis nun, ni sukcesis teni niajn bonajn rezoluciojn por la retejo.

<!--more-->

## Kio ŝanĝiĝis?

### Nova afiŝado kalendaro

Vi eble rimarkis: ni publikigas pli ofte.
Ekde la jaro komenciĝis, estis agado ĉiusemajne.

Unue en la blogo.
La tago de afiŝado ŝanĝiĝis.
Ĝis nun, novaj afiŝoj estis dividitaj lunde.
De nun, ili estas vendrede.
Ne estas pri la legantaro (ni pardonpetas), ni nur provas esti pli regulado.

Publikaĵoj lunde devis forlasi la semajnfinon kiel lasta rimedo por skribi se la semajno ne permesis ĝin.
Praktike, la semajno estis dediĉita al aliaj temoj planante uzi la rezervon, tiam semajnfine ni ripozis, post kvin tagoj de laboro.
Tiel naskiĝis nova limo: skribi _antaŭ_ la semajnfino.

Ankaŭ rakontoj aktivis.
La [semajnaj defioj][stylohebdo] revenis.
Ni dividas la partoprenoj tuj kiam ili estas pretaj kaj tradukitaj, ofte sabate aŭ dimanĉe.
La [dumonataj defioj][defidustylo] baldaŭ revenos ankaŭ.
Nuntempe mi malfruas pri la lasta.


### Novaj formatoj por afiŝoj

Ĉi tio intime rilatas al la antaŭa: la afiŝoj mallongiĝas.
Certe, ĝi faciligas trovi tempon por skribi, sed ankaŭ por legi: estas pli facile legi kvinmuntan artikolon ol legi niajn divagojn dum kvaronhoro.

La celo estas resti pli centrita pri temo, trakti ĝin mallonge kaj, laŭeble, tranĉi en serio la afiŝojn, kiuj estus tro longaj.

Ankaŭ aperas novaj formatoj por dividi sciojn pli ludeme.
Tiel ni skribis la pasintan semajno pri [regex kvizoj][regex-quizzes].
Ni devas atendi por vidi ĉu vi ŝatos ilin, sed ni esperas ke estos publiko.


## Kaj tiam?

Ni laboras pri grafika reverkado de la retejo.
Ni neniam estis vere kontentaj pri ĝia aspekto, sed neniu UI aŭ UX-dezajnisto pasis ĉi tien port doni al ni konsilon.

Ne gravas, konforme al niaj provoj konigi la [daŭrigebla IT][s-it], ni provos pli minimalistan interfacon (kaj finfine skribos la presan stilfolion; tio estis [en la temo-spurilo][print-css-issue] por tro longa).
Ni esperas fini tion antaŭ la fino de la kvarono, sed ne garantias ĝin.

La respondo en du monatoj, en la sekva "Kio okazas?"


[stylohebdo]: {{< relref path="/inspirations/weekly" >}}
[defidustylo]: {{< relref path="/inspirations/bimonthly" >}}
[regex-quizzes]: {{< relref path="/blog/2022/01-28-regex-quizzes" >}}
[s-it]: {{< ref path="/tags/sustainable-it" lang="en" >}}
[print-css_issue]: https://gitlab.com/keyboardplaying/website/keyboardplaying.org/-/issues
