---
date: 2022-01-21T7:00:00+01:00
title: Open VSX estas malfermitkoda alternativo al la Marketplace de Microsoft
#subtitle: A witty line as a subtitle
slug: open-vsx-malfermitkoda-alternativo-marketplace-microsoft
description: |-
  La Marketplace de Microsoft estas mirinda deponejo de malfermitkodaj etendaĵoj.
  Paradokse, malfermitkodaj programadaj medioj ne rajtas konekti al ĝi.
  Open VSX estas ĉi tie por provizi alternativon.
author: chop
categories: [ software-creation, software ]
tags: [ open-source, programming, web-dev ]
keywords: [ Open VSX, VS Code, VSCodium, Theia, vendoplaco, etendaĵoj, Microsoft, malfermitkoda ]

references:
- id: open-vsx
  name: Open VSX Registry
  title: false
  url: https://open-vsx.org/
  lang: en
- id: ms-marketplace
  name: Terms of Use of Microsoft's Marketplace
  title: false
  url: https://marketplace.visualstudio.com/VSCode
  lang: en
- id: eclipse-open-vsx
  name: 'Eclipse Open VSX: A Free Marketplace for VS Code Extensions'
  title: true
  date: 03/2020
  url: https://www.eclipse.org/community/eclipse_newsletter/2020/march/1.php
  lang: en
  author:
    name: Sven Efftinge, Miro Spönnemann
---

Laborante kun VSCodium, povas okazi, ke vi ne povas trovi etandaĵon disponeblan en la Marketplace.
Jen la klarigo.

<!--more-->

## Rakonta tempo

Jen kiam mi sidas sur maljuna seĝon, ĝustigas miajn okulvitrojn kaj legas al vi rakonton.

Iam, programisto[^fn-whos-this] sekvis lernilon por komenci krei integriĵon por malfermitkoda ilo.
Kial li devis instali etendaĵon de Microsoft en sia VSCodium, li ne trovis ĝin en la menuo.

La etendaĵo estis videbla en la retejo de la Marketplace, kaj ankaŭ en la sama menuo en VS Code.
Li pensis, ke Microsoft trovis manieron limigi la uzon de ĉi tiu etendaĵo nur al Microsoft-produktoj kaj tial elektis ilian programada medio por tiun lernadon.

Kelkajn monatojn poste, li provis instali alian etendaĵon, ĉi-foje senrilatan al Microsoft.
Tamen, ĝi ŝajnis neatingebla en la sama maniero.
Ĝi estis klare malfermita al ĉiuj[^fn-klavaro], kaj tamen ne aperis en la menuo _Etendaĵoj_.

Ĉi-foje, li volis kompreni, kaj rapide malkovris ke VSCodium ne uzas la Marketplace de Microsoft.
Anstataŭe, ĝi dependas de Open VSX.
La etendaĵoj pri kiuj li interesiĝis ĵus ne estis publikitigiaj tie.

[^fn-whos-this]: Ĝi ne estas tro da mistero, ni parolas pri mi.
[^fn-klavaro]: Ĝi estas etendaĵo por uzi la X-sistemon en VS Code/VSCodium.
Mi skribos pli pri tio en februaro.



## Kio estas Open VSX, and kial?

**Open VSX estas memgastigiebla, vendisto-neŭtrala, komunum-movita, malfermitkoda etendaĵa distribupunkto, alternativa al [Visual Studio Marketplace][ms-marketplace] de Microsoft**.

La etendaĵoj sendube estas unu el la plej grandaj fortoj de VS Code, sed vi eble ne volas uzi [propriet-licencitan Microsoft-binaron][vscode-not-open-source].
Ne problemo, estas alternativojn: VSCodium, Theia, Eclipse Che…
_Sed_ la Marketplace de Microsoft _ne_ estas malfermitkoda kaj ĝiaj uzkondiĉoj limigas tiujn ilojn aliri VS Code-etendaĵojn tiel.

> La Marketplace ebligas al vi aliri aŭ aĉeti produktojn aŭ servojn, kiuj estas dezajnitaj por labori kun kaj etendi la kapablojn de Microsoft Visual Studio, Visual Studio por Mac, Visual Studio Code, GitHub Codespaces, Azure DevOps, Azure DevOps Server kaj postaj produktoj kaj servoj (la "**En-Ampleksaj Produktoj kaj Servoj**") ofertitaj de ni kaj GitHub, Inc. ("**GitHub**")
> {{< ref-cite "ms-marketplace">}}

> Ofertoj de la Marketplace estas intencitaj por uzado nur kun En-Ampleksaj Produktoj kaj Servoj, kaj vi povas instali kaj uzi Merkatajn Ofertojn nur kun En-Ampleksaj Produktoj kaj Servoj.
> {{< ref-cite "ms-marketplace">}}

> Elŝuti etendaĵojn de la Marketplace de Microsoft por iu ajn uzo krom en produkto de Microsoft ankaŭ estas malpermesita.
> {{< ref-cite "eclipse-open-vsx" >}}

Microsoft do kreis grandan registron de malfermitkodaj etendaĵoj, sed limigas ilian uzon al siaj proprietaj solvoj ekskluzive.

Jen eniras Open VSX:

* Vi povas uzi malfermitkodan registron, kun [servilo disponebla por ĉiuj ĉe https://open-vsx.org][open-vsx].
Ambaŭ Theia kaj VSCodium uzas ĝin defaŭlte.
* Vi eĉ povas gastigi ĝin mem.

Tiel, vi povas kontroli la fontojn kaj de via programada medio kaj de la etendaĵa deponejo.


## Eldonu viajn etendaĵojn al Open VSX

Se vi estas prizorganto de etendaĵo por VS Code kaj vi volas ĝin havebla al malfermitkodaj programadaj medioj, vi ankaŭ prenu tempon por publikigi ĝin al Open VSX.

La kompleta proceduro ne estas malfacila:

> 1. Registriĝi ĉe open-vsx.org per GitHub OAuth[^fn-github-oauth].
> 2. Kreu alirĵetonon (access token) kaj kopiu ĝin.
> 3. Rulu `npx ovsx create-namespace <publisher> --pat <token>` kun la eldonejo (publisher) specifita en la package.json de la etendaĵo.
> 4. Rulu `npx ovsx publish --pat <token>` en la dosierujo de la etendaĵo, kiun vi volas publikigi..
> {{< ref-cite "eclipse-open-vsx" >}}

Ankaŭ eblas krei tirpeto (_pull request_) al [deponejo de GitHub uzata por nutri la registron][ovsx-seeding-repo].


[^fn-github-oauth]: Ironie, oni devas uzi proprietan solvon por aŭtentigi al malfermitkoda registro.
Kompromisoj foje estas necesaj.


## Fino

Estas domaĝe, ke Microsoft ŝlosas malfermitkodajn etendaĵojn al siaj propraj sistemoj, sed Open VSX estas ĉi tie por korekti tiun maljuston.


{{% references %}}

[vscode-not-open-source]: {{< relref path="/blog/2022/01-14-vscode-not-open-source" >}}
[ovsx-seeding-repo]: https://github.com/open-vsx/publish-extensions
