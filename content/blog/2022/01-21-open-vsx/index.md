---
date: 2022-01-21T7:00:00+01:00
title: Open VSX Is an Open-Source Alternative to Microsoft's Marketplace
#subtitle: A witty line as a subtitle
slug: open-vsx-open-source-alternative-microsoft-marketplace
description: |-
  Microsoft's Marketplace is an awesome repository for open source extensions but, paradoxically, open-source editors are not allowed to connect to it.
  Open VSX is here to provide an alternative.
author: chop
categories: [ software-creation, software ]
tags: [ open-source, programming, web-dev ]
keywords: [ Open VSX, VS Code, VSCodium, Theia, marketplace, extensions, Microsoft, open source ]

references:
- id: open-vsx
  name: Open VSX Registry
  title: false
  url: https://open-vsx.org/
  lang: en
- id: ms-marketplace
  name: Terms of Use of Microsoft's Marketplace
  title: false
  url: https://marketplace.visualstudio.com/VSCode
  lang: en
- id: eclipse-open-vsx
  name: 'Eclipse Open VSX: A Free Marketplace for VS Code Extensions'
  title: true
  date: 03/2020
  url: https://www.eclipse.org/community/eclipse_newsletter/2020/march/1.php
  lang: en
  author:
    name: Sven Efftinge, Miro Spönnemann
---

When working with VSCodium, it's possible you can't find an extension you've seen in the marketplace.
Here's why.

<!--more-->

## Storytime

This is the piece where I sit like a grampa in an old chair and I put on my glasses to read you a story.

Once upon a time, a developer[^fn-whos-this] followed a tutorial to start developing an integration for an open-source solution.
The guide told him to install a Microsoft extension in his VS Code.
So, he opened the _Extensions_ menu of his VSCodium and searched for it, but got no result.

Sure enough, the extension was visible on the marketplace on the web and in VS Code, but not in VSCodium.
He thought Microsoft had found a way to lock this particular extension for non-VS Code editors and used Microsoft's editor to go on with his learning.

A few months later, he tried to install another extension, which was absolutely not related to Microsoft, and it appeared to be unavailable, too.
That one[^fn-klavaro] was clearly open to everyone, and yet, it wasn't available in the _Extensions_ menu.

He decided to investigate, this time, and discovered that VSCodium doesn't use Microsoft's marketplace, but Open VSX instead.
The extensions he wanted just hadn't been published to that one.

[^fn-whos-this]: OK, you got me: it was I who lived that story.
[^fn-klavaro]: It was an extension to enable the X-system in VS Code/VSCodium.
I'll write more about that in February.


## What's Open VSX, and Why?

**Open VSX is a self-hostable, vendor-neutral, community-driven, open-source alternative extension outlet to Microsoft's [Visual Studio Marketplace][ms-marketplace]**.

VS Code's extensions are undoubtedly one of its greatest strengths, but you may not want to use [Microsoft's binaries under a proprietary license][vscode-not-open-source].
That's OK, there are alternatives: VSCodium, Theia, Eclipse Che…
_But_ Microsoft's Marketplace is _not_ open source and its terms of use limit those tools from accessing VS Code extensions that way.

> The Marketplace enables you to access or purchase products or services which are designed to work with and extend the capabilities of Microsoft Visual Studio, Visual Studio for Mac, Visual Studio Code, GitHub Codespaces, Azure DevOps, Azure DevOps Server, and successor products and services (the “**In-Scope Products and Services**”) offered by us and GitHub, Inc. (“**GitHub**”).
> {{< ref-cite "ms-marketplace">}}

> Marketplace Offerings are intended for use only with In-Scope Products and Services and you may install and use Marketplace Offerings only with In-Scope Products and Services.
> {{< ref-cite "ms-marketplace">}}

> Downloading extensions from the Microsoft Marketplace for any use other than in Microsoft products is prohibited as well.
> {{< ref-cite "eclipse-open-vsx" >}}

So Microsoft created a wide registry of open-source products, but restrict their use to their proprietary solutions exclusively.

And here comes Open VSX:

* You get to use an open source registry, with [one server available for everyone at https://open-vsx.org][open-vsx].
Both Theia and VSCodium use this registry by default.
* You can also host it yourself.

Thus, you can have control over the sources of both your IDE and the extension repository.


## Publish Your Extensions to Open VSX

If you're a maintainer of a VS Code extension and you'd like to make sure it's available for open source editors too, you should also take the time to publish it to Open VSX.

The full procedure is not that difficult:

> 1. Register on open-vsx.org through GitHub OAuth[^fn-github-oauth].
> 2. Create an access token and copy it.
> 3. Run `npx ovsx create-namespace <publisher> --pat <token>` with the publisher name specified in extension's package.json.
> 4. Run `npx ovsx publish --pat <token>` in the directory of the extension you want to publish.
> {{< ref-cite "eclipse-open-vsx" >}}

There's also the possibility to create a pull request to a [seeding GitHub repository][ovsx-seeding-repo].


[^fn-github-oauth]: You may notice the irony of using a proprietary solution to log in to an open source registry, but compromises are necessary sometimes.


## The End

It's too bad that Microsoft locking open source extensions to its own systems through its terms of use, but Open VSX's here to help make them available to everyone.


{{% references %}}

[vscode-not-open-source]: {{< relref path="/blog/2022/01-14-vscode-not-open-source" >}}
[ovsx-seeding-repo]: https://github.com/open-vsx/publish-extensions
