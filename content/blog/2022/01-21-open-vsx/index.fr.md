---
date: 2022-01-21T7:00:00+01:00
title: Open VSX, une alternative ouverte au marketplace de Microsoft
#subtitle: A witty line as a subtitle
slug: open-vsx-alternative-open-source-marketplace-microsoft
description: |-
  Le marketplace de Microsoft est un dépôt merveilleux pour les extensions open source, mais les produits open source n'ont paradoxalement pas le droit de s'y connecter.
  Open VSX est là pour offrir une laternative.
author: chop
categories: [ software-creation, software ]
tags: [ open-source, programming, web-dev ]
keywords: [ Open VSX, VS Code, VSCodium, Theia, marketplace, extensions, Microsoft, open source ]

references:
- id: open-vsx
  name: Open VSX Registry
  title: false
  url: https://open-vsx.org/
  lang: en
- id: ms-marketplace
  name: Terms of Use of Microsoft's Marketplace
  title: false
  url: https://marketplace.visualstudio.com/VSCode
  lang: en
- id: eclipse-open-vsx
  name: 'Eclipse Open VSX: A Free Marketplace for VS Code Extensions'
  title: true
  date: 03/2020
  url: https://www.eclipse.org/community/eclipse_newsletter/2020/march/1.php
  lang: en
  author:
    name: Sven Efftinge, Miro Spönnemann
---

Si vous travaillez avec VSCodium, il peut arriver que vous ne trouviez pas une extension pourtant disponible dans le marketplace.
Voici l'explication.

<!--more-->

## Père Codeur, raconte-nous une histoire

C'est le moment où je m'assois dans une chaise à bascule, j'ajuste mes lunettes et je vous lis une histoire.

Il était une fois un développeur[^fn-whos-this] qui suivait un tutoriel pour apprendre à développer une intégration sur un produit ouvert.
Le guide lui indiqua d'installer une extension Microsoft pour son VS Code.
Il ouvrit donc le menu _Extensions_ de son VSCodium et la chercha, sans succès.

L'extension était pourtant bien visible sur le site web du marketplace et dans VS Code, mais pas dans VSCodium.
Il pensa que Microsoft avait trouvé un moyen de limiter l'utilisation de cette extension aux produits Microsoft uniquement et choisit donc leur éditeur pour continuer son apprentissage.

Quelques mois plus tard, il tenta d'installer une autre extensions, qui n'avait cette fois aucun lien avec Microsoft.
Elle apparut cependant indisponible de la même façon.
Elle était clairement ouverte à tous[^fn-klavaro], et n'apparaissait pourtant pas dans le menu _Extensions_.

Il prit cette fois-ci la décision de découvrir le fin mot de l'histoire et apprit rapidement que VSCodium n'utilise pas le marketplace de Microsoft.
Au lieu de ça, il s'appuie sur Open VSX.
Les extensions qui l'intéressaient n'y avaient juste pas été publiées.


[^fn-whos-this]: D'accord, il n'y a pas trop de mystère, on parle de moi.
[^fn-klavaro]: Il s'agit d'une extension pour activer le système X dans VS Code/VSCodium.
Plus à ce sujet courant février.


## Open VSX, c'est quoi et pourquoi ?

**Open VSX est un point de distribution d'extensions alternatif au [Visual Studio Marketplace][ms-marketplace], open source, géré par la communauté, indépendant des fournisseurs, et autohébergeable**.

Les extensions sont certainement l'une des plus grandes forces de VS Code, mais vous n'avez peut-être pas envie d'utiliser le [binaire de Microsoft sous licence propriétaire][vscode-not-open-source].
Pas de souci, il y a des alternatives : VSCodium, Theia, Eclipse Che…
_Mais_ la marketplace de Microsoft _n'est pas_ open source et ses conditions d'utilisations ne permettent pas à ces outils de s'y connecter.

That's OK, there are alternatives: VSCodium, Theia, Eclipse Che…
_But_ Microsoft's Marketplace is _not_ open source and its terms of use limit those tools from accessing VS Code extensions that way.

> La Marketplace vous permet d'accéder ou acheter des produits et services qui sont conçus pour fonctionner et étendre les capacités de Microsoft Visual Studio, Visual Studio pour Mac, Visual Studio Code, GitHub Codespaces, Azure DevOps, Azure DevOps Server, et les produits et services qui leur succèdent (les "**Produits et services du champ d'application**) offerts par nous et GitHub, Inc. ("**GitHub**").
> {{< ref-cite "ms-marketplace">}}

> Les offres Marketplace sont destinées à être utilisées uniquement avec les produits et services du champ d'application, et vous pouvez installer et utiliser les offres Marketplace uniquement avec les produits et services du champ d'application.
> {{< ref-cite "ms-marketplace">}}

> Le téléchargement d'extensions depuis la marketplace Microsoft pour tout usage autre que dans un produit de Microsoft est interdit également.
> {{< ref-cite "eclipse-open-vsx" >}}

Microsoft a donc créé un vaste registre de produits open source, mais a restreint leur utilisation exclusivement à leurs solutions propriétaires.

C'est ici qu'arrive Open VSX :

* Vous pouvez utiliser un registre open source, avec [un serveur disponible pour tous à https://open-vsx.org][open-vsx].
Theia comme VSCodium l'utilisent par défaut.
* Vous pouvez même choisir de l'héberger vous-même.

Ainsi, vous pouvez contrôler les sources aussi bien de votre IDE que du registre d'extensions.


## Publiez vos extensions dans Open VSX

Si vous maintenez une extension VS Code et que vous souhaitez qu'elle soit disponible aux éditeurs ouverts, vous devriez prendre cinq minutes pour la publier dans Open VSX.

La procédure complète n'est pas compliquée :

> 1. Enregistrez-vous sur open-vsx.org via GitHub OAuth[^fn-github-oauth].
> 2. Créez un access token et copiez-le.
> 3. Exécutez la commande `npx ovsx create-namespace <publisher> --pat <token>` avec le publisher renseigné dans le package.json de l'extension.
> 4. Exécutez la commandeRun `npx ovsx publish --pat <token>` dans le répertoire de l'extension que vous voulez publier.
> {{< ref-cite "eclipse-open-vsx" >}}

Il y a également la possibilité de créer une _pull request_ vers [un dépôt GitHub servant à alimenter le registre][ovsx-seeding-repo].


[^fn-github-oauth]: On pourra noter l'ironie : il faut utiliser une solution propriétaire pour s'authentifier à un registre open source. Il faut parfois savoir faire des compromis.


## Fin

Que Microsoft s'accapare des extensions open source pour ses propres systèmes est fort dommage, mais Open VSX est là pour réparer cette injustice.


{{% references %}}

[vscode-not-open-source]: {{< relref path="/blog/2022/01-14-vscode-not-open-source" >}}
[ovsx-seeding-repo]: https://github.com/open-vsx/publish-extensions
