---
date: 2021-05-03T07:00:00+02:00
title: Prefer Standard Fonts for Your Website
subtitle: Better UX, lighter footprint…
slug: fonts-web-ecodesign
description: |-
  Sometimes, we want to use a custom font so that our website will stand out.
  Sometimes, it's a requirement from our client.
  Yet, there is cause not to do so.
  Here are reasons and advices for sticking to standard, web-safe fonts.
author: chop
categories: [ software-creation ]
tags: [ web-dev, typography, sustainable-it, ux ]
keywords: [ footprint, ecodesign, font, ui, ux, serif ]

references:
- id: cnumr-ecoconception
  name: Référentiel d'écoconception web
  url: https://collectif.greenit.fr/ecoconception-web/
  lang: fr
  author:
    name: Collectif Conception responsable de service numérique
- id: system-font-stack
  name: "System Font Stack: its history and rationale"
  title: true
  url: https://medium.com/towards-more-beautiful-web-typography/survey-system-font-stack-5f73a3b39776
  date: March 2020
  lang: en
  author:
    name: Masa Kudamatsu, Medium
- id: web-safe-fonts
  name: Best Web Safe Fonts That Work With HTML and CSS
  title: true
  url: https://templatetester.com/blog/web-safe-fonts-html-css/
  lang: en
  author:
    name: Michael, author at How to Create a Website
- id: fontsarena-os
  name: OSFonts
  url: https://fontsarena.com/articles-about/os-fonts/
  lang: en
  author:
    name: FontsArena
- id: cssfontstack
  name: CSS Fonts
  url: https://www.cssfontstack.com/
  lang: en
  author:
    name: Dan's Tools
- id: designed-to-last
  name: This Page is Designed to Last
  title: true
  url: https://jeffhuang.com/designed_to_last/
  date: December 2019
  lang: en
  author:
    name: Jeff Huang
- id: reduce-webfont-size
  name: Reduce WebFont Size
  title: true
  url: https://web.dev/reduce-webfont-size/
  date: August 2019
  lang: en
  author:
    name: Ilya Grigorik
---


When designing a website, we're often tempted to use custom webfonts to make it stand out.
Yet, the French collective for a responsible design of digital services provides one simple rule in their referential for web ecodesign:

> Use standard typefaces
>
> {{< ref-cite "cnumr-ecoconception" >}}

Let's have a look at what's behind this rule.

<!--more-->


## Standard VS Custom Typefaces

### Definitions

On a web page, you can use CSS to control which fonts are used to display content.
You can specify a font stack, a list of fonts that will be tried and selected if available.

For instance, if you write the rule `font-family: Helvetica, Arial, sans-serif;`, the browser will first try if Helvetica is present.
If it is, it will use it; otherwise, it will do the same with Arial.
Finally, if all other have failed, it will discover `sans-serif` and use the OS's default sans serif.

But you can also use the `@font-face` CSS at-rule to get finer control and use custom typefaces.

```css
@font-face {
  font-family: "Open Sans";
  src: url("/fonts/OpenSans-Regular-webfont.woff2") format("woff2"),
       url("/fonts/OpenSans-Regular-webfont.woff") format("woff");
}
```

The example above would tell the browser which files to download to dislay content obeying the `font-family: 'Open Sans'` rule.

As long as you don't rely on a typeface defined in a `@font-face` with an `src` attribute, you are using **standard typefaces**, relying instead on what will be installed on your viewer's device.
I'll oppose this to the **custom, `@font-face` defined typefaces**.


### Pros and Cons

When you work for a client, it's not unusual to have a picture and be required to make something that's exactly the same.
Using a custom typeface, you're sure **the website will display the same way on all devices**.
Well, not really anymore, because of the variety of screens and sizes on the market.

On the other hand, **the browser will have to download the font**.
This has several consequences.
The most obvious is that you need as many more requests as there are fonts, you use a bit more bandwidth, [your website's footprint is a bit heavier][footprint-digital].
They also often induce a **slight UX degradation**, as the page is first rendered without the custom font and everything shifts when the font is finally available, sometimes moving links just as the user clicks on it.
Or it may just add some seconds to the loading time, which is just as detestable for a user.

Relying on standard fonts is the contrary: **it shows immediately**, but it implies that **rendering will be (slightly) different on different devices**.
If you choose Helvetica as a default font, it will probably display on Apple devices but not on Window, Linux or Android.
You just have to wonder: is it that problematic?

Standard fonts have one additional advantage: **they will integrate more smoothly with the OS's interface**.
For user experience, not breaking the user's habits is a best practice.
See for instance the [System Font Stack][system-font-stack] and its implementation by Github or Twitter.



## Construct Your Standard Font Stack

Basically, it's not that complicated:

1. Define the OSs you'll be targeting.
2. Find a set of fonts present on most of these OSs.
[FontsArena][fontsarena-os] offers a guide of the default fonts, [CSS Fonts][cssfontstack] gives availability statistics for common fonts.
3. Define which font you'd like to be used over which and write them in that order.
4. **Always terminate your font stack with either `sans-serif`, `serif` or `monospace`**.
If the browser can't find any of the fonts you've specified, it'll at least know which font family will be the best fit.

As examples, I can propose the three font stacks I use for this website:
* Titles: `'Ubuntu Condensed', 'Open Sans Condensed', 'Dejavu Sans Condensed', 'Arial Narrow', 'Segoe UI', 'Helvetica Neue', Helvetica, Verdana, Arial, sans-serif`.
* Code excerpts: `'Fira Code', Monoid, 'Ubuntu Mono', 'Lucida Sans Typewriter', 'Lucida Console', monaco, 'Bitstream Vera Sans Mono', monospace`.
* Everything else: `Ubuntu, Geneva, Verdana, Tahoma, sans-serif`.

Now, looking at the "code" stack, you may notice fonts that will be absent from most OSs (e.g. [Fira Code and Monoid][coding-fonts]).
Not an issue: if they're available, it'll look more like it does on my machine; otherwise, it'll be just as legible, which is the real purpose.

If you're lacking inspiration, you can start with [Template Tester's list of 13 web-safe fonts][web-safe-fonts].
Despite their name, keep in mind that these are only [_likely_ to be available][wiki-web-safe-fonts] on all OSs.
For instance, _Times New Roman_ and _Arial_ are not installed by default on most Linux distributions.



## What If You _Need_ Custom Typefaces

Sometimes, you don't have a choice: the client's designer or graphical charter enforces the use of a specific font.
I'd recommend limiting those as much as possible.
To quote from a page "designed to last:"

> **Stick with the 13 web safe fonts +2** – we're focusing on content first, so decorative and unusual typefaces are completely unnecessary.
> Stick with a small palette and the 13 web-safe fonts.
> Okay, so maybe you really need a neo-grotesque typeface that's not Arial/Helvetica, or you need a geometric typeface.
> Then go with what's minimally necessary, like Roboto (for neo-grotesque) and Open Sans (for geometric); they're the two most popular typefaces on Google Fonts so most likely to already be cached on your users' computer.
> Besides those +2 fonts, your focus should be about delivering the content to the user effectively and making the choice of font to be invisible, rather than stroking your design ego.
> Even if you use Google Fonts, they don't need to be hotlinked.
> Download the subset you need and locally serve them from your own folders. 
> 
> {{< ref-cite "designed-to-last" >}}

Just a note: it may seem obvious, but if you need a font only to display a brand name, you'd better use an image ([bitmap or svg][bitmap-vs-svg]).

In any case, if you have to use custom typefaces, you should [have a look at best practices][reduce-webfont-size].
This will help reducing their size and save some bandwidth, thus decreasing your footprint.


{{% references %}}

[wiki-web-safe-fonts]: https://en.wikipedia.org/wiki/Web_typography#Web-safe_fonts

[digital-footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}} "The Footprint of the Digital World"
[coding-fonts]: {{< relref path="/blog/2019/12-18-coding-fonts" >}} "Choose Your Font for Coding"
[bitmap-vs-svg]: {{< relref path="/blog/2021/03-29-svg-instead-of-images" >}} "From Bitmap Images to Inline SVGs"
