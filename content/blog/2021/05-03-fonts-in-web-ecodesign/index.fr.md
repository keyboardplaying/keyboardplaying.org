---
date: 2021-05-03T07:00:00+02:00
title: Préférez les fontes standard pour votre site web
subtitle: Meilleure UX, empreinte plus légère…
slug: polices-web-ecoconception
description: |-
  Parfois, on a envie d'une police personnalisée pour que notre site sorte du lot.
  Parfois, c'est une contrainte de notre client.
  Pourtant, il y a des arguments contre ce choix.
  Voici quelques raisons et conseils pour se limiter à des polices standards.
author: chop
categories: [ software-creation ]
tags: [ web-dev, typography, sustainable-it, ux ]
keywords: [ footprint, ecodesign, font, ui, ux, serif ]

references:
- id: cnumr-ecoconception
  name: Référentiel d'écoconception web
  url: https://collectif.greenit.fr/ecoconception-web/
  lang: fr
  author:
    name: Collectif Conception responsable de service numérique
- id: system-font-stack
  name: "System Font Stack: its history and rationale"
  title: true
  url: https://medium.com/towards-more-beautiful-web-typography/survey-system-font-stack-5f73a3b39776
  date: March 2020
  lang: en
  author:
    name: Masa Kudamatsu, Medium
- id: web-safe-fonts
  name: Best Web Safe Fonts That Work With HTML and CSS
  title: true
  url: https://templatetester.com/blog/web-safe-fonts-html-css/
  lang: en
  author:
    name: Michael, author at How to Create a Website
- id: fontsarena-os
  name: OSFonts
  url: https://fontsarena.com/articles-about/os-fonts/
  lang: en
  author:
    name: FontsArena
- id: cssfontstack
  name: CSS Fonts
  url: https://www.cssfontstack.com/
  lang: en
  author:
    name: Dan's Tools
- id: designed-to-last
  name: This Page is Designed to Last
  title: true
  url: https://jeffhuang.com/designed_to_last/
  date: December 2019
  lang: en
  author:
    name: Jeff Huang
- id: reduce-webfont-size
  name: Reduce WebFont Size
  title: true
  url: https://web.dev/reduce-webfont-size/
  date: August 2019
  lang: en
  author:
    name: Ilya Grigorik
---


Lorsque l'on conçoit un site web, on est souvent tenté d'utiliser des polices personnalisée pour qu'il soit différent du reste.
Pourtant, dans leur référentiel d'écoconception web, le collectif pour la conception responsable de services numériques indique la règle suivante :

> Favoriser les polices standards
>
> {{< ref-cite "cnumr-ecoconception" >}}

Penchons-nous un peu plus sur cette règle.

<!--more-->


## Fontes standards ou personnalisées

### Définitions

Sur une page web, vous pouvez utiliser le CSS pour contrôler quelles polices sont utilisées pour l'affichage.
Vous pouvez définir une pile de fonte (_font stack_), c'est-à-dire une liste de polices à essayer.

Par exemple, si vous écrivez la règle `font-family: Helvetica, Arial, sans-serif;`, le navigateur testera d'abord si Helvetica est présente.
Si c'est le cas, c'est la police qui sera utilisée ; sinon, il fera la même chose avec Arial.
En dernier recours, si aucune autre fonte n'est présente, il tombera sur le mot-clé générique `sans-serif` et utilisera la police sans empattement par défaut du système.

Il est également possible, avec la règle `@font-face`, d'obtenir un contrôle plus fin des polices.

```css
@font-face {
  font-family: "Open Sans";
  src: url("/fonts/OpenSans-Regular-webfont.woff2") format("woff2"),
       url("/fonts/OpenSans-Regular-webfont.woff") format("woff");
}
```

L'exemple ci-dessous indique au navigateur quels fichiers télécharger pour respecter la règle `font-family: 'Open Sans'`.

Tant que vous ne vous appuyez pas sur un `@font-face` à l'aide d'un attribut `src`, vous utilisez une **fonte standard**, en faisant confiance à ce qui est installé sur l'appareil de votre visiteur.
J'oppose ceci aux **fontes personnalisées, définies par `@font-face`**.


### Avantages et inconvénients

Lorsque vous travaillez pour un client, il n'est pas inhabituel de recevoir une image et devoir créer un site qui en soit la reproduction parfaite.
En utilisant une fonte personnalisée, vous vous assurez que **le site sera affiché de la même manière sur tous les appareils**.
Enfin, plus tant que ça, en raison de la variété d'écrans et de tailles sur le marché.

D'un autre côté, **le navigateur devra télécharger la fonte**.
Cela a plusieurs conséquences.
Le plus évident est que votre site fera autant de requêtes supplémentaires que vous aurez de polices, vous utiliserez un peu plus de bande-passante, [l'empreinte de votre site sera un peu plus lourde][footprint-digital].
Cela implique souvent **une légère dégradation de l'expérience utilisateur**, car la page sera d'abord affichée sans la police personnalisée et tout risque de clignoter ou changer de place lorsque la fonte sera finalement disponible, faisant parfois bouger des liens sur lesquels l'utilisateur s'apprête à cliquer.
Ou cela pourrait simplement ajouter quelques secondes au temps de chargement, ce qui est tout aussi détestable pour un utilisateur.

S'appuyer sur les fontes standards est le contraire : **l'affichage est immédiat**, mais il implique **un rendu (légèrement) différent selon les appareils**.
Si vous choisissez Helvetica comme police par défaut, elle s'affichera certainement sur un appareil Apple mais pas sous Windows, Linux ou Android.
Il faut juste vous poser une question : est-ce vraiment un problème ?

Les fontes standards on un avantage additionnel : **elles s'intègrent plus naturellement avec l'interface du système d'exploitation**.
Pour l'expérience utilisateur, ne pas déroger aux habitudes est une bonne pratique.
Vous pouvez en particulier regarder la [« pile de fontes système »][system-font-stacks] et son implémentation par Medium, Github ou Twitter.



## Construisez votre pile de fontes

Le principe de base n'est pas compliqué :

1. Définissez les systèmes d'exploitation que vous ciblez.
2. Trouvez une collection de fontes présentes sur la plupart de ces systèmes.
[FontsArena][fontsarena-os] offre un guide des polices par défaut, [CSS Fonts][cssfontstack] donne de statistiques de disponibilité pour des fontes habituelles.
3. Définissez quelles polices vous préférez à quelles autres et écrivez-les dans cet ordre.
4. **Terminez toujours votre pile par `sans-serif`, `serif` et `monospace`**.
Si le navigateur ne peut trouver aucune des autres fontes que vous avez spécifiées, il saura au moins quelle famille retenir.

À titre d'exemples, je peux vous proposer les trois piles de fontes que j'utilise pour ce site :
* Titres : `'Ubuntu Condensed', 'Open Sans Condensed', 'Dejavu Sans Condensed', 'Arial Narrow', 'Segoe UI', 'Helvetica Neue', Helvetica, Verdana, Arial, sans-serif`.
* Extraits de code : `'Fira Code', Monoid, 'Ubuntu Mono', 'Lucida Sans Typewriter', 'Lucida Console', monaco, 'Bitstream Vera Sans Mono', monospace`.
* Tout le reste : `Ubuntu, Geneva, Verdana, Tahoma, sans-serif`.

Si vous regardez ma pile « code », vous verrez que certaines polices seront certainement absentes de la plupart des systèmes (p.ex. [Fira Code and Monoid][coding-fonts]).
Ce n'est pas grave : si elles sont disponibles, cela ressemblera à ce que j'ai sur ma machine ; dans le cas contraire, ce sera tout aussi lisible, ce qui est le véritable but.

Si vous manquez d'inspiration, vous pouvez commencer avec [la liste de 13 fontes « _web-safe_ » de Template Tester][web-safe-fonts].
En dépit de leur nom, souvenez-vous malgré tout qu'il est seulement [_probable_][wiki-seb-safe-fonts] sur tous les OSs.
Par exemple, _Times New Roman_ et _Arial_ ne sont pas installées par défaut sur la plupart des distributions Linux.



## Si vous avez _besoin_ de fontes personnalisées

Parfois, vous n'avez pas le choix : le designer du client ou sa charte graphique impose l'usage d'une police spécifique.
Je recommanderais d'éviter ceci autant que possible.
Pour citer une page « conçue pour durer » :

> **Limitez-vous aux 13 police web-safe +2** – on se concentre sur le contenu en premier lieu, donc les fontes décoratives et inhabituelles sont complètement inutiles.
> Limitez-vous à une petite palette et les 13 polices web-safe.
> OK, vous avez peut-être réellement besoin d'une police néo-grotesque qui n'est pas Arial/Helvetica, ou vous avez besoin d'une fonte géométrique.
> Dans ce cas, choisissez le strict minimum nécessaire, comme Roboto (pour le néo-grotesque) ou Open Sans (pour le géométrique) ; ce sont les deux polices les plus populaires sur Google Fonts et sont donc certainement déjà dans le cache du navigateur de vos ordinateurs.
> Outre ces +2 fontes, vous devriez vous concentrer pour faire parvenir efficacement votre contenu à l'utilisateur et rendre le choix des fontes invisibles, plutôt que de caresser votre ego de designer.
> Même si vous utilisez Google Fonts, vous n'avez pas besoin d'un lien direct.
> Télécharger le sous-ensemble dont vous avez besoin et servez-le depuis vos propres dossiers.
> 
> {{< ref-cite "designed-to-last" >}}

Une petite remarque : cela peut paraître évident, mais si vous utilisez une fonte uniquement pour afficher le nom ou le logo d'une marque, vous avez certainement intérêt à le remplacer par une image ([bitmap ou SVG][bitmap-vs-svg]).

Dans tous les cas, si vous utilisez des polices personnalisées, vous devriez [regarder les bonnes pratiques][reduce-webfont-size].
Cela vous aidera à réduire leur taille, économisant par la même occasion un peu de bande-passante et allégeant votre empreinte.


{{% references %}}

[wiki-web-safe-fonts]: https://en.wikipedia.org/wiki/Web_typography#Web-safe_fonts

[digital-footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}} "L'empreinte du monde numérique"
[coding-fonts]: {{< relref path="/blog/2019/12-18-coding-fonts" >}} "Choisir une police pour coder"
[bitmap-vs-svg]: {{< relref path="/blog/2021/03-29-svg-instead-of-images" >}} "D'images bitmap à des SVG en ligne"
