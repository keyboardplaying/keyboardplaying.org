---
date: 2021-06-28T07:00:00+02:00
title: C'est quoi, l'espéranto ?
#subtitle: A witty line as a subtitle
slug: introduction-esperanto
description: |-
  Depuis que j'ai commencé à apprendre l'Espéranto, on me pose certaines questions récurrentes lorsque j'en parle.
  Je me suis dit que les réponses pourraient intéresser les curieux.
  Aussi, ai-je décidé de les partager ici.
author: chop
#categories: [ software-creation ]
tags: [ esperanto ]
keywords: [ espéranto, langue construite, open source, apprentissage, écriture inclusive ]

references:
- id: wiki-esperanto
  name: Espéranto
  title: true
  url: https://fr.wikipedia.org/wiki/Esp%C3%A9ranto
  lang: fr
  author:
    name: Wikipedia
- id: wiki-esperantophone
  name: Espérantophone natif
  title: true
  url: https://fr.wikipedia.org/wiki/Esp%C3%A9rantophone_natif
  lang: fr
  author:
    name: Wikipedia
- id: wiki-esperantujo
  name: Espérantie
  title: true
  url: https://fr.wikipedia.org/wiki/Esp%C3%A9rantie
  lang: fr
  author:
    name: Wikipedia
- id: mastodon-aleks
  name: Aleks Andre
  url: https://esperanto.masto.host/@XanderLeaDaren
  lang: eo
  hide: true
---

J'ai débuté l'apprentissage de l'Espéranto il y a quelques semaines.
Depuis lors, lorsque j'en discute avec quelqu'un, je réponds (avec plaisir) à une série de questions sur le sujet.
Parce que d'autres que mes interlocuteurs pourraient s'interroger, j'ai décidé de partager ici les réponses aux plus communes d'entre elles.

<!--more-->

## C'est quoi, l'espéranto ?

L'espéranto est une langue construite.

Qu'est-ce qu'une langue construite, vous demandez-vous ?
La plupart des langues sont le fruit d'évolutions depuis des temps immémoriaux.
L'espagnol et l'italien sont les descendants directs du latin, le français inclut en outre des racines grecques.
Elles ont changé au cours des siècles, se mettant à jour pour correspondre aux usages.
Nous avons connu l'ancien français et le moyen français, que la plupart des Français d'aujourd'hui peineraient à déchiffrer.

Les langues construites ne sont pas nées ainsi.
Elles sont _conçues_ : à un moment, quelqu'un crée un ensemble de règles et un vocabulaire, et ainsi naît un nouveau langage.
Pour l'espéranto, le moment était 1887 et le quelqu'un était un jeune ophtalmologue polonais de 19 ans, L. L. Zamenhof.


## Où est-ce parlé ?

À la fois partout et nulle part.

Si vous demandez de quel pays l'espéranto est la langue officielle, il n'y en a aucun, donc on pourrait dire que l'espéranto n'est parlé nulle part.
Bien que Wikipédia nous apprenne qu'il existe des [Espérantophones natifs][wiki-esperantophone], il est bien plus probable que cette langue soit apprise à un stade ultérieur de la vie.

Et pourtant, l'espéranto est présent tout autour du globe, au sein de 120 pays.
Un contributeur Wikipédia a utilisé les données de la _Universala Esperanto-Asocio_ (Association Universelle d'Espéranto, ou UEA, la plus grande association d'espérantistes) pour créer une carte des membres à travers le monde.

{{< figure src="Relative_number_of_Esperanto_association_members_by_country_(2020).svg" caption="Nombre de membres de l'UEA par million d'habitants, par pays, en 2020." link="https://en.wikipedia.org/wiki/File:Relative_number_of_Esperanto_association_members_by_country_(2020).svg" attr="Kwamikagami" attrlink="https://commons.wikimedia.org/wiki/User:Kwamikagami" lic="CC BY-SA 4.0" >}}

Plus un pays est sombre, plus vous avez de chances d'y croiser un espérantiste.


## Pourquoi aurait-on besoin d'une langue construite ? On n'en a pas déjà assez ?

Vous connaissez en fait plusieurs langues construites issues du monde du divertissement.
Le sindarin parlé par les Elfes de la Terre du Milieu fut inventé par l'auteur J. R. R. Tolkien, ainsi que plusieurs autres dans cet univers.
Le klingon de _Star Trek_ et le Dothraki dans la série _Game of Thrones_ sont construits également.

Mais le divertissement n'est pas la seule utilisation pour les langues construites.
L'espéranto fait partie d'une catégorie appelée « langues auxiliaires », des langues conçues dans le but de fournir une meilleure communication, en transcendant d'autres barrières notamment culturelles.
Parmi elles, je connaissais l'interlingua, le volapük[^fn-volapuk] et l'espéranto[^fn-source-connaissance].

[^fn-volapuk]: C'est assez difficile de définir le volapük comme « outil de communication » car son concepteur a voulu restreindre son usage à une élite.
[^fn-knowledge-vector]: Sur les trois, j'en connais deux via [Aleks][mastodon-aleks], grâce à qui j'ai finalement franchi le pas d'apprendre l'espéranto et qui a été d'une grande aide.


## Pourquoi tu aimes l'espéranto ?

J'aime que ce soit simple pour la majorité.
Du fait de sa conception, c'est une langue qui n'a pas d'exceptions, pas de verbes irréguliers, pas de questions sur la prononciation…
Zamenhof déclarait qu'il suffisait d'une heure pour apprendre sa grammaire, ce qui était proche de la vérité pour ceux ayant une connaissance des langues européennes.
L'espéranto est en effet basé sur ce qui était parlé en Europe à l'époque.

Le vocabulaire nécessite de l'apprentissage, comme pour toute nouvelle langue, mais en m'appuyant sur mes connaissances du français, de l'anglais et de l'allemand, je suis souvent capable de comprendre de nouveaux mots (voire de deviner certains).
La terminaison d'un mot indique sa nature (_-o_ désigne un nom, _-a_ un adjectif, _-e_ un adverbe, _-s_ un verbe conjugué…).
Cela signifie aussi que vous pouvez changer la nature d'un mot en modifiant le suffixe tout en conservant son sens grâce au radical.
Ceci permet de dire les choses de nombreuses façons, comme par exemple « _Tiu manĝo **estas bongusta**_ » (littéralement « Ce repas est bon ») ou « _Tiu manĝo **bongustas**_ » (approx. « Ce repas goûte bon »).

Un point qu'on ne peut pas ne pas aimer est que vous pouvez épeler un mot juste en l'écoutant.
Vous savez que ce n'est pas le cas en anglais et français ?
Nous avons des poèmes pour montrer les incohérences des prononciations de ces deux langues, mais c'est impossible en espéranto.
Tout comme l'italien, chaque lettre a un son qui lui est propre.
D'ailleurs, le nom des voyelles est leur prononciation, et celui des consonnes également suivi d'un _-o_ (T s'appelle donc _to_).

J'apprécie aussi le fait que l'espéranto [respecte beaucoup mieux les genres que d'autres langues]({{< relref path="/blog/2021/02-22-ecriture-inclusive" >}}).
Tout d'abord, chaque mot non neutre a une forme féminine.
On l'obtient toujours en prenant le masculin et en ajoutant une particule _-in-_ après le radical, tout comme le suffixe _-in_ en allemand.
Par exemple, le féminin d'_amiko_ (ami, _Freund_ en allemand) est _amik**in**o_ (_Freund**in**_ en allemand).
Par ailleurs, lorsqu'un groupe comprend des individus des deux genres, on ne part pas du principe que le masculin l'emporte.
Au lieu de ça, on préfère l'utilisation d'un préfixe _ge-_ (_Ambaŭ knaboj kaj knabinoj estas miaj **ge**amikoj._ Des garçons et des filles sont mes amis.).

On pourrait penser que, puisqu'il s'agit d'une langue construite, il n'existe pas de culture ou communauté.
C'est tout à fait faux !
Pour commencer, c'est la langue construite la plus parlée au monde.
Il existe également l'Espérantie (_Esperantujo_ ou  _Esperantio_, littéralement « la nation de l'espéranto »), qui regroupe tous les espérantistes.
Celle-ci a son drapeau (le _Verda Flago_, le drapeau vert) et un hymne (_La Espero_, un poème écrit par Zamenhof ; à sa première lecture lors d'une conférence, bien qu'il soit écrit dans une langue construite, plusieurs personnes dans l'audience ont été émues aux larmes).
Vous pouvez trouver de la littérature, de la musique, des podcasts et des chaînes Youtube[^fn-dro-locjo] en espéranto.

[^fn-dro-locjo]: L'association Espéranto-France a par exemple [publié une interview de Loïc](https://esperanto-france.org/esperanto-aktiv-120-decouverte) il y a quelques mois.
Il s'agit d'un astrophysicien français qui partage ses connaissances dans la langue de Zamenhof.
Il a notamment commenté en direct l'arrivée de Perseverance sur Mars, sur [sa chaîne Youtube, D-ro Loĉjo](https://www.youtube.com/channel/UCSE4dWlC9CWL4TEdqnaAEww).

{{< figure src="verda-flago.svg" caption="Le _Verda Flago_, le drapeau de la communauté espérantiste" link="https://en.wikipedia.org/wiki/File:Flag_of_Esperanto.svg" >}}

Un dernier point qui m'a séduit : la volonté de partager.
L'espéranto a été conçu comme un outil universel, mis à la disposition de tous.
Ce n'est pas sans me rappeler les valeurs du logiciel libre ou open source dans le monde du développement : c'est une création dont chacun doit pouvoir profiter s'il le souhaite.


## Comment tu apprends ?

J'ai démarré avec [Duolingo], qui est génial pour commencer et a l'immense avantage de permettre d'entendre la langue et de faire travailler la compréhension orale.
Si vous voulez apprendre par l'exemple, c'est un bon moyen de démarrer.
Ils ont des leçons d'Espéranto pour les anglophones et les francophones (ce que j'ai découvert plusieurs jours après avoir démarré la version anglaise).
La seule limitation à mes yeux est qu'il n'y a aucune explication[^fn-klarigoj-duolingo] : si vous ne comprenez pas par vous-même la règle de grammaire qui s'applique, il vous faudra chercher par vous même.

[^fn-klarigoj-duolingo]: [Jindra a remarqué][jindra-comment] qu'il existe un menu « _Tips_ » qui offre des explications, mais uniquement sur la version en ligne de Duolingo, pas dans l'application pour téléphone.

Pour éviter ceci, [Espéranto-France fournit des cours][ikurso], en ligne et avec un correcteur humain avec qui vous pourrez échanger.
J'ai prévu de faire le cours en dix leçons pour compléter mon approche Duolingo, et peut-être plus tard le cours plus avancé (_Gerda malaperis_).

Si cela ne vous convient pas, vous pouvez aussi regarder d'autres plateformes dédiées à l'espéranto.
[lernu] est une solution multilingue de référence, par exemple, mais il existe certainement bien d'autres dont j'ignore tout.

Oh, et bien entendu, l'échange reste le meilleur moyen d'apprendre.
Profitez de la large communauté, présente sur tous les réseaux.
Sur Mastodon, par exemple, vous pourriez croiser [Aleks][mastodon-aleks], l'ami qui m'a convaincu de me jeter à l'eau.


{{% references %}}
[duolingo]: https://www.duolingo.com/
[lernu]: https://lernu.net/en
[ikurso]: https://ikurso.esperanto-france.org/
[jindra-comment]: {{< ref path="/blog/2021/06-28-what-is-esperanto" lang="en" >}}#c3950230-d96c-11eb-9887-234a69c16dd9
