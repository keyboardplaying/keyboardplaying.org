---
date: 2021-12-20T07:00:00+01:00
title: Kio okazas? (decembro 2021)
#subtitle: A witty line as a subtitle
slug: kio-okazas
description: |-
  Esperanto and mail notifications are now on the menu.
  Ni ankaŭ aldonis respondaron pri gastaj artikoloj.
author: chop
categories: [ kp.org ]
tags: [ news, esperanto ]
keywords: [ esperanto, staticman, mailgun ]
---

Sep monatojn ekde la lasta bilanco, kvar ekde la lasta afiŝo.
Kio ŝanĝiĝis de tiam?

<!--more-->

## Kio ŝanĝiĝis?

### Esperanto (ekde junio)

Mi parolas pri ĝi de kelkaj monatoj, ĝi jam finiĝis: Keyboard Playing nun ekzistas en Esperanto.
Kiel ĉe le aliaj lingvoj, kiam paĝo ekzistas en Esperanto, estas la literojn "EO" apud la ikono {{< icon icon="flag" title="Flago" >}}.

Ankoraŭ ne multe tradukiĝis, kaj mi ne povas promesi, ke mi ĉiam verkos novajn paĝojn en Esperanto, sed bonvolu indiki io, kio vi ŝatus vidi tradukitaj.
Vi povas ankaŭ indiki miajn erarojn; mi scias, ke mi ne ankoraŭ ĉiam skribas bone.


### Retpoŝtaj sciigoj (ekde aŭgusto)

Ĝis antaŭ nelonge, komentistoj neniel povis scii ke iu respondis al ili se ili ne revenis.
Tiu nun estas riparata: kiam vi dividas noton en paĝo, vi povas elekti ricevi retpoŝtajn sciigojn por estontaj komentoj.
Kelkaj detaloj:

* Se vi abonas, vi ricevos retpoŝton ĉiufoje kiam nova komento aldoniĝas al la paĝo.
* Vi povas malaboni, kiam vi volas, uzante la ligon ĉe la fino de ĉio retpoŝto.
* Pro la ĜDPR, vi devos konfirmi vian volon registriĝi.
Se vi markas la elektobutonon, kiam vi enigas vian komenton, vi ricevos retpoŝton kun ligo, kiun vi devos alklaki por validigi vian abonon.
Se vi ne vidas tiun ligon, vi _ne_ ricevos la sciigojn.

La solvo estas kreita per [forko][staticwoman] de [Statikman][staticman], kaj per [Mailgun][mailgun]
Eble mi skribos pli pri tio en alia afiŝo.


### Gastaj artikoloj (ekde decembro)

Mi ricevis kelkajn retmesaĝojn de homoj, kiuj interesiĝis pri publikigado de gastaj artikolok.
Kiam mi respondis, la korespondado ĉiam ĉesis.
Mi komprenas, ke la kondiĉoj, kiujn ni povas proponi, ne estas la plej allogaj, sed sendi mesaĝon por rifuzi estus komuna ĝentileco.

Por ŝpari ĉi tiu perdon de tempo al mi kaj al tiuj homoj, mi skribis rapidan [respondaro pri la gastaj artikoloj en Keyboard Playing][guest-articles].
Se vi interesas, vi nun scias kion fari!


## _Tio_ okupis vian _tutan_ tempon?

Evidente ne, mi distriĝis kun aliaj agadoj.
Mi precipe verkis du tekstojn por konkursoj, esperante, ke ili estos publikigitaj.
[Unu el ili][voyage-demain] ne estis, mi ankoraŭ atendas respondon por la alia.

Mi ankaŭ pasigas multe da tempo sekvante kio fariĝas [la libro, kiun ni publikigis ĉi-jare][carrefour].
Ni sciis, ke ni ne gajnos multe da mono de ĝi, sed ĝis nun, ni eĉ ne vendis sufiĉe por kovri la elspezojn.
Mi eble diros pli pri tion, poste.
Verŝajne estas tro malfrue por Kirstnasko, sed se vi serĉas ian frujaran donacon por franclingvano, ne hezitu!


## Kaj post tio?

Mi havas neniu ideo.
Novaj ideoj venas al mi, por la retejo kaj ankaŭ por aliaj projektoj.
La sola afero, da kiu mi ne havas sufiĉe, estas tempo.

Mi ne povis fari unu afiŝon ĉiun monaton ĉi-jare, eble unu ĉiun duan monaton estos pli realisma por 2022.
Ni provos kaj vidos.

Mi deziras al vi ĉiuj feliĉan Kristnaskon kaj feliĉan novan jaron, kaj mi esperas, ke vi revenos en 2022.


[staticwoman]: https://github.com/hispanic/staticwoman
[staticman]: https://staticman.net
[mailgun]: https://www.mailgun.com/
[guest-articles]: {{< relref path="/about/guest-articles" >}}
[voyage-demain]: {{< ref path="/stories/2021/voyage-de-demain" lang="fr" >}}
[carrefour]: {{< ref path="/blog/2021/05-31-carrefour-disponible" lang="fr" >}}
