---
date: 2021-01-25T07:00:00+01:00
title: You probably shouldn't have "J2EE" on your résumé
subtitle: But maybe you want interviewers to ask about it…
slug: jee-not-j2ee
description: |-
    "J2EE" still is quite present on resumes and, more disturbingly, in job descriptions.
    You should be sure it's what you mean when using this initialism, as some people may be inclined to tease you.
author: chop
#categories: [ misc ]
tags: [ java ]
keywords: [ java, jee, j2ee, résumé, career ]
---

Recently, a colleague of mine came with a wide smile and said, "I have a candidate to interview. There's J2EE on his resume, but he's too young to have worked with it. I might have, not him."

That's a recurring joke among senior Java technicians: it's not uncommon for candidates to hear _J-2-E_ and write it _J2EE_ when they should simply use _JEE_.
It's not completely incorrect, but it may cause some confusion.

<!--more-->

## The Difference Between J2EE and JEE

To sum things up: J2EE and JEE are the same thing, but not in the same version.
In both cases, _J_ stands for _Java Platform_ and _EE_ means _Enterprise Edition_.

J2EE is thus _Java Platform 2, Enterprise Edition_.
This "2" may puzzle you, but it's related to Java 1.2, the first version to have its enterprise edition.
It took some years before the "1." simply vanished.
Yet, to make things more mysterious, the "2" remained until Java 1.5.

That means there has been J2EE (1.2), 1.3 and 1.4 until 2006, when JEE 5 (or Java EE 5) was released.
I was still in school at the time.
That's why, if you're still quite new to the world of software creation, I hope you've never had to maintain J2EE code.


## JEE Today

JEE has known several releases since then: from Java EE 5 to Java EE 8, there was not much to say (about its name, at least).
Yet, in 2017, Oracle announced it would submit Java EE to the Eclipse Foundation, which named it EE4J (Enterprise Edition for Java), but was forced to change it.
Oracle owns the trademark for the name "Java" and the Foundation couldn't use it, so they changed the meaning of JEE to _Jakarta EE_.

The familiar initialism was thus preserved and reminded some developers of the [Jakarta Project][jakarta-project] from the Apache Software Foundation, the foster home of several Java tools and utilities, some of which are still used to this day.


## Final Piece of Advice

To recruiters: don't use J2EE today.
Ever.
Unless it's a "migrating out of J2EE" project.
In any other case, candidates may flee.

More seriously, to candidates: be sure which of J2EE or JEE you should use.
If you use both, be prepared for interviewers to ask questions about those.


[jakarta-project]: https://en.wikipedia.org/wiki/Jakarta_Project
