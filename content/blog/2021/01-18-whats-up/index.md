---
date: 2021-01-18T07:00:00+01:00
title: What's up? (January 2021)
#subtitle: No funny subtitle found
slug: whats-up
description: |-
  The French version gained its own domain, progressive enhancement actually brings better support, the French version got its own domain and a side-project explains why KP remained silent.
author: chop
categories: [ kp.org ]
tags: [ news, sustainable-it, web-dev ]
keywords: [ carrefour des imaginaires, graceful degradation, support, internet explorer, IE, esbuild, rollup, download proxy ]
---


Keyboard Playing has been calm for some months in 2020.
It was, however, not forgotten.
It's alive again and some changes were made in the last weeks.


<!--more-->

## What the Holdup Was

### A Book Is Out, Including Short Stories of Mine

First, why was the website "paused" for so long?
Well, mainly because I worked on the self-publication of a book.

{{< figure src="/img/carrefour/cover-v1-small.jpg" link="/img/carrefour/cover-v1.jpg" title="_Crossroads of the imaginations_" caption="Our book’s cover" >}}

This book is collection of short stories and illustrations from members of the [writing/drawing community][commudustylo] I'm part of.
It includes propositions for our [bimonthly challenges][bimonthly], written by {{< author "tony" >}}, {{< author "ngorzo" >}}, {{< author "sancho" >}} and myself, and drawn by {{< author "vinzouille" >}}, {{< author "ngorzo" >}} and {{< author "captain-jahmaica" >}}.

If you're curious, you might want to read [the lessons I learned from managing this project][side-project-lessons], or if you want to support the community, you may want to [buy this book][carrefour-bookelis], though it's available only in French.


## What's Changed on the Website?

### Progressive Enhancement (or Graceful Degradation)

Ever since Hugo included [ESBuild][esbuild] to handle JavaScript bundling, I've been tempted to move out of Rollup.
However, I feared losing the Babel transpilation meant losing support for old browsers.

Then, I stumbled upon [this post from _The Ethically-Trained Programmer_][kill-ie11], and I retained two main ideas:

* IE11 is end-of-line, so why should it provide the same experience as newer browsers?
* If you don't test on IE and only rely on Babel, your script is probably not working anyway.

I knew both of these points, of course, but seeing it written reminded me that maybe I could review how I supported IE11.
Right enough, a quick IE11 test showed that it didn't work anyway.

I inspired myself from the techniques Carl Johnson exposed in [his post][kill-ie11] to make JS lighter, [built by Hugo directly][hugo-esbuild] instead of using an additional Rollup + Babel step.
So, I reduced the footprint of Keyboard Playing's scripts while enhancing support for older browsers.
I'm actually quite satisfied with the result and must agree with Carl: stopping support for IE11 is a key to [progressive enhancement][wiki-progressive-enhancement] (or maybe a form of [graceful degradation][wiki-graceful-degradation]).


### New Download Proxy Transformations

The Download Proxy worked well and has helped me a few times since I deployed it last year, but base64 encoding is not the most user-friendly transformation.
I added some others, two of which allow for getting the original file without a command line.
Head to [the dedicated post][dl-proxy-new] to learn more about it.


### Brand-New .fr domain

Finally, in November, [I bought `keyboardplaying.fr`][tweet-fr-domain] to take the place of my not-so-elegant `fr.keyboardplaying.org`.
Not a big change, but still easier to remember for French-speaking readers.


## What's Next?

On the blog side, there are still a lot of ideas to write about.
I've tried to begin the habit of writing drafts and posts in advance, so that I can release posts more regularly, but I'll be happy if [each month has at least one post][tweet-resolution].

The website is still not fully what I imagined it to be.
I still want to include a "print" stylesheet, author pages and other ideas I've been sitting on.

This has to be done among other projects I have, [creative writing][stories] to do…
Life as usual, then.


[commudustylo]: https://twitter.com/commudustylo
[bimonthly]: {{< relref path="/inspirations/bimonthly" >}}
[side-project-lessons]: {{< relref path="/blog/2020/12-21-managing-personal-project" >}}
[carrefour-bookelis]: https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html

[esbuild]: https://github.com/evanw/esbuild
[hugo-esbuild]: https://gohugo.io/hugo-pipes/js/
[kill-ie11]: https://blog.carlmjohnson.net/post/2020/time-to-kill-ie11/
[wiki-progressive-enhancement]: https://en.wikipedia.org/wiki/Progressive_enhancement
[wiki-graceful-degradation]: https://en.wikipedia.org/wiki/Fault_tolerance

[dl-proxy-new]: {{< relref path="/projects/download-proxy" >}}

[tweet-fr-domain]: https://twitter.com/KeyboardPlaying/status/1329462046497398785
[tweet-resolution]: https://twitter.com/KeyboardPlaying/status/1346050743250292736

[stories]: {{< relref path="/stories" >}}
