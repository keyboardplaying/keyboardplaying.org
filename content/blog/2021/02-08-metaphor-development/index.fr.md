---
date: 2021-02-08T07:00:00+01:00
title: Utilisez des métaphores pour échanger avec une population non technique
#subtitle: A witty line as a subtitle
slug: metaphore-projet-developpement-informatique
description: |-
  Échanger avec les clients fait partie de notre travail, mais comprendre ce que nous faisons ne fait pas partie du leur.
  Parfois, une image peut grandement faciliter la compréhension mutuelle.
author: chop
categories: [ software-creation ]
tags: [ management ]
keywords: [ communication, architecture logicielle, maison, comics, dette technique ]
---

Il y a [une image que j'aime beaucoup][metaphor] pour parler du développement logiciel : la construction d'une maison.
Comme toute métaphore, elle a ses limites, mais elle aide les personnes qui n'ont pas un passif technique de comprendre ce que leurs demandes représentent pour nous, en les comparant à quelque chose qu'ils peuvent comprendre.

<!--more-->

## Construire un projet informatique est comme construire une maison

L'image est simple, vraiment : votre projet est une maison (ou un immeuble pour les plus importants).
L'idée n'est pas de moi !
Il y a dix ans, mon chef de projet rénovait une maison qu'il avait achetée et ces comparaisons lui venaient naturellement.

Une fois, un client voulait discuter une fonctionnalité et il répondit : « On ne peut pas faire ça maintenant. Ce serait comme poser une fenêtre, mais nous n'avons pas encore les murs. »
Ça, le client le comprit.
Nous aurions pu passer du temps à expliquer qu'un canal de communication ne peut fonctionner que si le moteur qui fournit les données à communiquer est déjà présent, mais percevez-vous à quel point ce discours peut paraître abstrait à quelqu'un qui ne travaille pas dans notre domaine ?

Mettez-vous à la place du client : il ne connaît rient au code ou à la technologie derrière leur projet, et ils font appel à nous pour ne pas avoir à s'en soucier.
Ils posent des questions et nous répondons avec un charabia technique.
Nous tapons des mots aléatoires et des caractères ésotériques sur un écran et, quelques semaines ou mois plus tard, nous livrons une nouvelle fonctionnalité ou application.
Cela pourrait aussi bien être de la magie démonique !

Mais tout le monde sait que la construction ou la rénovation d'une maison implique de poser des briques, de les cimenter correctement, de ne pas négliger les fondations, la plomberie, l'électricité…
Utiliser une image permet d'expliquer ce que nous faisons en donnant quelque chose que notre interlocuteur peut comprendre.


## Quelques illustrations de Vincent Déniel

Il y a quelques mois, j'ai découvert les [dessins][techdrawings] de [Vincent Déniel][vincentdnl].
Je les adore parce qu'ils représentent assez précisément, avec un humour que j'apprécie, ce que nous vivons tous les jours.

En prime, il a utilisé cette métaphore dans plusieurs de ses dessins.
Voici une sélection de mes préférés.
N'hésitez pas à parcourir [son site][techdrawings] pour plus.


### La merveilleuse explication de la dette technique

{{< figure src="/img/vincentdnl/technical-debt.png" link="https://vincentdnl.com/drawings/technical-debt" caption="_« Je ne comprends pas pourquoi c'est si long d'ajouter une nouvelle fenêtre. »_" alt="Un client face à une maison en ruines se plaint du temps requis pour l'ajout d'une fenêtre" >}}

La dette technique est un investissement pour gagner du temps à un instant _T_, mais comme toute dette, les intérêts s'accumulent.
Vous feriez bien de les rembourser dès que vous le pouvez pour ne pas avoir à tout payer en une fois.


### Une revisite du célèbre « Je veux un site comme Facebook en trois semaines »


{{< figure src="/img/vincentdnl/a-simple-website.png" link="https://vincentdnl.com/drawings/a-simple-website" _caption="_« Je veux un simple immeuble comme celui-ci. Vous pouvez le faire tout seul en trois semaines, pas vrai ? »_" alt="Un client demande la construction d'un gratte-ciel en trois semaines" >}}


### Le mythe du développeur full-stack

{{< figure src="/img/vincentdnl/full-stack-developer.png" link="https://vincentdnl.com/drawings/full-stack-developer" caption="_« J'ai besoin que tu répares le toit, fasses de la plomberie, peignes la maison, répares l'électricité et poses les tuiles. »_" alt="Un manager demande à un homme suréquipé de réparer le toit, faire de la plomberie, peindre la maison…" >}}


### Autres dessins exploitant cette métaphore

Vincent a plusieurs autres dessins dans la même veine :

- [Ce n'est pas parce qu'on a les écrans que ça fonctionne](https://vincentdnl.com/drawings/demo-day)
- [Construisez les choses avant de les rendre jolies](https://vincentdnl.com/drawings/priorities)
- [En allant vite maintenant, on risque les mauvaises surprises](https://vincentdnl.com/drawings/testing-tdd)
- [Faites confiance aux spécialistes pour le choix de leurs outils](https://vincentdnl.com/drawings/questioning)
- [Faites attention lorsque vous découpez votre monolithe en microservices](https://vincentdnl.com/drawings/breaking-the-monolith)


## Trouvez des images avec lesquelles vous serez à l'aide

Bien entendu, la construction de maison n'est qu'une image parmi d'autres.
Vous devriez trouver un domaine qui vous parle.
J'ai parfois utilisé la voiture : le tableau de bord et les commandes sont une très bonne illustration d'une interface qui communique avec un moteur, le _back end_.

Et vous, utilisez-vous des métaphores pour expliquer votre travail ?
Envie d'en partager dans les commentaires ?


[metaphor]: {{< relref path="/stories/2019/metaphore" >}}
[vincentdnl]: https://twitter.com/vincentdnl/
[techdrawings]: https://vincentdnl.com/drawings/
