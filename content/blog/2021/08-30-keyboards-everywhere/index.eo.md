---
date: 2021-08-30T07:00:00+02:00
title: Klavaroj ĉie…
#subtitle: A witty line as a subtitle
slug: klavaroj-cie
description: |-
  Vi probable scias pri la plej oftaj klavaroj, sed ĉu vi povas pensi pri ĉiuj?
author: chop
categories: [ keyboards ]
#tags: [ java ]
keywords: [ keyboard, piano ]
---

Ĉi tiu versio de la retejo estas interreta de preskaŭ du jaroj kaj mi ankoraŭ ne skribis pri klavaroj, do jen iom da lego antaŭ la fino de la somero.

<!--more-->

## Iom da historio

Kiam mi kreis ĉi tiu retejo, mi pasigis iom da tempo elektante ĝian nomon.
Mi volis ion, kio povus kunlabori, kio ne sonis (tro) nerda, kaj kiu povus plivastigi preter la nura aspecto de programkreado.

Mia plej bona amikino estas pianisto (interalie) kaj mi esperis, ke eble iam ŝi kontribuos.
Do, kion komunumas programisto kaj pianisto?
Tiam mi pensis pri la klavaroj, kaj antaŭ tiam aperis la nomo.

Kelkajn semanojn aŭ monatojn poste, mi finfine projektis la logoon, kiun mi pensis, por montri la kunfandiĝon de ambaŭ tiuj interesoj.

{{<figure src="kp-logo.svg" caption="La logo de Keyboard Playing" link="https://gitlab.com/keyboardplaying/kp-logo" attr="chop" lic="CC BY-SA 4.0" >}}

Alia provo montri tiujn malsamajn interesojn estis la kaprubandoj de la retejo[^fn-sustainable], kiu montris hazardan bildon de klavaro.
Tiuj bildojn inkluzivis la jenajn:

[^fn-sustainable]: Mi ankoraŭ ne sciis pri [la efiko de senutilaj bildoj][intro-sustainable-it] aŭ la "nur uzu bildojn kiam ili helpas kompreni" prinicpon.


{{<figure src="alex_vincent-playing_cc-by-sa.jpg" alt="Manoj ludantaj orgenon" attr="Xander Lea Daren" attrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}

Vi povas vidi manojn ludi sur la bildo.
Tiuj estas la manoj de la respondeculo pri ĉi tiu afiŝo: kiam mi sciigis lin, ke li aperis en mia retejo, li rigardis la disponeblajn kapbildojn kaj diris al mi, ke multaj klavaroj mankas en mia elekto.

Li kompreneble nepre pravis.



## Difino

Kio estas klavaro?
Laŭvorte, ĝi estas aro de klavoj.

Tiuj klavoj permesas al homo regi aparaton per premo liverita de iliaj fingroj.


## La evidentaj

### Komputilaj flankaparatoj…

Mi daŭre skribas pri programado, do evidente komputila klavaro estu en al elekto.

{{<figure src="alex_bépo_cc-by-sa.jpg" alt="Deproksima foto de BÉPO-klavaro" attr="Xander Lea Daren" atrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}
{{<figure src="chop_keyboard_cc-by.png" alt="Deproksima foto de alia klavaro" attr="chop" lic="CC BY 2.0" >}}
<!--{{<figure src="alex_mac_cc-by-sa.jpg" attr="Xander Lea Daren" attr_link="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}-->


### Kaj ilia familio

Sed tiuj klavaroj ne aperis malpene.
Kial la klavoj ne vicigas de vico al vico?
Kial ili estas en ĉi tiu ordo anstataŭ alia?

Nu, mi revenos al tio, espereble antaŭ la fino de la jaro, sed tio estas pro ĝia prapatro: la tajpilo.

{{<figure src="alex_typewriter-underwood_cc-by-nc-sa.jpg" alt="Foto de antikva tajpilo" attr="Xander Lea Daren & Hoper Lyle Celtic" lic="CC BY-NC-SA 2.0" >}}

Kaj ĉar ni komencis paroli pri genlinio, ni eble ankaŭ parolos pri la posteuloj.
Prenu ekzemple la projekcian klavaron: anstataŭ tajpi sur fizika aparato, vi povus tajpi klavojn, kiujn lasero projekcias sur iu ajn surfaco laŭ via elekto.

{{<figure src="fuse_laser-keyboard_cc-by-sa.jpg" caption="A projection keyboard on a wooden desk." link="https://commons.wikimedia.org/wiki/File:ProjectionKeyboard_2.jpg" attr="Adrian Purser" attrLink="https://www.flickr.com/photos/fuse/" lic="CC BY-SA 2.0" >}}

Kaj estas la nun ĉiea virtuala klavaro, sur la ekrano de vian inteligenta telefono.
Sed ni ĉesu ĉi tie nuntempe kun la genlinio de komputilaj klavaroj!


## La malproksima familio

### La famuloj

Havi klavarojn tradukas homajn ordonojn al rapidaj, precizaj movoj aŭ operacioj estas nenio nova.
Ĝi ne estis inventita por la tajpilo.

Imagu harpon: vi havas multajn kordojn kaj vi devas pinĉi ilin laŭ specifa maniero.
Tiu postulas iom da malfacila laboro.
Se mi nur povus premi klavojn, tio farus la pinĉadon!

Mi povas: jen la klavecino!
Mi batas klavojn kaj kordoj pinĉiĝas.

Kaj jen ĝia kuzo la piano: mi batas klavojn kaj kordoj batiĝas.

{{<figure src="alex_blankaj_nigraj_cc-by-sa.jpg" alt="Piano klavaro" title="Blankaj kaj nigraj" link="https://www.flickr.com/photos/xanderleadaren/5364471225/" attr="Xander Lea Daren" attrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}

Mi jam dividis foton de orgeno, kun manoj ludantaj ĝin.
Iuj tuborgenoj havas [multe pli impresajn ludotablojn][wiki-organ-consoles], kun du ĝis sep klavaroj, pluraj dekoj aŭ centoj da teniloj, kaj pedaloj, kiujn oni povus pensi kiel plia klavaro.

Sed mi supozas, ke vi jam vidis iujn el tiuj.
Ni provu trovi iujn klavarojn, kiujn vi eble ankoraŭ ne konas.


### La vjelo

Ĉu vi iam pri la vjelo?
Ĝi povas esti la mezepoka prapatro de la violono: rado frotas la kordojn por produkti la sonon (kiel arko), kaj klavoj estas premitaj por determini la tonon (kiel la fingroj en la kazo de violono).

{{<figure src="frinck51_hurdy-gurdy_cc-by-sa.jpg" caption="Vjelo" link="https://commons.wikimedia.org/wiki/File:Louvet_Drehleier.JPG" attr="Frinck51" attrLink="https://commons.wikimedia.org/wiki/User:Frinck51~commonswiki" lic="CC BY-SA 2.0" >}}

Mi amas tiun instrumenton, sed ĝi estas malfacile trovebla en nuntempa muziko.
Se vi ŝatas metalon, mi tre rekomendas la svisan grupon [Eluveitie](http://www.eluveitie.ch/), kiu uzas ambaŭ violono kaj vjelon, inter aliaj interesaj instrumentoj.

Se metalo ne estas por vi, eble provu [Patty Gurdy](https://pattygurdy.com/).
Ŝi faras kovrilojn de famaj kantoj kaj ankaŭ verkas siafn proprajn, do vi eble malkovros ion, kion vi ŝatas.

Ambaŭ Eluveitie and Patty dividas multajn el siaj kreaĵoj en Youtube, kaj ili ankaŭ haveblas ĉe Spotify aŭ Deezer.


### La akordiono kaj ĝia familio

En filmoj kaj serioj, kiam oni provas pensigi la romantikan Parizon, ofte estas akordiono en la sonpentraĵo.
Kiel la vjelo, ĝi havas facile identigeblan voĉon.

Sed ĉu vi iam prenis la tempon rigardi unu?
Ambaŭflanke de la blekegoj estas blokoj.
Ĉiu portas klavaron.
La formo de la flavaro dependas de la fino kaj de la modelo de la instrumento: foje similas al tiu de piano, foje al tiu de malnova tajpilo…

{{<figure src="bellow-driven-instruments_public.jpg" caption="Bellow-driven instruments" link="https://en.wikipedia.org/wiki/File:Squeeze_boxes_accordion_bandoneon_concertina_diatonic_chromatic.jpg" >}}


## Kien mi iris kun ĉi tio?

Nenie.
Mi volis nur ĉirkaŭiri la klavarojn.
Mi certas, ke mi maltrafis multajn!

Etendante nian difinon iomete, ni povus ikluzivi ksilofonojn, marimbojn, la[Quadrangularis Reversum](https://en.wikipedia.org/wiki/Instruments_by_Harry_Partch#Quadrangularis_Reversum)…
La listo estas senfina.

Kaj vi, kiun klavaron _vi_ ŝatus dividi?


[intro-sustainable-it]: {{< relref path="/series/intro-sustainable-it" lang="en" >}}
[wiki-organ-consoles]: https://eo.wikipedia.org/wiki/Ludotablo_(orgeno)
