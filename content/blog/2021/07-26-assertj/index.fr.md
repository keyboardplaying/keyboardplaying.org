---
date: 2021-07-26T07:00:00+02:00
title: Utilisez AssertJ pour des tests plus explicites
#subtitle: A witty line as a subtitle
slug: assertj-tests-plus-explicites
description: |-
  Le test est essentiel pour assurer la robustesse du code, mais l'intention derrière un test n'est pas toujours évidente.
  Aujourd'hui, j'ai souhaité partager au sujet d'AssertJ et de la désignation chaînée pour les assertions.
author: chop
categories: [ software-creation ]
tags: [ java ]
keywords: [ java, junit, fluent interface, désignation chaînée, assertj, tests ]

references:
- id: junit5
  name: JUnit 5
  url: https://junit.org/junit5/
  lang: en
- id: assertj
  name: AssertJ - fluent assertions java library
  title: true
  url: https://assertj.github.io/doc/
  lang: en
  author:
    name: AssertJ
- id: baeldung-assertj
  name: Introduction to AssertJ
  title: true
  url: https://www.baeldung.com/introduction-to-assertj
  lang: en
  author:
    name: Baeldung
---

On ne va pas discuter aujourd'hui le bien-fondé des tests.
Je veux simplement vous suggérer une bibliothèque qui permet de les écrire différemment et, selon moi, les rendre plus compréhensibles.

<!--more-->

## Mon problème avec les tests

De toute ma carrière, JUnit est le seul framework de tests que j'ai utilisé en Java (enfin, les seuls, puisque j'ai commencé avec JUnit 4 et me suis depuis familiarisé avec [Jupiter][junit5]).
Vous pouvez me traiter d'ignorant, mais j'apprécie ce framework.
Il couvre la plupart des bases nécessaires à tous mes tests.

Il y a cependant un détail qui revient souvent me tracasser, cependant : l'écriture des assertions.
Les lire n'est pas toujours évident, pour plusieurs raisons :

* Certaines erreurs sont récurrentes, comme **l'inversion de la valeur « attendue » et la valeur « réelle »**, ce qui peut faire de la recherche d'erreur un enfer si on ne prend pas le temps de se demander ce qu'est vraiment censé faire le code.
* Cela figure probablement parmi **le code le plus procédural** qu'il nous soit donné d'écrire en Java.
Juste une suite d'instructions.
* Je n'ai jamais connu un seul développeur (moi compris) qui prenne le temps d'écrire des messages pour les assertions qui échoueraient, et même les commentaires sont rares (mais je pense qu'il s'agit là d'un sujet pour un autre billet).

Je pourrais continuer mais vous saisissez l'idée.

Il existe plusieurs façons d'écrire les tests de façon plus lisible.
Nous pourrions par exemple évoquer Cucumber et Gherkin, mais une autre fois.
Aujourd'hui, je vais me concentrer sur la désignation chaînée et plus particulièrement sur AssertJ.


## La désignation chaînée à la rescousse

Savez-vous ce qu'est la désignation chaînée ?
En anglais, on parle de _fluent interface_.
Il s'agit d'une approche de programmation orientée objet qui consiste à _chaîner_ les appels de méthode au sein d'une seule instruction.
Le modèle du Builder est un exemple.

```java
final Book gameOfThrones = Book.builder()
    .title("Le Trône de fer")
    .author("George R. R. Martin")
    .series("Le Trône de fer")
    .build();
```

Je préfère cette approche à ce que je connaissais avant ça.
J'ai troujours trouvé pénible d'appeler les accesseurs un par un…

```java
final Book gameOfThrones = new Book();
gameOfThrones.setTitle("Le Trône de fer");
gameOfThrones.setAuthor("George R. R. Martin");
gameOfThrones.setSeries("Le Trône de fer");
```

… et le constructeur prenant tous les arguments permet lui aussi de créer des objets immutables, mais il devient rapidement verbeux et difficile à manipuler :
* Vous devez connaître la position de chaque paramètre : les revues de code sont plus complexes sans la documentation ou un IDE efficace.
* Il faut soit plusieurs variantes du constructeur, soit le risque d'appeler en laissant de nombreux arguments `null`.

```java
// Je ne connais pas l'année de publication, je laisse vide
final Book gameOfThrones = new Book("Le Trône de fer", "George R. R. Martin", null, "Le Trône de fer");
```

Cet exemple est bien entendu très grossier, mais il vous donne un rapide aperçu de la désignation chaînée.
Vous pouvez aussi penser à la Stream API.

Toutefois, il y a un point intéressant commun à plusieurs implémentations que j'ai vues de la désignation chaînée : elle donne l'impression de remplacer les listes d'instructions par des phrases, quelque chose que même un non-codeur pourrait lire.
AssertJ figure parmi ces implémentations.


## Démarrer avec AssertJ

À quoi ressembleraient vos tests écrits avec AssertJ ?
Voyons quelques exemples :

```java
assertThat(tyrion)
    .isNotEqualTo(robert)
    .isIn(lannisterFamily);

assertThat(melisandre.getName())
    .startsWith("Mel")
    .endsWith("dre")
    .isEqualToIgnoringCase("melisandre");

assertThat(nedsChildren)
    .hasSize(5)
    .contains(robb, arya)
    .doesNotContain(joffrey);
```

Figurez-vous écrire la même chose avec les assertions de JUnit et dites-moi ce que vous préférez.

Pour commencer à utiliser AssertJ, il vous faut d'abord récupérer la dépendance.
En Maven :

```xml
<dependency>
  <groupId>org.assertj</groupId>
  <artifactId>assertj-core</artifactId>
  <version>3.20.2</version>
  <scope>test</scope>
</dependency>
```

Ensuite, dans vos classes de tests, il ne reste plus qu'à remplacer les imports d'assertions JUnit par la ligne suivante :

```java
import static org.assertj.core.api.Assertions.assertThat;
```

À partir de là, vous pouvez utiliser l'autocomplétion de votre éditeur, [la documentation officielle][assertj] ou [certains tutos de quelque renommée][baeldung-assertj] pour voir quelles assertions vous sont offertes.

`assertThat` est la méthode principale, mais plusieurs autres sont disponibles.
[La documentation][assertj] liste les suivantes :

```java
import static org.assertj.core.api.Assertions.assertThat;  // main one
import static org.assertj.core.api.Assertions.atIndex; // for List assertions
import static org.assertj.core.api.Assertions.entry;  // for Map assertions
import static org.assertj.core.api.Assertions.tuple; // when extracting several  properties at once
import static org.assertj.core.api.Assertions.fail; // use when writing exception tests
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown; // idem
import static org.assertj.core.api.Assertions.filter; // for Iterable/Array assertions
import static org.assertj.core.api.Assertions.offset; // for floating number assertions
import static org.assertj.core.api.Assertions.anyOf; // use with Condition
import static org.assertj.core.api.Assertions.contentOf; // use with File assertions
```

Sur cette rapide introduction, je vous laisse faire vos propres essais et me dire ce que vous en pensez.
En ce qui me concerne, je sais que, depuis que je l'ai découvert, j'ai tendance à remplacer toutes mes assertions JUnit.
Vous pouvez d'ailleurs voir des exemples d'AssertJ avec JUnit 5 [au sein de mon projet Download proxy][dlproxy-tests].



{{% references %}}
[dlproxy-tests]: https://gitlab.com/keyboardplaying/download-proxy/-/blob/main/src/test/java/org/keyboardplaying/web/dl/service/DownloadProxifierTest.java
