---
title: Afiŝi vian artikolon en Keyboard Playing
slug: gastaj-artikoloj
description: |-
    Si vous souhaitez publier un article sur ce site, voici ce qu'il faut savoir.
---


Oni foje kontaktas nin pro ekscii ĉu Keyboard Playing (KP) akceptus gastajn artikolojn.
La mallonga respondo kompreneble estas "Jes!"
Jen respondaro pri tiu eblo.


## Ĉu KP akceptas gastajn artikolojn?

Jes!
Ni ĉiam antaŭvidis Keyboard Playing pli kiel komunumon ol kiel individuan blogon.
Se vi volas skribikaj afiŝi ĉi tie, legu plu!


## Pri kioj temoj mi povus skribi?

[La kreinto de KP estas programisto][pri-kp], sed li ne deziris limigi sian retejon al sia sola agado.
Jen kial li komencis de sia amo al klavaroj: _ĉio temo rilata al klavaroj kaj tio, kion oni faras kun ilin, estas bonvena_.

Ĝi kovras amason da aferoj: programaro kreado, verkado, muziko, eble eĉ videoludoj?
La temoj foje eĉ etendiĝis al profesioj ĉirkaŭ tiuj primaraj temoj.
Ekzemple, ni jam dividis [konsiletojn pri kiel estri kaj labori kun programistoj][tag-management].

Si vi ne certas ĉu via temo apartenas al ĉi tiu listo, kontaktu nin kaj ni decidos kune.


## En kiuj lingvoj mi devus skribi?

Vi eble rimarkis, ke la retejon estas eldonita en tri lingvoj: esperanto, la franca kaj la angla.
Vi ne bezonas skribi en ĉiuj tiuj lingvoj por esti afiŝita ĉi tie.
Elektu tiun, kiun vi plej ŝatas — aŭ tiun, kiu plej taŭgas por via temo.

Ni eble povos proponi tradukon en la aliajn lingvojn, se ĝi taŭgas kaj ni havas la necesan tempon.
Kompreneble, vi povos decidi ĉu tio ŝajnas akceptebla al vi aŭ ne.


## Ĉu mia artikolo estus ŝanĝita?

Ne sen via permeso.
Ni povos proponi provlegadon, sed ĝi estos propono.
Vi povos akcepti aŭ malakcepti niajn sugestojn.

Ni rezervas la rajton de aldoni enkondukan aŭ finan alineon.
En ĉi tiu kazo, la formatado klare separos vian laboron de niaj aldonoj.


## Kiel mia intelekta proprieto estus protektita?

Unue, via aŭtora nomo estos montrata ĉe la supro de la artikolo, kaj ni povas aldoni ligilon al paĝo de via elekto.
Ĉiuj paĝoj de la retejo estas rajtigitaj (vidu piedlinon), sed ni povas specifi Creative Commons permesilon de via elekto por via artikolo, ekzemple se vi volas aldoni "nekomerce" klaŭzon aŭ permesi publikan uzon de via kreaĵo (CC0).


## Ĉu mi estus pagita?

Bedaŭrinde, ne.
Ni kredas, ke ĉiu laboro meritas kompenson, sed ni ne povas pagi vin.

[KP antaŭ ĉio restas persona projekto][pri-kp].
Ni ne montras reklamado kaj havas neniun enspezo.
Ni tamen havas kostojn (gastigo, domajna nomo…).
Ĉar nia bilanco jam estas negativa, ni ne povas permesi aldoni pli elspezojn.

Ni sincere bedaŭras.


## Kio pri la rakontoj?

La [rakontoj][rakontoj] estas grava parto de ĉi tiu versio de la retejo.
Plej multaj el ili estas relative mallongaj: novaĵoj aŭ eĉ mikronovaĵoj.
Eble ili poste bonvenigos aliajn formojn, pli longajn, lterskribajn aŭ interagajn.

La respondoj donitaj en ĉi tiu paĝo por la artikoloj validas, krom la temoj: lasu vian imagon kuri!


## Mi intersiĝas. Kio okazas nun?

Unue, <a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>retpoŝtu al ni</a>:
Ni diskutos vian ideon kune.

Se ni ĉiuj bonfaras daŭrigi, ni uzos testan version de la retejo por montri al vi kiel aspektus via laboro, kaj poste ni planos liberigi ĝin kune.


[pri-kp]: {{< relref path="/about/about-kp" >}}
[tag-management]: {{< ref path="/tags/management" lang="en" >}}
[rakontoj]: {{< relref path="/stories" >}}
