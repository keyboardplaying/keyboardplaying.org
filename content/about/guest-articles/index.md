---
title: Publishing your article on Keyboard Playing
slug: guest-articles
description: |-
    If you wish to publish an article on this website, here's what you should know.
---


We've been contacted a few times to know if Keyboard Playing (KP) would be open to hosting guest articles.
The short answer is _yes_, of course.
Here's a FAQ about what you should know about it.


## Is KP open to guest articles?

Yes!
We'd be thrilled to welcome some guest articles.
Keyboard Playing was always envisioned as a community rather than a personal blog.
If you wish to share your voice and mind here, please read on!


## What topics would you be ready to host articles about?

[Keyboard Playing was created by a programmer][about-kp] who didn't want to restrict the website to this only field.
So he derived from his love for keyboards: _any topic related to keyboards and what we do with them would be welcome_.

That can be a variety of things: software creation, writing, playing music, maybe even videogaming?
It also progressively expanded to professions around those topics.
For isntance, we sometimes give advice on [how to manage and work with developers][tag-management].

If you're unsure your topic would fit that list, just contact us and we'll see together what we can do.


## What languages should I write my article in?

You may have noticed the website is published in three languages: English, French and Esperanto.
You don't need to write in all these languages to be published here, just pick the one you're more at ease with — or the one that's most relevant to your article.

We may be able to offer a translation to the other languages, if it makes sense and if we can reserve the necessary time.
Of course, you'll have your say on whether or not it's OK for you.


## Would you alter my article?

Not without your permission.
We may offer a proofreading, but this will be provided as suggestions you may accept or not.

We reserve the right to add an introductory or conclusive paragraph.
In this case, it will be highlighted so that it will not be confused with your work.


## How would be my intellectual property be protected?

First, your author name will be displayed at the top of the article, and we can add a link to a page of your choosing.
All pages of the website are licensed (see the footer), but we can enforce a specific Creative Commons license for your article, if you wish to add a "non-commercial" clause for instance, or even make your creation public (CC0).


## Would I get paid?

Unfortunately, no.
We think that all work deserve a compensation, but we don't have the means to pay you.

[KP is a personal side project][about-kp] before anything else.
There is no advertising, nor any source of income.
There are, on the other hand, fees (hosting, domains…).
Our balance sheet being already negative, we can't afford to add more expenses to it.

So, sorry.


## How about stories?

[Stories][stories] are a big part of the new version of this website.
They're mostly short stories or microstories.
Maybe one day longer, epistolary or interactive ones, too.

The answers to all questions on this page remain valid: we accept stories, under the same conditions as articles.
The only difference is that there are fewer restrictions about the topics: let your mind go wild!


## I'm interested, how should we proceed?

Just <a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>send us an email</a>!
We'll discuss your idea further.

If we agree to proceed, we'll use a test version to show you how your article would look like on our website, and then we'll plan the roll out together.


[about-kp]: {{< relref path="/about/about-kp" >}}
[tag-management]: {{< relref path="/tags/management" >}}
[stories]: {{< relref path="/stories" >}}
