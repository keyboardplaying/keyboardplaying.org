---
title: Pri Keyboard Playing
url: /pri
aliases: [/about]
classes: [ large-page ]

cascade:
  tipping: false
  metadata:
    lite: false
  pager: false
  related: false
  comments: false
  scripts: [ toc ]
---
