---
title: Kuketa politiko
slug: kuketa-politiko
description: |-
  Ĉi tiu kuketa politiko klarigas kio kuketoj estas kaj certigi ke ni ne uzas ilin.
cover:
  src: mae-mu-kID9sxbJ3BQ-unsplash.jpg
  alt: Brown cookies in a round bowl.
  by: Mae Mu
  link: https://unsplash.com/photos/kID9sxbJ3BQ
  authorLink: https://unsplash.com/@picoftasty
  license: Unsplash
---

Ĉi tiu kuketa politiko klarigas kiel Keyboard Playing ("**ni**", "**nin**" aŭ "**nia**") uzas kuketojn por rekoni vin kiam vi vizitas niajn retejojn ĉe https://eo.keyboardplaying.org ("**Retejoj**").
Ĝi klarigas, kiajn kiel viajn rajtojn regi nian uzadon de ilin.


## Resumo por homoj

Kuketoj estas malgrandaj datumaj dosieroj, kiuj estas metitaj sur vian aparaton kiam vi vizitas retejon.
Ni ne uzas ilin.

Ĉar ĉi tiu estas esenca kuketo por la ĝusta funkciado de nia retejo, ne ekzistas Kuketo-Konsenta Administranto.


## Kio estas kuketoj?

Kuketoj estas malgrandaj datumoj dosieroj, kiuj estas metitaj sur vian komputilon aŭ poŝtelefonon kiam vi vizitas retejon.
Kuketoj estas vaste uzataj de retejposedantoj por funkciigi siajn retejojn, aŭ por funkcii pli efike, kaj ankaŭ por doni raportajn informojn.

Kuketoj definitaj de la posedanto de la retejo (ĉi-kaze Keyboard Playing) nomiĝas "unuaj partiaj kuketoj."
Kuketoj definitaj de aliaj krom la posedanto de la retejo nomiĝas "triaj partiaj kuketoj."
Triaj partiaj kuketoj ebligas triaj partiaj trajtoj aŭ funkcij esti provizataj sur aŭ tra la retejo (ekz. reklamado, interaga enhavo kaj analizado).
La partioj, kiuj agordas ĉi tiujn triajn partiajn kuketojn, povas rekoni vian komputilon kiam ĝi vizitas la koncernan retejon kaj ankaŭ kiam ĝi vizitas iujn aliajn retejojn.


## Ĉu ni uzas kuketojn?

Ni ne uzas kuketojn, nek iun alian teknologion, kiu stokas informojn en via aparato.


## Kiel vi povas regi kuketojn?

Vi povas agordi aŭ modifi viajn retumilajn kontrolojn por akcepti aŭ rifuzi kuketojn.
Se vi elektas fari tion, vi ankoraŭ povas uzi niajn Retejojn  kvankam via aliro al iuj funkcioj kaj areoj de niaj Retejoj eble estos limigita.
Ĉar la rimedoj per kiuj vi povas rifuzi kuketojn per viaj retumilaj kontroloj varias de retumilo al retumilo, vi devas viziti la helpmenuon de via retumilo por pliaj informoj.


## Ĉu ni servas celitan reklamadon?

Ni ne servas reklamadon kaj ne dividas viajn foliumajn informojn kun iu ajn triaj partiaj.


## Kiom ofte ni ĝisdatigos ĉi tiun kuketan politikon?

Ni eble ĝisdatigos ĉi tiun kuketan politiko de tempo al tempo, por reflekti, ekzemple, ŝanĝojn al la kuketoj, kiujn ni uzas aŭ pro aliaj funkciaj, juraj aŭ reguligaj kialoj.
Bonvolu do revidi ĉi tiun kuketan politikon regule por resti informita pri nia uzo de kuketoj kaj rilataj teknologioj.


## Kie vi povas akiri pliajn informojn?

Se vi havas demandojn pri nia uzo de kuketoj aŭ aliaj teknologioj, bonvolu retpoŝti nin al
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.
