---
title: Download Proxy
description: |-
  A tool to allow the download of trusted files blocked by a firewall, through redirection or transformation of the download stream.
author: chop

aliases:
    - /tools/download-proxy/
    - /blog/2020/04/download-proxy-back/
    - /blog/2021/01/new-transformations-download-proxy/

tipping: true
---

When you work in a company's IT service, it's not unusual to need a tool or resource that you can't download.
Most often, it's because of security rules.
Maybe a dedicated process exists to ask for those files, but it most often require filling a form and waiting for hours or day for it to be processed.

As a result, many developers create their own way of bypassing such restrictions: a dirty script on a personal server, an SSL tunnel for the luckiest and cleanest…
As often, when there's a common need, there's a way to mutualize.
This project is just that: a tool that takes some of the best ideas colleagues told me about to circumvent an annoying corporate firewall.

<div class="notice">

Those firewalls exist for a reason: security.
It (the security) should be paramount, as well as on an individual level as on a corporate one.
I designed this tool to help developers work, but it should be considered a last resort solution.

In case you _do_ use this tool, please do so cautiously and be sure you can trust the file you are downloading.
I do not condone any form of illegal use of this tool.
**You bear the sole responsibility of what you are doing with the Download Proxy.**

</div>


## Useful links

* **Live version**: https://proxy.keyboardplaying.org (might take some time to start up, be patient)
* **Swagger UI**: https://proxy.keyboardplaying.org/q/swagger-ui/
* **GitLab**: https://gitlab.com/keyboardplaying/download-proxy


## The Transformations

The Download Proxy works by transforming the file so that it escapes detection from the firewall.
Below are the transformations available in the current version.


### Identity (redirect)

**Since**: 2.0

**What it does**:
The file is not altered in any way, but the firewall will detect it as being downloaded from the domain hosting the proxy, instead of its original domain.

**When to use it**:
This transformation is the most efficient when the rule blocking the file's download is related to its origin rather than to the file itself.

**How to get back the original file**:
Since the original file was not altered, no additional action is required.


### Base64 Encoding

**Since**: 2.0

**What it does**:
The downloaded file is a text representing the original download in base64 encoding.

**When to use it**:
To bypass a firewall that uses file-type detection, as it will see only a text file.
For firewalls that check the content of the files, this might result in long analyses.

**How to get back the original file**:
You need to decode the downloaded file.
On most operating systems, one command line is enough.

```shell
# On Linux or in Git Bash
base64 -d my-file.ext.b64 > my-file.ext

# On Windows
certutil -decode file.ext.b64 file.ext
```


### Zipping

**Since**: 1.0, 2.1[^fn-zip-20]

**What it does**:
The original file is zipped into the file you will download.

**When to use it**:
To bypass a firewall that uses file-type detection.
Nowadays, most firewalls inspect the content of zips, though, so this might not be sufficient.

**How to get back the original file**:
Unzip the downloaded file to get the file you wished to download.

[^fn-zip-20]: This transformation disappeared in the 2.0 but was reintroduced as soon as possible.


### Masquerading as JPEG

**Since**: 2.1

**What it does**:
The original file will be added JPEG's [magic numbers][magic-numbers] to the downloaded file.

**When to use it**:
To bypass a firewall that uses file-type detection, as it will see only an image.

**How to get back the original file**:
You need to remove the first three and last two bytes of the file you downloaded, and rename it so it has the original extension.
On Linux or in Git Bash, you should be able to do so via the following command:

```shell
sed 's/^\xFF\xD8\xFF//g' file.ext.jpg | sed 's/\xFF\xD9$//' > file.ext
```


### Zipping and Concatenating to JPEG

**Since**: 2.1

**What it does**:
The original file is zipped and appended to an image.
Both the firewall and your computer will see an image, but some software will be able to unzip it.[^fn-htg-zip-into-picture]

**When to use it**:
To bypass a firewall that uses file-type detection, as it will see only an image, if "Masquerading as JPEG" is too complicated to revert.

**How to get back the original file**:
On Windows, use [7-Zip][^fn-7zip] or [WinRAR].
The `unzip` command is a Linux-compatible solution.
**Do not change the file's extension!**

[^fn-htg-zip-into-picture]: This transformation was inspired from [a post on How-To Geek][htg-zip-img].
[^fn-7zip]: In 7-Zip for instance, don't try to find your favorite right click > _Extract_ menu. Instead, open your jpeg in 7-Zip's file manager.

[magic-numbers]: https://en.wikipedia.org/wiki/Magic_number_(programming)#Format_indicators
[htg-zip-img]: https://www.howtogeek.com/119365/how-to-hide-zip-files-inside-a-picture-without-any-extra-software/
[7-Zip]: https://www.7-zip.org/
[WinRAR]: https://www.rarlab.com/download.htm
