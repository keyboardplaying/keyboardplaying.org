---
title: Download Proxy (elŝuta prokurilo)
description: |-
  Ilo por elŝuti fidindajn dosierojn, kiujn fajroŝimirlo blokas, per alidirekto aŭ tansformo de la fluo.
author: chop

tipping: true
---

Kiam vi laboras en la informadika teknologio sektoro, ne estas nekutime bezini ilon aŭ datumon, kiun vi ne povas elŝuti.
Plej ofte, tio estas pro sekurecaj reguloj.
Foje ekzistas procezo por peti tiujn dosierojn, sed ĝi kutime implikas plenigi formularojn kaj tiam atendi plurajn horojn aŭ tagojn por ke ili estu procesitaj.

Kiel rezulto, multaj programistoj kreas sian propran solvon por eviti tiajn restriktojn: "quick and dirty" skripto sur obskura servilo, SSL-tunelo por la plej puraj…
Kiel ofte, kiam estas komuna bezono, ekzistas maniero kunigi.
Tiu projekto celas ĝuste tio: provizi ilon konstruatan sur la plej bonaj ideoj, kiujn miaj kolegoj efektivigis por eviti obstinan firmaan fajroŝirmilo.

<div class="notice">

Fajroŝimirloj ekzistas por kialo: sekureco.
Ĝi (sekureco) devus esti ĉefa zorgo, kaj ĉe la individua kaj ĉe la firmaa nivelo.
Mi desegnis la elŝuta prokurilo por helpi programistojn labori, sed ĝi devus esti uzata nur kiel lasta rimedo.

_Se vi uzas_ ĉi tiun prokurilon, estu singarda kaj nur elŝutu fidindajn dosierojn.
Mi ne aprobas ajnan formon de uzado de ĉi tiu ilo por kontraŭleĝaj celoj.
**Vi portas la solan respondecon pri via uzo de la elŝuta prokurilo.**

</div>


## Utilaj ligiloj

* **Tuja ilo**: https://proxy.keyboardplaying.org (eble necesas kelkajn minutojn por startigi, estu pacienca)
* **Swagger UI**: https://proxy.keyboardplaying.org/q/swagger-ui/
* **GitLab**: https://gitlab.com/keyboardplaying/download-proxy


## La transformoj

La elŝuta prokurilo funkcias transformante la dosieron por ke ĝi eskapas detekton de la fajroŝimirlo.
Malsupre estas la transformoj disponeblaj en la nuna versio.


### Identeco (alidirektado)

**Ekde**: 2.0

**Kion ĝi faras**:
La dosiero neniel estas modifita, sed la fajroŝimirlo detektos, ke gxi estas elŝutita el la domajno gastiganta la prokurilon, anstataŭ ĝia originala domajno.

**Kiam uzi ĝin**:
Tiu ĉi transformo estas la plej efika kiam la regulo bloganta la elŝuton rilatas al la origino prefere ol al la dosiero mem.

**Kiel rehavi la originalan dosieron**:
Ĉar la dosiero ne estas ŝanĝita, neniu aldona ago estas necesa.


### Base64 kodigo

**Ekde**: 2.0

**Kion ĝi faras**:
La elŝutito estas tekstdosiero, kies enhavo estas la originala dosieron en base64 kodigo.

**Kiam uzi ĝin**:
Por preteriri fajroŝimirloj, kiuj uzas dosiertipan detekton, ĉar ĝi vidos nur tekstodosieron.
Se la fajroŝimirlo kontrolas dosierenhavon, tio povas rezultigi skanaĵojn, kiuj daŭras dekojn da minutojn.

**Kiel rehavi la originalan dosieron**:
Vi povas malkodi la elŝutitan dosieron.
En la plej multaj operaciumoj, komandlinio sufiĉas.

```shell
# En Linux aŭ en Git Bash
base64 -d my-file.ext.b64 > my-file.ext

# En Windows
certutil -decode file.ext.b64 file.ext
```


### Zip-densigo

**Ekde**: 1.0, 2.1[^fn-zip-20]

**Kion ĝi faras**:
La originala elŝuto estas densigita kiel zip-dosiero.

**Kiam uzi ĝin**:
Por preteriri fajroŝimirloj, kiuj uzas dosiertipan detekton.
Tamen, plej multaj fajroŝimirloj nuntempe inspektas la enhavon de zip-dosieroj, do ĉi tio transformo eble ne sufiĉas.

**Kiel rehavi la originalan dosieron**:
Maldensigu la elŝutitan dosieron.

[^fn-zip-20]: Tiu transformo malaperis en la 2.0, sed estis reenkondukita kiel eble plej baldaŭ.


### JPEG alivestiĝo

**Ekde**: 2.1

**Kion ĝi faras**:
La [magiaj nombroj][magic-numbers] de JPEG-oj estas aldonitaj al la elŝutita dosiero.

**Kiam uzi ĝin**:
Por preteriri fajroŝimirloj, kiuj uzas dosiertipan detekton, ĉar ĝi vidos nur bildon.

**Kiel rehavi la originalan dosieron**:
Vi devas forigi la tri unuajn kaj du lastajn bitokojn de la elŝuto, kaj renomi ĝin por restarigi la ĝustan finaĵo.
En Linux aŭ en Git Bash, vi povas fari tion per la sekva komandlinio:

```shell
sed 's/^\xFF\xD8\xFF//g' file.ext.jpg | sed 's/\xFF\xD9$//' > file.ext
```


### Zip-densigo kaj JPEG-kunkatenado

**Ekde**: 2.1

**Kion ĝi faras**:
La originala dosiero estas densigita kaj apudmetita kun bildo.
Kaj la fajroŝimirlo kaj via komputilo vidos bildon, sed iu programaro povos maldensigi la originalan dosieron.[^fn-htg-zip-into-picture]

**Kiam uzi ĝin**:
Por preteriri fajroŝimirloj, kiuj uzas dosiertipan detekton, ĉar gi vidos nur bildon, se reverti la JPEG alivestiĝo estas tro komplika.

**Kiel rehavi la originalan dosieron**:
En Windows, uzu [7-Zip][^fn-7zip] aŭ [WinRAR].
En Linux, la komando `unzip` funkcias.
**Ne ŝanĝu la dosier-finaĵon!**

[^fn-htg-zip-into-picture]: [Afiŝo en How-To Geek][htg-zip-img] inspiris tiun transformon.
[^fn-7zip]: En 7-Zip ekzemple, ne uzu la dekstran klakon > _Maldensigi_ menuon. Anstataŭe, malfermu la bildon en la dosiermastrumilo de 7-Zip.

[magic-numbers]: https://eo.wikipedia.org/wiki/Magia_nombro
[htg-zip-img]: https://www.howtogeek.com/119365/how-to-hide-zip-files-inside-a-picture-without-any-extra-software/
[7-Zip]: https://www.7-zip.org/
[WinRAR]: https://www.rarlab.com/download.htm

