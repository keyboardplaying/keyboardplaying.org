---
title: Introduction au numérique responsable
description: |-
  Notre monde numérique est loin d'être virtuel.
  Il a une profonde empreinte sur notre monde.
  Les créateurs/créatrices logiciel(le)s devraient avoir à cœur de le rendre plus durable, mais encore faut-ils qu'ils/elles soient sensibilisés à ce sujet.
  Cette série est une brève introduction à ce vaste sujet.
slug: intro-numerique-responsable
weight: 50
---
