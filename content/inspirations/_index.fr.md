---
title: Inspirations et défis
description: |-
  Je fais partie d'une communauté d'apprentis écrivains.
  Afin de rester motivés et de continuer à écrire, nous nous créons de petits défis sur une base hebdomadaire et une bimestrielle.
  J'en partage ici quelques unes que je juge acceptables.
  N'hésitez pas à les commenter pour m'aider à les améliorer.
url: /histoires/inspirations
---
