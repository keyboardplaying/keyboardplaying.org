---
title: Semajnaj plumoj
description: |-
  Nova defio ĉiu vendredo.
  Unu rikura regulo: resti mallonga.
weight: 2
slug: semajnaj

termCover:
  banner: false
  src: thom-holmes-k-xKzowQRn8-unsplash.jpg
  alt: Kajero estas malfermita sur tablo, ambaŭ paĝoj kovritaj per skribo
  by: Thom Holmes
  authorLink: https://unsplash.com/@thomholmes
  link: https://unsplash.com/photos/k-xKzowQRn8
  license: Unsplash
---
