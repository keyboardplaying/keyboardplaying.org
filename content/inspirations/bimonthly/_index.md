---
title: Bimonthly challenges
description: |-
  The bimonthly challenge should lead to larger pieces produced than the weekly ones.
weight: 2
slug: bimonthly

termCover:
  banner: false
  src: milkovi-FTNGfpYCpGM-unsplash.jpg
  alt: Hands of someone typing on a typewriter
  by: MILKOVÍ
  authorLink: https://unsplash.com/@milkovi
  link: https://unsplash.com/photos/FTNGfpYCpGM
  license: Unsplash
---
