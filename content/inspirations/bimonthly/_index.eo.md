---
title: Defioj de la Plumo
description: |-
  La defoij de la Plumo estas ofertitaj ĉiujn du monatojn kaj celas pli konsekvencan kaj strukturitan produktadon ol la Semajna Plumo.
weight: 2
slug: dumonataj

termCover:
  banner: false
  src: milkovi-FTNGfpYCpGM-unsplash.jpg
  alt: Du manoj tajpas sur skribmaŝino
  by: MILKOVÍ
  authorLink: https://unsplash.com/@milkovi
  link: https://unsplash.com/photos/FTNGfpYCpGM
  license: Unsplash
---
