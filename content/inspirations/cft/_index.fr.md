---
title: Appels à textes
description: |-
  Des éditeurs réalisent parfois des appels à textes (en général des concours dont le prix est d'être publié).
  Les textes écrits ici l'ont été pour ce type de concours et n'ont pas été retenu ou bénificient de l'accord de l'éditeur pour être publié sur le site.
weight: 3
slug: at
---
