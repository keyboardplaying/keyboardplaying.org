---
title: Calls for texts
description: |-
  Editors sometimes do "call for papers" (read "contest that'll win your text the right to be published if selected").
  The texts in this category were written for such contests and were not retained or obtained the agreement of the editor to be published on this site.
weight: 3
slug: cft
---
