---
title: Daily life
description: |-
  Some events of daily life are worth retelling, or they can trigger the imagination.
weight: 1
slug: daily-life
---
