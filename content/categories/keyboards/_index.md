---
title: Keyboards
description: |-
  Well, it's in the name of the site.
  Any kind of keyboard is welcome.
weight: 2
slug: keyboards

termCover:
  banner: false
  src: xanderleadaren-typematrix-bepo.jpg
  alt: A TypeMatrix keyboard with a BÉPO skin on.
  by: Alexandre André
  authorLink: https://esperanto.masto.host/@xanderleadaren
  link: https://www.flickr.com/photos/xanderleadaren/9484344043/
  license: CC BY-SA 2.0
---
