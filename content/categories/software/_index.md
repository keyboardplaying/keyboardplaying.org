---
title: Software
description: |-
  Software is all around us, it makes our life easier.
  Still, it's not a silver bullet and maybe there's more to it than we know.
  Here are thoughts about software, and maybe some ideas for a better use of it.
weight: 3
slug: software

termCover:
  banner: false
  src: thisisengineering-raeng-3Z3gnA99PL8-unsplash.jpg
  alt: Laptop with a software-assisted design software on screen.
  by: ThisisEngineering RAEng
  authorLink: https://unsplash.com/@thisisengineering
  link: https://unsplash.com/photos/3Z3gnA99PL8
  license: Unsplash
---
