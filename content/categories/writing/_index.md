---
title: Writing
description: |-
  We're not born writers but we can practice.
  Let's share here some conventions and recommendations about writing.
weight: 4
slug: writing

termCover:
  banner: false
  src: coding-extends-writing.jpg
  alt: A notebook page with a fountain pen. On the page, you can read "public class Coding extends Writing."
  by: chop
  license: CC BY 4.0
---
