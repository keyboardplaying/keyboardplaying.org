---
title: Programaro-kreado
description: |-
  Programaro-kreado kovras diversajn profesiojn, de programisto ĝis arkitekto, por ne mencii analiziston kaj multajn aliajn.
  Ĉi tiu kategorio traktas temojn por ĉiuj ĉi tiuj homoj kaj aliaj, inkluzive de konsiletoj pri iloj kaj plej bonaj praktikoj por disvolviĝo, kaj ideoj por arkitekturo aŭ projekt-administrado.
weight: 3
slug: programaro-kreado

termCover:
  banner: false
  src: fabian-grohs-GVASc0_Aam0-unsplash.jpg
  alt: Kajera paĝo kun notoj pri funkcioj kaj algoritmoj, apud klavaro
  by: Fabian Grohs
  authorLink: https://unsplash.com/@grohsfabian
  link: https://unsplash.com/photos/GVASc0_Aam0
  license: Unsplash
---
