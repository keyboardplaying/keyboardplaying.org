---
date: 2022-01-15T22:25:00+01:00
title: The Museum of Erroneous Sciences
slug: museum-erroneous-sciences
author: chop
license: CC BY-NC-ND 4.0

description: |-
  A school trip to discover the mistakes of our ancestors.

inspirations: [ weekly ]
keywords: [ flash fiction, museum, kids, science, history, archeology, time travel, physics, quantum physics, mathematics, Einstein ]

challenge:
  period: 2022-01-14
  link: https://twitter.com/commudustylo/status/1481945438160891912
  rules:
  - type: theme
    constraint: science
---

"Welcome to our museum, kids!
I suppose your teacher has already explained to you what we preciously keep here and what we'll be showing you today?
Yes, tell me!"

"The scienfitic mistakes our ancestors made!"

"Exactly!
You'll see there's a lot of them.
We'll be speaking about many things, today.
Do you know some?
Go ahead, we're listening."

"My dad says history's not a science, but that it was even worse before."

"Ha ha, yeah, your dad's not wrong.
See, in the past, archeologists looked for historical remains, like buildings or records.
Only then, historians tried to put it all together so that we'd get a coherent vision of what happened before their era.
It was a difficult job, mainly because the writings were not always objective.
Sometimes, the people doing the writing would try to hide that they did something wrong, just like when you do something bad and tell a little lie to avoid being punished."

"But my mom works for insurances, and she uses time windows to see what happened before.
She says it's perfect because she can see everything, anywhere and anywhen.
Why didn't historians do this?
Because it's relugat… regimen… forbidden?"

"That's a very good question!
The use of time windows is indeed regulated, because we don't want those who can use them to snoop on their neighbours, for example.
But you are right!
Today's historians use them a lot, just like anyone who has to investigate events in the past like, say, policemen and insurance companies."

"Then why not before?"

"Because it simply didn't exist!
Our ancestors would have been totally unable to invent those windows.
Does anyone have any idea why?
No?
I'll give you a clue: it has to do with this museum."

"Because of a scientific mistake?"

"Yes, that's it!
Scientists in the past didn't properly understand physics.
Since Ancient Greece, and even before, our ancestors wanted to observe the world and understand its rules.
They calculated, and they managed to find results that looked like what they observed, so they thought they were correct and they went on.
Then, they built very powerful computers to calculate for them and they programmed simulations and it gave them results that agreed with their theories, so they were happy.
They didn't understand that they introduced their own errors in the programming."

"How did they realize they were wrong?"

"What were the errors?"

"How did they go that far if their whole science was wrong?"

"Ha ha, wait a minute, that's a whole lot of questions to answer all at once.
So, how did they realize they were wrong?
Well, they quite quickly understood their equations didn't apply to the very big: they had hell of a time to understand the universe and the movement of galaxies.
For a long time, they thought it was expanding and deduced that, at one time, it appeared in a big bang.
Their rules also failed to explain the very little, so they created another set of rules, which they labeled 'quantum physics.'"

"My sister read that Einstenn could probably have corrected all their mistakes if he had lived long enough."

"Yes, historians—with time windows—could observe that Albert Einstein understood things quite intuitively, and they think that, given one or two more decades, he could have seen what he was missing.
It's because of the tools he had been taught that he couldn't figure it right away.
Do you know what I'm talking about?"

"English?"

"Ha ha, no.
Einstein was born and raised in Germany, so it would have been German in his case, but language wasn't the problem.
It was the mathematics.
You see, physicians used mathematics _a lot_.
They wrote many equations, it was sort of their universal language.
One day, someone discovered that the very foundations of mathematics were incorrect, and it was as if a card had been removed from the bottom of a house of cards: it all collapsed.
So we rebuilt, but better this time, and we made a lot of new discoveries along the way.
Now you understand why mathematics has such an important place in our museum: it's the root cause of most of the scientific mistakes our ancestors made.
How would you like to start our tour with the maths room?"

"Yeeeeeeees!"
