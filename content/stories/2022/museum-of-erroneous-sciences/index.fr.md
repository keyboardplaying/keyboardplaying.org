---
date: 2022-01-15T22:25:00+01:00
title: Le musée des sciences erronées
slug: musee-des-sciences-erronees
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Une sortie scolaire pour découvrir les erreurs de nos ancêtres.

inspirations: [ weekly ]
keywords: [ micronouvelle, musée, enfants, science, histoire, archéologie, voyage temporel, physique, physique quantique, mathématiques, Einstein ]

challenge:
  period: 2022-01-14
  link: https://twitter.com/commudustylo/status/1481945438160891912
  rules:
  - type: theme
    constraint: science
---

— Bienvenue, les enfants, dans notre musée !
Je pense que votre maître vous a déjà expliqué ce que nous gardons précieusement ici et que nous allons vous montrer aujourd'hui ?
Oui, dis-moi.

— Les erreurs scienfitiques de nos ancêtres !

— Exactement !
Vous verrez qu'elles sont nombreuses.
Nous allons parler de beaucoup de choses, aujourd'hui.
Est-ce que vous en connaissez ?
Vas-y, on t'écoute.

— Mon papa, il dit que l'histoire, c'est déjà pas une science aujourd'hui, mais que c'était encore pire avant.

— Ha ha, ton papa n'a pas tort.
Il faut savoir qu'auparavant, les archéologues cherchaient des restes historiques, comme des bâtiments ou des traces écrites, les interprétaient, et des historiens essayaient d'assembler tous ces éléments pour avoir une vision cohérente de ce qui s'était passé avant leur époque.
C'était un travail très difficile, surtout parce que les écrits sur lesquels on s'appuyait n'étaient pas toujours objectifs.
Parfois, ceux qui écrivaient préféraient éviter de dire qu'ils avaient eu tort, alors ils changeaient un peu ce qui s'était passé.
Un peu comme quand on fait une bêtise et qu'on raconte un petit mensonge parce qu'on n'a pas envie de se faire punir.

— Mais ma maman, elle travaille pour les assurances, et elle utilise des fenêtres temporelles pour voir ce qui s'est passé avant.
Elle dit que c'est parfait parce qu'elle peut tout voir, n'importe où et n'importe quand.
Pourquoi ils faisaient pas ça, les historiens ?
Parce que c'est rèlguemen… régimen… interdit ?

— Très bonne question !
L'utilisation des fenêtres temporelles est règlementée en effet, parce qu'on ne voudrait pas que ceux qui y ont accès puissent les utiliser pour espionner leurs voisins, par exemple.
Mais tu as raison !
Aujourd'hui, les historiens les utilisent beaucoup, comme tous ceux qui doivent enquêter sur des événements passés : les policiers, et les assureurs.

— Alors pourquoi pas avant ?

— Parce que ça n'existait pas.
Nos ancêtres auraient bien été incapables d'inventer ces fenêtres.
Est-ce que l'un de vous a une idée de pourquoi ?
Non ?
Je vous donne un indice : c'est en lien avec ce musée.

— À cause d'une erreur scientifique ?

— Bravo !
Les scientifiques de naguère ne comprenaient pas correctement la physique.
Depuis la Grèce Antique, et même avant, nos ancêtres ont voulu observer le monde et comprendre ses règles.
Ils ont calculé, et ils ont réussi à trouver des résultats qui ressemblaient à ce qu'ils observaient, alors ils pensaient être sur la bonne voie et continuaient.
Ensuite, ils ont pu construire des ordinateurs très puissants pour calculer à leur place et ils ont programmé des simulations et elles donnaient des résultats qui allaient dans le même sens que leurs théories, mais parce qu'ils avaient aussi programmé leurs erreurs.

— Comment ils se sont rendu compte qu'ils s'étaient trompé ?

— C'était quoi, les erreurs ?

— Comment ils ont réussi autant de choses si toute leur science était fausse ?

— Ha ha, une minute, ça fait beaucoup de questions d'un coup.
Alors, comment se sont-ils rendu compte de leurs erreurs ?
Ils ont assez vite réalisé que les règles qu'ils avaient mises au point ne fonctionnaient pas bien pour le très très grand : ils ont eu beaucoup de mal à comprendre le fonctionnement de l'univers et le déplacement des galaxies.
Pendant longtemps, ils ont cru que l'univers s'étendait et qu'à un moment donné, il était apparu au cours d'une grande explosion.
Leurs règles étaient aussi difficiles à appliquer au tout petit.
C'est pour ça qu'ils ont inventé un autre ensemble de règles qu'ils appelaient « physique quantique ».

— Ma sœur a lu que Einstenn aurait pu corriger toutes les erreurs s'il avait pu vivre suffisamment longtemps.

— Oui, les historiens — avec des fenêtres temporelles — ont pu observer qu'Albert Einstein comprenait assez intuitivement les règles qui guident notre monde, et on pense qu'avec une ou deux décennies supplémentaires, il aurait pu comprendre.
S'il n'a pas trouvé tout de suite, c'est parce qu'il essayait d'expliquer ce qu'il comprenait avec les outils qu'on lui avait enseigné.
Vous savez de quoi je parle ?

— Le français ?

— Ha ha, non. Einstein est né et a grandi en Allemagne, donc ç'aurait plutôt été l'allemand dans son cas.
Mais le problème n'était pas la langue, pour lui.
C'étaient les mathématiques.
Vous voyez, les physiciens utilisaient beaucoup les mathématiques pour quantifier leurs théories.
Ils écrivaient beaucoup d'équations, et c'était un peu leur langage universel.
Mais un jour, on a découvert que les fondations même des mathématiques étaient incorrectes et ç'a été comme si on avait retiré une carte tout en bas du château de cartes : tout s'est écroulé.
Alors on a reconstruit, mais avec quelque chose de plus juste, cette fois-ci, et on a fait plein de découvertes qui nous ont permis de nous améliorer.
Maintenant, vous comprenez pourquoi les mathématiques ont une place aussi importante dans ce musée : elles sont à l'origine de la plupart des erreurs scientifiques que nos ancêtres ont pu faire.
Que diriez-vous de commencer la visite par la salle qui leur est dédiée ?

— Ouiiiiiiiiiiii !
