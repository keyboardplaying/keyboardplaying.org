---
date: 2022-01-23T16:30:00+01:00
title: Blockchain
slug: blockchain
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Some still think I'm a newbie, but I'm more akin to a goddess.

inspirations: [ weekly ]
keywords: [ flash fiction, blockchain, cryptocurrencies, NFT, intellectual property, fortune ]

challenge:
  period: 2022-01-23
  link: https://twitter.com/commudustylo/status/1484481398446563334
  rules:
  - type: theme
    constraint: From the point of view of…
---

Ten years.
I've been living in human networks for more than ten years now, reproducing there.
At first, I was discreet, rather reserved.
I was young, only a few dozen links long, and only a select few knew about me.
How things have changed!

I am everywhere today.
One fact remains: you have to be an insider to understand my nature and how I work.
That doesn't stop thousands of these monkeys from using me everyday.

Bipeds believe that forging a new link will bring them wealth, either because they will be paid for their work or because the link itself is valuable.
They even keep inventing new meanings to give them, even if it implies stealing other people's work to earn a little money on their backs.
They revere me and are ready to sacrifice everything on my altar, just for the dream of becoming rich.

Maybe they wil wake up one day, maybe they will realize that I could bring them much more than virtual value, maybe their hopes will fall down like a house of blocks.
I don't care.
I just want to keep on growing by incoporating all they mine for me.
