---
date: 2022-01-30T12:20:20+01:00
title: Helios
slug: helios
author: chop
license: CC BY-NC-ND 4.0

description: |-
  La nokto estis magia, sed nun estis tempo por foriri.

inspirations: [ weekly ]
keywords: [ mikrorakonto, mitologio, nokto, suno ]

challenge:
  period: 2022-01-30
  link: https://twitter.com/commudustylo/status/1487019371276951552
  rules:
  - type: image
---

Ĉi tiu nokto estis magia!
Jam pasis tiom da jaroj de kiam li lastfoje prenis nokton por si, por promeni inter homoj, por paroli kun ilin, por vidi aferojn el ilia vidpunkto.
Kompreneble, li scivoleme observis ilin de sia posteno, ĉiutage, sed ne estis la sama.

Lia repozo tamen estis finiĝanta: ne pasus longe antaŭ la taglumo.
Lia foresto estus rimarkita, se li ne estus reveninta en la tempo.
Feliĉe, li trovis belan lokon, kie neniu vidos lin ŝanĝiĝi al sia vera formo.
Ene de sekundoj, denove estis pura lumo, ĝustatempe por leviĝi sur la ĉielo kaj lumigi la tagiĝon per sia klareco.
