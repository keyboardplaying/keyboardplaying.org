---
date: 2022-02-06T20:30:00+01:00
title: The Hitchhiker
slug: the-hitchhiker
author: chop
license: CC BY-NC-ND 4.0

description: |-
  The road is long and lonely.
  Maybe the hitchhiker he lets in will make it more pleasant.

inspirations: [ weekly ]
keywords: [ flash fiction, white lady, vanishing hitchhiker, supernatural, urban legend, ghost, haunted highway, prostitute ]

preface: This short story is temporarily unavailable. It will come back soon.

challenge:
  period: https://twitter.com/commudustylo/status/1489574874163130369
  link: 2022-02-06
  rules:
  - type: theme
    constraint: Urban legends
---
