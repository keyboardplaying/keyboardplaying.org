---
date: 2022-02-06T20:30:00+01:00
title: L'auto-stoppeuse
slug: auto-stoppeuse
author: chop
license: CC BY-NC-ND 4.0

description: |-
  La route est longue et pénible, en solitaire.
  L'auto-stoppeuse qu'il fait monter à bord la rendra peut-être plus agréable.

inspirations: [ weekly ]
keywords: [ micronouvelle, dame blanche, auto-stoppeuse fantôme, surnaturel, légende urbaine, fantôme, autoroute hantée, prostituée ]

preface: Cette nouvelle a temporairement été retirée. Elle reviendra prochainement.

challenge:
  period: https://twitter.com/commudustylo/status/1489574874163130369
  link: 2022-02-06
  rules:
  - type: theme
    constraint: Légendes urbaines
---
