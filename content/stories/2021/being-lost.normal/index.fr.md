---
date: 2021-06-01T19:00:00+02:00
title: Se perdre…
slug: se-perdre
license: CC BY-NC-ND 4.0

description: |-
  Un jour d'ennui, je pris mes rollers pour aller me perdre en ville.
  Au lieu de ça, je découvris un musée, mais sa visite s'avéra mouvementée.

cover:
  banner: false
  src: metz-cour-d-or-grenier-chevremont.jpg
  alt: Le grenier de Chèvremont du musée de la cour d'or
  by: Rémi Diligent
  link: https://commons.wikimedia.org/wiki/File:Mus%C3%A9es_de_Metz.jpg

keywords: [ nouvelle, fantastique, musée, art, histoire, rome antique, moyen âge, révolution, pirate, voyage dans le temps ]

postface: |-
  Cette histoire a été écrite dans le cadre d'un Défi du Stylo.
  Je m'étais dans un premier temps fixé comme objectif supplémentaire de l'écrire au passé composé, mais le résultat ne fut pas probant, trop déconstruit pour un récit « traditionnel », mais pas assez pour une imitation du style oral.

  J'ai donc choisi de réécrire le même défi de deux façons : l'une qui imite un récit de vive voix, entre deux amis ; la seconde qui respecte les codes que l'on attend des récits écrits, au passé simple et avec une construction qui vise à faciliter la lecture.

  Vous avez certainement reconnu l'inspiration d'un récit oral sur cette page.
  L'autre version est disponible [en cliquant ici](../le-jour-ou-j-me-suis-perdu/)

challenge:
  period: 2021-05-15
  rules:
  - type: theme
    constraint: perdu
---

Avez-vous déjà essayé de vous perdre dans votre ville ?
Je veux dire, vraiment vous perdre, à ne plus avoir de repères et ne plus savoir comment retourner d'où l'on vient.
Se trouver en un lieu inconnu et rendre compte que les bâtiments ne sont pas construits comme à notre habitude et se rappeler que d'autres que nous ont des cultures, des histoires et des vies différentes.

Je l'ai fait.
J'ai essayé.
J'en retiens qu'il n'est pas aisé d'y parvenir dans la ville où l'on a grandi.

Vous vous interrogez certainement sur l'origine d'une idée aussi saugrenue, mais l'histoire en est pourtant simple : je m'ennuyais.
C'étaient les vacances de printemps, qui précédaient mes dernières semaines de cours de première avant les congés d'été.
Les professeurs nous avaient bien entendu donné du travail, mais je m'en étais acquitté tout de suite pour profiter pleinement de mon temps libre.
Voilà ce que j'essayais de faire ce jour-là, avec mes amis partis au loin et mes parents occupés à leur travail.

C'était d'autant plus rageant que la météo était clémente et invitait à profiter de l'extérieur.
Seulement, notre appartement était trop modeste pour bénéficier d'une terrasse ou d'un balcon.
Il m'était par conséquent impossible de m'installer sur une chaise longue pour lire.
Si je ne pouvais pas profiter du beau temps depuis chez moi, le mieux semblait encore être de partir en promenade, mais je n'avais aucune destination en tête.

Voilà la genèse de mon idée : plutôt que d'errer sans but, je pouvais chercher un endroit que je ne connaissais pas.
Je pouvais faire de mon mieux pour me perdre.

Une fois cette idée acceptée, il ne restait plus qu'à la mettre en œuvre.
J'attrapai donc un sac dans lequel j'enfournai ce qui pouvait m'être utile, pris mes rollers pour me déplacer plus rapidement, puis me lançai à l'aventure.
Je m'apprêtai à suivre le chemin de tous les jours, vers le lycée, avant de réaliser que mon objectif étant de me retrouver le plus rapidement possible en terrain inconnu, il serait judicieux d'aller à l'opposé.

Comme un marin se laissant guider par le vent, je naviguais à vue, tournant la tête à chaque croisement et choisissant les voies qui n'éveillaient aucun souvenir.
Le quadrillage urbain est malheureusement ainsi fait que l'on parcourt souvent un ensemble de rues parallèles sans jamais passer par celles qui les rejoignent.
Voilà à quoi je m'évertuais, chaque tentative me ramenant à des paysages connus au croisement suivant, où je devais appliquer mon second critère de décision : choisir la direction qui m'éloignait de chez moi.

Après une ou deux heures de ce petit jeu, j'étais dans un quartier d'immeubles qui semblaient dater des années 60 ou 70.
Comme celui où je vivais, ils semblaient avoir été pensé pour paraître modernes, avec des lignes simples et souvent carrées, et des façades défraîchies.
Plusieurs d'entre eux arboraient encore les fenêtres d'origine, avec leurs montant en aluminium et leur isolation thermique inexistante.
Sur un côté de la rue, il y avait un petit parc, avec un chemin qui descendait vers un lavoir, ou plutôt deux lavoirs se faisant face, de chaque côté d'un cours d'eau qui rejoignait la rivière, avec un pont entre les deux, comme une incursion d'une époque dans une autre.

Ce fut cependant la vue quelques dizaines de mètres plus loin qui acheva de me plonger dans cet état d'esprit : d'un côté de la rue, les bâtiments étaient de la même période que ceux de ma résidence, tandis que de l'autre, ils semblaient sortir directement de la fin du Moyen Âge.
On aurait dit que la route était une frontière entre deux époques.

Cette vision eut tant d'effet sur mon imagination qu'il me fallut quelques minutes pour reprendre mes esprits et m'engouffrer dans la première rue du quartier ancien.
Poursuivant mon errance volontaire, je découvris à cette occasion que ma ville n'était pas aussi plate que je l'avais jusqu'alors imaginée, me hissant à la force de mes jambes le long d'une rue dont la pente était non négligeable.
Cette ascension fut une belle épreuve et j'arrivai essoufflé au sommet, où je choisis de m'arrêter quelques instants pour récupérer, tout en observant les alentours.
Au bout de la rue, en continuant tout droit, je reconnus les arches des bâtiments et, sur ma droite, en levant la tête, j'aperçus les pointes de la cathédrale au-dessus des toits.
J'étais à une centaine de mètres de la place d'armes et, derrière elle, du centre-ville.
Ma tentative pour laisser derrière moi tout repère était un échec total.

Je fus toutefois surpris en me retournant : un vieux bâtiment, d'apparence anodine, affichait sur sa façade « Musée de la Cour d'or ».
Je songeai que cela pouvait être un moyen plus constructif de passer le temps, et mis mes baskets aux pieds et les rollers dans le sac avant d'entrer.
Ma main fouillait dans mon portefeuille à la recherche de mon épargne d'argent de poche lorsque la dame de l'accueil m'arrêta en m'indiquant que la visite était gratuite.
Elle me demanda en revanche de bien vouloir lui laisser mon sac, consigne à laquelle j'obéis sans discuter avant de passer la première porte.

Celle-ci menait à une première galerie en sous-sol.
La mise en scène était bien pensée : profitant de l'absence de lumière naturelle, les concepteurs de l'exposition avaient choisi un sol et des murs noirs.
On aurait presque pu avoir le sentiment d'être perdu dans le vide sans les pièces exposées, des découvertes archéologiques très bien mises en valeur par un éclairage intelligemment pensé.
Ce en passant devant les restes des thermes que je réalisais que ma ville était déjà là du temps des Romains.
Les explications du musée semblaient même indiquer qu'elle bénéficiait alors d'une certaine importance commerciale.

La lumière du jour m'attendait un peu plus loin, se faufilant à travers une porte qui donnait sur le cloître.
La galerie débouchait en effet sur une salle où étaient exposés encore quelques pièces de l'époque romaine.
Devant moi, une guide expliquait à un groupe d'enfants que le mot « copain » datait des légions et qu'il signifiait littéralement « celui avec qui on partage le pain ».
Le seul latin que je connais est celui que j'ai appris en lisant Astérix, mais ça a suffi à me faire penser à _panem et circenses_.
Co-panem, copain… Le lien était assez naturel.

Le groupe continua et je fus tenté quelques secondes de les suivre, mais j'ai finalement préféré faire comme à mon habitude : absorber autant d'informations que possible en lisant les affichettes du musée.
Cela ne m'occupa qu'un temps et je finis par sortir par la même porte que les enfants quelques minutes plus tard, débouchant sur le cloître et une vue qui me laissa bouche bée : un peu partout, des gens en toge se déplaçaient.
Ceux qui croisaient mon regard levaient la main droite et disaient « Ave » en souriant.
Complètement stupéfait, je les imitais simplement.

Un autre visiteur me fit sursauter en sortant de la salle derrière moi et me faisant sursauter.
Je me repris et décidai de profiter de cette reconstitution.
Un légionnaire est passé pour donner un message à un autre acteur.
Son costume me laissa une forte impression : il est courant, dans ce genre de représentation, de voir de loin qu'il s'agit de facsimilés, mais pas cette fois-ci.
À quelques mètres à peine, le métal semblait être métallique et lourd, et le cuir semblait épais et rigide.
Une idée du coût associé à ce niveau de qualité et de prestation me traversa la tête et je me demandai comment le musée pouvait le financer sans faire payer la visite.
Même le cloître était décoré, les piliers maquillés pour ressembler à des colonnes grecques, loin de ce qu'on attend dans une abbaye médiévale.

Des bruits de métal provenant d'un recoin attisèrent ma curiosité et je m'approchai jusqu'à découvrir une forge.
On pouvait sentir la chaleur de la fournaise à plusieurs mètres et l'homme qui travaillait là connaissait manifestement son art, chauffant et tapant le métal avant de le plonger dans son bac d'eau, qui dégageait au passage d'épais nuages de vapeur qui contribuait à l'humidité ambiante.

Préférant quitter la moiteur de cet atelier pourtant intéressant, je repris la visite et passai la porte suivante.
Le changement d'ambiance fut radicale, passant d'une villa romaine ensoleillée à ce qui semblait être une cérémonie religieuse dans une chapelle.
J'essayais de comprendre le discours de celui qui officiait, mais il le faisait en latin.
Certains acteurs échangeaient à voix basse devant moi, mais je ne parvenais pas à les comprendre.
J'entendais clairement, mais ils ne semblaient pas parler français, bien que des sons soient semblables.
Je crus comprendre que l'homme seul au premier rang était Charlemagne et qu'il s'agissait des funérailles de sa femme.
L'ambiance me parut soudainement encore plus pesante et le fait de ne comprendre aucun des intervenants réduisit fortement son intérêt.
Aussi passai-je à la suite du parcours.

Le Moyen Âge m'attendait de l'autre côté de la porte.
Pas celui auquel on pense spontanément, empli de châteaux forts, de catapultes et de chevaliers en armure.
Non, c'était plutôt la fin du Moyen Âge, lorsque commençait à poindre les prémisses de la Renaissance.
Il n'y avait pas plus d'affichette que dans les précédentes reconstitutions, mais on situait plutôt bien l'époque juste aux costumes et au langage utilisé.
Le français était reconnaissable, cette fois-ci.
Ce n'était pas le français actuel, mais ce n'était pas non plus le vieux français qui demande beaucoup de temps à déchiffrer.
J'imaginais que c'est cette langue que l'on appelle le moyen français.

Cette saynète prenait place dans une salle fermée.
Deux hommes étaient penchés sur une table et pointaient diverses zones de ce qui semblait être un parchemin.
Le premier semblait être le propriétaire des lieux et il paraissait indiquer au second comment il voulait que les armoiries soient disposées.
L'autre homme opinait du chef et posait quelques questions pour confirmer, puis indiqua qu'il avait bien compris les consignes.
D'autres acteurs étaient dans la pièce, perchés sur des escabeaux pour peindre le plafond.

En observant de plus près leur travail, je constatais qu'ils avaient commencé à couvrir celui-ci de blasons familiaux ou locaux.
Je reconnus notamment les trois oiseaux blancs disposés le long d'une bande rouge sur fond d'or : les armes de ma région, mais les exemples étaient variés et nombreux : ici, des mouchetures d'hermines semblables à celles du drapeau breton ; là, un lion noir sur fond jaune ; des poissons, jaunes sur fond bleu ou blancs sur fond rouge, et j'en passe.
Je regrettai un instant que mes notions de hiéraldique soient si légères.
J'étais en effet convaincu qu'il y avait des choses à comprendre dans la signification de ces emblèmes, ainsi que dans leur disposition sur ce plafond, mais les connaissances nécessaires me manquaient.
J'hésitai à poser la question aux acteurs, mais ils semblaient tant vivre leur acte que je n'osai pas les interrompre.

Les yeux examinant toujours le plafond, je passai la porte suivante et décrochai finalement le regard pour constater que j'étais de retour dans le cloître.
Seulement, ce n'était plus le cloître que j'avais vu quelques minutes plus tôt.
Les Romains avaient disparu, avec leurs toges, leurs légionnaires, leurs colonnes et leur forge.
Le lieu ressemblait désormais à ce qu'on était en droit d'attendre d'un cloître médiéval.
Quant aux occupants, ils n'étaient pas des moines, comme on eût été en droit de le penser.
Leurs costumes évoquaient plutôt des soldats, de la même époque que dans la salle précédente ou un peu plus tard, mais toujours avant la Renaissance.
En les écoutant, je fus surpris de reconnaître de l'allemand.
Non pas que mes connaissances soient très pointues, mais mes parents avaient insisté pour que j'en fasse ma deuxième langue au collège.
Nous vivions dans une ville qui avait été germanique plusieurs fois par le passé, et l'Allemagne avait un rôle important au sein de l'Europe.
Mes quatre années d'étude de cette langue ne me permettaient pas de saisir toutes les subtilités de ce qui se disait, mais ils semblaient parler d'armées française et d'un roi Charles.
Cela ne m'éclairait cependant pas tellement et je regrettai un instant que le musée n'eût pas opté pour des langues plus explicites.
Qui choisit de faire ses reconstitutions en latin, en vieux français ou en allemand ?

La réalisation me frappa de plein fouet : _qui_ choisit de faire ses reconstitutions en latin, en vieux français ou en allemand ?
Ce n'étaient pas des acteurs qui m'entouraient, ce n'était pas de l'imitation.
Tout était authentique.
Il n'y avait qu'une seule explication à tout cela : chacune des portes que j'avais franchies m'avait fait voyager dans le temps.
Un simple pas m'avait permis de sauter cinq-cents, mille, deux-mille ans.
Je fus saisi d'un vertige, puis d'un désir incontrôlable d'en voir davantage.
Comment savoir ce que je ratais si je me limitais au parcours balisé ?

Jusqu'ici, je n'avais vu que des scènes relativement insignifiantes, mais pouvais-je voir des points cruciaux de l'Histoire ?
Rencontrer des célébrités passées ? Napoléon, Einstein ?
La question classique des séries TV me monta en tête : était-il possible d'empêcher des catastrophes ?
Pouvais-je tuer Hitler et prévenir la Seconde Guerre Mondiale ?

Dans une tentative de discrétion qui m'aurait révélé à n'importe quel observateur, je regardai autour de moi à la recherche d'un garde.
Ne voyant personne, je me suis dirigé vers une porte devant laquelle était placé un cordon difficile à manquer et, après un dernier coup d'œil, j'ai entrouvert.
Je sus immédiatement que c'était un jackpot : ce n'était guère qu'une rue de l'autre côté, mais pas une rue de ma ville.
L'architecture était différente, elle m'évoquait un voyage de classe que nous avions fait à Paris pour y voir l'Assemblée Nationale.
Passant de l'autre côté, je tirai la porte derrière moi et pivotai sur moi-même pour observer les alentours en commençant par la droite.
À quelques centaines de mètres, un amas de meubles brisés bloquait la rue, comme une décharge sauvage, attisant ma curiosité.
Je fis un pas en avant lorsque des cris dans mon dos m'arrêtèrent.
Non, ce n'étaient plus des cris, cela n'avait plus rien d'humain.
C'étaient des hurlements, exprimant une rage devenue bestiale.

Je me retournai et ce qui me faisait face ressemblait aux Français du tableau de Delacroix, sans sa Liberté et son Gavroche, mais avec la même volonté.
Le tableau ne fait pas justice à la réalité cependant.
Avec le son et le mouvement, cette vision n'était pas inspirante, elle devenait effrayante.
Sans prendre le temps de réfléchir, j'ai plongé vers la seule issue à ma portée pour échappée à cette ruée : la porte par laquelle j'étais arrivé.

Les émotions se sont succédées assez rapidement.
D'abord, le soulagement en repoussant la porte d'un coup de pied : je n'avais pas été piétiné par une foule en colère.
Puis rapidement vint la réalisation que j'étais dans une maison.
Pas dans le cloître d'une abbaye médiévale d'où j'étais venu, mais dans une maison d'un Paris en plein révolution française.

Mon premier réflexe fut de vérifier l'autre côté de la porte.
En l'ouvrant, je me retrouvai nez à nez avec des sans-culottes qui me dévisageaient d'un air surpris, jusqu'à ce que je referme rapidement.
La panique commença à se saisir de moi.
Étais-je coincé ?
J'essayai toutes les portes de la maison, espérant que l'une d'elle ouvrirait sur mon point d'origine, sans succès.

Paniqué, je finis par sortir et essayer d'autres maisons jusqu'à trouver une porte qui me montre autre chose que Paris révolutionnaire.
Malheureusement, ce que je voyais n'était pas non plus mon époque ni le cloître.
Deux choix s'offraient à moi : traverser à la recherche d'une autre porte ou tenter de la trouver ici.
J'essayai d'y réfléchir posément, mais n'en était émotionnellement pas capable.
Je venais d'essayer plusieurs dizaines de portes avant de trouver celle-ci et j'avais peur qu'elle s'évapore comme la première.
D'un autre côté, il serait peut-être plus simple de trouver d'autres passages à l'époque que j'apercevais de l'autre côté du seuil.
Je pense que c'est ce que je voulais croire au moment où je l'ai franchi.

Une routine s'instaura rapidement : arriver à un nouvel âge, se lancer à la recherche du passage suivant, s'y jeter en espérant que l'ère suivante soit celle où m'attendrait la porte vers chez moi.
Je perdis rapidement le compte des portails que je passais, je ne prêtais plus attention aux périodes que je traversais et je n'essayais plus de les reconnaître.

L'une d'elle me marqua tant je craignis d'y rester coincé à tout jamais.
J'arrivai par une porte qui s'écroula derrière moi.
La végétation avait recouvert des ruines et tous les encadrements de porte que je croisais étaient cassés.
Je ne connaissais pas suffisamment le fonctionnement des portails pour savoir si un passage pouvait se former dans ces conditions.
Je trouvai par chance un vieil immeuble d'appartements encore à peu près en état et y dénichai une porte ouvrant vers ailleurs.

La dernière étape de mon épopée fut le pont d'un navire.
Alors que j'évaluais encore mes options, une main attrapa l'arrière de mon t-shirt et me souleva du sol.
Je ne parvins pas à voir le personnage qui m'exhibait ainsi comme un trophée mais je crus que sa voix rauque allait crever mon tympan lorsqu'il cria :

— Cap'taine ! On dirait qu'on a un passager clandestin.

Les matelots sur le pont faisaient semblent de ne pas écouter ce qui se passait, mais ils étaient tous beaucoup moins actifs qu'à mon arrivée et de nombreux regards en coin me guettaient, avec un air avide.
L'homme à côté du barreur descendit lentement les marches.
Au-delà de ses vêtements, de bien meilleure qualité que ceux du reste de l'équipage, il dégageait un charisme, une prestance dont aucun autre homme sur le pont ne pouvait s'enorgueillir.
Il s'approcha, m'examina de la tête aux pied sans mot prononcer.
Une fois sa curiosité satisfaite, il se tourna vers celui qui me tenait et prononça simplement deux mots :

— La planche.

Sur ce, il retourna vers l'endroit d'où il était venu.
J'eus le sentiment de tomber alors que la brute qui me tenait laissa retomber son bras et me traîna à travers le pont.
L'équipage avait arrêté de faire semblant de travailler et me suivait désormais franchement du regard.
Plusieurs rictus s'étaient formés sur les visages.

Les jambes de mon porteur s'arrêtèrent à proximité du bastingage, son bras entama un mouvement et sa main me lâcha à mi-course, me projetant au début d'une planche sous laquelle s'étendait l'océan.
Mes propres doigts s'agrippèrent au bord de la planche, empêchant de justesse mon élan de m'emporter dans le vide, mais mes yeux se fixèrent sur l'étendue bleue à perte de vue et je fus paralysé.
J'allais mourir sur un bateau pirate, loin de chez moi et de mon époque, et personne ne saurait jamais ce que j'étais devenu.

Celui qui m'avait porté me hurla d'avancer, bientôt rejointe par le chœur des matelots du pont, mais bouger le moindre muscle m'était impossible.
Je fermai les yeux et les incitations se turent soudainement ; je pensai que je bloquai tous mes sens, mais une nouvelle voix se fit entendre, forte mais pas rauque comme celle des marins.

— Capitaine, vous oubliez notre arrangement !

Avant même d'avoir songé à tourner la tête, mon regard était fixé sur ce nouveau venu à la carrure imposante.
Il se tenait juste devant la porte par laquelle j'étais arrivé.
Son costume était semblable à celui des gardes que j'avais croisés dans le musée et je me surpris à reprendre espoir.

Le capitaine, accoudé au bastingage près de la barre, le fixa d'un air las pendant quelques secondes avant de baisser la tête en soupirant.

— N'envoyez donc pas des divertissements à mes hommes pour les leur retirer au dernier moment, dit-il.

J'ignorais ce que signifiait cet échange pour moi, mais les marins s'étaient éloignés de moi et s'écartait devant l'homme en costume, qui se déplaçait dans ma direction.
Fasciné par ce qui se passait, je fus surpris par une vague et le roulis me fit basculer, mais le gardien m'attrapa à temps et me ramena sur le pont, me remit sur mes pieds et me guida en me tenant par l'épaule.
Les matelots ne nous quittaient pas du regard, mais ils ne firent pas non plus la moindre tentative de nous arrêter.

Nous arrivâmes à la cabine du capitaine sans encombre et la porte nous conduisit en Grèce Antique.
Je me suis laisser guider par cet homme et nous avons à nouveau traversé les époques.
Il maugréait une sorte de remontrance à mon égard, mais je n'en saisis pas tout.
Il me fit remarquer que le parcours était balisé pour une bonne raison et je saisis quelques remarques concernant le glissement géographique, les sauts incontrôlés et d'autres expressions qui ne semblaient pas avoir beaucoup de sens.
Je fus tenté de l'interroger, mais n'eus pas le courage d'affronter la contrariété qui marquait ses traits.

L'homme en costume connaissait visiblement sa route.
Il n'hésita jamais sur la direction ou la porte à prendre, me conduisant à chaque fois vers la suivante sans étape ni hésitation, et nous arrivèrent rapidement à la caisse du musée.
Il me lâcha une fois la dernière porte passée, referma celle-ci et se dirigea vers la première galerie sans un mot à mon attention.
J'ouvris la bouche pour le remercier, mais aucun son n'en sortit.

Tournant mon regard, je constatai que la dame de l'accueil avait observé la scène en silence, tenant mon sac dans sa main.
À nouveau, j'ouvris la bouche pour exprimer ma gratitude, mais son doigt se leva et la sévérité de ses traits m'arrêta.
Elle tendit le bras qui portait mes affaires et je m'en saisis, penaud.

Le retour ne comporta aucun élément marquant, ni même difficulté particulière.
Ma tentative de me perdre avait été un échec du début à la fin.
Pour ce qui était de la ville, en tout cas.
En ce qui concernait le temps, c'était une autre histoire.
