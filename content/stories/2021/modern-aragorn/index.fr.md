---
date: 2021-01-15T13:03:45+01:00
title: Lâchez rien, les gars !
slug: lachez-rien
author: chop
license: CC BY-NC-ND 4.0

description: |-
  C'est l'grand moment, pas question de s'débobiner maint'nant !

inspirations: [ weekly ]
keywords: [ micronouvelle, ville, argot ]

challenge:
  period: 2021-01-15
  link: https://twitter.com/commudustylo/status/1350028835392270336
  rules:
  - type: general
    constraint: Réinterpréter le discours d'Aragorn à l'aube de l'assaut sur le Mordor.
---

Les gars d'la résidence du Gondor !
Et ceux d'celle du Rohan !
Mes frères d'une aut'mère !
J'vois qu'au fond d'vous, z'avez la trouille.
Je pige.
Moi aussi, j'pourrais l'avoir.
Un jour, p't'êt' qu'on l'aura tous et qu'on se barrera comme des péteux en lâchant tous ceux qui comptent sur nous.

Mais pas aujourd'hui.
C'jour-là, c'est l'jour où toute la cité s'ra en train d'se casser la tronche.
Mais c'pas aujourd'hui !

Pour la fierté d'not'cité, on va gagner ce match, et on va leur mett'une peignée, parce qu'on est la cité du Soleil Couchant !
