---
date: 2019-08-23T11:43:55+02:00
title: Golems et mutantes
slug: golems-et-mutantes
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Il suffit parfois d'un peu d'imagination pour observer des créatures fabuleuses.

inspirations: [ weekly ]
keywords: [ micronouvelle, poésie, rêve, golem, mutant, nuage, surnaturel ]

challenge:
  period: 2019-08-23
  rules:
  - type: general
    constraint: 'Utiliser la phrase : « [Il observait des golems éteints défiant des vagabondes mutantes.](https://twitter.com/whatisbot236/status/1163162237378408448) »'
---

Allongé sur son plaid, à l'ombre du grand chêne, il observait le ciel.
Une somnolence s'abattait sur lui et donnait aux nuages des apparences fantastiques.
Là, d'immenses robots, immobiles, tenaient leurs armes levées, menaçant des femmes aux cheveux hirsutes, dotées d'un nombre de membres supérieur à la normale, poussées par le vent dans des directions éparses.
Il observait des golems éteints défiant des vagabondes mutantes.
