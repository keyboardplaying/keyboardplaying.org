---
date: 2019-11-08T14:37:05+01:00
title: Il pleut des cordes, et d'autres choses
slug: il-pleut-des-cordes-et-d-autres-choses
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Les gens me font rire à fuir quelques gouttes de pluie.
  Mais je dois admettre que cette averse s'annonce plutôt violente...

inspirations: [ weekly ]
keywords: [ micronouvelle, pluie, météorite ]

challenge:
  period: 2019-11-08
  rules:
  - type: theme
    constraint: la pluie
---

« Quel temps de chien ! »

Ça me fait marrer.
Je suis certainement le seul, mais ça me fait marrer.
Les gens n'aiment pas la pluie.
Pour moi, c'est certainement le moment le plus tolérable du mois d'août.
Cette chaleur permanente...
Si je n'avais pas l'ordinateur dans mon sac à dos, je ne m'encombrerais même pas du parapluie !

« Poc. »

Comment ça, « poc » ?
Encore un ? Et un autre ?
L'orage virerait-il à la grêle ?
La maison n'est plus très loin, j'ai peut-être intérêt à me hâter.

Oh ! Au vu du bruit, c'était un gros, celui-là.
L'alarme de la voiture sonne, la carrosserie a dû morfler.
Le propriétaire va pleurer.

OUAH ! C'était quoi, ce machin qui est tombé à mes pieds ?!
Jamais vu de la grêle faire un trou dans le béton.
Mais merde ! Ce truc a foutu le feu à mon parapluie !
Je n'ai donc pas imaginé la chaleur quand il est tombé...

Bordel, mais d'où viennent ces...
Oh merde...
J'imagine que c'est le grand frère, qui arrive tout là-haut.
Hm. Courir ne me servira plus à rien, vu sa taille...
