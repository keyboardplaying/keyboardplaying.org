---
date: 2019-12-14T22:27:07+01:00
title: Frémis devant la beauté
slug: fremis-devant-la-beaute
author: chop
license: CC BY-NC-ND 4.0

description: |-
  L'expérience de Stanley est un succès !
  Suffisamment pour qu'il se laisse aller à une rêverie dont la beauté ne peut pas le laisser indifférent.

inspirations: [ weekly, songfic ]
keywords: [ micronouvelle, songfic, ADN, science, biologie, évolution ]
songfic:
  title: Shudder Before the Beautiful
  link: https://youtu.be/7oQ8sNSYXmQ
  artist: Nightwish
  artistLink: http://nightwish.com/

challenge:
  period: 2019-12-13
  rules:
  - type: songfic
---

Stanley n'en revenait pas.
Certes, il avait conçu ses expériences en espérant produire un résultat tel que celui qu'il avait sous les yeux, mais il avait à peine osé rêver de son succès jusqu'à aujourd'hui.
Pourtant, les faits étaient là, posés sur le papier : au sein de son échantillon, initialement un mélange similaire à ce qu'avait dû être l'atmosphère primitive de notre planète, des composants étaient apparus.
Des acides aminés, les composants de nos protéines ; des sucres ; des lipides ; même des acides nucléiques, sur lesquels est construit l'ADN...
À partir de molécules toxiques à nos organismes, il était parvenu à créer les précurseurs de la vie.
Ses mains et la feuille qu'elles tenaient, tremblantes, tombèrent sur le plan de travail, tandis qu'il s'affaissait sur un tabouret de son laboratoire.
Une rêverie s'empara de lui, et il imagina la Terre, après sa création.

L'atmosphère n'était pas la même que celle qu'il connaissait aujourd'hui.
Elle était même irrespirable : méthane, ammoniac, vapeur d'eau, gaz carbonique et hydrogène sulfuré.
Cette soupe primordiale évoluait sous l'effet des différentes énergies qu'elle recevait, notamment les rayonnements du soleil, et les basiques monomères réagirent pour donner d'autres composés, se polymériser.
Les molécules s'allongèrent, se complexifièrent, s'arrangèrent en protéines ou en chaînes d'acide ribonucléique.
Le hasard est bien souvent sous-estimé.
De ce chaos, des formes structurées, riches, infiniment plus stables étaient nées.
Ce n'était pourtant que le début d'une longue série de transformations.

Les lipides s'assemblèrent pour créer des parois, délimitant des structures cellulaires.
L'acide désoxyribonucléique entra en compétition avec son cousin et, plus stable que celui-ci, gagna une place prépondérante.
Par le biais des informations stockées sur ces supports, les réactions chimiques au sein de ces cellules devinrent des opérations programmées, des suites d'instructions ordonnées et répétables.
Parmi celles-ci, on trouvait une demande de réplication de ces informations, permettant aux cellules de se multiplier.
Comme des opérateurs humains travaillant à la chaîne sur des opérations de précision, les enzymes faisaient parfois des erreurs de transcription.
Un caractère était modifié, une phrase entière supprimée ou dupliquée, et la copie devenait complètement différente de l'original.
Les conséquences pouvaient être minimes ou radicales : on parlait ici du programme de vie d'une cellule, de sa naissance à sa mort.
Dans certains cas, il devenait illisible et la cellule n'était pas viable.
Dans d'autres, la différence passait inaperçue.
Restait le cas le plus intéressant : le changement.

Certaines cellules se distinguaient de leur aïeule grâce à ces transcriptions erronées.
Elles pouvaient développer des caractéristiques propres, et parfois des capacités qui leur permettaient de mieux survivre à leur environnement.
Étant mieux adaptées, elles se multipliaient davantage que leurs ancêtres.
L'évolution continuait.
Des cellules commencèrent à collaborer, à travailler en groupe.
Les premiers organismes pluricellulaires apparurent.
Des plantes sous-marines en premier lieu, qui développèrent un mécanisme de photosynthèse, leur permettant d'absorber le dioxyde de carbone et de relâcher du dioxygène.
Les plantes gagnèrent la surface, puis la terre ferme.

Finalement émergea une forme de conscience.
D'abord sous forme de centres nerveux, qui évoluèrent rapidement pour donner naissance à des cerveaux.
Les organismes qui les développèrent acquirent également des membres : nageoires pour explorer les mers, puis pattes pour arpenter la terre et enfin ailes pour prendre leur essor et découvrir les cieux.
L'évolution et la sélection menaient une lutte constante, et il sembla à Stanley que la vie explosait devant ses yeux en une myriade infinie de formes sublimes.
Un frémissement le parcourut devant tant de beauté.
