---
date: 2020-12-24T12:00:00+01:00
title: Le bunker
slug: le-bunker
authors: [chop, sancho]
license: CC BY-NC-ND 4.0

description: |
    Tom ne se souvient pas de l'extérieur.
    Aussi loin que remonte sa mémoire, il a toujours vécu dans le bunker.
    Comme tous les résidents, il s'appuie sur leur ravitailleur, jusqu'au jour où la durée de la sortie de celui-ci l'inquiète.

inspirations: [ bimonthly ]
keywords: [ nouvelle, carrefour des imaginaires, post-apocalyptique, claustrophobie, survivalisme, monstres, romance, enfance, criquets ]

challenge:
  period: 2019-10-31
  rules:
  - type: theme
    constraint: peur
postface: |
    Je n'ai pas relevé notre challenge sur la peur au moment voulu par faute de temps à l'époque.
    sancho de son côté avait une idée intéressante, mais pas le temps de la concrétiser.
    Il m'a partagé le squelette de son histoire et m'a laissé y ajouter un peu de chair.

    Deux fins ont été imaginées pour cette nouvelle et soumises au vote de [la Communauté](https://twitter.com/commudustylo).
    Vous avez lu ici celle qui a été la moins populaire.
    Pour découvrir l'autre, je vous invite à lire notre recueil de nouvelles, [_Carrefour des imaginaires_](https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html).
---

Tous les résidents se levaient, quittant la cantine l'un derrière l'autre, prêts à vaquer à leurs occupations.
Tous à l'exception de Tom.
C'était à son tour de débarrasser la table du repas.
Il se leva et commença à faire le tour de la table, tout en suivant Marie des yeux tandis qu'elle partait avec les autres.
Elle ralentit et se retourna quelques secondes avant de passer la porte, puis disparut.
Il savait qu'en temps normal, elle serait restée avec lui pour l'assister, mais les repas étaient préludes de crampes abdominales depuis quelques jours.
Le mieux pour elle était de se reposer.

Tom ramassait les assiettes en acier inoxydable une par une.
Il utilisait les couverts assortis pour pousser les quelques restes dans le bac de récupération.
Une fois le tour de table terminé, il emporta la pile vers le petit évier pour rincer rapidement tous les ustensiles qu'il avait rassemblés, ainsi que les casseroles et tout ce qui avait servi à la préparation du petit déjeuner.

Il était incapable de garder son esprit concentré sur ces tâches ingrates et repensa au début de cette journée.
Malheureusement, jusqu'ici, rien ne permettait de la distinguer de la précédente ou de celle d'avant.
Les membres de cette petite communauté n'avaient plus beaucoup de choses à se dire.
Ils vivaient en autarcie depuis si longtemps qu'ils avaient déjà épuisé tous les sujets de conversation.
Yvan était le seul à pouvoir apporter encore un peu de vie et de nouveauté.

Sa rapide vaisselle terminée, Tom attrapa le bac contenant les épluchures et autres déchets organiques, et se dirigea vers la serre.
Bien entendu, elle n'avait de serre que le nom : comme le reste du bunker, elle était sous terre, sans accès à la lumière extérieure.
Ce n'était pas gênant pour les champignons, qui en réclamaient très peu.
Les endives, quant à elles, poussaient dans le noir complet, la racine plongée dans l'eau.
Pour le reste, des rangées de LED basse consommation de couleurs variées reproduisaient le spectre lumineux dont avaient besoin les plantes pour se développer.
L'idée était de reproduire ce que les végétaux auraient connu s'ils avaient pu se développer à l'extérieur.
Les LED s'éteignaient treize heures par jour pour simuler la nuit ; juste après son filtrage, l'air qui entrait dans le complexe souterrain passait forcément par la serre, soufflé par le ventilateur afin de produire une brise qui devait inciter les plants à se consolider.
Un système de poulies reliées à un pluviomètre à l'extérieur permettait d'ouvrir plus ou moins fort une vanne alimentée par la source et de simuler une pluie dont la force dépendait des précipitations à l'extérieur.

Marie aimait beaucoup la serre et passait la plupart de son temps à trouver les meilleures conditions de culture de chaque plante : cette variété de pousses préférait-elle les LED rouges, blanches ou bleues ?
Elle pouvait y passer des heures, mais elle n'était pas là aujourd'hui.
Tom vida les déchets dans le bac de compost en cours de remplissage.
Celui-ci serait bientôt plein et pourrait constituer la couche tiède sur laquelle se développeraient les prochains plants.
La chaleur dégagée par la fermentation permettait de réussir pratiquement toutes les boutures et faisait gagner du temps sur l'arrivée à maturité — jusqu'à quinze jours de moins pour les laitues !
En prime, les arrosages nécessaires au démarrage des semis accéléraient le processus de compostage, et le contenu de la couche tiède servait de terreau pour les plants suivants.
La serre avait été mûrement réfléchie.

Avant de partir, il jeta un œil au terrarium.
Les criquets semblaient en forme.
Dans leur abri de verre, ils bénéficiaient d'une lumière plus vive que tous les végétaux cultivés dans la serre.
Plus vive d'ailleurs que l'éclairage des quartiers communs.
C'étaient également les habitants du bunker les plus actifs.

Tom décida qu'il était temps de passer à l'étape suivante de sa journée et jugea que faire sa toilette serait une bonne idée.
Il fit donc un détour par le dortoir qu'il partageait avec son père et Jack, afin d'attraper un ensemble de vêtements propres, avant de se rendre dans la zone sanitaire, à l'opposé de la serre.
Il préférait dans la mesure du possible aller à la selle avant de se laver et s'enferma par conséquent dans une des cabines.
Ses aînés s'étaient longtemps plaints qu'Yvan se soit inspiré du modèle « à la turque », mais Tom ne saisissait guère ce que cela signifiait.
Il ne se souvenait pas d'avoir connu autre chose.
Ayant terminé, il attrapa la douchette pour nettoyer ses zones intimes.
L'eau de la source ruisselait au-dessus de la zone sanitaire.
Y construire quelques dérivations pour alimenter des chasses d'eau et douchettes n'avait pas dû être très compliqué.

Son pantalon remonté, il se retourna pour tirer la chasse… Mais pourquoi s'obstinait-il à prendre cette cabine ?
Le réservoir de la chasse d'eau, fissuré depuis trois ans, avait explosé et s'était vidé sur lui deux mois plus tôt.
Il sortit et remplit un seau au robinet, avant de le vider dans les toilettes.
Il savait que tout cela partait à la fosse septique de l'ancienne ferme à côté de laquelle avait été construit le bunker.
John, son père, avait été plombier « dans une de ses vies antérieures, avant tout ça ».
Il lui avait expliqué comment l'installation fonctionnait : pourquoi avoir la source au-dessus des sanitaires leur permettait d'avoir de la pression sans avoir besoin de pompes ; comment la fosse septique filtrait les éléments indésirables de l'eau avant de laisser couler celle-ci ; pourquoi il était heureux qu'elle soit à l'opposé de la source, afin de ne pas contaminer leur eau ; pourquoi, faute de pouvoir l'entretenir et la curer, elle finirait certainement par devenir un inconvénient plus qu'un avantage pour le bunker.

Cela demeurait très abstrait pour Tom.
Tout fonctionnait presque aussi bien que dans ses premiers souvenirs.
Il lui était difficile de s'inquiéter.
De plus, les journées étaient déjà suffisamment longues sans s'ajouter des préoccupations inutiles.
Il remplit une bassine, attrapa ses vêtements de rechange et un gant de toilette, et s'isola dans une cabine individuelle.
Il se lava du mieux qu'il put.
Malgré la présence de la source, l'eau restait une ressource de luxe.
L'alimentaire était la priorité, le sanitaire suivait, mais les anciens, ceux qui se souvenaient « d'avant », de « dehors », de « l'extérieur », regrettaient souvent de ne pas avoir la possibilité de prendre un bain ou même une douche.
Mais ces plaintes, comme les rires et les discussions, semblaient s'effacer, usées par le temps enfermé dans cette capsule de sauvetage.

« Avant ».
Il était jeune lorsqu'ils s'étaient enfermés dans le bunker.
De l'extérieur, il ne lui restait que quelques vagues impressions et ce que les autres racontaient.
Une vingtaine d'années auparavant, la tension montait entre plusieurs nations et Yvan avait proposé de créer un abri dans lequel une dizaine de personnes pourrait se réfugier si les choses venaient à dégénérer.
Il avait trouvé un réseau de grottes naturelles sur le terrain de sa ferme et voulait y créer son abri.
Ceux qui en deviendraient les résidents s'étaient cotisés pour payer la construction du bunker, s'y assurant par là une place si leurs craintes devenaient réalité.
Yvan avait mis un an à concevoir les plans, puis quatre à les concrétiser dans le secret.

Tom avait fini ses ablutions.
Il terminait de se sécher lorsqu'il entendit claquer une porte, puis des haut-le-cœur.
Marie !
Il enfila rapidement son pantalon et se précipita pour la rejoindre.
Il lui fut facile de trouver dans quelle cabine elle était au son de ses souffrances.
Décider que faire une fois devant cette porte fut une autre affaire.
Il opta pour laisser l'intéressée décider.

— Marie ?
Est-ce que ça va ?
Tu veux que j'entre ?

Il entendit l'eau de la douchette couler, suivie de la chasse d'eau, puis la porte s'ouvrit.
Marie avait le regard clair lorsqu'elle lui répondit :

— Tout va bien.
Merci, Tom.
Je vais aller me recoucher quelque temps.
Tu devrais peut-être terminer de t'habiller.

Elle lui sourit et se dirigea à pas calmes vers son dortoir.
Il était inquiet mais ne savait qu'en penser.
Yvan saurait ce qu'il fallait faire, il suffisait d'attendre son retour.
Il retourna attraper le reste de ses vêtements et s'habilla en réfléchissant à ce qu'il pourrait faire de sa journée.
Il pouvait prendre de l'avance sur ses corvées, ou délester Marie des siennes.
Il posa son linge sale sur la pile de lessive, qu'il devrait nettoyer demain.
Il ne trouva pas la force de s'en occuper aujourd'hui.
C'était certainement la tâche qu'il appréciait le moins.
Il pourrait peut-être l'échanger à Jack contre un tour à la serre.

La bibliothèque aurait pu lui fournir un divertissement, s'il n'avait pas déjà lu trois fois tous les livres qui ne l'avaient pas lassé avant la fin.
Par ailleurs, il n'avait pas envie d'endurer les trois CD que Rosa écoutait inlassablement et sans discontinuer.

Ses pensées furent interrompues par une lumière rouge dans le lobby : la porte extérieure était ouverte.
Le témoin s'éteignit lorsqu'elle se referma.
Yvan était rentré !
Quelques minutes s'écoulèrent, le temps qu'il retire sa combinaison de protection et la laisse dans le sas.
Un second témoin, jaune, marqua l'ouverture puis la fermeture de la porte intérieure.
Tom se dirigea vers le bas de l'escalier pour y attendre son héros.

Les tensions internationales ne s'étaient pas apaisées durant la construction du bunker, bien au contraire.
De guerre froide, on était passé à guerre ouverte et les conflits s'étaient progressivement rapprochés de la région.
Tom n'avait que cinq ans lorsque, douze années plus tôt, ils étaient descendus dans le bunker.
Yvan avait commencé à écouter les transmissions militaires sur un vieux poste radio dans son atelier.
Puis vint la catastrophe : un camp adverse utilisa une arme nouvelle qui dévasta le pays.
La radio rendit l'âme quelques jours plus tard, mais elle endura suffisamment longtemps pour qu'Yvan puisse les avertir des millions de morts et des résidus toxiques que l'arme avait laissés derrière elle.
Ils avaient compris, à ce moment, qu'ils resteraient plus longtemps que prévu dans le bunker.

Les premiers mois, tout s'était passé comme prévu, mais les réserves du garde-manger avaient diminué rapidement en dépit de la serre.
Il était évident qu'une source de nourriture complémentaire serait nécessaire.
Yvan avait bricolé une tenue qui devrait permettre de sortir sans être affecté par les résidus.
Il n'avait pu en faire qu'une seule, faute de matériaux, et prit la responsabilité d'explorer les environs du bunker à la recherche de vivres consommables, dans des emballages hermétiques.

Ce matin, il descendait les marches d'un pas las, portant sur son épaule le vieux sac de marin qu'il utilisait pour rapporter les quelques provisions qu'il trouvait lors de ses excursions.
Il était devenu impossible de deviner la couleur d'origine de la toile, mais elle était solide et n'était toujours pas trouée après des années d'utilisation.
Tom tendit le bras pour délester son idole, qui lui passa le sac sans mot piper avant de se diriger vers la cantine.
Le butin de la nuit était léger.

— Je t'ai gardé une part de notre petit déjeuner, Yvan.
C'était une salade de riz.
J'ai utilisé le reste du sac que tu avais trouvé la dernière fois.
On y a ajouté quelques criquets pour un peu de croustillant.
Tu en veux ?

Tom n'eut pour réponse qu'un grognement, mais il ne s'en offusqua pas.
Il n'était pas rare que son aîné rentre diminué de ses explorations.
Il ne sortait que la nuit, le moment où les créatures de l'extérieur dormaient, afin de chercher des boîtes de conserve, céréales ou autres aliments abandonnés.
Seulement, les années passant, leur ravitailleur devait explorer de plus en plus loin et restait dehors de plus en plus tard.
Il rentrait de plus en plus souvent avec un mal de crâne handicapant, les yeux rouges de fatigue.
Pourtant, jamais il n'avait cédé sa place, sortant toujours cinq nuits par semaine.

— J'ai pas trouvé grand-chose, cette nuit.

Tom sursauta.
Il n'avait pas l'habitude qu'Yvan lui adresse la parole en mangeant, mais il ne dit rien, afin de ne pas décourager le ravitailleur.

— Je pense que tu l'as senti en prenant le sac.
Deux ou trois vieux cartons de céréales, ça sera certainement un peu rassis.

— La bonne nouvelle, c'est que tu es rentré entier, répondit le jeune homme après une hésitation.

Le vieux bougon mâcha sa bouchée de salade, le regard dans le vide, avant de répondre.

— C'est vrai.
Y a pas eu de bestiole pour m'emmerder, cette nuit.
C'était plutôt calme.
Et l'autre bonne nouvelle, c'est que j'ai aperçu une vieille baraque isolée sur une route que j'ai pas prise cette nuit.
Je l'explorerai certainement demain.
J'suis crevé.
Tu veux bien débarrasser pour moi, s'te plait ?
demanda l'homme après avoir déjà fait deux pas vers la porte.

Tom hésita une seconde avant de l'interpeller.

— Yvan ?

L'intéressé s'arrêta et tourna la tête, montrant son profil au garçon.

— Marie est malade.
Elle a vomi tout à l'heure.

— T'es sûr que c'est pas une intoxication alimentaire ?
demanda Yvan en se retournant.
Ce s'rait pas la première fois.

— Non, ça fait plusieurs jours qu'elle a mal au ventre.
Elle ne voulait pas qu'on t'embête avec ça, mais je me fais du souci.

— Elle est où, là ?

— Dans son dortoir, elle se repose.

— Un bon remède à beaucoup de choses.
J'vais faire pareil, je lui en toucherai un mot au déjeuner.

La matinée s'écoula, longue, monotone et semblable à tant d'autres.
À toutes les autres.
Le déjeuner fut aussi calme que le petit déjeuner.
Yvan ne se présenta qu'à la fin du repas.
Il demanda à Marie de le suivre dans son atelier, qui faisait généralement office d'infirmerie, notamment parce que c'était la seule pièce qu'il était possible de fermer pour préserver un tant soit peu la vie privée des patients, et parce que l'établi était suffisamment grand et éclairé pour servir de table d'examen.

Un quart d'heure plus tard, Tom, qui guettait du coin de la cantine, vit Marie sortir et s'empressa de la rejoindre pour prendre de ses nouvelles.
Il ne vit pas Yvan sortir à son tour, éteignant les lumières et se dirigeant vers son dortoir en se passant la main sur le visage.

— Alors Marie, qu'est-ce qu'il a dit ?

— La même chose que ce que je t'ai dit : ce n'est certainement rien.
Il m'a demandé de quoi faire un examen complémentaire demain matin pour être sûr, mais il n'est pas inquiet.
Il pense qu'il y a un médicament efficace qu'il a bon espoir de trouver cette nuit.

— Il t'a demandé de quoi faire un examen complémentaire ?
Comment ça ?

— Des fois, Tom, tu ferais mieux de laisser planer le mystère, tu sais ?
Il m'a demandé de faire pipi dans ce bocal demain pour faire une analyse.
Satisfait ?
Je vais m'occuper des lessives qui t'attendent pour demain.
Tu viens me donner un coup de main ou tu préfères lire avec Rosa ?

Tom fut décontenancé par le contraste entre l'irritation de Marie et son invitation à la suivre.
Ils passèrent plusieurs heures à faire les lessives des deux derniers jours, puis elle se dirigea vers la serre tandis que lui, fourbu, décida finalement de passer un peu de temps à la bibliothèque.

L'heure du dîner arriva.
C'était au tour de Jack de s'occuper du repas du soir et ils pouvaient s'attendre à manger encore une fois une bouillie dont les ingrédients ne seraient plus identifiables.
Pour son plus grand plaisir, Yvan se joignit à eux.
Son repos lui avait fait le plus grand bien et il avait retrouvé sa mine épanouie.
Gaillard, il interrogeait gaiement les occupants du bunker sur les nouveautés en son absence.
Celles-ci étaient cependant rares et il eut vite fait le tour du sujet.

Tout le monde sentait ce qui allait arriver.
Tout le monde savait ce qui allait arriver et l'attendait avec impatience.
Alors que les réponses des résidents se faisaient de plus en plus pauvres, la tension en chacun d'eux montait.
Le moment approchait.
Celui qui faisait que cette vie, enfermés dans un bunker, n'était pas encore devenue complètement insupportable.

— Yvan, tu nous racontes ton exploration de cette nuit ?
demanda John.
J'ai envie d'autres histoires que celles des livres de la bibliothèque.

— Surtout que tu dois tous les connaître par cœur, maintenant, pas vrai ?
répondit Yvan avec un grand sourire.
Non, je ne raconterai pas la dernière nuit.
Elle fut particulièrement morne.
J'ai beaucoup marché, sans voir le moindre signe de vie.
Pas même un corbin.

C'est comme ça qu'Yvan nommait les oiseaux qu'il rencontrait lors de ses sorties.
Il les leur avait décrits : d'énormes corvidés noirs, carnassiers, charognards quand ils le pouvaient mais n'hésitant pas à s'en prendre à une proie vivante lorsqu'elle se présentait à eux dans un moment de faim.
Au fil des années, ils étaient devenus plus imposants, comme la plupart des animaux.
Pratiquement tous avaient changé et étaient devenus plus agressifs.
Jack était convaincu que les résidus causaient des mutations importantes de ce qu'ils ne tuaient pas.
Les corbins figuraient parmi les créatures dont Yvan se méfiait le plus : en dépit de tous les changements que leur espèce avait pu connaître, ils avaient conservé l'intelligence de leurs ancêtres, tout en développant une soif de sang sans précédent.

— Non, continua le narrateur.
Revenons plutôt sur un évènement du passé.
Vous vous souvenez de la meute de lupins qui m'a attaqué la dernière fois ?

— Non, j'ai oublié, répondit Rosa.
Raconte-nous encore.

Yvan n'était pas dupe.
Personne ne l'était.
Rosa avait beau être la doyenne du bunker, elle était loin d'être sénile, mais ces moments partagés étaient si précieux qu'il fallait les faire durer.

— Bien, donc il y a une semaine environ, alors que j'explorais, pas loin d'un vieux corps de ferme, j'entends des bruits dans les fourrés qui m'entourent.
À gauche, puis à droite, et ensuite derrière.
Je comprends un peu tard que des lupins se sont regroupés en meute, que je suis leur proie du moment et qu'ils m'encerclent…

Un frisson parcourut Tom à l'idée des lupins.
Lorsqu'Yvan les avait décrits au garçon quelques années plus tôt, son discours ressemblait à : « Tu as déjà vu le livre de photos animalières ?
Bah oui, tu l'épluchais sans cesse quand t'étais gamin.
Donc, tu prends un loup.
C'est beau, un loup.
C'est majestueux, avec un beau pelage.
On dirait un gentil toutou tout mignon.
Bah un lupin, c'est pas un loup.
C'est un lointain descendant mutant.
Ce sont nos chiens de berger qui ont pris le cataclysme en plein dans leur gueule — et c'est pas un gros mot dans ce cas-là — et ils ont évolué.
Leur pelage est tout mité, avec des gros trous.
Leurs mâchoires sont si énormes qu'elles débordent de leurs babines.
Quand ils sont face à toi, tu vois toujours leurs dents acérées.
J'en ai déjà vu un arracher une branche grosse comme mon bras sans effort !
Et là, je te parle pas encore de leurs griffes.
Pour moi, il n'y avait que les tigres ou les panthères qui en avaient des comme ça !
Mais le pire, c'est pas leurs crocs ou leurs griffes.
Le pire, c'est leurs yeux.
Des yeux rouges.
Quand ils te regardent avec ces yeux-là, avec leurs dents à l'air libre et le filet de bave qui en coule, tu sais qu'ils n'ont qu'une seule idée en tête : c'est te bouffer !
Te faire payer pour tous les siècles où les chiens ont été à la botte des hommes, et s'en mettre plein la panse au passage.
»

Tout le monde écouta avidement l'histoire de comment Yvan avait pu se réfugier dans les étables, seulement pour se rendre compte que les bêtes l'avaient suivi, et comment il leur avait échappé in extremis par les toits, parvenant à les enfermer par un concours de circonstances et un gros coup de chance.
Les auditeurs furent également captivés lorsqu'il raconta comment, la nuit précédente, il était repassé à côté de cette ferme et avait constaté que la porte avait été abattue par la force brute de ces animaux mutants.
Par leur force brute et certainement par leur désespoir, car plusieurs carcasses de lupins étaient là, à moitié dévorées par le reste de la meute.
Ces bêtes étaient sans foi ni loi, et aucun lien ne les pousserait à une quelconque loyauté.
Pas entre eux et encore moins vis-à-vis des humains.

Les histoires s'enchaînaient.
Le repas était terminé, les parties de belote défilaient et Rosa s'était endormie sur sa chaise.
Remportant la dernière manche, Yvan se leva en déclarant :

— C'est pas l'tout, mais il est temps que je me mette en route.
J'ai pas mal de chemin à faire cette nuit pour explorer et attraper tout ce que j'ai prévu.
Allez vous coucher gentiment, on se retrouve demain matin.

Tom rejoignit son dortoir.
L'ameublement était composé de lits superposés offrant quatre couchettes, quatre armoires, quatre bureaux et deux commodes qui faisaient office de tables de nuit.
John dormait au-dessus de lui tandis que Jack occupait la couchette supérieure de l'autre superposition.
La dernière place était celle d'Yvan, mais Tom ne se souvenait pas la dernière fois où ils avaient dormi en même temps.

Entre les ronflements de son père et les marmonnements de Jack, qui parlait dans son sommeil, Tom eut du mal à s'endormir.
Il se réveilla dans la nuit et devina une silhouette à l'entrée de leur chambre.
Il se frotta les yeux pour y voir plus clair et, lorsqu'il les rouvrit, l'homme se dressait à mi-chemin entre la porte et son lit.
Dans la pénombre, il distingua les traits d'Yvan, mais ceux-ci étaient ravagés.
D'énormes cloques couvraient son visage, et un trou dans la chair de sa joue laissait apparaître ses mâchoires.
Son œil gauche était exorbité, comme s'il n'avait plus de paupières.
Il lui manquait des touffes entières de cheveux, laissant apparaître de véritables trous dans sa chevelure frisée.

Tom était paralysé face à cette vision.
Il n'arrivait pas à déchiffrer l'expression de celui qu'il attendait si souvent au bas des escaliers.
Son visage lui évoquait la face d'une bête sauvage.
Il n'avait aucun doute : Yvan avait subi les transformations qui avaient autrefois changé les chiens en lupins.
Comment ?
Sa combinaison avait-elle été insuffisante ?
Abîmée ?
Trop ancienne ?
Une autre question lui traversa l'esprit, et il fut frappé par l'absurdité de s'interroger sur ce point en un moment si critique : comment allaient-ils nommer cette nouvelle espèce ?
Les contaminés ?
Les humanins ?
Les Homo Mortem ?
Ou tout simplement les zombies ?

Tom retenait sa respiration, espérant ne pas attirer davantage l'attention de la répugnante et terrifiante créature qui avait autrefois été son mentor et son ami, mais il était trop tard : dans un grognement qui gagna en intensité jusqu'à devenir un hurlement, l'être se précipita vers lui en tendant les bras pour l'attraper.
Tom essaya de lui échapper et tomba de son lit.
Désorienté, il regarda autour de lui.
Tout était calme.
Même son père ne ronflait pas, sa respiration étant presque silencieuse.
L'éclairage de nuit du hall commun ne révélait aucun mouvement et personne ne se promenait dans la chambre.
Tout ça n'avait été qu'un mauvais rêve.

Tom s'installa de nouveau dans son lit et essaya de se rendormir, mais la terreur du cauchemar ne l'avait pas complètement quitté.
Il finit par sombrer malgré tout dans un sommeil agité.

Le lendemain, Tom vit Marie déposer un flacon dans l'atelier avant de se diriger vers la cantine.
Ils prirent leur petit déjeuner en silence avec les autres.
Elle grignota d'un appétit léger et Tom l'entendit plus tard vomir comme la veille, mais elle alla s'isoler dans la serre sans chercher sa compagnie.
Yvan tardait à rentrer.
Le jeune homme chercha comment s'occuper l'esprit.
Il était malheureusement à court de corvées et opta pour la salle de sport.

Ils bénéficiaient de peu d'équipement pour se défouler.
Lors de la conception du bunker, le sport n'avait pas été une priorité.
Déjà parce que la survie était l'unique priorité.
Ensuite parce que, pour dépenser de l'énergie, il faut en assimiler davantage.
Ceci semblait entrer en contradiction avec l'idée de faire durer le garde-manger.
En dépit de ces considérations, et parce qu'il n'était pas prévu originellement de rester si longtemps sous terre, Yvan avait tout de même jugé bon d'avoir un minimum de matériel afin d'éviter une perte musculaire trop importante en raison de l'inactivité.
Quelques haltères, trois ou quatre cordes à sauter et un vélo de salle.
L'usure et les nombreux rafistolages de celui-ci indiquaient à quel point il avait pu aider les occupants du bunker à tromper leur ennui.
Tom monta dessus et pédala, laissant ses préoccupations de côté pour se concentrer sur l'effort.

Au déjeuner, Yvan n'était toujours pas revenu.
Tom essaya de se distraire en relisant La Communauté de l'Anneau, mais il ne parvint pas à se concentrer.
Où était Yvan ?
Pourquoi ne rentrait-il pas ?
Pourquoi aujourd'hui, alors qu'il devait ramener des médicaments ?
Son rêve le troublait.
Était-ce une prémonition ?
Était-il arrivé quelque chose à leur ravitailleur ?

Marie essaya de le rassurer au cours du dîner.

— Ce n'est pas la première fois qu'il passe une journée dehors.
Tu te souviens de la fois où il est parti au sud et qu'il a dû rester dehors plus d'une semaine pour réussir à semer les pillards qui le suivaient ?
Il a dit qu'il devait aller loin cette nuit.
Il a dû avoir du mal à rentrer à temps et a trouvé un abri pour la journée, afin de ne pas se faire attaquer par les créatures diurnes.
Il va faire le reste de la route cette nuit.

Tom écoutait et acquiesçait, mais n'était pas convaincu.
Cela faisait longtemps qu'un dîner n'avait pas été égayé par les histoires de leur explorateur.
Marie avait beau essayer de reprendre ses aventures pour lui remonter le moral, cela n'avait pas le même effet.

Elle partit se coucher tôt après le repas, tandis que les anciens sortaient les cartes.
Sans Yvan, la partie serait calme, mais les habitudes avaient la vie dure.
Tom observa le jeu sans y participer, plongé dans ses pensées.
Son inquiétude pour Marie le rongeait.
Son rêve continuait de le tourmenter en l'absence de signe d'Yvan.
Le pire était de ne rien faire : comment pouvait-il rester ici à attendre alors que deux des êtres les plus chers à son cœur étaient peut-être en train de mourir ?

— Je pars à sa recherche, déclara-t-il soudain d'un ton calme et résolu.

Les joueurs s'arrêtèrent.
Ceux qui lui tournaient le dos pivotèrent.
Ce fut finalement son père qui rompit le silence :

— À sa recherche, à qui ?

— Je vais retrouver Yvan, voyons !

— Mais t'as pas écouté Marie, ou quoi ?
Elle a raison, il va rentrer pendant la nuit !

— Il nous avait dit qu'il rentrerait au matin.
Ce n'est pas normal.

— Un imprévu, ça arrive, intervint calmement Rosa.
Je ne m'inquiète pas pour Yvan.
Il ne craint pas grand-chose.

— De toute façon, on ne peut pas sortir du bunker, commenta Jack.
Yvan est le seul à avoir la clé du sas, et il l'emporte avec lui à chaque fois qu'il sort.

— Il reste la sortie de secours.

Le bunker avait été conçu avec deux issues.
La porte principale était camouflée et menait à un sas.
Toute entrée et sortie se faisait normalement par ce biais.
Cependant, Yvan avait anticipé que le refuge pourrait être assiégé par des pillards ou autres forces hostiles et avait prévu une sortie de secours à l'autre extrémité du bunker.

John rappela que cette porte n'était prévue pour être utilisée qu'en cas d'extrême urgence, à quoi son fils répondit que la vie de leur seul ravitailleur répondait à cette qualification.
Le père rappela que l'issue de secours ne pouvait pas être refermée, le jeune homme proposa de mettre en place une barricade en attendant une meilleure solution.

— Et concernant l'extérieur ?
Tu comptes sortir sans tenue de protection ?
Yvan a la seule à notre disposition.

— Je survivrai quelques heures.
Cela fait plus de dix ans, les effets ont dû s'atténuer.
De toute façon, entre mourir dévoré ou contaminé à l'extérieur et crever ici affamé parce qu'on n'a que la serre pour subvenir à nos besoins, je préfère tenter un truc qui sauvera peut-être certains d'entre nous qu'attendre gentiment la fin !

John resta bouche bée et ne trouva pas de réponse.
La dernière réplique de Tom sembla pénétrer l'esprit de Jack.
Son corps se redressa et ses muscles s'affermirent, reflet de sa volonté.

— T'as raison, gamin !
On va pas attendre d'y rester.
Je t'accompagne.

La bouche de John s'ouvrit un peu plus et sa voix ne semblait être guère plus qu'un souffle lorsqu'il reprit :

— Mais comment saurez-vous seulement où aller ?

— On trouvera, papa, déclara Tom, convaincu que sa décision était la bonne.

Il n'attendit d'ailleurs pas d'autre réponse.
Se levant, il se dirigea vers le hall commun et bifurqua complètement à droite.
Ce fut la voix de Jack qui l'arrêta.

— Eh, attends une minute !
On va pas partir comme ça, avec juste ce qu'on a sur le dos.

Tom se retourna, la curiosité visible sur son visage, mais ne prononça pas un mot.
Jack continua :

— J'ai un vieux couteau dans mes affaires, au dortoir.
Ça peut toujours être utile.
Un vieux sac à dos, aussi.
On devrait aussi faire un tour dans l'atelier d'Yvan, il y a certainement des trucs qui peuvent servir.
Et quand on vadrouille, il faut au moins avoir à boire.
Les bouteilles en acier de la cantine font de bonnes gourdes.
Attrapes-en trois, de la bouffe si tu trouves des trucs qu'on peut manger sur le pouce, et rejoins-moi à l'atelier.
Je vais attraper mes frusques pendant ce temps.

Tom ne protesta pas et obtempéra, les remarques de Jack lui semblant pertinentes.
Qui était-il pour les remettre en cause, de toute façon ?
Jack avait connu l'extérieur, « avant », alors que lui ne se souvenait que du bunker.

Les derniers occupants le regardaient.
Ils ne s'interposaient pas, mais ne l'aidaient pas non plus.
Ils semblaient juste stupéfaits que la routine du bunker puisse être compromise de façon si spectaculaire.
John semblait le plus affecté : son unique fils s'apprêtait à quitter la seule sécurité qu'il ait jamais connue.
Pourtant, il était comme figé.

Tom avait terminé de fouiller le placard.
Trois gourdes vides d'un litre chacune, qu'ils pourraient remplir à la source avant de partir, et deux boîtes de conserve périmées depuis trois ans.
Des haricots rouges et des cœurs de palmier ; de quoi manger en cas de besoin.
Il prit également le paquet de céréales qu'Yvan avait rapporté la veille et se dirigea vers l'atelier, où Jack avait déjà entamé ses recherches.
Aucune des lampes torches ne fonctionnait.
Leurs seules trouvailles furent une machette, une vieille paire de jumelles dont une des lentilles manquait et un rouleau de corde.

Jack chargea le sac, puis le mit sur ses épaules.
Les gourdes et la corde représentaient un poids non négligeable pour quelqu'un qui n'avait pas fait de randonnée depuis douze ans, mais Tom ne pouvait pas réaliser cela.
L'homme prit la machette et confia son couteau au jeune homme, qui le glissa dans une poche.

Ils se dirigèrent vers la porte de secours du bunker et s'apprêtaient à l'ouvrir lorsque des pas se firent entendre derrière eux.

— Tom !
Ne fais pas ça, fiston.
C'est trop dangereux, dehors.

— Ne t'inquiète pas, papa.
Nous serons de retour avant le lever du soleil avec Yvan.
Barricadez la porte derrière nous.

— Non, c'est de la folie !
Tu as perdu la tête !
Tu ne seras pas le seul en danger.
Les résidus vont entrer et contaminer l'intérieur du bunker.
Ou des animaux mutants affamés.
Tu nous mets tous en danger, pour une seule personne !

Tom n'en croyait pas ses oreilles et commença malgré lui à élever la voix à son tour.

— C'est grâce à lui qu'on est encore en vie !
C'est lui qui a permis la construction de ce bunker !
C'est lui aussi qui trouve le moyen de nous rapporter de la nourriture !
La serre ne suffirait pas pour nous tous.

— Mais je ne te parle pas de lui !
Je te parle du rat de gouttière qu'on a accueilli en dernière minute au moment de sceller le bunker.
Tout ça parce que ta mère s'est dit que ce serait une bonne idée parce que vous aviez le même âge.
Et figure-toi qu'elle…

John s'effondra par terre.
Tom avait les joues en feu et ne saisissait plus ce qu'il se passait.
Sa raison s'était arrêtée lorsque son père avait qualifié Marie de « rat de gouttière ».
Le jeune homme observait la scène dans l'espoir de comprendre ce qui s'était passé quand Jack se baissa pour attraper John sous les aisselles en disant :

— Viens m'aider à le mettre dans son plumard.

— Tu l'as… ?

— Je l'ai assommé.
La conversation ne partait pas dans une bonne direction, on s'rait pas encore parti demain matin si on avait attendu.

Ils déposèrent l'homme inconscient sur la couchette de son fils et retournèrent à l'issue de secours.
Jack força pour abaisser le levier de la porte blindée, grippé après des années sans avoir été utilisé, et ils se mirent à deux pour pousser la porte, puis firent de même de l'autre côté pour tenter de la refermer, sans y parvenir totalement.

Le noir était total.
Et pour cause !
La porte ne donnait visiblement pas directement sur l'extérieur.
Ils semblaient être dans un tunnel naturel, qu'ils suivirent aveuglément.
Après plusieurs heurts et chutes, ils durent finalement s'arrêter face à une surface plane.
Ce n'était pas de la pierre.
Du bois ?
Un panneau, certainement placé là pour camoufler l'entrée du tunnel.
Ils tentèrent de le pousser, mais il ne bougea que peu.

Jack commençait déjà à s'impatienter du temps perdu pour sortir et décida d'user de force brute.
Il appliqua tout son poids dans un coup de pied qui traversa le bois vermoulu.
À partir de là, ils n'eurent plus de grande difficulté à dégager le panneau.
Le mur végétal qui s'était développé dessus s'avéra plus coriace mais la machette fut efficace contre cet obstacle.
Ils débouchèrent ainsi dans une petite clairière, éclairée par une demi-lune qui fascina Tom jusqu'à ce que Jack interrompe ses pensées.

— J'ai une grande question pour toi, maintenant, gamin : dans quelle direction on part ?

Tom réfléchit.
Il essaya de se souvenir des histoires d'Yvan, mais les quelques points de repère dont il parlait étaient toujours loin du bunker.
En outre, la sortie qu'il empruntait débouchait à un autre endroit.

Après l'obscurité totale du tunnel, la lumière argentée de la lune suffisait presque à y voir clair et Tom devinait une sente à travers les arbres.
Le temps leur était compté, faisant de leur vitesse de progression un atout majeur.
Moins d'obstacles signifiaient un déplacement plus rapide.

— Là, on suit ce chemin.

Ils avançaient, Jack en tête, la machette en main pour parer à toute rencontre désagréable.
Plus tard, ils entendirent derrière eux la voix de John, criant au loin, appelant son fils, lui demandant pardon et le priant de rentrer.
Les cris semblèrent gagner en panique et Tom voulut l'appeler pour le rassurer, mais Jack lui couvrit la bouche d'une main en pointant sa propre oreille de l'autre.
Tom écouta plus attentivement et entendit des grognements, puis des éclats de voix, mais ils n'étaient pas humains.
Jack chuchota :

— Des lupins.
Il est trop tard pour ton père.
Et on a intérêt à bouger avant qu'ils nous reniflent, ou on suivra le même trajet jusqu'au fond de leur estomac.

Les cris de son père s'étaient tus.
Cela faisait trop d'un coup.
La disparition d'Yvan, la découverte de l'extérieur, la Lune, la forêt, la mort de son père, la menace sur la santé de Marie… Marie.
Il se concentra sur elle pour raffermir sa volonté et avancer.
Une larme lui échappa en songeant à son père, mais il n'avait pas de temps à perdre.
Retrouver Yvan, sauver Marie.

Ils avançaient, suivant la direction que leur indiquait leur instinct.
Tom avait déjà remarqué que la musculature d'Yvan était plus développée que celle des résidents confinés au bunker, mais la taille limitée du refuge ne lui avait jamais laissé deviner à quel point ils manquaient d'endurance.
À force de ne pas dépasser les limites de leur petit monde protégé, leurs muscles s'étaient atrophiés.
Ils fatiguaient rapidement et faisaient des pauses fréquentes.
À chacune d'entre elles, ils regardaient autour d'eux, scrutaient les environs immédiats dans le faible éclairage lunaire, en quête d'un signe du passage d'Yvan : une empreinte de pas, une brindille cassée… Jusqu'ici, ils n'avaient rien trouvé.

Lors d'une pause, Jack s'allongea par terre et observa le ciel à travers la cime des arbres.

— J'avais oublié, putain !

— Oublié quoi ?

— À quel point c'est beau !
Comment j'ai pu m'enfermer dans ce bunker pendant toutes ces années ?

— C'était le seul moyen de survivre aux résidus, tu n'as pas vraiment eu le choix.

— Ça fait plusieurs heures qu'on est dehors et on ressent rien.
Y a plus d'résidus !
On est bons pour sortir.
Y a p't-êt' même d'autres survivants, ailleurs.
J'aimerais bien les retrouver, histoire qu'on essaie de reconstruire.

— Tu ne penses pas qu'Yvan les aurait rencontrés, depuis le temps ?
Mis à part des pillards qui ont essayé de le tuer ou de trouver le refuge, il n'en a jamais croisé.

— ‘l est limité, Yvan.
I' doit revenir au bunker chaque matin.
Mais y en a p't-êt', plus loin.

— On n'aura pas le temps de les chercher, Jack.
C'est Yvan notre priorité.

— T'inquiète, p'tit !
J'te laisserai pas tomber.
On va l'retrouver.
J'pense surtout à après.
J'retournerai pas dans ce bunker.
L'extérieur m'a trop manqué.
Mais on retrouve Yvan d'abord.
Allez, on s'est assez reposé, ‘faut avancer.

Ils continuèrent ainsi pendant plusieurs heures, jusqu'à deviner une masse se détachant des arbres.
Une masse plus carrée, plus structurée.
Plus proche du bunker que des plantes de la serre.

— Jack, regarde ça.
Yvan a dit qu'il avait dû renoncer à explorer une baraque isolée parce que ça ne lui aurait pas permis de rentrer à temps.
Tu penses que ça peut être ça ?

— C'est pas une baraque isolée, ça, répondit Jack en plissant les yeux.
Ou en tout cas pas une petite.
On dirait qu'il y a tout un corps de ferme.

— Une ferme ?
Tu penses que ça peut être celle où il avait enfermé les lupins ?

— Là où les machins s'étaient bouffés entre eux ?
Possib… Merde !
T'as vu ça ?

— Quoi ?
demanda Tom.

Jack n'avait pas attendu sa réponse.
Il avait posé le sac à dos à terre et fouillait dedans.
Il en tira finalement les jumelles et les posa sur ses yeux, touchant une mollette, puis l'autre, avant de grommeler un juron.
Il retourna les jumelles, repéra le trou béant où manquait un disque de verre, puis regarda à travers une seule des deux lentilles.

— Gamin, j'suis plus tout jeune, maintenant.
T'aurais dû voir bien avant moi qu'il y avait de la lumière !

— J'ai vu.
C'est anormal ?
Le bunker est éclairé en permanence.

— C'est vrai que t'as connu que ça, toi.
Dans le bunker, y a nous.
À l'extérieur, c'est rare qu'on laisse des lumières brûler pour rien.
Et quand y a plus d'humains parce qu'un truc les a tous tués, c'est étonnant que quoi que ce soit d'électrique fonctionne encore.

— Alors tu penses que… Quoi ?

— Je pense qu'on est tombés sur un groupe de survivants.
Si ça se trouve, ils ont croisé Yvan et ils l'ont capturé.

— Pourquoi ils auraient fait ça ?

— Qu'est-ce j'en sais, moi ?
Pour lui piquer ses réserves ?
Pour l'interroger sur les trucs intéressants du coin ?
Si c'est tes pillards, va savoir.
Ou pour le bouffer, p't-êt' !

— Le bouff… T'es sérieux, là ?

— Gamin, le bunker nous a bien protégé.
Quand t'as plus rien à bouffer, ton meilleur pote peut devenir un simple morceau de bidoche à tes yeux.
Alors un autre survivant que tu connais pas, ça peut être un festin pour un groupe qu'a pas eu de viande depuis longtemps.

— Quoi ?
On peut pas les laisser faire, il faut qu'on aille le sauver.

— Et comment, qu'on va y aller !

Jack prit le contrôle du duo, qui se dirigea vers la ferme, s'approchant en se faisant le plus discret possible.
Ils ignoraient à combien d'adversaires ils avaient affaire, jusqu'au moment où une femme, qui semblait avoir l'âge de Rosa, sortit du bâtiment éclairé et se dirigea vers un autre, qui s'alluma après qu'elle ait disparu à l'intérieur.

— OK, donc elle est seule, là-d'dans.
C'est le moment où jamais.
On va pouvoir l'interroger pour savoir à quoi s'attendre.

Tom acquiesça et suivit son compagnon, lequel tenait la machette en l'air, prêt à l'abattre sur toute créature dangereuse.
Ils se faufilèrent dans le bâtiment et virent la femme traîner un tabouret le long d'une allée bordée de boxes.
Jack hurla :

— Ne bougez plus !
Dites-nous où est notre camarade !

La femme sursauta et se retourna, visiblement apeurée.
Le tabouret lui était tombé des mains.
Jack pointait sa lame vers elle, et elle avait les yeux rivés sur le métal piqué de points de rouille.

Des voix s'élevèrent des boxes, mais Tom savait que ce n'étaient pas des gorges humaines qui les produisaient.
Il devinait que le cri de Jack avait dérangé ces créatures dans leur sommeil.
Ces survivants avaient-ils capturé des lupins en si grand nombre ?
Dans quel but ?
Et avec quoi les nourrissaient-ils ?
Était-il déjà trop tard pour sauver Yvan ?

Jack ne détourna pas son regard de la femme, perplexe.
Celle-ci finit par répondre d'une voix chevrotante.

— Pardon ?
Quel camarade ?

— Ne faites pas l'innocente !
Nous savons que vous l'avez capturé.
Où est-il ?
Qu'avez-vous fait de lui ?

La confusion sur le visage de la femme semblait à son paroxysme.
La peur y était clairement visible également.

— Je ne comprends pas de quoi vous parlez.
Je suis là pour la traite matinale de…

— Assez !
D'où venez-vous ?
Pourquoi ne vous a-t-il jamais vus auparavant ?

— Quoi ?
Mais nous vivons ici depuis presque cinquante ans.

— Jack… tenta timidement Tom, perturbé par l'incompréhension de la femme.

Peut-être disait-elle la vérité, peut-être leur manquait-il des éléments pour comprendre.
Mais avant que le garçon puisse faire part de ses doutes, il entendit un bruit de pas sur le gravier derrière lui.
Il n'eut pas le temps de se retourner et ressentit un choc derrière la tête, vit un grand flash, puis plus rien.

La première chose que Tom sentit en revenant à lui fut la douleur qui semblait fissurer son crâne.
Petit à petit, il prit conscience d'autres détails : il était allongé, il ne voyait que le noir, ses yeux étaient fermés.
Lorsqu'il les ouvrit, il vit des murs gris, comme ceux du bunker.
Il était sur une couchette qui lui rappelait son lit, avec un matelas fin et mou sur un sommier dur.
Il essaya de se lever, mais la douleur lui vrilla le cerveau et il retomba.
Il avait eu le temps d'apercevoir des barreaux.
Trois murs de béton gris fermé par une imposante grille.
Il n'y avait aucun doute, il était dans une cage.

Tout lui semblait confus.
Comment était-il arrivé là ?
Il essaya de se remémorer.
Il était à la recherche d'Yvan, avec Jack.
Ils avaient trouvé un refuge de survivants et avaient pris l'une d'elle à part pour l'interroger, puis le trou noir.
Aurait-il été assommé par un complice ?
Étaient-ce eux qui l'avaient enfermé là ?
Et où diable était Jack ?

Il tâta ses poches à la recherche du couteau qu'Yvan lui avait confié, le seul moyen qu'il avait de se défendre, mais il n'était plus dans sa poche.
Ses ravisseurs le lui avaient certainement confisqué.

Tom essaya de regarder autour de lui, mais il était seul dans cette cellule.
Des sons lui parvenaient comme étouffés, mais ils lui parvenaient néanmoins.
Il entendait des voix humaines.
Peut-être d'autres prisonniers ?

— Jack ?

— Ta gueule !
cria une voix rauque qui semblait venir du côté de sa prison.

— Ta gueule aussi, Dumont, répondit une autre voix.

Tom entendit les pieds d'une chaise frotter par terre, puis des pas, accompagnés par le bruit de bouts de métal qui s'entrechoquent.
Des clés ?
Quelques secondes plus tard, une porte s'ouvrit en produisant un son similaire à celle du sas dans le bunker.

— Ah, tu arrives à pic.
Je crois qu'il vient de se réveiller.
Il a appelé son copain, j'allais voir comment il se sent.

— Merci, Philippe, dit une voix qui semblait familière à Tom.
Je vous revaudrai ça.

— T'en fais pas pour ça, Yvan.
Désolé pour ton autre gars.

Yvan ?
La voix était devant la grille quand elle avait répondu.
Tom dressa lentement la tête et distingua vaguement la silhouette de leur ravitailleur à côté d'un homme vêtu de bleu, lequel semblait déverrouiller une porte.

— Yvan ?

— Boucle-la, Tom, s'il te plait.
On causera quand tu te sentiras mieux.

Yvan entra dans la pièce et aide le garçon à se lever.
Sa tête tournait, mais l'homme lui passa un bras sous l'aisselle pour le soutenir.
Le geôlier indiqua :

— Notre médecin pense qu'il n'y a rien de grave, mais tu devrais quand même l'emmener à l'hôpital pour t'en assurer.

— Ouais, je ferai ça.
Merci encore.

— Tu as tout ce qu'il te faut ?

— Oui, j'ai signé toutes les paperasses avant de le récupérer.
Vous savez où me joindre de toute façon.

— Ça marche.
Besoin d'aide pour l'emmener ?

— Non, ça va aller, merci.
Il pèse pas lourd.
Passe une bonne journée.

Tom écouta, mais ne dit rien.
Parce qu'Yvan le lui avait demandé, d'abord.
Parce qu'il ne comprenait rien à ce qui se passait ensuite.
Il se sentait complètement perdu.
Yvan lui fit monter des escaliers, traverser un espace où de nombreuses personnes s'affairaient autour de bureaux, puis sortir à l'extérieur, en plein jour — Tom était ébloui, il ne se souvenait pas de la dernière fois où il avait vu le soleil — et monter dans une cabine métallique vitrée, avec des fauteuils.
Il attacha le garçon à l'un d'eux, posa sur ses genoux une enveloppe beige et referma la porte.

Le ravitailleur fit le tour de la cabine, ouvrit une autre porte à la gauche de Tom et s'installa, s'attacha et referma.
Il toucha quelque chose et le sol se mit à vibrer.
L'étrange cellule se mit en mouvement, visiblement contrôlée par Yvan.
Celui-ci était silencieux et renfrogné.
Pas comme lorsqu'il rentrait éreinté de sa nuit, non.
Non, il était contrarié.
Tom ne disait rien, il attendait.

Sa tête semblait retrouver sa place, sa vision se faisait plus nette, ses acouphènes s'estompaient.
Quelques minutes plus tard, Yvan finit par prendre la parole.

— Qu'est-ce qui vous a pris, bande de nigauds ?
Quelle putain d'idée vous avez eu de vouloir sortir ?

— On est venu à ta rescousse.
Tu avais dit que tu rentrerais au matin et on n'avait aucune nouvelle de toi le soir.
On s'inquiétait.
Quand on est tombé sur la ferme et qu'on a vu de la lumière, on a pensé que c'étaient des pillards et qu'ils t'avaient capturé.

— Vous aviez faux sur toute la ligne !
C'est des vieux fermiers innocents que vous avez braqués avec une machette au petit matin.
Vous avez failli faire crever la vieille Mathilda de peur !
Les cris des bêtes ont attiré son mari.
Il vous a neutralisé et a appelé la police.
Heureusement, j'avais pris les devants en prévenant les flics que deux travailleurs un peu simplets de ma propre ferme s'étaient fait la malle.
Ils ont pensé à moi quand ils t'ont embarqué.

Tom se remémora l'épisode de la ferme une fois de plus et réalisa l'absence de son compagnon.

— Où est Jack ?

— Pierre t'a assommé avec le manche d'une fourche.
Quand Jack t'a entendu t'effondrer, il est devenu vraiment menaçant avec sa machette et Pierre n'a pas trouvé d'autre solution que de le planter.
Jack ne rentrera jamais.

Ces mots eurent un drôle d'écho, tandis que la voix de Jack résonna dans les oreilles du garçon : « J'retournerai pas dans ce bunker.
»

Le jeune homme resta silencieux quelques minutes, essayant de donner un sens à ce qu'il avait vu depuis sa sortie, quand Yvan reprit :

— Ton père ira bien, au fait.
Il est déjà dans le bunker, lui.

— Il n'a pas été dévoré par les lupins ?
demanda Tom, incrédule.

L'ancien ricana.

— Non, c'étaient pas des lupins.
Les lupins n'existent pas.
C'étaient les chiens de garde de ma ferme.
Les gardes qui font le tour de la propriété sont tombés sur lui, les chiens se sont énervés et il s'est évanoui.
La frayeur combinée au coup qu'il avait pris juste avant l'ont fait tomber dans les vapes, apparemment.
Bref, les gars l'ont ramené à la ferme et je l'ai ramené au bunker.
J'ai expliqué à tout le monde que je l'avais trouvé dehors et que j'avais pu faire fuir les lupins à temps.
Ils m'ont raconté vos conneries et j'ai dit que je partais à votre recherche.
Au passage, j'ai prévenu la police au cas où.

Tom essayait d'assembler les morceaux, mais il lui manquait quelque chose pour les faire coller ensemble.
Après quelques instants de réflexion, il demanda :

— Yvan, je ne comprends rien.
Qui sont ces gens ?
Pourquoi dis-tu que les lupins n'existent pas ?
Et cette espèce de cabine dans laquelle on se déplace, c'est bien une voiture ?

— Ah, c'est vrai.
J'imagine que je te dois bien ça.
Commençons par ce qui est simple : oui, c'est une voiture.
Maintenant, pour le reste : la catastrophe n'a jamais eu lieu.
Il n'y a pas eu d'arme, pas de mort massive, pas de résidus, pas de mutation…

— Mais… ça fait douze ans qu'on vit dans ce bunker !

— Et tu crois sincèrement qu'on aurait tenu aussi longtemps si tout avait été aussi pourri que je vous le disais ?
J'ai dû faire venir des gens pour la fosse septique deux ou trois fois ; le remplacement des panneaux solaires a commencé il y a deux ans ; les filtres à air ont été changés six fois, même s'ils ne servent à rien.
Je suis plutôt fier de mon petit bunker, mais on n'aurait jamais pu y tenir plus d'une année.
Et je ne te parle pas des vivres !

— Non, ce n'est pas possible.
C'est une autre de tes histoires !

— J'aimerais bien, Tom.
J'aimerais bien.

— Mais… pourquoi ?

— J'peux pas te dire, Tom.
On s'est mis à l'abri et j'ai écouté la radio pour savoir ce qui se passait.
J'ai fini par réaliser que les conflits s'éloignaient de nous, mais j'ai pas pu le dire aux autres.
Je sais pas si je voulais tester mon bunker en conditions réelles, si c'est par orgueil et que je ne voulais pas admettre que je m'étais planté, ou si j'ai juste goûté au pouvoir que j'avais sur vous.

« Alors j'ai inventé ma première histoire.
L'arme, les résidus, c'est né à ce moment-là.
J'ai même saboté la radio pour que personne ne puisse entendre une autre version.

La voiture s'arrêta dans la cour d'une ferme.
Yvan appuya sur une télécommande et la porte d'une grange s'ouvrit devant eux.
Il ne reprit pas la parole.
Tom essayait d'assembler les morceaux tant bien que mal.

— Mais pourquoi t'as commencé à sortir, dans ce cas ?

— Je fais pas que raconter des histoires.
Enfin si, mais y a toujours une part de vérité.
Le bunker ne pouvait pas être autonome plus longtemps en termes de nourriture.
Ça faisait neuf mois qu'on vivait dedans et notre garde-manger se vidait dangereusement.
Encore quelques mois et ce serait la famine, alors j'ai commencé à sortir.
Ç'a été marrant de voir que ma ferme avait continué à tourner.
Mes gars s'étaient démerdés comme ils pouvaient.

— Attends, t'as débarqué comme ça, après des mois, comme si de rien n'était ?

— Légalement, j'avais disparu depuis moins d'un an.
Tout m'appartenait encore.
Les premiers mois, il a fallu que je sorte souvent le jour pour régler les détails administratifs.
Les flics voulaient savoir où j'étais passé pendant tout ce temps.
Et vous aussi, d'ailleurs.
Dix disparus, comme ça, ça laisse des traces.
Sans parler de Marie.
Dans les faits, ta mère a enlevé une enfant.
Ça partait d'une bonne intention, mais ça a emmerdé pas mal de gens.

— Et comment tu t'en es sorti ?

— Avec une autre histoire basée sur la vérité : on s'est tous barrés, on a payé un passeur pour aller en zone sûre.
Ta mère a vu une gamine qu'elle a voulu sauver et s'est barrée avec.
Les autres au village savaient déjà tous qu'on flippait à cause de la guerre, l'histoire est passée crème.
Et puis j'avais choisi de revenir chez moi maintenant que les choses s'étaient calmées dans la région.

La voiture était entrée dans la grange, dont la porte s'était fermée derrière eux, et le ravitailleur avait arrêté le moteur.
Il ne se tourna pas vers son passager, fixant obstinément le volant devant lui.

— Ça a marché.
Les flics ont supposé que vous étiez tous partis au loin.
Ils ont vu sur vos comptes que vous aviez sorti de grosses sommes d'argent, comme si vous aviez senti le coup venir et que vous vous étiez préparés.
Quand ils ont regardé le mien, je me suis dit que j'avais eu le nez creux de passer par un compte-écran.
Pour eux, j'avais fait exactement comme vous.

« Personne a trop fouiné, et personne a jamais trouvé l'entrée du bunker, notamment parce que personne la cherchait.
Elle est juste là, derrière cette étagère, dit-il en indiquant le meuble face à la voiture d'un signe de tête.
Ceux qui viennent voient une réserve et un garage.
Il y a une porte qui mène à la maison.
Quand je vais me coucher, je passe par chez moi et je disparais ensuite ici.
Les employés n'y voient que du feu.

Tom ne disait rien.
Il était abasourdi par le choc de ces révélations.
Cela n'empêchait pas Yvan de continuer, comme si parler le permettait de se soulager d'un poids accumulé pendant des années.

— Ça a quand même commencé à faire bizarre de plus trop me voir le jour, mais les gens m'ont foutu la paix.
Je gère tout l'administratif de la ferme la nuit, donc ils savent que je suis toujours là.
De temps en temps, je quitte le bunker un peu plus tôt pour aller à un dîner avec une ou deux connaissances.
Avant, il m'arrivait de faire la fermeture des bistrots.
C'était dur, ces matins-là, quand je rentrais au bunker avec la gueule de bois.
T'étais encore minot, t'arrêtais pas de piailler et de me tourner autour quand tu me voyais descendre l'escalier.

Un sourire triste se dessina sur le visage de l'homme, qui sembla soudain vieillir de plusieurs années.
Tom n'arrivait pas à croire ce qu'il entendait.
Rien de tout cela n'était possible.
Ils n'avaient pas le droit d'entrer dans le sas pour les protéger des résidus.
L'air était filtré pour les protéger.
Et les vivres qu'il rapportait, les conserves rouillées et périmées depuis des années, les cartons dévorés par l'humidité… Il n'avait pas pu mentir sur ça.

— Mais la nourriture que tu trouves, les conserves, les céréales… Elles ont toujours passé des années à l'extérieur.

— Du pipeau, répondit Yvan en tournant finalement vers Tom son regard et son sourire tristes.
On avait prévu de grandes réserves de nourriture au départ.
Bien plus que ce qu'on pouvait entreposer dans le bunker.
En fait, elle est stockée à côté juste là, sur ces étagères.
Mais j'étais le seul à le savoir et tout le monde était trop pressé de se mettre à l'abri pour y faire attention quand on est rentrés.

« Du coup, je vais piocher dedans, un peu au hasard.
Au rythme où je les « trouve », je peux encore les faire durer quelques années.
Régulièrement, je mets une ou deux conserves dans un bain d'eau, pendant quelques jours ou semaines, pour qu'elles donnent l'impression d'avoir du vécu.
Avec quelques coups de lame et de l'eau salée, on a des marques de rouille encore plus rapidement.
Je déchire un carton et le laisse tremper à moitié aussi.
Et voilà !
De la nourriture « d'avant ».
‘Pis y a les jours où j'ai la flemme et vous avez de la chance : j'ai trouvé un stock préservé.
Youpi !
Ah, et les céréales à moitié bouffées par les souris ?
C'était authentique, celui-là…

— Mais… rien… rien n'est vrai ?

— Si.
Qu'on est une famille.
En quelque sorte.
La vieille Rosa le sait.
Elle ne l'a jamais dit, mais elle n'a jamais été dupe.
Elle sait aussi qu'elle n'a rien de mieux à l'extérieur et qu'elle a tout intérêt à rester avec vous.

Le poids de cet échange priva Tom de toute capacité de réflexion.
Yvan débloqua les deux ceintures de sécurité, descendit de voiture et ouvrit la portière de Tom.
Il aida le jeune homme à descendre.
Ce dernier se laissa faire, attrapant l'enveloppe qui avait été posée sur ses genoux.
Il sentit un objet bouger à l'intérieur et reconnut le poids du couteau d'Yvan.

C'est alors qu'un doute le saisit, une question qu'il ne put retenir :

— Pourquoi est-ce que tu me dis tout ça ?

Yvan sembla à nouveau prendre plusieurs années.

— Parce que tu en as trop vu.
Je ne peux pas te laisser retourner au bunker.
Je ne peux pas non plus te laisser partir.

Il lui fallut un effort avant de pouvoir terminer sa réponse.
Il inspira profondément, se redressa et planta son regard dans celui de Tom.

— Tu dois disparaître.

Tom resta choqué un instant.
Yvan, son idole, celui à qui tous les résidents du bunker devaient la vie, venait calmement de lui annoncer qu'il allait prendre la sienne.
Il demanda alors, d'un ton presque résigné :

— Et quelle sera l'histoire de ma fin ?
Les lupins nous ont mangés, Jack et moi ?

— Je ne sais pas encore.
Peut-être qu'une leçon sur la folie humaine ou les effets des résidus ferait du bien aussi.
Jack a pété un câble et je n'ai retrouvé que ton corps haché à coups de machette.
Je pourrais même te ramener pour montrer… Ah non, ne t'inquiète pas !
Ce sera indolore pour toi.
J't'aime comme un fils, Tom, tu l'sais bien.

— Je vois.
Est-ce qu'au moins tu as le médicament pour Marie ?

Yvan ne put réprimer un petit rire.

— Mais t'as toujours pas pigé ?
T'as fait exactement ce que ta mère espérait quand elle a recueilli cette gamine.
J'ai pigé dès qu'elle s'est dessapée pour que je l'examine, avec ses p'tits seins plus gonflés que d'habitude.

Tom sentit la colère monter en lui en entendant Yvan parler ainsi de Marie, mais Yvan ne s'en rendit pas compte et continua :

— Nausées matinales.
J'ai pris le temps de tester son flacon avant de revenir vous chercher et ça confirme : vous avez commencé à repeupler, bandes de crétins !
Elle est pas malade, elle est enceinte !
Ça me tue, d'ailleurs !
On vous a jamais expliqué ça.
Vous avez pigé tous seuls ?
Et vous avez fait ça où ?
Dans la serre ?

Tom était à nouveau sous le choc d'une révélation qu'il n'attendait absolument pas.
Lui, père d'un enfant ?
Mais Yvan a parlé d'un traitement…

— Mais alors, le médicament…

— On peut pas avoir un nouveau-né dans le bunker.
Ce serait ingérable.
Et vous auriez quoi à lui transmettre ?
C'est vrai, je vous ai fait peur pour que vous ne sortiez pas, mais la peur a toujours régi vos vies, bien avant ça !
C'est la peur qui vous a fait financer et entrer à la base.
C'est parce que t'as la trouille que ta copine soit malade que t'es sorti me chercher.
C'est parce que vous aviez les pétoches de ce que vous ne compreniez pas que vous êtes allés agresser deux pauvres malheureux.
Pour le bien que ça vous en a fait !
Ton père a failli littéralement crever de terreur, Jack est crevé pour de bon et t'étais derrière les barreaux moins de douze heures après avoir été lâché dans la nature.
Qu'est-ce que vous voulez transmettre à un p'tit ?
Vous avez toujours été inaptes à la vie dans notre société et vous serez en plus perdus dans celle-ci !
C'est pas parce qu'y a pas eu d'apocalypse que le monde a pas changé.
Il a changé, et dans les grandes largeurs !
Fini, le système capitaliste et la croissance à gogo.
Fini, le pognon qui dirige le système.
On est revenu à un modèle plus proche de la planète et de ce qu'elle peut nous offrir…

Tout en parlant, Yvan avançait vers l'étagère qui dissimulait l'entrée du bunker.
Tom avait perdu le fil de son discours, un mélange de finalité et de rage se mêlant en lui.
L'aîné tendit le bras et tira sur un montant du meuble, qui pivota sans bruit et sans trace pour révéler la porte blindée jumelle à celle de l'entrée du sas, avant de reprendre.

— Tom !
Je ne peux pas faire ce qu'il faut faire ici, ça créerait trop d'emmerdes.
Mais les autres verront que le bunker a été ouvert, donc on va rentrer dans le sas sans traîner et faire ça proprement et rapidement, et puis je descendrai expliquer ce qu'il s'est passé, OK ?

***

La lumière rouge s'était éteinte depuis plusieurs minutes.
John attendait anxieusement que sa sœur jaune s'allume à son tour.
Il n'en pouvait plus de se demander si les lupins avaient rattrapé son fils ou si Yvan l'avait retrouvé à temps.
Finalement, la lueur apparut, en même temps que le son de la porte du sas retentit en haut de l'escalier.
Des pas lents descendirent les marches.
Deux pieds seulement.
Yvan n'avait pas pu sauver Tom.
John baissa la tête et se dirigea vers son dortoir.

— Tom !
s'exclama Marie.

John eut un moment de pause et se retourna vers la figure qui approchait à présent du hall commun.
C'était son fils, défait, des marques de sang sur ses vêtements, mais vivant.
Il se précipita vers lui pour le serrer dans ses bras.

— Tom, mon fils !
Je suis désolé.

— Non, papa, c'est moi qui suis désolé, répondit-il en lui rendant son étreinte.

— Tu ne sortais pas du sas, je commençais à croire qu'il n'y avait personne.

— J'ai eu besoin d'un moment à moi avant de rentrer, et d'un autre pour comprendre comment ouvrir la porte intérieure.

Les autres résidents arrivèrent, attirés par les voix qu'ils avaient entendues.
Ce fut Rosa qui eut le courage de poser les questions qui fâchent :

— Jack ?
Yvan ?

Tom relâcha son père et la regarda.
Il avala sa salive avant de répondre.

— Jack ne rentrera pas.

Encore une fois, il entendit l'écho de Jack faisant cette déclaration, se mêlant à celui d'Yvan affirmant la même chose.
Mais il avait déjà décidé de la vérité sur laquelle il voulait baser son histoire.

— En voyant l'extérieur, il a décidé qu'il parcourrait le monde à la recherche de survivants, pour essayer de créer une nouvelle communauté et reconstruire quelque chose.

— Et Yvan ?
demanda un autre résident.

Tom secoua la tête tristement.
Il savait ce qu'il voulait dire, mais les mots restèrent coincés dans sa gorge.
Les derniers qu'Yvan avait prononcés, tout en crachant du sang, lui revinrent en mémoire :
« Merci, gamin.
J'préfère qu'ça s'termine comme ça…
Laisse pas pourrir mon corps ici…
Ça attirerait de la vermine…
L'incinérateur…
C'est le gros four au fond…
Ça fera l'affaire…
Comme quand ta mère… est morte…
Pense… toujours à… tirer l'étagère avant… »

Puis l'homme s'était tu, définitivement.
Tom avait placé le corps dans l'incinérateur.
Il lancerait la combustion plus tard.
Le sas était étonnamment bien fourni et Tom avait pu éponger le sang sur le sol et le vider dans un évier, avant de s'asseoir sur un banc, la tête dans les mains.
Il devrait bientôt affronter ceux qui étaient sa famille depuis plus d'une décennie et il devait avoir les idées claires.
Il lui fallait des réponses aux questions qu'ils poseraient sans aucun doute.
Des histoires.
Avec un semblant de vérité.

— Il était trop tard quand nous l'avons retrouvé.
Il était incapable de revenir.
Il semblait dévoré de l'intérieur par des parasites mutants.
Il nous a dit comment retrouver l'entrée du bunker, nous a donné quelques conseils et nous a demandé de…

Tom fut saisi d'un sanglot en songeant au couteau de Jack s'enfonçant dans la chair de son mentor.
Il fit un effort pour se reprendre et se redresser.

— Yvan n'est plus.
J'ai ramené tout ce que je pouvais de son matériel.
Je serai le nouveau ravitailleur.
