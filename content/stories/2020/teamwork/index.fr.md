---
date: 2020-03-01T18:45:31+01:00
title: Travail d'équipe
slug: travail-d-equipe
author: chop
license: CC BY-NC-ND 4.0

inspirations: [ weekly ]
keywords: [ micronouvelle, pas ce que vous croyez, biologie, ADN ]

challenge:
  period: 2020-02-28
  rules:
  - type: theme
    constraint: collaboration
---

C'est impressionnant, ce qu'on a réussi à faire !
N'importe laquelle de nos tâches est d'une complexité affreuse, tellement que, pour la plupart, nous avons chacun une spécialité et nous ne faisons pratiquement que ça.
Pourtant, prise individuellement, aucune de nos activités n'a de sens.
Alors quand on voit... ça !
Ce projet, c'est vraiment la preuve qu'une équipe est bien plus que la somme des individus qui la composent.

<!--more-->

J'ignore qui a écrit notre plan, mais ce type --- ou cette nana --- est un génie !
Il y a tellement de phases et il faut être tellement nombreux pour pouvoir le réaliser...
J'ai un peu parcouru le début du script.
Le projet commence avec un gars.
Un seul pauvre mec qui devait se démerder pour démarrer.
Et son premier taf, ç'a été de créer une équipe.
Cette équipe, pendant un bon moment, elle a juste dû se démerder pour grossir, pour multiplier le nombre de membres.

L'auteur avait prévu qu'il leur faudrait à peu près trois semaines pour être assez nombreux et commencer à se spécialiser.
Ce n'est qu'à partir de là que les gars ont commencé à avoir chacun leur tâche.
Pour la plupart d'entre nous, c'est de produire un truc en particulier.
On est un peu tous comme sur des chaînes à l'usine.
Et en parallèle, on doit continuer à se débrouiller pour renouveler les troupes.
Bordel, ça le ferait pas si on n'avait plus assez de force de frappe pour répondre aux demandes du projet !

Enfin, sauf du côté de la Direction.
Eux, ils ont été élus dans les premières années et puis les inscriptions ont été fermées.
Pas de leur fait, me direz-vous !
Ça aussi, ça fait partie du plan.
Depuis, il y en a quelques uns qui disparaissent tout le temps, mais surtout, ils passent leur temps à se réorganiser et à faire du réseautage entre eux.
Bien entendu, de temps à autre, ils nous envoient des demandes par pneumatiques, et on fait ce qu'on peut pour y répondre.

Mais vraiment, je reste ébahi de l'organisation conçue par le plan.
On est quand même {{< numfmt 3000 >}} milliards à bosser sur le projet.
Et c'est sans compter les {{< numfmt 3800 >}} autres milliards à traîner dans le coin et donner un coup de main !
Vous vous doutez bien que, quand il y a autant de monde, c'est difficile de gérer les entrées et sorties.
Il faut réussir à faire le tri entre ceux qui aident et ceux qui se tapent l'incruste.
Heureusement, faire la police, c'est aussi la tâche de certains d'entre nous.
Il y en a qui sont adeptes des paralysants, d'autres qui sont un peu plus gloutons et mangent les intrus tout crus --- littéralement.

Il y a aussi des trucs un peu plus chelous qui se passent parfois.
Certains gars ont eu une version altérée du plan, comme si quelqu'un espérait détourner notre bonne volonté pour fabriquer autre chose.
Je crois que le dernier portait une mention « COVID-19 ».
Vous vous dites peut-être que c'est bizarre, comme nom, mais en même temps, on travaille tous sur des trucs avec des noms tordus.
Chaque section du plan est appelée par une série de lettres en majuscules avec parfois un ou deux chiffres.

Une fois, on a aussi un groupe qui a débloqué.
On sait pas trop pourquoi, ils ont décidé de grossir plus que le plan ne le prévoyait.
On n'avait pas la place pour loger tout le monde, mais on savait pas trop comment gérer ça.
Il a fallu demander de l'aide à l'extérieur de l'équipe.

Mais bordel, ça vaut le coup !
Regardez-moi ce résultat : à partir de quelques milliers de milliards d'individus comme nous, sans individualité réelle, des ouvriers spécialisés qui passent leur vie entière à se multiplier sur leur lieu de travail, des chaînes de montage, on a créé une gigantesque usine pensante.
Ça a de la gueule, non ?
