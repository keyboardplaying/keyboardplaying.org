---
date: 2020-03-01T18:45:31+01:00
title: Teamwork
slug: teamwork
author: chop
license: CC BY-NC-ND 4.0

inspirations: [ weekly ]
keywords: [ flash fiction, not what you think, biology, DNA ]

challenge:
  period: 2020-02-28
  rules:
  - type: theme
    constraint: collaboration
---

Gosh! What a great thing we achieved!
Any of our tasks seems awfully complex, so much that most of us do only one thing.
And yet, each of this task is meaningless taken individually.
Then you get to see... this!
This project, it's really the proof that a team is so much more than the sum of its members.

<!--more-->

I don't know who wrote that plan, but that guy---or gal---is a genius!
There are so many phases and we have to be so many to even think of tackling it...
I got around reading the beginning of the script.
The project began with one guy.
One single fellow who had to work out how to start the whole thing off.
And his first gig was to build up a team.
That team, for a whole lot of time, they just had to figure out how to get bigger, to multiply their number.

The author had planned they needed about three weeks to be ready and begin to specialize.
Only then did the guys got each their own task.
For most of us, it's to produce something.
Not just anything, but the plan tells us how to build it.
We're just like in a factory on assembly lines.
And in the same time, we must manage to renew people.
Gorram, it just wouldn't do to not have enough people to answer the project's requirements!

Well, except for the Direction.
They were elected in the first years, and then you could no longer register.
Not because they decided it, mind you!
That, too, was part of the plan.
Since then, some of them disappear all the time, but, mainly, they just continuously reorganize and network.
Of course, from time to time, they send us a request via a pneumatic letter and we do our best to satisfy.

But really, I'm still flabbergasted from the organization the plan designed.
Look, we're 3 trillion who work on the project.
And that doesn't include the 3.8 trillion others hangin' around and givin' a hand!
Sure, you can guess that, with that many people, it's hard to know comes in and who comes out.
You need to make a difference between those who are helping and those who just enjoy the place.
Fortunately, that's also a task for some of us.
Some of our policemen love paralyzers while others prefer to eat the intruders---literally.

Some funny stuff also happens sometimes.
There are times when guys receive a modified version of the plan, as if someone would like to use our labor force to build something else.
I think the last one I heard of was called "COVID-19."
You may think that's a weird name, but we all work on weirdly named things.
Each section of the plan is called with a series of uppercase letters and sometimes one or two digits.

Once, there's also that group who lost it.
We don't really know why, they decided they had to get bigger than the plan told them.
We didn't have enough room for all of 'em, but we didn't know how to handle it.
We had to ask for help outside the team.

But Hell! It's worth it!
Look at that: from nothing else than a few trillions of basic individuals like us, without any real individuality, specialized workers who spent their whole lives multiplying in their workplace, on assembly lines, we built a gigantic thinking factory.
It looks rather great, doesn't it?
