---
date: 2020-04-12T15:01:26+02:00
title: The church in my village
slug: the-church-in-my-village
author: chop
license: CC BY-NC-ND 4.0

description: |-
    Our village is just a small town in the country side, but I love its church!
    To be precise, I love its bell tower!

inspirations: [ weekly ]
keywords: [ flash fiction, medieval, brittany, invasion, bible, church, bell ]

challenge:
  period: 2020-04-10
  rules:
  - type: theme
    constraint: bells

cover:
  src: boudewijn-bo-boer-eX1s8F9AjeM-unsplash.jpg
  alt: A church in the middle of a small village in France
  by: Boudewijn "Bo" Boer
  authorLink: https://unsplash.com/@boudewijnboer
  link: https://unsplash.com/photos/eX1s8F9AjeM
  license: Unsplash
---

God, that church is beautiful!
Yes, you may think it's a bad start: the lad begins with a blasphemous swear to admire the Lord's house.
Our priest thinks the same, but let's be honest: I've never been _that_ religious.

Oh, my parents are, of course.
I went to catechism every Saturday.
We met the parish priest in the vestiary and he would spend the afternoon telling us about the Bible and how the Lord sacrificed himself to save us from our sins.

That was the bad start for me.
To teach us of Love, He sent His son in a way that would have allowed the rabble to stone Mary to death.
That son, God himself actually, alternated between the prophet who guides Men and teaches benevolence, and a bad-tempered lad who wrecks havoc in a market just because he doesn't like it.
Teaching and benevolence went away, just like that?
And in the end, it's His son---or Himself, with the Holy Spirit and Trinity thing---that He let perish on his cross.
But why?
Where's the saving in that?
We still sin just as much today!
Most people I know only repent while confession lasts, so that they can say they've been forgiven, or even only at the extreme unction, when they know it's their last chance for absolution.

Hell, Christ isn't God's first intervention to "save" His creation.
Read what happened before the lad came, things were sometimes a bit brutal.
Our God of Love, in the Old Testament, was rather vehement and quick to punish.
Think of how he expelled Adam and Eve from the Garden of Eden because they had eaten a piece of fruit.
He told her that all women would suffer when giving birth to pay for her sin.
One wrong step, from one person, that half of mankind will have to pay back for until the end of time.
Ain't that a form of divine speculation?

It ain't the worst part, either.
Remember Noah, too: God was unsatisfied with what His creation was becoming, so He decided to wipe it clean.
Too bad for all the pious ones in the world, only Noah's family would escape the drowning.
Why didn't He send Christ back then?
Why did He later almost forced Abraham to sacrifice his son to test his faith?
If He loves us so much, why does He leave us alone to face what the Church blames on the Evil One?
Why did He do nothing to save the hand of my father, still a pious man today, when he sacrificed it to protect a child?

So, much to my parents' and the priest's displeasure, I have a hard time believing God is all they say He is and still lets us feel this unjust suffering.
Even the tales changing from a loving Lord to an impetuous one seem incoherent to me.
But what do _I_ know?
I'm not a great lord with a fine education, just a commoner from the depths of the countryside.
A serf in the service of my suzerain.

If I love our church, it's not because of what it represents in the eyes of believers, but more because it is proof that even poor peasants like us can achieve beautiful buildings when given the means.
Above all, I love the bells.
I love the sound of metal resonating.

My father is the village's blacksmith.
I've watched him practice his art since I was a child.
The sound of the hammer, the metal pieces ringing, the anvil absorbing the force of his blows...
That's what I hear when the bells are ringing.
And I'm lucky enough to hear them better than anyone!
Watching my father wasn't enough to get his talent.
Even today, with one missing hand, he's faster and more talented than me at the forge.
On the other hand, working the metal as soon as I could lift a hammer gave me more strength than most.
Already then, I would regularly ask to climb into the bell tower to see the treasures hidden up there.
Our bells are not small, and my powerful musculature combined with this passion led the priest to offer me the job of bell-ringer.
Later, after the accident and my disenchantment with religion, he was not that much into looking for a replacement.

For our small parish, our bells don't demerit!
Finding someone able to animate them and give them the right rhythm is not an easy task.
Our belfry houses five beauties.
I can spend hours just admiring them.
This one, which I ring every hour, is the Great Bell.
We found her in the ruins of the cathedral in the neighboring town, after the invaders were repelled.
My father forged a clapper to replace the missing one.
It's an unusual task for a blacksmith, but his work gives our bell a unique sound.

That one is the Great Malo, in honor of Sant Maloù and all he did for our country.
She's the one to signal the time of gathering to the faithful.
On the other beam, there's Little Mary, which is heard only for marriages and some other joyful occasions.
The fishermen say she sounds so clear, they can hear her far out to sea.
Her sister's the Big Mary, which rings only for mourning, most often at funerals.
Both Marys play the same note, but the Big one is an octave lower.

And finally, there's the alarm bell.
I've never heard much of her.
As you may've guessed, it rings when...

"Ringer! Bell-ringer! Are you up there?"

I'm being called.
I need to go down quickly.

"Aymeric! I'm here.
What's going on?"

"Sails have been sighted at sea.
We fear they are invaders.
The burgomaster asks that the tocsin be rung!"

Very well, here's my chance to hear the sound of our alarm bell, with redoubled blows!
At work, the tocsin won't ring by itself.
