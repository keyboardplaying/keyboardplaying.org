---
date: 2020-04-12T15:01:26+02:00
title: L'église de mon village
slug: l-eglise-de-mon-village
author: chop
license: CC BY-NC-ND 4.0

description: |-
    Notre village n'est qu'une petite bourgade en campagne, mais que j'aime son église !
    Pour être précis, que j'aime son clocher !

inspirations: [ weekly ]
keywords: [ micronouvelle, médiéval, bretagne, invasion, bible, église, cloche ]

challenge:
  period: 2020-04-10
  rules:
  - type: theme
    constraint: les cloches

cover:
  src: boudewijn-bo-boer-eX1s8F9AjeM-unsplash.jpg
  alt: Une église se dressant au milieu d'un petit village en France
  by: Boudewijn "Bo" Boer
  authorLink: https://unsplash.com/@boudewijnboer
  link: https://unsplash.com/photos/eX1s8F9AjeM
  license: Unsplash
---

Bon Dieu, que cette église est belle !
Et oui, vous vous dites certainement que c'est mal entamé : le gaillard commence par un juron blasphématoire pour admirer la maison du Seigneur.
Notre curé est bien du même avis que vous, mais il faut bien le reconnaître : je n'ai jamais été particulièrement religieux.

Oh, mes parents le sont bien entendu.
J'allais au catéchisme chaque samedi.
On retrouvait le curé dans le revestiaire et il passait l'après-midi à nous raconter la Bible et comment le Seigneur s'est sacrifié pour nous sauver de nos péchés.

C'est là que c'était mal entamé pour moi.
Pour nous enseigner l'Amour, Il a envoyé Son fils d'une façon qui aurait autorisé la populace de l'époque à crever Marie par lapidation.
Ce fils, qui était Dieu lui-même, a alterné entre le prophète qui guide les Hommes et leur enseigne la bienveillance et l'amour mutuel, et le colérique qui saccage un marché juste parce que ça ne lui plaît pas.
Ils sont passés où, l'enseignement et la bienveillance ?
Et finalement, c'est Son fils --- ou Lui-même, si on écoute ce que dit le cureton --- qu'Il a laissé périr sur sa croix.
Mais pourquoi ?
En quoi ça nous sauve ?
On pèche toujours autant aujourd'hui !
Et la plupart de tous ceux que je connais ne se repentent qu'en confession, histoire de pouvoir dire qu'ils ont été pardonnés, ou même uniquement à l'extrême onction, quand ils savent que c'est leur dernière chance de se faire absoudre.

Et pis le Christ, c'est pas la première intervention que Dieu a menée pour « sauver » Sa création.
Si on relit tout ce qu'il s'est passé avant ça, c'était parfois un peu brusque.
Notre Dieu d'Amour, dans l'Ancien Testament, était plutôt véhément et prompt à punir.
Pensez à la façon dont il a expulsé Adam et Ève du jardin d'Éden parce qu'ils avaient croqué dans un fruit, en disant à la femme que toute son engeance souffrira pour donner vie pour payer son péché à elle.
Un faux pas, commis par une personne, que la moitié de l'humanité devra éternellement rembourser.
C'est pas une forme de spéculation divine, ça ?

Et c'est pas le pire.
Rappelez-vous Noé, aussi : Dieu n'était pas satisfait de ce que devenait Sa création, donc Il a décidé d'en faire table rase.
Tant pis pour tous les pieux de ce monde, seule la famille de Noé a été sauvée.
Pourquoi n'a-t-Il pas envoyé le Christ à cette époque ?
Pourquoi a-t-Il pratiquement forcé Abraham à sacrifier son fils pour tester sa foi ?
S'Il nous aime tant, pourquoi nous laisse-t-Il seul face aux forces que l'Église attribue au Malin ?
Pourquoi n'a-t-Il rien fait pour sauver la main de mon père, qui reste encore aujourd'hui un homme pieu, lorsque celui-ci a sacrifié sa propre chair pour protéger un enfant ?

Non, au grand dam de mes parents et du curé, j'ai du mal à croire que Dieu soit tout ce qu'ils nous disent de Lui et qu'Il nous laisse pourtant subir toute cette douleur injuste.
Même les récits qui alternent entre un Seigneur aimant et un impétueux me semblent incohérents.
Mais qu'est-ce que j'en sais, au final ?
Je ne suis pas un grand seigneur avec une fine éducation, juste un rustaud du fin fond de la province.
Un roturier.
Un serf au service de mon suzerain.

Si j'aime notre église, ce n'est pas pour ce qu'elle représente aux yeux des croyants, mais bien davantage parce que c'est une preuve que même des gueux comme nous sommes capables de bâtir de beaux édifices, quand on nous en donne les moyens.
Surtout, j'aime les cloches.
J'aime le son du métal qui résonne.

Mon père est le forgeron du village.
Depuis tout petit, je vais l'observer pratiquer son art.
Le bruit du marteau, les pièces de métal qui sonnent, l'enclume qui absorbe la force de ses coups...
Voilà ce que j'entends lorsque sonnent les cloches.
Et j'ai la chance de les entendre mieux que quiconque !
Observer mon père ne m'a pas donné son talent.
Même aujourd'hui, privé d'une main, il forge mieux et plus vite que moi.
En revanche, en travaillant le métal dès que mes bras ont pu soulever le marteau, j'ai développé une force que peu d'autres ont.
Déjà à cet âge, je demandais régulièrement à monter dans le clocher pour y voir les trésors qu'il abritait.
Nos cloches ne sont pas petites, et ma puissante musculature associée à cette passion ont conduit le prêtre à me proposer le poste de sonneur.
Ce n'est que plus tard, après l'accident, que mon désamour de la foi a été manifeste à ses yeux, mais il ne tenait guère à me chercher un remplaçant.

Il faut dire que, pour notre petite paroisse, nos cloches ne déméritent pas !
Trouver quelqu'un qui puisse les animer et leur donner le bon rythme n'est pas chose aisée.
Notre beffroi abrite cinq beautés.
Je peux passer des heures à les admirer.
Celle-ci, que je sonne toutes les heures, c'est le Grand Bourdon.
Nous l'avons récupéré parmi les ruines de la cathédrale de la ville voisine, après avoir repoussé l'envahisseur.
Mon père lui a façonné un battant pour remplacer celui qui n'a pas été retrouvé.
Tâche inhabituelle pour un forgeron, mais son œuvre confère un son unique à notre bourdon.

Celle-ci est surnommée le Gros-Malo, en l'honneur de Sant Maloù et de ce qu'il a pu faire pour notre contrée.
C'est elle qui signale aux fidèles qu'il est l'heure de se retrouver pour prier notre Seigneur. 
Sur l'autre charpente, on a la Petite Marie, qui ne se fait entendre que pour les mariages et quelques autres fêtes joyeuses.
Les pêcheurs disent qu'elle sonne tellement clair qu'on l'entend loin au large.
Quant à sa sœur, la Grosse Marie, elle ne résonne que pour marquer le deuil, le plus souvent lors de funérailles.
Les deux jouent la même note, mais la Grosse Marie la joue une octave plus basse.

Et enfin, il y a le Braillard.
Je n'ai pas vraiment eu l'occasion de l'entendre jusqu'alors.
Il est destiné à...

--- Sonneur ! Sonneur ! Es-tu là-haut ?

On m'appelle.
Il me faut descendre rapidement.

--- Aymeric ! Je suis ici.
Que se passe-t-il ?

--- Des voiles ont été aperçues au large.
On craint une attaque des envahisseurs.
Le bourgmestre demande à ce que sonne le tocsin !

Très bien, voici donc pour moi l'occasion d'entendre le son de notre Braillard.
Et à coups redoublés !
Au travail, le tocsin ne va pas se sonner seul.
