---
title: Rakontoj
description: |-
  Kelkaj noveloj, noveletoj kaj verkada ekzercoj
url: /rakontoj
aliases: [ /stories ]
classes: [ large-page ]

cascade:
  scripts: [ main ]
  classes: [ use-lettrine, use-asterisms ]
  toc: false
---
