---
title: The Caravan
description: |-
  The Caravan is a convoy of spaceships exploring the universe in search for answers.
  Many races join or leave it on the way.
  Many occasions for stories…
weight: 10
slug: caravan

termCover:
  src: convoi-sillage.cover.jpg
  alt: A fleet of spaceshipts, from the Wake comics
  by: Philippe Buchet
  link: http://poukram.org/index_fe.php?type=4
  socialSrc: convoi-sillage.jpg
  license: copyright Delcourt Production
---
