---
title: La Caravane
description: |-
  La Caravane est un convoi de vaisseaux explorant l'univers à la recherche de réponses.
  De nombreuses races la rejoignent ou la quittent.
  De nombreuses opportunités d'histoires…
weight: 10
slug: caravane

termCover:
  src: convoi-sillage.cover.jpg
  alt: Un convoi de vaisseaux, extrait de la bande dessinée Sillage
  by: Philippe Buchet
  link: http://poukram.org/index_fe.php?type=4
  socialSrc: convoi-sillage.jpg
  license: copyright Editions Delcourt
---
