# About MonkeyUser comics

This directory contains [MonkeyUser comics](https://monkeyuser.com/).
They are in this directory only so that your browsers won't have to open connections to a third-party site.
